## Mutações:
- HiddenBlocks
    - Change weight
    - Add node (IOH)
    - Add connection (IOH)
    - Mix node?
- Block mutations
    - Add empty Block
    - Duplicate Block
    - Remove block


# Possíveis melhorias futuras:
- Utilizar um smart pointer na ConnectionView:
    - A melhor ideia até o momento seria utilizar empacotar cada ConnectionMatrix em uma RefCell, e deixar as regras do Borrowchecker em runtime.
        - R.: Utilizei o Cell em cada elemento da matriz do ConnectionMatrix de modo que somente é necessário utilizar referência & para alterar o valor (Interior mutability).
        - Para alterações que envolvam mudanças estruturais na rede (adição ou remoção de nodos, por exemplo) deve-se eliminar todas as referências aos elementos (transformar as connections views para connection_address)
    - ConnectionMatrix com crescimento sem aumento dos Nones:
        Da forma que está, adicionar novos nodos aumenta o tamanho da matrix de conexões e consequentemente o tamanho do vetor que armazena seus valores. No cruzamento genético de duas matrizes, seria bastante ruim (seria?) eliminar o nodo (eliminando uma linha ou coluna) deslocando todo o resto da matriz. A solução seria um esquema de armazenamento em matriz esparsa onde somente os elementos com algum valor seriam armazenados. Desta forma pode-se excluir nodos sem deslocar o resto da matriz.Por agora acho que vou manter o tamanho das matrizes fixas para evitar explosões no seu tamanho (que talvez seja a melhor ideia do que buscar um equilíbrio com parâmetros de evolução). 

## Considerações sobre o uso das "connection matrices":
Vantagens:
- Número pré definido de conexões máximas;
- Não precisa gerenciar o histórico de mutações para reconstruir;
- Fácil de gerar filhos de dois blocos "pais";
- Fácil de iniciar em um estado bastante interconectado;

Desvantagens:
- Perde-se o caminho evolutivo;
- Tamanho "fixo" do bloco se não implementado um mecanismo de adição/remoção de nodos;
- Implementação utilizando índices;

## Sobre o uso da arquitetura de "handlers" ou "views" para manipulação dos nodos e conexões de uma "connection matrix":
- O problema com referências normais (&mut) é que os HiddenBlocks são owner das ConnectionMatrix, e quando se tenta passar uma referência mutável de um valor na connection matrix, por exemplo o valor da ligação, em uma função da HB que usa "&mut self" acabe-se ficando com 2 referências mutáveis para o mesmo valor: a global do &mut self e a particular da view; Neste sentido talvez a solução seria os HiddenBlocks terem uma referência (Rc?) de um RefCell da CM. Neste caso também as Views carregariam uma referência deste tipo.
- Para que o detentor do ticket possa alterar alguma coisa vai ser necessário o uso da "interior mutability" - Refcell - juntamente com o um smartpointer que conta o número "empréstimos" Rc. A ideia é emprestar para várias partes do programa os tickets, mas estes devem fazer uso e em seguida dropá-los (ou seria melhor deixar que fiquem com o ticket?).
- Com Refcell as regras de mutabilidade são checadas em "runtime". O programa vai travar se forem violadas.
- Uma segunda opção seria deixar o esquema de índices restrito aos HB + CMs. No nível de Network estes não apareceriam. Neste sentido a interface entre HB e CM seria um pouco maior. Também existe a questão de lidar manualmente com os indices.



