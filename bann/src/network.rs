//! An bock based artificial neural network model aimed to evolve parts
//! of the network independently (the blocks)

use rand::seq::index;
use std::fs::File;
// Used to save and load networks to tha disk
use serde::{Deserialize, Serialize};

use crate::connection_matrix::IConnectionMatrix;
use crate::hidden_block::{
    HiddenBlock, HiddenBlockConnection, HiddenBlockConnectionAddress, HiddenBlockNode,
};
use crate::innov_generator;
/// Network's view to a Connection
#[derive(Debug, Clone, Copy)]
pub struct NetworkConnection<'a> {
    pub hidden_block_index: usize,
    pub hidden_block_connection: HiddenBlockConnection<'a>,
}

/// Network's connection address
pub struct NetworkConnectionAddress {
    pub hidden_block_index: usize,
    pub hidden_block_connection_address: HiddenBlockConnectionAddress,
}

/// Network's view to a Node
#[derive(Debug, Clone, Copy)]
pub struct NetworkNode {
    pub hidden_block_index: usize,
    pub hidden_block_node: HiddenBlockNode,
}

/// Network factory - builds networks with common settings.
#[derive(Debug, Clone)]
pub struct NetworkFactory {
    network: Option<Network>,
    main_block_size: (usize, usize, usize),
    hidden_blocks_count: usize,
    hidden_blocks_size: usize,
}

/// Main block of the network (layers: input, output, interposer)
/// parameters
#[derive(Serialize, Deserialize, Debug, Clone, Copy, PartialEq)]
pub struct MainBlock {
    pub inputs: u32,
    pub outputs: u32,
    pub interposers: u32,
}

enum MainBlockLayerKind {
    Input,
    Output,
    Interposer,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Network {
    pub main_block: MainBlock,
    pub hidden_blocks: Vec<HiddenBlock>,
}
// /// Injnovation unique global id generator
// #[derive(Debug)]
// pub struct InnovGenerator {
//     last: AtomicU32,
// }

// impl InnovGenerator {
//     fn new(innov_start: u32) -> Self {
//         Self {
//             last: AtomicU32::new(innov_start)
//         }
//     }

//     pub fn get_next(&mut self) -> u32 {
//         // FIXME Check if this ordering is the right one
//         self.last.fetch_add(1, Ordering::Relaxed)
//     }
// }

impl Network {
    /// Converts a block network into a amorphous (aann)
    pub fn to_aann(&self) -> aann::Network {
        let mut nodes = Vec::new();
        let mut links = Vec::new();
        // Creating the input nodes
        for _ in 0..self.main_block.inputs {
            nodes.push(aann::Node::new("i", 0.))
        }
        // Creating the output nodes
        for _ in 0..self.main_block.outputs {
            nodes.push(aann::Node::new("o", 0.))
        }
        // Creating the interposers hidden nodes
        for _ in 0..self.main_block.interposers {
            nodes.push(aann::Node::new("h", 0.))
        }
        // Including the blocks
        for block in self.hidden_blocks.iter() {
            // Creating the bock nodes
            let start_node = nodes.len();
            for i in 0..block.hidden_nodes_n {
                nodes.push(aann::Node::new("h", block.biases[i]));
            }
            // Input links
            for link in block.input_matrix.iter() {
                let input_index = match link.input_node {
                    x if x < self.main_block.inputs as usize => x,
                    _ => {
                        //(self.main_block.inputs + self.main_block.outputs) as usize
                        //    + link.input_node
                        self.main_block.outputs as usize + link.input_node
                    }
                };
                links.push(aann::Link::new(
                    input_index,
                    link.output_node + start_node,
                    link.get_weight().unwrap(),
                ))
            }
            // Output links
            for link in block.output_matrix.iter() {
                links.push(aann::Link::new(
                    link.input_node + start_node,
                    self.main_block.inputs as usize + link.output_node,
                    link.get_weight().unwrap(),
                ))
            }
            // Internal hidden links
            for link in block.hidden_matrix.iter() {
                links.push(aann::Link::new(
                    link.input_node + start_node,
                    link.output_node + start_node,
                    link.get_weight().unwrap(),
                ))
            }
        }
        aann::Network::new(nodes, links)
    }

    pub fn set_connection_weight(&self, netcon: NetworkConnection, value: Option<f32>) {
        self.hidden_blocks[netcon.hidden_block_index]
            .set_connection_weight(&netcon.hidden_block_connection, value);
    }

    pub fn set_node_bias(&mut self, netnode: NetworkNode, value: f32) {
        match netnode.hidden_block_node {
            HiddenBlockNode::Hidden { index, .. } => {
                self.hidden_blocks[netnode.hidden_block_index].biases[index] = value
            }
            _ => panic!("Trying to change non hidden node bias is not allowed."),
        }
    }

    /// Renew a connection build it up again from the address in the old given.
    pub fn renew_connection(&self, netcon: NetworkConnection) -> NetworkConnection {
        let renew_con = self.hidden_blocks[netcon.hidden_block_index]
            .renew_connection(netcon.hidden_block_connection);
        NetworkConnection::new(netcon.hidden_block_index, renew_con)
    }

    /// Lists the connection of the network
    pub fn list_connections(&self) -> Vec<NetworkConnection> {
        let mut list = Vec::new();
        for (i, hblock) in self.hidden_blocks.iter().enumerate() {
            for hcon in hblock.list_connections().iter() {
                list.push(NetworkConnection::new(i, *hcon));
            }
        }
        list
    }

    /// Lists the connection of the network **
    pub fn list_active_nodes(&self) -> Vec<NetworkNode> {
        let mut list = Vec::new();
        for (i, hblock) in self.hidden_blocks.iter().enumerate() {
            for node_i in 0..hblock.hidden_nodes_n {
                if hblock.is_node_active(node_i) {
                    let hbnode = HiddenBlockNode::new_hidden(hblock, node_i);
                    list.push(NetworkNode::new(i, hbnode));
                }
            }
        }
        list
    }

    /// Lists the unconnection of the network
    pub fn list_unconnections(&self) -> Vec<NetworkConnection> {
        let mut list = Vec::new();
        for (i, hblock) in self.hidden_blocks.iter().enumerate() {
            for hcon in hblock.list_unconnections().iter() {
                list.push(NetworkConnection::new(i, *hcon));
            }
        }
        list
    }

    /// Lists the unconnection from active nodes of the network
    pub fn list_unconnections_from_active_nodes(&self) -> Vec<NetworkConnection> {
        let mut list = Vec::new();
        for (i, hblock) in self.hidden_blocks.iter().enumerate() {
            for hcon in hblock.list_unconnections_from_active_nodes().iter() {
                list.push(NetworkConnection::new(i, *hcon));
            }
        }
        list
    }

    /// Save to file json
    pub fn save_file(&self, file_name: &str) {
        let f = File::create(file_name).unwrap();
        serde_json::to_writer(&f, self);
        //let serialized = serde_json::to_string(&self).unwrap();
        //println!("serialized = {}", serialized);
    }

    /// Simplify all hiddenblocks (remove nodes without output)
    /// TODO Check bots different behavior when simplified.
    pub fn simplify(&mut self) {
        for hdblock in self.hidden_blocks.iter_mut() {
            hdblock.simplify();
        }
    }

    pub fn check_sanity(&self) {
        for hb in &self.hidden_blocks {
            hb.check_sanity();
        }
    }

    /// Appends an new input node
    pub fn append_input_node(&mut self) {
        for hdblock in self.hidden_blocks.iter_mut() {
            hdblock.append_input();
        }
        self.main_block.inputs += 1;
    }

    /// Insert an new input node at a given index.
    /// Shifts others to accommodate
    pub fn insert_input_node(&mut self, index: usize) {
        for hdblock in self.hidden_blocks.iter_mut() {
            hdblock.insert_input(index);
        }
        self.main_block.inputs += 1;
    }

    /// Removes an input node at a given index
    pub fn remove_input_node(&mut self, index: usize) {
        for hdblock in self.hidden_blocks.iter_mut() {
            hdblock.remove_input(index);
        }
        self.main_block.inputs -= 1;
    }

    /// Appends an new output node
    pub fn append_output_node(&mut self) {
        for hdblock in self.hidden_blocks.iter_mut() {
            hdblock.append_output();
        }
        self.main_block.outputs += 1;
    }

    /// Insert an output node ate a given index
    /// Shifts others to accommodate
    pub fn insert_output_node(&mut self, index: usize) {
        for hdblock in self.hidden_blocks.iter_mut() {
            hdblock.insert_output(index);
        }
        self.main_block.outputs += 1;
    }

    /// Removes an output node at a given index
    pub fn remove_output_node(&mut self, index: usize) {
        for hdblock in self.hidden_blocks.iter_mut() {
            hdblock.remove_output(index);
        }
        self.main_block.outputs -= 1;
    }
}

impl MainBlock {
    pub fn new(inputs: u32, outputs: u32, interposers: u32) -> Self {
        Self {
            inputs,
            outputs,
            interposers,
        }
    }
    pub fn count_total_nodes(&self) -> usize {
        (self.inputs + self.outputs + self.interposers) as usize
    }
}

impl<'a> NetworkConnection<'a> {
    pub fn new(
        hidden_block_index: usize,
        hidden_block_connection: HiddenBlockConnection<'a>,
    ) -> Self {
        Self {
            hidden_block_index,
            hidden_block_connection,
        }
    }

    pub fn get_weight(&self) -> Option<f32> {
        self.hidden_block_connection.get_weight()
    }

    pub fn into_address(&self) -> NetworkConnectionAddress {
        NetworkConnectionAddress {
            hidden_block_index: self.hidden_block_index,
            hidden_block_connection_address: self.hidden_block_connection.into_address(),
        }
    }
}

impl NetworkNode {
    pub fn new(hidden_block_index: usize, hidden_block_node: HiddenBlockNode) -> Self {
        Self {
            hidden_block_index,
            hidden_block_node,
        }
    }

    pub fn get_bias(&self) -> Option<f32> {
        self.hidden_block_node.get_bias()
    }
}

impl NetworkFactory {
    pub fn new(inputs: usize, outputs: usize, interposers: usize) -> Self {
        let main_block = MainBlock {
            inputs: inputs as u32,
            outputs: outputs as u32,
            interposers: interposers as u32,
        };
        let hidden_blocks = Vec::new();
        let network = Some(Network {
            main_block,
            hidden_blocks,
        });
        Self {
            network,
            main_block_size: (inputs, outputs, interposers),
            hidden_blocks_count: 1,
            hidden_blocks_size: 10,
        }
    }

    /// Sets the number of hidden blocks and size
    pub fn set_hidden_blocks(mut self, count: usize, size: usize) -> Self {
        self.hidden_blocks_count = count;
        self.hidden_blocks_size = size;
        self
    }

    /// Sets the innov value (hidden block uniq id)
    pub fn set_innov_start(self, val: u32) -> Self {
        innov_generator::set_innov(val);
        self
    }

    /// Building the network with the settings
    pub fn build(&mut self) -> Network {
        // Populating the hidden blocks
        for _ in 0..self.hidden_blocks_count {
            match self.network {
                Some(ref mut x) => {
                    let hb = HiddenBlock::new(
                        self.hidden_blocks_size,
                        x.main_block,
                        innov_generator::next_innov(),
                    );
                    x.hidden_blocks.push(hb);
                }
                None => panic!("Trying to build a non existing network"),
            };
        }
        std::mem::replace(&mut self.network, None).unwrap()
    }

    pub fn load_file(file_name: &str) -> Network {
        let f = File::open(file_name).unwrap();
        serde_json::from_reader(&f).unwrap()
        //let serialized = serde_json::to_string(&self).unwrap();
        //println!("serialized = {}", serialized);
    }

    /// Simple network to use as a test case
    /// It may change...
    pub fn new_test_network() -> Network {
        let main_block = MainBlock {
            inputs: 3,
            outputs: 2,
            interposers: 5,
        };
        let mut hidden_blocks = Vec::new();
        let mut hb1 = HiddenBlock::new(3, MainBlock::new(3, 2, 5), innov_generator::next_innov());
        hb1.input_matrix.set_weight(0, 0, Some(0.2));
        hb1.input_matrix.set_weight(1, 0, Some(0.2));
        hb1.input_matrix.set_weight(4, 2, Some(0.2));
        hb1.output_matrix.set_weight(0, 1, Some(0.2));
        hb1.output_matrix.set_weight(1, 3, Some(0.2));
        hb1.hidden_matrix.set_weight(0, 1, Some(0.2));
        hb1.hidden_matrix.set_weight(0, 2, Some(0.2));
        hb1.hidden_matrix.set_weight(2, 1, Some(0.2));
        let mut hb2 = HiddenBlock::new(2, MainBlock::new(3, 2, 5), innov_generator::next_innov());
        hb2.input_matrix.set_weight(1, 1, Some(0.2));
        hb2.input_matrix.set_weight(2, 0, Some(0.2));
        hb2.output_matrix.set_weight(0, 4, Some(0.2));
        hb2.output_matrix.set_weight(1, 4, Some(0.2));
        hb2.output_matrix.set_weight(1, 0, Some(0.2));
        hidden_blocks.push(hb1);
        hidden_blocks.push(hb2);
        Network {
            main_block: main_block,
            hidden_blocks: hidden_blocks,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_network_simplification() {
        let mut net = NetworkFactory::new_test_network();
        let mut aann_full = net.to_aann();
        net.simplify();
        let mut aann_simplified = net.to_aann();
        let mut res_full = Vec::new();
        let mut res_simplified = Vec::new();
        for i in 0..100 {
            res_full.push(aann_full.output());
            res_simplified.push(aann_simplified.output());
            aann_full.input(vec![1., 1., 1.]);
            aann_simplified.input(vec![1., 1., 1.]);
            aann_full.tick();
            aann_simplified.tick();
        }
        assert_eq!(res_full, res_simplified);
    }

    #[test]
    fn test_network_simplification_with_mutations_and_whatnot() {
        use crate::breeding::mate_networks;
        use crate::mutations::*;
        let mut net = NetworkFactory::new_test_network();
        for i in 0..net.hidden_blocks[0].output_matrix.input_n {
            for j in 0..net.hidden_blocks[0].output_matrix.output_n {
                net.hidden_blocks[0]
                    .output_matrix
                    .set_weight(i, j, Some(-1.));
            }
        }
        let mut mutations: Vec<Box<dyn IMutation>> = Vec::new();
        for i in 0..100 {
            for i in 0..100 {
                mutations.push(Box::new(MutationPerturbBiases::new(0.4, 0.3)));
                mutations.push(Box::new(MutationAddConnection::new(0.4)));
                mutations.push(Box::new(MutationRemoveConnection::new(0.2)));
                mutations.push(Box::new(MutationPerturbSingleBias::new(0.4, 0.4)));
                mutations.push(Box::new(MutationAddMixNode::new(0.4)))
            }
            let mut net_old = net.clone();
            net.mutate(&mutations);
            net = mate_networks(&net_old, &net);
            let mut aann_full = net.to_aann();
            net.simplify();
            let mut aann_simplified = net.to_aann();
            let mut res_full = Vec::new();
            let mut res_simplified = Vec::new();
            net.check_sanity();
            for i in 0..100 {
                res_full.push(aann_full.output());
                res_simplified.push(aann_simplified.output());
                aann_full.input(vec![1., 1., 1.]);
                aann_simplified.input(vec![1., 1., 1.]);
                aann_full.tick();
                aann_simplified.tick();
            }
            assert_eq!(res_full, res_simplified);
        }
    }
}
