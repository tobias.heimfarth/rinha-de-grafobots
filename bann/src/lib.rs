pub mod network;
pub mod hidden_block;
pub mod connection_matrix;
pub mod graphviz_interface;
pub mod mutations;
pub mod to_dot;
pub mod breeding;
mod innov_generator;