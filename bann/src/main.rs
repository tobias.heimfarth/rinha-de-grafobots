use bann::graphviz_interface::render_bann;
use bann::mutations::{
    IMutation,
    MutationAddConnection,
    MutationRemoveConnection,
    MutationAddNode,
    MutationPerturbBiases,
    MutationPerturbWeights,
    MutationPerturbSingleBias,
    MutationPerturbSingleWeight,
    MutationAddMixNode,
};
use bann::network::{NetworkFactory, MainBlock, Network};
use bann::hidden_block::{HiddenBlock};
use bann::to_dot::bann_to_dot;

use rand::prelude::*;

fn main() {
    //let mut network = NetworkFactory::new_test_network();
    let mut network = 
        NetworkFactory::new(3,4,5)
        .set_hidden_blocks(3,5)
        .build();
    bann_to_dot(&network, "test_network.dot");
    //let con = network.hidden_blocks[0].input_matrix.connection_from_index(0);
    //network.hidden_blocks[0].add_node_to_input_connection(&con.unwrap());
    //network.hidden_blocks[0].hidden_matrix.change_single_random_weight(1.);
    let mut mutations: Vec<Box<dyn IMutation>> = Vec::new();
    //mutations.push(Box::new(MutationPerturbWeights::new(0.9, 0.1)));
    //mutations.push(Box::new(MutationPerturbBiases::new(0.9, 0.1)));
    //mutations.push(Box::new(MutationAddNode::new(1.)));
    //mutations.push(Box::new(MutationAddConnection::new(1.)));
    //mutations.push(Box::new(MutationAddConnection::new(1.)));
    //mutations.push(Box::new(MutationAddConnection::new(1.)));
    //mutations.push(Box::new(MutationAddConnection::new(1.)));
    for i in 0..20 {
        //mutations.push(Box::new(MutationAddNode::new(1.)));
        mutations.push(Box::new(MutationAddConnection::new(0.4)));
        mutations.push(Box::new(MutationRemoveConnection::new(0.2)));
        mutations.push(Box::new(MutationPerturbSingleBias::new(0.4,0.4)));
    }
    network.mutate(&mutations);
    bann_to_dot(&network, "test.dot");
    let mut mutations: Vec<Box<dyn IMutation>> = Vec::new();
    mutations.push(Box::new(MutationAddMixNode::new(1.)));
    network.mutate(&mutations);
    bann_to_dot(&network, "test_mix_node.dot");
    network.simplify();
    bann_to_dot(&network, "test_simplified.dot");
    let mut mutations: Vec<Box<dyn IMutation>> = Vec::new();
    mutations.push(Box::new(MutationRemoveConnection::new(1.)));
    network.mutate(&mutations);
    bann_to_dot(&network, "test_network_connection_removed.dot");
    println!("{}", network.hidden_blocks[0].input_matrix);
    let unconnection = network.draw_random_unconnection().unwrap();
    let uncon_copy = unconnection.clone();
    network.set_connection_weight(unconnection,Some(2.0));
    println!("{:?}", network.renew_connection(uncon_copy) );
    network.save_file("asdf.json");
    let mut network = NetworkFactory::load_file("asdf.json");
    bann_to_dot(&network, "saved_and_reloaded.dot");
    test_network_simplification();
}


fn test_network_simplification() {
    let mut net = NetworkFactory::new_test_network();
    let mut mutations: Vec<Box<dyn IMutation>> = Vec::new();
    for i in 0..20 {
        //mutations.push(Box::new(MutationAddNode::new(1.)));
        mutations.push(Box::new(MutationAddConnection::new(0.4)));
        mutations.push(Box::new(MutationRemoveConnection::new(0.2)));
        mutations.push(Box::new(MutationPerturbSingleBias::new(0.4,0.4)));
    }
    net.mutate(&mutations);
    let mut aann_full = net.to_aann();
    bann_to_dot(&net, "full.dot");
    aann::to_dot::aann_to_dot(&aann_full, "full_aann.dot");
    net.simplify();
    let mut aann_simplified = net.to_aann();
    bann_to_dot(&net, "simplified.dot");
    aann::to_dot::aann_to_dot(&aann_simplified, "simplified_aann.dot");
    let mut res_full = Vec::new();
    let mut res_simplified = Vec::new();
    for i in 0..20 {
        println!("{:?}", aann_full.output());
        println!("{:?}", aann_simplified.output());
        res_full.push(aann_full.output());
        res_simplified.push(aann_simplified.output());
        let input = vec![rand::random::<f32>() - 0.5, rand::random::<f32>() - 0.5, rand::random::<f32>() - 0.5];
        aann_full.input(input.clone());
        aann_simplified.input(input.clone());
        aann_full.tick();
        aann_simplified.tick();
        
    }
    assert_eq!(res_full, res_simplified);
}