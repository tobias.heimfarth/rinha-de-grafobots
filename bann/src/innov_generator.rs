// FIXME A "for now" rough way of keeping track of the global innov id
// to hidden blocks. A more sophisticated approach will be probably be
// required.
use std::sync::atomic::{AtomicU32, Ordering};

static INNOV_GEN:AtomicU32 = AtomicU32::new(0);

pub fn next_innov() -> u32 {
    INNOV_GEN.fetch_add(1, Ordering::Relaxed)
}

pub fn set_innov(val: u32) {
    INNOV_GEN.store(val, Ordering::Relaxed);
}