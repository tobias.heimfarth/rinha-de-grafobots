use rand::prelude::*;

use crate::connection_matrix::{ConnectionMatrix, IConnectionMatrix};
use crate::hidden_block::HiddenBlock;
use crate::innov_generator::next_innov;
use crate::network::Network;

/// Checks if a given network has a given innov hidden block
fn has_innov(net: &Network, innov: u32) -> Option<usize> {
    for (i, hb) in net.hidden_blocks.iter().enumerate() {
        if hb.innov == innov {
            return Some(i);
        }
    }
    None
}

enum HiddenBlockMatch {
    Pair(u32, usize, usize),
    Net1Single(u32, usize),
    Net2Single(u32, usize),
}

// ** Parei aqui.. tentando melhorar essa classificação dos blocks
// fn match_hidden_blocks(net1: &Network, net2: &Network) -> Vec<HiddenBlockMatch> {
//     let matches: Vec<(u32, usize, usize)> = net1
//         .hidden_blocks
//         .iter()
//         .enumerate()
//         .filter_map(|(i, hb)| match has_innov(&net2, hb.innov) {
//             Some(j) => Some((hb.innov, i, j)),
//             _ => None,
//         })
//         .collect();

// }

/// Lists the hidden blocks common to both networks
fn get_common_innov_list(net1: &Network, net2: &Network) -> Vec<(u32, usize, usize)> {
    let commons: Vec<(u32, usize, usize)> = net1
        .hidden_blocks
        .iter()
        .enumerate()
        .filter_map(|(i, hb)| match has_innov(&net2, hb.innov) {
            Some(j) => Some((hb.innov, i, j)),
            _ => None,
        })
        .collect();
    commons
}

/// Rough network mating.
/// FIXME do something better
pub fn mate_networks(net1: &Network, net2: &Network) -> Network {
    // Sanity checks
    sanity_check_networks_mate_pairs(&net1, &net2);
    // Creating new network to be filled
    let mut new_net = Network {
        main_block: net1.main_block.clone(),
        hidden_blocks: Vec::new(),
    };
    // Initiating random number generator
    let mut rng = rand::thread_rng();
    // Getting the common innovs to both networks
    let commons = get_common_innov_list(net1, net2);
    let net1_commons: Vec<usize> = commons.iter().map(|(_, i, j)| *i).collect();
    let net2_commons: Vec<usize> = commons.iter().map(|(_, i, j)| *j).collect();
    let net1_uniques: Vec<usize> = (0..net1.hidden_blocks.len())
        .filter(|i| !net1_commons.contains(i))
        .collect();
    let net2_uniques: Vec<usize> = (0..net2.hidden_blocks.len())
        .filter(|i| !net2_commons.contains(i))
        .collect();
    // Getting one random block from the common pairs
    for (_, i, j) in commons {
        if rng.gen_bool(0.5) {
            new_net.hidden_blocks.push(net1.hidden_blocks[i].clone());
        } else {
            new_net.hidden_blocks.push(net2.hidden_blocks[j].clone());
        }
    }
    let max_hidden_blocks: usize = 20;
    // Getting random blocks from the uniques
    for i in net1_uniques {
        if rng.gen_bool(0.5) && new_net.hidden_blocks.len() < max_hidden_blocks {
            new_net.hidden_blocks.push(net1.hidden_blocks[i].clone());
        }
    }
    for j in net2_uniques {
        if rng.gen_bool(0.5) && new_net.hidden_blocks.len() < max_hidden_blocks {
            new_net.hidden_blocks.push(net2.hidden_blocks[j].clone());
        }
    }
    // Exclude some block randomly
    if rng.gen_bool(0.1) && new_net.hidden_blocks.len() > 1 {
        new_net
            .hidden_blocks
            .remove(rng.gen_range(0..new_net.hidden_blocks.len()));
    }
    // Duplicate block
    let hb_length = new_net.hidden_blocks.len();
    let mut duplicates = Vec::new();
    for hb in new_net.hidden_blocks.iter() {
        if rng.gen_bool(0.1 / hb_length as f64) && new_net.hidden_blocks.len() < max_hidden_blocks {
            let mut duplicated_block = hb.clone();
            duplicated_block.innov = next_innov();
            duplicates.push(duplicated_block)
        }
    }
    new_net.hidden_blocks.extend(duplicates);
    // Returning the new network
    new_net
}

fn sanity_check_networks_mate_pairs(net1: &Network, net2: &Network) {
    // number of hidden networks match?
    // if net1.hidden_blocks.len() != net2.hidden_blocks.len() {
    //     panic!("Number of hidden blocks does not match")
    // }
    // main block size match?
    if net1.main_block != net2.main_block {
        panic!("Main blocks does not match")
    }
}

/// Simple struct to hold the genetic similarity results
pub struct SimilarityPoints {
    // Similarity points
    pub points: f32,
    // Total possible points
    pub total: i32,
}

impl SimilarityPoints {
    fn new() -> Self {
        Self {
            points: 0.0,
            total: 0,
        }
    }
}

impl std::ops::AddAssign for SimilarityPoints {
    fn add_assign(&mut self, other: Self) {
        *self = Self {
            points: self.points + other.points,
            total: self.total + other.total,
        }
    }
}

pub enum GeneticSimilarity {
    Incompatible,
    Compatible(SimilarityPoints),
}

/// Computes a value that try to measure the genetic distance between
/// two networks.
pub fn get_similarity(net1: &Network, net2: &Network) -> GeneticSimilarity {
    if net1.main_block != net2.main_block {
        GeneticSimilarity::Incompatible
    } else {
        let mut similarity = SimilarityPoints::new();
        for hb_net1 in &net1.hidden_blocks {
            for hb_net2 in &net2.hidden_blocks {
                if let Some(hbs_similarity) = compute_hidden_blocks_similarity(&hb_net1, &hb_net2) {
                    similarity += hbs_similarity;
                }
            }
        }
        GeneticSimilarity::Compatible(similarity)
    }
}

fn compute_hidden_blocks_similarity(
    hb1: &HiddenBlock,
    hb2: &HiddenBlock,
) -> Option<SimilarityPoints> {
    if hb1.innov != hb2.innov {
        None
    } else {
        // Computing similarity for the input connection matrix
        let mut similarity = SimilarityPoints::new();
        similarity += compute_connection_matrix_similarity(&hb1.input_matrix, &hb2.input_matrix);
        similarity += compute_connection_matrix_similarity(&hb1.output_matrix, &hb2.output_matrix);
        similarity += compute_connection_matrix_similarity(&hb1.hidden_matrix, &hb2.hidden_matrix);
        Some(similarity)
    }
}

/// Computes a simple similarity "index" for two given connection matrices
fn compute_connection_matrix_similarity(
    cm1: &ConnectionMatrix,
    cm2: &ConnectionMatrix,
) -> SimilarityPoints {
    let min_input_n: usize = std::cmp::min(cm1.input_n, cm2.input_n);
    let min_output_n: usize = std::cmp::min(cm1.output_n, cm2.output_n);
    let mut similarity = SimilarityPoints::new();
    for input_i in 0..min_input_n {
        for output_i in 0..min_output_n {
            match (
                cm1.get_weight(input_i, output_i),
                cm2.get_weight(input_i, output_i),
            ) {
                (Some(w1), Some(w2)) => similarity.points += 1. / (1. + (w1 - w2).abs()),
                (None, None) => similarity.points += 1.,
                _ => (),
            }
        }
    }
    // The total is the count of different possible connections, that is
    // the total from each connection matrix minus the overlap
    similarity.total = (cm1.count_total_possible_connections()
        + cm2.count_total_possible_connections()
        - min_input_n * min_output_n) as i32;
    similarity
}

// /// From two compatible hidden blocks one new hidden block is made by
// /// mixing their characteristics.
// /// For now, only works for same size hidden blocks
// pub fn mate_hidden_blocks(hdb1: &HiddenBlock, hdb2: &HiddenBlock) -> HiddenBlock {
//     // Initiating random number generator
//     let mut rng = rand::thread_rng();
//     // Rough sanity check
//     if hdb1.input_n != hdb2.input_n && hdb1.output_n != hdb2.output_n {
//         panic!("Can not mate hidden blocks with different dimensions.")
//     }
//     let new_hb = HiddenBlock::new(hdb1.input_n, hdb1.output_n);
//     for i in 0..hdb1.values.len() {
//         let rand: f64 = rng.gen();
//         if rand > 0.5 {
//             new_hb.values[i] = hdb1.values[i];
//         } else {
//             new_hb.values[i] = hdb2.values[i];
//         }
//     }
//     new_hb
// }
