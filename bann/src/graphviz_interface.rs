// Old interface, use to_dot instead

use crate::network::Network;
//use crate::connection_matrix::IConnection;
use std::borrow::Cow;
use std::io::Write;

type Nd = usize;
type Ed<'a> = &'a (usize, usize, f32);

pub struct Graph {
    pub nodes: Vec<&'static str>,
    pub edges: Vec<(usize, usize, f32)>,
    pub weights: Vec<f32>,
}

/// Generates a dot file (graphviz) with the bann network
pub fn render_bann<W: Write>(output: &mut W, bann_network: &Network) {
    let mut nodes = Vec::new();
    let mut edges = Vec::new();
    let mut weights = Vec::new();
    // Creating the input nodes
    for _ in 0..bann_network.main_block.inputs {
        nodes.push("i");
    }
    // Creating the output nodes
    for _ in 0..bann_network.main_block.outputs {
        nodes.push("o");
    }
    // Creating the interposers hidden nodes
    for _ in 0..bann_network.main_block.interposers {
        nodes.push("h");
    }
    // The hidden block nodes must be offseted by the main block nodes
    // let mut start_node
    //     = bann_network.main_block.inputs
    //     + bann_network.main_block.outputs
    //     + bann_network.main_block.interposers;

    // Hidden block links and nodes
    for block in bann_network.hidden_blocks.iter() {
        // Creating the bock nodes
        let start_node = nodes.len();
        for _ in 0..block.hidden_nodes_n {
            nodes.push("h");
        }
        // Searching for all links
        // Input links
        for link in block.input_matrix.iter() {
            edges.push((
                link.input_node,
                link.output_node + start_node as usize,
                link.get_weight().unwrap(),
            ));
            weights.push(link.get_weight().unwrap());
        }
        // Output links
        for link in block.output_matrix.iter() {
            edges.push((
                link.input_node + start_node as usize,
                link.output_node,
                link.get_weight().unwrap(),
            ));
            weights.push(link.get_weight().unwrap());
        }
        // Internal hidden links
        for link in block.hidden_matrix.iter() {
            edges.push((
                link.input_node + start_node as usize,
                link.output_node + start_node as usize,
                link.get_weight().unwrap(),
            ));
            weights.push(link.get_weight().unwrap());
        }
    }
    println!("{:?}", edges);
    render_to(
        output,
        Graph {
            nodes: nodes,
            edges: edges,
            weights: weights,
        },
    );
}

pub fn render_to<W: Write>(output: &mut W, graph: Graph) {
    dot::render(&graph, output).unwrap()
}
impl<'a> dot::Labeller<'a, Nd, Ed<'a>> for Graph {
    fn graph_id(&'a self) -> dot::Id<'a> {
        dot::Id::new("example2").unwrap()
    }
    fn node_id(&'a self, n: &Nd) -> dot::Id<'a> {
        dot::Id::new(format!("N{}", n)).unwrap()
    }
    fn node_label<'b>(&'b self, n: &Nd) -> dot::LabelText<'b> {
        dot::LabelText::LabelStr(self.nodes[*n].into())
    }
    fn edge_label<'b>(&'b self, n: &Ed) -> dot::LabelText<'b> {
        dot::LabelText::LabelStr((format!("{}", n.2)).into())
    }
}

impl<'a> dot::GraphWalk<'a, Nd, Ed<'a>> for Graph {
    fn nodes(&self) -> dot::Nodes<'a, Nd> {
        (0..self.nodes.len()).collect()
    }
    fn edges(&'a self) -> dot::Edges<'a, Ed<'a>> {
        self.edges.iter().collect()
    }
    fn source(&self, e: &Ed) -> Nd {
        e.0
    }
    fn target(&self, e: &Ed) -> Nd {
        e.1
    }
}
