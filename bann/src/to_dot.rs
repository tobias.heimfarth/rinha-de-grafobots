use crate::network::Network;
use dot_writer::{Attributes, Color, DotWriter, NodeId, Rank, Shape, Style};
use std::fs;

/// Creates a dot format file with the network
pub fn bann_to_dot(bann_network: &Network, file_name: &str) {
    let mut output_bytes = Vec::new();
    {
        let mut writer = DotWriter::from(&mut output_bytes);
        writer.set_pretty_print(false);
        let mut digraph = writer.digraph();
        //digraph.graph_attributes().set("clusterrank", "local", false);
        {
            // Input layer nodes
            let mut cluster = digraph.cluster();
            cluster
                .set_label("Input layer")
                .set_style(Style::Filled)
                .set_color(Color::LightGrey)
                .set_rank(Rank::Source);
            cluster
                .node_attributes()
                .set_style(Style::Filled)
                .set_color(Color::White);
            for i in 0..bann_network.main_block.inputs {
                let id = NodeId::from(format!("i{}", i));
                cluster.node_named(id);
            }
        }
        {
            // Output layer nodes
            let mut cluster = digraph.cluster();
            cluster
                .set_style(Style::Unfilled)
                .set_color(Color::Blue)
                .set_rank(Rank::Sink)
                .set_label("Output layer");
            cluster
                .node_attributes()
                .set_style(Style::Unfilled)
                .set_color(Color::Blue);
            for i in 0..bann_network.main_block.outputs {
                let id = NodeId::from(format!("o{}", i));
                cluster.node_named(id);
            }
        }
        {
            // Interposer layer nodes
            let mut cluster = digraph.subgraph();
            // cluster
            //     .set_label("Interposer layer")
            //     .set_style(Style::Filled)
            //     .set_color(Color::Red);
                //.set_rank(Rank::Same);
            //cluster.set_rank_direction(dot_writer::RankDirection::TopBottom);
            cluster
                .node_attributes()
                .set_style(Style::Filled)
                .set_color(Color::Black)
                .set_font_color(Color::White);
            for i in 0..bann_network.main_block.interposers {
                let id = NodeId::from(format!("p{}", i));
                cluster.node_named(id);
            }
        }
        // Hidden block links and nodes
        for (i, block) in bann_network.hidden_blocks.iter().enumerate() {
            {
                let mut cluster = digraph.cluster();
                cluster
                    .set_label(&format!("Hidden Block innov {}", block.innov))
                    .set_style(Style::Filled)
                    .set_color(Color::PaleGreen);
                cluster
                    .node_attributes()
                    .set_style(Style::Filled)
                    .set_rank(Rank::Same);
                // Creating the bock nodes
                for j in 0..block.hidden_nodes_n {
                    // Distinct color if the node is not active
                    if block.is_node_active(j) {
                        cluster.node_attributes().set_color(Color::White);
                    } else {
                        cluster.node_attributes().set_color(Color::Red);
                    }

                    let id = NodeId::from(format!("b{}h{}", i, j));
                    // Adding the bias to the label.
                    cluster
                        .node_attributes()
                        .set_label(&format!("b{}h{}\n{}", i, j, block.biases[j]));
                    cluster.node_named(id);
                }
            }
            // Searching for all links
            // Input links
            for link in block.input_matrix.iter() {
                let id_in = match link.input_node {
                    x if x < bann_network.main_block.inputs as usize => {
                        NodeId::from(format!("i{}", link.input_node))
                    }
                    _ => NodeId::from(format!(
                        "p{}",
                        link.input_node - bann_network.main_block.inputs as usize
                    )),
                };
                let id_out = NodeId::from(format!("b{}h{}", i, link.output_node));
                digraph
                    .edge(id_in, id_out)
                    .attributes()
                    .set_label(&format!("{}", link.get_weight().unwrap()));
            }
            // Output links
            for link in block.output_matrix.iter() {
                let id_out = match link.output_node {
                    x if x < bann_network.main_block.outputs as usize => {
                        NodeId::from(format!("o{}", link.output_node))
                    }
                    _ => NodeId::from(format!(
                        "p{}",
                        link.output_node - bann_network.main_block.outputs as usize
                    )),
                };
                let id_in = NodeId::from(format!("b{}h{}", i, link.input_node));
                digraph
                    .edge(id_in, id_out)
                    .attributes()
                    .set_label(&format!("{}", link.get_weight().unwrap()));
            }
            // Internal hidden links
            for link in block.hidden_matrix.iter() {
                let id_in = NodeId::from(format!("b{}h{}", i, link.input_node));
                let id_out = NodeId::from(format!("b{}h{}", i, link.output_node));
                digraph
                    .edge(id_in, id_out)
                    .attributes()
                    .set_label(&format!("{}", link.get_weight().unwrap()));
            }
        }
    }
    let data = String::from_utf8(output_bytes).unwrap();
    fs::write(file_name, data).expect("Unable to write file");
}
