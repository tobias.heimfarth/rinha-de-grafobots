//! Matrices with connection values between an input set of nodes and
//! an output set.

use std::fmt;
use std::ops::Index;
use std::{cell::Cell, process::Output};

// Used to save and load networks to tha disk
use serde::{Deserialize, Serialize};

/// Interface describing the basic interactions with a connection matrix
pub trait IConnectionMatrix {
    fn new(input_n: usize, output_n: usize) -> Self;

    /// Adds an input node filled with Nones at the end of the input list
    fn append_input_node(&mut self);

    /// Insert an input node in the given index shifting the rest
    fn insert_input_node(&mut self, index: usize);

    /// Removes an input node by its index (changes the matrix size)
    fn remove_input_node(&mut self, index: usize);

    /// Cleans an input node connections (setting all to None but leaving
    /// the matrix size untouched)
    fn clean_input_node(&mut self, index: usize);

    /// Adds an output node filled with Nones to the end of the output list
    fn append_output_node(&mut self);

    /// Insert an output node in the given index shifting the rest
    fn insert_output_node(&mut self, index: usize);

    /// Removes an output node by its index(changes the matrix size)
    fn  remove_output_node(&mut self, index: usize);

    /// Cleans an input node connections (setting all to None but leaving
    /// the matrix size untouched)
    fn clean_output_node(&mut self, index: usize);

    /// Returns a list of indexes of the non connected node pairs
    fn list_unconnections_indexes(&self) -> Vec<usize>;

    /// Returns a list of indexes of the connected node pairs
    fn list_connections_indexes(&self) -> Vec<usize>;

    /// Returns the number of active connections
    fn connections_len(&self) -> usize;

    /// Returns the total possible connections (connected + unconnected)
    fn count_total_possible_connections(&self) -> usize;

    /// Returns a connection for a given index
    fn get_connection_index(&self, index: usize) -> ConnectionView;

    /// Returns a connection weight from a given pair of indexes,
    /// input and output
    fn get_weight(&self, line: usize, col: usize) -> Option<f32>;

    /// Sets a connection weight from a given pair of indexes,
    /// input and output
    fn set_weight(&mut self, line: usize, col: usize, value: Option<f32>);

    /// Returns the connection weight for a given index
    fn get_weight_index(&self, index: usize) -> Option<f32>;

    /// Sets a connection weight
    fn set_weight_index(&self, index: usize, value: Option<f32>);

    // Returns a connection view from the in node index and the out node index
    fn connection_from_nodes_indexes(
        &self,
        in_node_index: usize,
        out_node_index: usize,
    ) -> ConnectionView;

    // Returns the input and output nodes indexes from a connection
    // index
    fn io_nodes_indexes(&self, index: usize) -> (usize, usize);
}
#[derive(Serialize, Deserialize, Debug, Clone)]
/// Simple Matrix value entry. Abstraction to allow tests using different
/// kinds of entry types with the same api.
pub struct MatrixValue {
    pub value: Cell<Option<f32>>,
}

impl MatrixValue {
    pub fn new(val: Option<f32>) -> Self {
        Self {
            value: Cell::new(val),
        }
    }

    pub fn get(&self) -> Option<f32> {
        self.value.get()
    }

    pub fn set(&self, value: Option<f32>) {
        self.value.set(value);
    }

    pub fn get_mut(&mut self) -> &mut Option<f32> {
        self.value.get_mut()
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
/// Main connection matrix struct.
pub struct ConnectionMatrix {
    pub values: Vec<MatrixValue>,
    pub input_n: usize,
    pub output_n: usize,
    current: usize,
}

#[derive(Debug, Clone, Copy)]
/// Basic connection data view to pass around
pub struct ConnectionView<'a> {
    pub input_node: usize,
    pub output_node: usize,
    pub weight: &'a MatrixValue,
    pub index: usize,
}

impl<'a> ConnectionView<'a> {
    fn new(connection_matrix: &'a ConnectionMatrix, index: usize) -> Self {
        let (input_n, output_n) = get_line_col(index, connection_matrix.output_n);
        ConnectionView {
            input_node: input_n,
            output_node: output_n,
            weight: &connection_matrix.values[index],
            index,
        }
    }

    /// Returns the weight of the view (just passes the weight field value for
    /// compatibility with more elaborated future (past?) versions)
    pub fn get_weight(&self) -> Option<f32> {
        self.weight.get()
    }
}

impl IConnectionMatrix for ConnectionMatrix {
    fn new(input_n: usize, output_n: usize) -> Self {
        Self {
            input_n,
            output_n,
            values: vec![MatrixValue::new(None); input_n * output_n],
            current: 0,
        }
    }

    fn append_input_node(&mut self) {
        let new_line: Vec<MatrixValue> = vec![MatrixValue::new(None); self.output_n];
        self.values.extend(new_line);
        self.input_n += 1;
    }

    fn insert_input_node(&mut self, index: usize) {
        // Computing the first value to be shifted
        let next_input_index: usize = index *self.output_n;
        // Shifting 
        for _ in 0..self.output_n {
            self.values.insert(next_input_index, MatrixValue::new(None))
        }
        self.input_n += 1;
    }

    fn remove_input_node(&mut self, index: usize) {
        for i in (index * self.output_n..((index + 1) * self.output_n)).rev() {
            self.values.remove(i);
        }
        self.input_n -= 1;
    }

    fn clean_input_node(&mut self, index: usize) {
        for i in (index * self.output_n..((index + 1) * self.output_n)).rev() {
            self.values[i].set(None);
        }
    }

    fn append_output_node(&mut self) {
        for i in 0..self.input_n {
            self.values
                .insert((i + 1) * self.output_n + i, MatrixValue::new(None));
        }
        self.output_n += 1;
    }

    fn insert_output_node(&mut self, index: usize) {
        for i in 0..self.input_n {
            self.values
                .insert(i * self.output_n + index + i, MatrixValue::new(None));
        }
        self.output_n += 1;
    }   

    fn remove_output_node(&mut self, index: usize) {
        for i in (0..self.input_n).rev() {
            self.values.remove(i * self.output_n + index);
        }
        self.output_n -= 1;
    }

    fn clean_output_node(&mut self, index: usize) {
        for i in (0..self.input_n).rev() {
            self.values[i * self.output_n + index].set(None);
        }
    }

    fn list_unconnections_indexes(&self) -> Vec<usize> {
        let none_elements_indexes: Vec<usize> = self
            .values
            .iter()
            .enumerate()
            .filter_map(|x| match (x.0, x.1.get()) {
                (index, None) => Some(index),
                _ => None,
            })
            .collect();
        none_elements_indexes
    }

    fn list_connections_indexes(&self) -> Vec<usize> {
        let elements_indexes: Vec<usize> = self
            .values
            .iter()
            .enumerate()
            .filter_map(|x| match (x.0, x.1.get()) {
                (index, Some(_x)) => Some(index),
                _ => None,
            })
            .collect();
        elements_indexes
    }

    fn count_total_possible_connections(&self) -> usize {
        self.input_n * self.output_n
    }

    fn connections_len(&self) -> usize {
        self.values
            .iter()
            .filter(|&x| !x.get().is_none())
            .collect::<Vec<&MatrixValue>>()
            .len()
    }

    fn get_connection_index(&self, index: usize) -> ConnectionView {
        ConnectionView::new(&self, index)
    }

    fn get_weight_index(&self, index: usize) -> Option<f32> {
        self.values[index].get()
    }

    fn set_weight_index(&self, index: usize, value: Option<f32>) {
        self.values[index].set(value);
    }

    fn connection_from_nodes_indexes(
        &self,
        in_node_index: usize,
        out_node_index: usize,
    ) -> ConnectionView {
        let index = in_node_index * self.output_n + out_node_index;
        ConnectionView::new(&self, index)
    }

    fn io_nodes_indexes(&self, index: usize) -> (usize, usize) {
        get_line_col(index, self.output_n)
    }

    fn get_weight(&self, line: usize, col: usize) -> Option<f32> {
        let index = line * self.output_n + col;
        self.values[index].get()
    }

    fn set_weight(&mut self, line: usize, col: usize, value: Option<f32>) {
        let index = line * self.output_n + col;
        self.values[index].set(value);
    }

}

// Methods to allow the simplification of the network (simplify methods in upstream)
impl ConnectionMatrix {
    /// Starting from a given set of nodes, checks which other nodes are
    /// connected upstream. That is, only nodes that have some influence in
    /// that given set are marked.
    pub fn mask_upstream_connected_nodes(&self, start_nodes: &[usize]) -> Vec<bool> {
        let mut conmask = vec![false; self.output_n];
        start_nodes.iter().for_each(|i| conmask[*i] = true);
        for i in start_nodes.iter() {
            self.mark_input_nodes(*i, &mut conmask);
        }
        conmask
    }

    /// Mark all input nodes from a given start, recursively
    fn mark_input_nodes(&self, node_index: usize, mask: &mut [bool]) {
        for i in self.list_input_connections(node_index).iter() {
            if !mask[*i] {
                mask[*i] = true;
                self.mark_input_nodes(*i, mask)
            }
        }
    }

    // Lists all the inputs nodes from a given node
    fn list_input_connections(&self, node_index: usize) -> Vec<usize> {
        let mut cons = Vec::new();
        for i in 0..self.output_n {
            if self.get_weight(i, node_index).is_some() {
                cons.push(i);
            }
        }
        cons
    }

    pub fn check_sanity(&self) {
        //println!("{} {}",self.values.len(),self.input_n*self.output_n);
        assert!(self.values.len() == self.input_n*self.output_n);
    }
}

// Simple print format for the connection matrix in columns
impl fmt::Display for ConnectionMatrix {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for i in 0..self.input_n {
            for j in 0..self.output_n {
                match self.get_weight(i, j) {
                    Some(x) => write!(f, "\t{}", x)?,
                    _ => write!(f, "\tNone       ")?,
                }
            }
            write!(f, "\n")?;
        }
        Ok(())
    }
}

//
// ConnectionMatrix Iterator related stuff
//

impl ConnectionMatrix {
    /// Iterates over the existing connections providing a
    /// ConnectionView of each
    pub fn iter(&self) -> ConnectionMatrixIter {
        ConnectionMatrixIter {
            connection_matrix: self,
            current: 0,
        }
    }

    pub fn iter_unconnections(&self) -> UnconnectionMatrixIter {
        UnconnectionMatrixIter {
            connection_matrix: self,
            current: 0,
        }
    }

    /// True if the given input node is connected to some other node (active)
    pub fn is_input_node_active(&self, index: usize) -> bool {
        let mut active = false;
        for i in 0..self.output_n {
            if self.get_weight(index, i).is_some() {
                active = true;
                break;
            }
        }
        active
    }

    /// True if the given output node is connected to some other node (active)
    pub fn is_output_node_active(&self, index: usize) -> bool {
        let mut active = false;
        for i in 0..self.input_n {
            if self.get_weight(i, index).is_some() {
                active = true;
                break;
            }
        }
        active
    }

    /// Checks if a given unconnection is associated with some ative node
    /// (active = have at least one connection)
    pub fn is_nodes_active(&self, con: &ConnectionView) -> bool {
        self.is_input_node_active(con.input_node) || self.is_output_node_active(con.output_node)
    }
}

/// Connection matrix iterator
/// To be used by ConnectionMatrix.iter()
pub struct ConnectionMatrixIter<'a> {
    pub connection_matrix: &'a ConnectionMatrix,
    pub current: usize,
}

impl<'a> Iterator for ConnectionMatrixIter<'a> {
    type Item = ConnectionView<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.current < self.connection_matrix.count_total_possible_connections() {
            //let (input_n, output_n) = get_line_col(self.current, self.connection_matrix.output_n);
            self.current += 1;
            match self.connection_matrix.values[self.current - 1].get() {
                Some(_) => Some(ConnectionView::new(
                    self.connection_matrix,
                    self.current - 1,
                )),
                None => self.next(),
            }
        } else {
            None
        }
    }
}

/// Unconnection matrix iterator
/// To be used by UnconnectionMatrix.iter()
pub struct UnconnectionMatrixIter<'a> {
    pub connection_matrix: &'a ConnectionMatrix,
    pub current: usize,
}

/// Allows the use of iterations over the unconnections
impl<'a> Iterator for UnconnectionMatrixIter<'a> {
    type Item = ConnectionView<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.current < self.connection_matrix.count_total_possible_connections() {
            //let (input_n, output_n) = get_line_col(self.current, self.connection_matrix.output_n);
            self.current += 1;
            if let Some(_) = self.connection_matrix.values[self.current - 1].get() {
                self.next()
            } else {
                Some(ConnectionView::new(
                    &self.connection_matrix,
                    self.current - 1,
                ))
            }
        } else {
            None
        }
    }
}

/// Another way to get the value of a connection by line and rol.
/// Just a syntax sugar for the .get_weight method.
// impl Index<(usize, usize)> for ConnectionMatrix {
//     type Output = Option<f32>;

//     fn index(&self, address: (usize,usize)) -> &Self::Output {
//         &self.get_weight(address.0, address.1)
//     }
// }

/// Returns the integer of a division and the remaining
pub fn div_rem<T: std::ops::Div<Output = T> + std::ops::Rem<Output = T> + Copy>(
    x: T,
    y: T,
) -> (T, T) {
    let quot = x / y;
    let rem = x % y;
    (quot, rem)
}

/// If a 2D matrix is stored in a 1D array, this function returns the
/// line and column of a given index and the number o columns the matrix have
pub fn get_line_col(index: usize, columns: usize) -> (usize, usize) {
    let (line, col) = div_rem(index, columns);
    (line, col)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_line_col() {
        let index = get_line_col(100, 12);
        assert_eq!(index, (8, 4));
    }

    #[test]
    fn test_connection_matrix_iterator() {
        let mut connection_matrix = ConnectionMatrix::new(5, 6);
        connection_matrix.set_weight(2, 3, Some(5.));
        // Just the connection above should go through the iteration (skipping the None ones)
        for con in connection_matrix.iter() {
            assert_eq!(con.input_node, 2);
            assert_eq!(con.output_node, 3);
            assert_eq!(con.get_weight(), Some(5.));
        }
        assert_eq!(connection_matrix.values[15].get().unwrap(), 5.);
    }

    #[test]
    fn test_connection_matrix_add_node_input() {
        let mut connection_matrix = ConnectionMatrix::new(3, 4);
        for v in connection_matrix.values.iter_mut() {
            v.set(Some(1.));
        }
        connection_matrix.append_input_node();
        // Testing if the input len was updated correctly
        assert_eq!(connection_matrix.input_n, 4);
        assert_eq!(connection_matrix.values.len(), 4 * 4);
        // Testing the matrix values
        for i in 0..connection_matrix.input_n {
            for j in 0..connection_matrix.output_n {
                if i < 3 {
                    assert_eq!(connection_matrix.get_weight(i, j).unwrap(), 1.);
                } else {
                    assert_eq!(connection_matrix.get_weight(i, j), None);
                }
            }
        }
    }

    #[test]
    fn test_connection_matrix_add_node_output() {
        let mut connection_matrix = ConnectionMatrix::new(3, 4);
        for v in connection_matrix.values.iter_mut() {
            v.set(Some(1.));
        }
        connection_matrix.append_output_node();
        // Testing if the input len was updated correctly
        assert_eq!(connection_matrix.output_n, 5);
        assert_eq!(connection_matrix.values.len(), 3 * 5);
        // Testing the matrix values
        for i in 0..connection_matrix.input_n {
            for j in 0..connection_matrix.output_n {
                if j < 4 {
                    assert_eq!(connection_matrix.get_weight(i, j).unwrap(), 1.);
                } else {
                    assert_eq!(connection_matrix.get_weight(i, j), None);
                }
            }
        }
    }

    #[test]
    fn test_connection_matrix_remove_input_node() {
        //    | O0 | O1 | O2 | O3
        // I0 |  0 |  1 |  2 |  3
        // I1 |  4 |  5 |  6 |  7   <- to be removed
        // I2 |  8 |  9 | 10 | 11
        let mut connection_matrix = ConnectionMatrix::new(3, 4);
        for (i, v) in connection_matrix.values.iter_mut().enumerate() {
            v.set(Some(i as f32));
        }
        connection_matrix.remove_input_node(1);
        // Testing if the input len was updated correctly
        assert_eq!(connection_matrix.input_n, 2);
        assert_eq!(connection_matrix.values.len(), 2 * 4);
        // Testing the matrix values
        assert_eq!(connection_matrix.get_weight(0, 0).unwrap(), 0.);
        assert_eq!(connection_matrix.get_weight(0, 1).unwrap(), 1.);
        assert_eq!(connection_matrix.get_weight(0, 2).unwrap(), 2.);
        assert_eq!(connection_matrix.get_weight(0, 3).unwrap(), 3.);
        assert_eq!(connection_matrix.get_weight(1, 0).unwrap(), 8.);
        assert_eq!(connection_matrix.get_weight(1, 1).unwrap(), 9.);
        assert_eq!(connection_matrix.get_weight(1, 2).unwrap(), 10.);
        assert_eq!(connection_matrix.get_weight(1, 3).unwrap(), 11.);
    }

    #[test]
    fn test_connection_matrix_remove_output_node() {
        //    | O0 | O1 | O2 | O3
        // I0 |  0 |  1 |  2 |  3
        // I1 |  4 |  5 |  6 |  7
        // I2 |  8 |  9 | 10 | 11
        //                ^ to be removed
        let mut connection_matrix = ConnectionMatrix::new(3, 4);
        for (i, v) in connection_matrix.values.iter_mut().enumerate() {
            v.set(Some(i as f32));
        }
        connection_matrix.remove_output_node(2);
        // Testing if the input len was updated correctly
        assert_eq!(connection_matrix.output_n, 3);
        assert_eq!(connection_matrix.values.len(), 3 * 3);
        // Testing the matrix values
        assert_eq!(connection_matrix.get_weight(0, 0).unwrap(), 0.);
        assert_eq!(connection_matrix.get_weight(0, 1).unwrap(), 1.);
        assert_eq!(connection_matrix.get_weight(0, 2).unwrap(), 3.);
        assert_eq!(connection_matrix.get_weight(1, 0).unwrap(), 4.);
        assert_eq!(connection_matrix.get_weight(1, 1).unwrap(), 5.);
        assert_eq!(connection_matrix.get_weight(1, 2).unwrap(), 7.);
        assert_eq!(connection_matrix.get_weight(2, 0).unwrap(), 8.);
        assert_eq!(connection_matrix.get_weight(2, 1).unwrap(), 9.);
        assert_eq!(connection_matrix.get_weight(2, 2).unwrap(), 11.);
    }

    #[test]
    fn test_connection_matrix_insert_input_node() {
        //    | O0 | O1 | O2 | O3
        // I0 |  0 |  1 |  2 |  3
        // I1 |  N |  N |  N |  N  <- To be inserted
        // I2 |  4 |  5 |  6 |  7 
        // I3 |  8 |  9 | 10 | 11
        //                
        let mut connection_matrix = ConnectionMatrix::new(3, 4);
        for (i, v) in connection_matrix.values.iter_mut().enumerate() {
            v.set(Some(i as f32));
        }
        connection_matrix.insert_input_node(1);
        // Testing if the input len was updated correctly
        assert_eq!(connection_matrix.input_n, 4);
        assert_eq!(connection_matrix.values.len(), 4 * 4);
        // Testing the matrix values
        assert_eq!(connection_matrix.get_weight(0, 0).unwrap(), 0.);
        assert_eq!(connection_matrix.get_weight(0, 1).unwrap(), 1.);
        assert_eq!(connection_matrix.get_weight(0, 2).unwrap(), 2.);
        assert_eq!(connection_matrix.get_weight(0, 3).unwrap(), 3.);
        assert_eq!(connection_matrix.get_weight(1, 0), None);
        assert_eq!(connection_matrix.get_weight(1, 1), None);
        assert_eq!(connection_matrix.get_weight(1, 2), None);
        assert_eq!(connection_matrix.get_weight(1, 3), None);
        assert_eq!(connection_matrix.get_weight(2, 0).unwrap(), 4.);
        assert_eq!(connection_matrix.get_weight(2, 1).unwrap(), 5.);
        assert_eq!(connection_matrix.get_weight(2, 2).unwrap(), 6.);
        assert_eq!(connection_matrix.get_weight(2, 3).unwrap(), 7.);
        assert_eq!(connection_matrix.get_weight(3, 0).unwrap(), 8.);
        assert_eq!(connection_matrix.get_weight(3, 1).unwrap(), 9.);
        assert_eq!(connection_matrix.get_weight(3, 2).unwrap(), 10.);
        assert_eq!(connection_matrix.get_weight(3, 3).unwrap(), 11.);
    }

    #[test]
    fn test_connection_matrix_insert_output_node() {
        //    | O0 | O1 | O2 | O3 | O4
        // I0 |  0 |  1 |  2 |  N | 3
        // I1 |  4 |  5 |  6 |  N | 7 
        // I2 |  8 |  9 | 10 |  N | 11
        //                      ^ new output (index = 3)
        let mut connection_matrix = ConnectionMatrix::new(3, 4);
        for (i, v) in connection_matrix.values.iter_mut().enumerate() {
            v.set(Some(i as f32));
        }
        connection_matrix.insert_output_node(3);
        // Testing if the input len was updated correctly
        assert_eq!(connection_matrix.output_n, 5);
        assert_eq!(connection_matrix.input_n, 3);
        assert_eq!(connection_matrix.values.len(), 3 * 5);
        // Testing the matrix values
        assert_eq!(connection_matrix.get_weight(0, 0).unwrap(), 0.);
        assert_eq!(connection_matrix.get_weight(0, 1).unwrap(), 1.);
        assert_eq!(connection_matrix.get_weight(0, 2).unwrap(), 2.);
        assert_eq!(connection_matrix.get_weight(0, 3), None);
        assert_eq!(connection_matrix.get_weight(0, 4).unwrap(), 3.);
        assert_eq!(connection_matrix.get_weight(1, 0).unwrap(), 4.);
        assert_eq!(connection_matrix.get_weight(1, 1).unwrap(), 5.);
        assert_eq!(connection_matrix.get_weight(1, 2).unwrap(), 6.);
        assert_eq!(connection_matrix.get_weight(1, 3), None);
        assert_eq!(connection_matrix.get_weight(1, 4).unwrap(), 7.);
        assert_eq!(connection_matrix.get_weight(2, 0).unwrap(), 8.);
        assert_eq!(connection_matrix.get_weight(2, 1).unwrap(), 9.);
        assert_eq!(connection_matrix.get_weight(2, 2).unwrap(), 10.);
        assert_eq!(connection_matrix.get_weight(2, 3), None);
        assert_eq!(connection_matrix.get_weight(2, 4).unwrap(), 11.);
    }
}
