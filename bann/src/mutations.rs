use std::process::exit;

/// Mutations related stuff
/// First some types are extended with common infrastructure methods
/// to help mutations definitions in the network
/// After that, the mutation are defined implementing in a plug-in style
/// with a defined interface
use crate::connection_matrix::{ConnectionMatrix, ConnectionView, IConnectionMatrix};
use crate::hidden_block::{
    ConnectionMatrixKind, HiddenBlock, HiddenBlockConnection, HiddenBlockConnectionAddress,
    HiddenBlockNode,
};
use crate::network::{Network, NetworkConnection, NetworkNode};
use rand::seq::SliceRandom;
//use rand::prelude::*;

//
// Methods related to the mutations
//

/// Extending the ConnectionMatrix capabilities to handle mutations
impl ConnectionMatrix {
    /// Return a random index of a non connected node pair
    pub fn draw_random_none_element_index(&self) -> usize {
        let indexes = self.list_unconnections_indexes();
        *indexes.choose(&mut rand::thread_rng()).unwrap()
    }

    /// Return a random index of a connected node pair
    pub fn draw_random_connected_element_index(&self) -> usize {
        let indexes = self.list_connections_indexes();
        *indexes.choose(&mut rand::thread_rng()).unwrap()
    }

    /// Change the current values of all matrix weights by a perturbation_factor
    pub fn perturbe_weights(&mut self, perturbation_factor: f32) {
        for weight in self.values.iter_mut() {
            match weight.get() {
                Some(x) => {
                    let increment: f32 = perturbation_factor * ( 2.* rand::random::<f32>() - 1.);
                    weight.set(Some(x + increment));
                }
                None => (),
            }
        }
    }

    /// Change the value of a single random existing connection weight
    pub fn change_single_random_weight(&mut self, perturbation_factor: f32) {
        let random_index = self.draw_random_connected_element_index();
        let increment: f32 = perturbation_factor * (2.* rand::random::<f32>() - 1.);
        self.values[random_index].set(Some(self.values[random_index].get().unwrap() + increment));
    }

    /// Adds a link to a random non connected node pair
    pub fn add_random_connection(&mut self, weight: Option<f32>) {
        let random_index = self.draw_random_none_element_index();
        let value = Some(weight.unwrap_or(2. * rand::random::<f32>() - 1.));
        self.values[random_index].set(value);
    }
}

/// Extending the HiddenBloc capabilities to handle mutations
impl HiddenBlock {
    /// Adds a random connection to the input
    pub fn add_random_connection_input(&mut self) {
        self.input_matrix.add_random_connection(None);
    }

    /// Adds a random connection to the output
    pub fn add_random_connection_output(&mut self) {
        self.output_matrix.add_random_connection(None);
    }

    /// Adds a random connection to the hidden network
    pub fn add_random_connection_hidden(&mut self) {
        self.hidden_matrix.add_random_connection(None);
    }

    /// Random connection from all matrixes
    pub fn draw_random_connection(&mut self) -> Option<HiddenBlockConnection> {
        let list = self.list_connections();
        let chosen = list.choose(&mut rand::thread_rng());
        match chosen {
            Some(x) => Some(*x),
            None => None,
        }
    }

    /// Random unconnection from all matrixes
    pub fn draw_random_unconnection(&mut self) -> Option<HiddenBlockConnection> {
        let list = self.list_unconnections();
        let chosen = list.choose(&mut rand::thread_rng());
        match chosen {
            Some(x) => Some(*x),
            None => None,
        }
    }

    /// Random unconnection with active node from all matrixes with the condition
    /// that the nodes are active
    pub fn draw_random_unconnection_from_active_nodes(&mut self) -> Option<HiddenBlockConnection> {
        let list = self.list_unconnections_from_active_nodes();
        let chosen = list.choose(&mut rand::thread_rng());
        match chosen {
            Some(x) => Some(*x),
            None => None,
        }
    }

    /// Adds a node (breaking a connection) to the hidden network
    pub fn add_node_to_random_connection(&mut self) {
        // FIXME test
        let drawn_connection = self.draw_random_connection();
        match drawn_connection {
            Some(con) => {
                let address = con.into_address();
                self.add_node_to_connection(address);
            }
            None => println!("No connection found to add node. Passing..."),
        }
    }

    /// Adds a node for a given HiddenBlockConnection
    pub fn add_node_to_connection(&mut self, con: HiddenBlockConnectionAddress) {
        match con.kind {
            ConnectionMatrixKind::Input => self.add_node_to_input_connection(con.index),
            ConnectionMatrixKind::Output => self.add_node_to_output_connection(con.index),
            ConnectionMatrixKind::Hidden => self.add_node_to_hidden_connection(con.index),
        }
    }

    ///    Adds a node breaking a given a input connection
    fn add_node_to_input_connection(&mut self, index: usize) {
        let (input_node, output_node) = self.input_matrix.io_nodes_indexes(index);
        let original_weight = self.input_matrix.get_weight_index(index);
        // Deactivating the connection
        self.input_matrix.set_weight_index(index, None);
        // Adding the new hidden node in the 3 matrixes
        self.add_new_node();
        // Creating the two new connections
        let new_node_index = self.input_matrix.output_n - 1;
        self.input_matrix
            .set_weight(input_node, new_node_index, Some(1.));
        self.hidden_matrix
            .set_weight(new_node_index, output_node, original_weight);
    }

    /// Adds a node breaking a given output connection
    pub fn add_node_to_output_connection(&mut self, index: usize) {
        let (input_node, output_node) = self.input_matrix.io_nodes_indexes(index);
        let original_weight = self.input_matrix.get_weight_index(index);
        // Deactivating the connection
        // TODO maybe avoid calling the values directly
        self.output_matrix.values[index].set(None);
        // Adding the new hidden node in the 3 matrixes
        self.add_new_node();
        // Creating the two new connections
        let new_node_index = self.output_matrix.input_n - 1;
        self.hidden_matrix
            .set_weight(input_node, new_node_index, Some(1.));
        self.output_matrix
            .set_weight(new_node_index, output_node, original_weight);
    }

    /// Adds a node breaking a given hidden connection
    pub fn add_node_to_hidden_connection(&mut self, index: usize) {
        let (input_node, output_node) = self.input_matrix.io_nodes_indexes(index);
        let original_weight = self.input_matrix.get_weight_index(index);
        // Deactivating the connection
        self.hidden_matrix.values[index].set(None);
        // Adding the new hidden node in the 3 matrixes
        self.add_new_node();
        // Creating the two new connections
        let new_node_index = self.hidden_matrix.input_n - 1;
        self.hidden_matrix
            .set_weight(input_node, new_node_index, Some(1.));
        self.hidden_matrix
            .set_weight(new_node_index, output_node, original_weight);
    }

    /// Change the current values of all matrix weights by a perturbation_factor
    pub fn perturbe_weights(&mut self, perturbation_factor: f32) {
        self.input_matrix.perturbe_weights(perturbation_factor);
        self.output_matrix.perturbe_weights(perturbation_factor);
        self.hidden_matrix.perturbe_weights(perturbation_factor);
    }

    /// Change the current values of all node biases by a perturbation_factor
    pub fn perturb_biases(&mut self, perturbation_factor: f32) {
        for bias in self.biases.iter_mut() {
            *bias += perturbation_factor * (2.*rand::random::<f32>() - 1.);
        }
    }
}

impl Network {
    /// Apply a list of mutations with the given probabilities
    pub fn mutate(&mut self, mutations_list: &Vec<Box<dyn IMutation>>) {
        for mutation in mutations_list.iter() {
            self.apply_mutation(mutation);
        }
    }

    /// Apply a single mutation to the network
    pub fn apply_mutation(&mut self, mutation: &Box<dyn IMutation>) {
        if rand::random::<f32>() < mutation.get_probability() {
            match mutation.apply(self) {
                Ok(_) => (),
                Err(s) => (),
            };
        }
    }

    /// Draw a random active node with even probability
    pub fn draw_random_active_node(&mut self) -> Option<NetworkNode> {
        let list = self.list_active_nodes();
        let chosen = list.choose(&mut rand::thread_rng());
        match chosen {
            Some(x) => Some(*x),
            None => None,
        }
    }

    /// Draw a random connection with even probability
    pub fn draw_random_connection(&self) -> Option<NetworkConnection> {
        let list = self.list_connections();
        let chosen = list.choose(&mut rand::thread_rng());
        match chosen {
            Some(x) => Some(*x),
            None => None,
        }
    }

    /// Draw a random unconnection with even probability
    pub fn draw_random_unconnection(&self) -> Option<NetworkConnection> {
        let list = self.list_unconnections();
        let chosen = list.choose(&mut rand::thread_rng());
        match chosen {
            Some(x) => Some(*x),
            None => None,
        }
    }

    /// Draw a random unconnection from active nodes with even probability
    pub fn draw_random_unconnection_from_active_nodes(&mut self) -> Option<NetworkConnection> {
        let list = self.list_unconnections_from_active_nodes();
        let chosen = list.choose(&mut rand::thread_rng());
        match chosen {
            Some(x) => Some(*x),
            None => None,
        }
    }

    /// Changes a random unconnection (from all the network) to a connection
    fn add_random_connection(&self, weight: f32) {
        //let chosen_unconnection = self.draw_random_unconnection_from_active_nodes();
        let chosen_unconnection = self.draw_random_unconnection();
        // FIXME log instead of message
        match chosen_unconnection {
            Some(x) => self.set_connection_weight(x, Some(weight)),
            None => println!("No unconnection left to choose, passing..."),
        }
    }

    /// Changes a random connection (from all the network) to a unconnection
    fn remove_random_connection(&self) {
        let chosen_connection = self.draw_random_connection();
        // FIXME log instead of message
        match chosen_connection {
            Some(x) => self.set_connection_weight(x, None),
            None => println!("No connection left to choose, passing..."),
        }
    }
}

/// Interface for mutations to include in the mutations list to be applied
/// in the network
pub trait IMutation {
    fn apply(&self, net: &mut Network) -> Result<(), String>;
    fn get_probability(&self) -> f32;
}

/// Mutation: Perturb all biases
#[derive(Clone, Copy)]
pub struct MutationPerturbBiases {
    probability: f32,
    perturbation_factor: f32,
}

impl MutationPerturbBiases {
    pub fn new(probability: f32, perturbation_factor: f32) -> Self {
        Self {
            probability,
            perturbation_factor,
        }
    }
}

impl IMutation for MutationPerturbBiases {
    fn apply(&self, net: &mut Network) -> Result<(), String> {
        for hb in net.hidden_blocks.iter_mut() {
            hb.perturb_biases(self.perturbation_factor)
        }
        Ok(())
    }

    fn get_probability(&self) -> f32 {
        self.probability
    }
}

/// Mutation: Perturb all weights
#[derive(Clone, Copy)]
pub struct MutationPerturbWeights {
    probability: f32,
    perturbation_factor: f32,
}

impl MutationPerturbWeights {
    pub fn new(probability: f32, perturbation_factor: f32) -> Self {
        Self {
            probability,
            perturbation_factor,
        }
    }
}

impl IMutation for MutationPerturbWeights {
    fn apply(&self, net: &mut Network) -> Result<(), String> {
        for hb in net.hidden_blocks.iter_mut() {
            hb.perturbe_weights(self.perturbation_factor)
        }
        Ok(())
    }

    fn get_probability(&self) -> f32 {
        self.probability
    }
}

/// Mutation:  Add a node to a network
#[derive(Clone, Copy)]
pub struct MutationAddNode {
    probability: f32,
}

impl MutationAddNode {
    pub fn new(probability: f32) -> Self {
        Self { probability }
    }
}

impl IMutation for MutationAddNode {
    fn apply(&self, net: &mut Network) -> Result<(), String> {
        match net.draw_random_connection() {
            Some(con) => {
                let hb_index = con.hidden_block_index;
                net.hidden_blocks[hb_index].add_node_to_random_connection();
            }
            None => (),
        }
        Ok(())
    }

    fn get_probability(&self) -> f32 {
        self.probability
    }
}

/// Mutation: Add connection to a network
#[derive(Clone, Copy)]
pub struct MutationAddConnection {
    probability: f32,
}

impl MutationAddConnection {
    pub fn new(probability: f32) -> Self {
        Self { probability }
    }
}

impl IMutation for MutationAddConnection {
    fn apply(&self, net: &mut Network) -> Result<(), String> {
        net.add_random_connection(rand::random::<f32>() * 2. - 1.);
        Ok(())
    }

    fn get_probability(&self) -> f32 {
        self.probability
    }
}

/// Mutation: Remove connection from a network
#[derive(Clone, Copy)]
pub struct MutationRemoveConnection {
    probability: f32,
}

impl MutationRemoveConnection {
    pub fn new(probability: f32) -> Self {
        Self { probability }
    }
}

impl IMutation for MutationRemoveConnection {
    fn apply(&self, net: &mut Network) -> Result<(), String> {
        net.remove_random_connection();
        Ok(())
    }

    fn get_probability(&self) -> f32 {
        self.probability
    }
}

/// Mutation: change single connection weight
#[derive(Clone, Copy)]
pub struct MutationPerturbSingleWeight {
    probability: f32,
    perturbation_factor: f32,
}

impl MutationPerturbSingleWeight {
    pub fn new(probability: f32, perturbation_factor: f32) -> Self {
        Self {
            probability,
            perturbation_factor,
        }
    }
}

impl IMutation for MutationPerturbSingleWeight {
    fn apply(&self, net: &mut Network) -> Result<(), String> {
        let netcon = net.draw_random_connection();
        match netcon {
            Some(x) => {
                let value = x.get_weight().unwrap()
                    * self.perturbation_factor
                    * (rand::random::<f32>() * 2. - 1.);
                net.set_connection_weight(x, Some(value))
            }
            None => (),
        }
        Ok(())
    }

    fn get_probability(&self) -> f32 {
        self.probability
    }
}

/// Mutation: perturb single node bias
#[derive(Clone, Copy)]
pub struct MutationPerturbSingleBias {
    probability: f32,
    perturbation_factor: f32,
}

impl MutationPerturbSingleBias {
    pub fn new(probability: f32, perturbation_factor: f32) -> Self {
        Self {
            probability,
            perturbation_factor,
        }
    }
}

impl IMutation for MutationPerturbSingleBias {
    // FIXME overcomplicated
    fn apply(&self, net: &mut Network) -> Result<(), String> {
        let netnode = net.draw_random_active_node();
        match netnode {
            Some(x) => {
                if let Some(bias) = x.get_bias() {
                    let value = bias * self.perturbation_factor * (rand::random::<f32>() * 2. - 1.);
                    net.set_node_bias(x, value)
                } else {
                    return Err("Could not set the node bias (main block node?).".to_string());
                }
            }
            None => return Err("Could not draw a random node".to_string()),
        }
        Ok(())
    }

    fn get_probability(&self) -> f32 {
        self.probability
    }
}

/// Mutation: change a single node bias
#[derive(Clone, Copy)]
pub struct MutationChangeSingleBias {
    probability: f32,
}

impl MutationChangeSingleBias {
    pub fn new(probability: f32) -> Self {
        Self { probability }
    }
}

impl IMutation for MutationChangeSingleBias {
    // FIXME overcomplicated
    fn apply(&self, net: &mut Network) -> Result<(), String> {
        let netnode = net.draw_random_active_node();
        match netnode {
            Some(x) => {
                if let Some(_bias) = x.get_bias() {
                    let value = rand::random::<f32>() * 2. - 1.;
                    net.set_node_bias(x, value)
                } else {
                    return Err("Could not set the node bias (main block node?).".to_string());
                }
            }
            None => return Err("Could not draw a random node".to_string()),
        }
        Ok(())
    }

    fn get_probability(&self) -> f32 {
        self.probability
    }
}

/// Mutation:  Add a mix node to a network, that is, a node
/// is inserted in a connection and a "mix" second input is added.
/// The idea is to not disrupt the old behavior totally, but to add
/// a second control to the output.
/// This mutation is done by a fairly complex surgery in the network,
/// I hope the patient survives...
#[derive(Clone, Copy)]
pub struct MutationAddMixNode {
    probability: f32,
}

impl MutationAddMixNode {
    pub fn new(probability: f32) -> Self {
        Self { probability }
    }

    /// Lists the eligible mix nodes for a given network connection
    /// The criteria is:
    ///     a) must be in the inputs or interposer from the network or
    ///        be an hidden node from the given hidden_block.
    ///     b) (afterthought: don't think its really necessary, skipping b)
    ///         Must not be already connected to the output original node.
    fn list_eligible_input_mix_nodes(
        &self,
        net: &Network,
        netcon: NetworkConnection,
    ) -> Vec<HiddenBlockNode> {
        let hdblock = &net.hidden_blocks[netcon.hidden_block_index];
        let eligible_input_mix_nodes = hdblock
            .list_nodes()
            .iter()
            .filter(|node| match node {
                HiddenBlockNode::Input { .. }
                | HiddenBlockNode::Interposer { .. }
                | HiddenBlockNode::Hidden { .. } => true,
                _ => false,
            })
            .map(|x| *x)
            .collect();
        eligible_input_mix_nodes
    }
}

impl IMutation for MutationAddMixNode {
    fn apply(&self, net: &mut Network) -> Result<(), String> {
        let mut randgen = rand::thread_rng();
        // Choosing a connection to insert the mix node
        let chosen_netcon = match net.draw_random_connection() {
            Some(con) => con,
            None => return Err("No connection found.".to_string()),
        };
        // Picking a mix node
        let mix_node_input = match self
            .list_eligible_input_mix_nodes(net, chosen_netcon)
            .choose(&mut randgen)
        {
            Some(x) => *x,
            None => return Err("No eligible mix node found".to_string()),
        };
        // Storing the original weight value
        let original_weight = chosen_netcon.get_weight();
        // Cleaning the original connection
        net.set_connection_weight(chosen_netcon, None);
        // Getting the io nodes from the original connection for easy
        let (original_input_node, original_output_node) = net.hidden_blocks
            [chosen_netcon.hidden_block_index]
            .get_io_nodes(&chosen_netcon.hidden_block_connection);
        // Changing into address to avoid multiple references
        let chosen_netcon = chosen_netcon.into_address();
        // Surfacing the picked hidden block for easier manipulation
        let hdblock = &mut (net.hidden_blocks[chosen_netcon.hidden_block_index]);
        // Choosing the mix node from the hidden non connected nodes
        let list = hdblock.list_disconnected_hidden_nodes();
        let mix_node = match hdblock
            .list_disconnected_hidden_nodes()
            .choose(&mut randgen)
        {
            Some(i) => *i,
            None => return Err("No non connected hidden node found.".to_string()),
        };
        // Picking a random initial small weight to the new mix connection
        let mix_con_weight = Some((rand::random::<f32>() * 2. - 1.) * 0.1);
        // // Making the changes in the connection matrixes
        // Connecting the mix input node to the mix node
        hdblock.set_connection_weight(
            &hdblock.get_connection(mix_node_input, mix_node)?,
            mix_con_weight,
        );
        // Connecting the original input node to the mix node with a unit weight
        hdblock.set_connection_weight(
            &hdblock.get_connection(original_input_node, mix_node)?,
            Some(1.),
        );
        // Connecting the mix node to the original output node
        hdblock.set_connection_weight(
            &hdblock.get_connection(mix_node, original_output_node)?,
            original_weight,
        );

        Ok(())
    }

    fn get_probability(&self) -> f32 {
        self.probability
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::network::NetworkFactory;

    #[test]
    fn test_draw_random_unconnection_from_network() {
        // setup
        let mut network = NetworkFactory::new_test_network();
        let unconnection = network.draw_random_unconnection();
        let a = network.draw_random_unconnection();
        //assert_eq(unconnection.0)
    }
}
