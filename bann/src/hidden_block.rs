use crate::connection_matrix::{ConnectionMatrix, ConnectionView, IConnectionMatrix};
use crate::network::MainBlock;
use serde::{Deserialize, Serialize};

/// A subset of the network (a block) that evolves independently
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct HiddenBlock {
    /// Bias
    pub biases: Vec<f32>,
    /// Number of hidden nodes
    pub hidden_nodes_n: usize,
    /// Matrix containing the connections weights between hidden nodes
    pub hidden_matrix: ConnectionMatrix,
    /// Matrix containing the connections weights between input nodes and hidden ones
    pub input_matrix: ConnectionMatrix,
    /// Matrix containing the connections weights between hidden nodes and output ones
    pub output_matrix: ConnectionMatrix,
    /// Innovation number
    pub innov: u32,
    /// Main block data for internal use.
    main_block: MainBlock,
}

#[derive(Debug, Clone, Copy)]
pub enum ConnectionMatrixKind {
    Input,
    Output,
    Hidden,
}

/// Address to a connection without references. To be used when the
/// hidden blocks structure are going to be changed (adding a node, for
/// example) and the indexes may shift. For this reason, once the change
/// was applied the address, and any other address in scope, can no
/// longer be trusted.
pub struct HiddenBlockConnectionAddress {
    pub kind: ConnectionMatrixKind,
    pub index: usize,
}
/// Hiddenblock's view to a connection
#[derive(Debug, Clone, Copy)]
pub struct HiddenBlockConnection<'a> {
    pub kind: ConnectionMatrixKind,
    pub connection: ConnectionView<'a>,
}

impl<'a> HiddenBlockConnection<'a> {
    /// Consumes the connection and returns a non referenced address
    /// to the connection, to be used in the matrixes structure changing
    /// methods.
    pub fn into_address(self) -> HiddenBlockConnectionAddress {
        HiddenBlockConnectionAddress {
            kind: self.kind,
            index: self.connection.index,
        }
    }
}

/// Kinds of nodes are accessible in a hidden block
#[derive(Debug, Clone, Copy)]
pub enum HiddenBlockNode {
    Input { index: usize },
    Output { index: usize },
    Hidden { index: usize, bias: f32 },
    Interposer { index: usize },
}

/// Hiddenblock's view to a node
// #[derive(Debug, Clone, Copy)]
// pub struct HiddenBlockNode {
//     pub kind: HiddenBlockNodeKind,
//     pub index: usize,
//     pub bias: f32,
// }

impl HiddenBlock {
    pub fn new(hidden_nodes_n: usize, main_block: MainBlock, innov: u32) -> Self {
        Self {
            //nodes_values: vec![0.; n_hidden_nodes],
            hidden_nodes_n: hidden_nodes_n,
            biases: vec![0.; hidden_nodes_n],
            hidden_matrix: ConnectionMatrix::new(hidden_nodes_n, hidden_nodes_n),
            input_matrix: ConnectionMatrix::new(
                (main_block.inputs + main_block.interposers) as usize,
                hidden_nodes_n,
            ),
            output_matrix: ConnectionMatrix::new(
                hidden_nodes_n,
                (main_block.interposers + main_block.outputs) as usize,
            ),
            innov,
            main_block,
        }
    }

    /// Adds an extra node to the hidden block
    pub fn add_new_node(&mut self) {
        // Making entries in all matrixes
        self.input_matrix.append_output_node();
        self.output_matrix.append_input_node();
        self.hidden_matrix.append_input_node();
        self.hidden_matrix.append_output_node();
        // Increasing hidden node count
        self.hidden_nodes_n += 1;
        // Adds the bias
        self.biases.push(0.);
    }

    /// Removes a node from the hidden block
    pub fn remove_node(&mut self, index: usize) {
        // Removing entries in all matrixes
        self.input_matrix.remove_output_node(index);
        self.output_matrix.remove_input_node(index);
        self.hidden_matrix.remove_input_node(index);
        self.hidden_matrix.remove_output_node(index);
        // decreasing hidden node count
        self.hidden_nodes_n -= 1;
        // Adds the bias
        self.biases.remove(index);
    }

    /// Adds an input to the block
    pub fn append_input(&mut self) {
        self.input_matrix.insert_input_node(self.main_block.inputs as usize);
        self.main_block.inputs += 1;
    }

    /// Adds an input to the block in a selected index shifting all others
    pub fn insert_input(&mut self, index: usize) {
        self.input_matrix.insert_input_node(index);
        self.main_block.inputs += 1;
    }

    /// Removes a given input node
    pub fn remove_input(&mut self, index: usize) {
        self.input_matrix.remove_input_node(index);
        self.main_block.inputs -= 1;
    }

    /// Adds an output to the block
    pub fn append_output(&mut self) {
        self.output_matrix.insert_output_node(self.main_block.outputs as usize);
        self.main_block.outputs += 1;
    }

    /// Adds an output to the block in a selected index shifting all others
    pub fn insert_output(&mut self, index: usize) {
        self.output_matrix.insert_output_node(index);
        self.main_block.outputs += 1;
    }

    /// Removes a given output node
    pub fn remove_output(&mut self, index: usize) {
        self.output_matrix.remove_output_node(index);
        self.main_block.outputs -= 1;
    }


    /// Lists all connection from the hidden block
    pub fn list_connections(&self) -> Vec<HiddenBlockConnection> {
        let mut list = Vec::new();
        for con in self.input_matrix.iter() {
            list.push(HiddenBlockConnection {
                kind: ConnectionMatrixKind::Input,
                connection: con,
            });
        }
        for con in self.output_matrix.iter() {
            list.push(HiddenBlockConnection {
                kind: ConnectionMatrixKind::Output,
                connection: con,
            });
        }
        for con in self.hidden_matrix.iter() {
            list.push(HiddenBlockConnection {
                kind: ConnectionMatrixKind::Hidden,
                connection: con,
            });
        }
        list
    }

    /// Lists all unconnection from the hidden block
    pub fn list_unconnections(&self) -> Vec<HiddenBlockConnection> {
        let mut list = Vec::new();
        for con in self.input_matrix.iter_unconnections() {
            list.push(HiddenBlockConnection {
                kind: ConnectionMatrixKind::Input,
                connection: con,
            });
        }
        for con in self.output_matrix.iter_unconnections() {
            list.push(HiddenBlockConnection {
                kind: ConnectionMatrixKind::Output,
                connection: con,
            });
        }
        for con in self.hidden_matrix.iter_unconnections() {
            list.push(HiddenBlockConnection {
                kind: ConnectionMatrixKind::Hidden,
                connection: con,
            });
        }
        list
    }

    /// Lists all unconnection from the hidden block that are
    /// related with active hidden nodes
    pub fn list_unconnections_from_active_nodes(&self) -> Vec<HiddenBlockConnection> {
        let mut list = Vec::new();
        for con in self
            .input_matrix
            .iter_unconnections()
            .filter(|con| self.input_matrix.is_output_node_active(con.index))
        {
            list.push(HiddenBlockConnection {
                kind: ConnectionMatrixKind::Input,
                connection: con,
            });
        }
        for con in self
            .output_matrix
            .iter_unconnections()
            .filter(|con| self.output_matrix.is_input_node_active(con.index))
        {
            list.push(HiddenBlockConnection {
                kind: ConnectionMatrixKind::Output,
                connection: con,
            });
        }
        for con in self
            .hidden_matrix
            .iter_unconnections()
            .filter(|con| self.hidden_matrix.is_nodes_active(con))
        {
            list.push(HiddenBlockConnection {
                kind: ConnectionMatrixKind::Hidden,
                connection: con,
            });
        }
        list
    }

    /// Lists all nodes that are related to the hidden block
    /// (inputs, interposers, hidden from block and output)
    pub fn list_nodes(&self) -> Vec<HiddenBlockNode> {
        let mut node_list = Vec::new();
        node_list.extend(
            (0..self.main_block.inputs).map(|index| HiddenBlockNode::Input {
                index: index as usize,
            }),
        );
        node_list.extend(
            (self.main_block.inputs..self.main_block.inputs + self.main_block.interposers).map(
                |index| HiddenBlockNode::Interposer {
                    index: index as usize,
                },
            ),
        );
        node_list.extend(
            (0..self.hidden_nodes_n).map(|index| HiddenBlockNode::new_hidden(&self, index)),
        );
        node_list.extend(
            (0..self.main_block.outputs).map(|index| HiddenBlockNode::Output {
                index: index as usize,
            }),
        );
        node_list
    }

    /// Changes a particular connection weight
    pub fn set_connection_weight(&self, hbcon: &HiddenBlockConnection, value: Option<f32>) {
        use ConnectionMatrixKind::*;
        match hbcon.kind {
            Input => self.input_matrix.values[hbcon.connection.index].set(value),
            Output => self.output_matrix.values[hbcon.connection.index].set(value),
            Hidden => self.hidden_matrix.values[hbcon.connection.index].set(value),
        }
    }

    /// Get a connection given by a pair of nodes
    pub fn get_connection(
        &self,
        in_block_node: HiddenBlockNode,
        out_block_node: HiddenBlockNode,
    ) -> Result<HiddenBlockConnection, &str> {
        use HiddenBlockNode::*;
        let hbcon = match (in_block_node, out_block_node) {
            (
                Input { index: in_node } | Interposer { index: in_node },
                Hidden {
                    index: out_node, ..
                },
            ) => HiddenBlockConnection {
                kind: ConnectionMatrixKind::Input,
                connection: self
                    .input_matrix
                    .connection_from_nodes_indexes(in_node, out_node),
            },
            (
                Hidden { index: in_node, .. },
                Hidden {
                    index: out_node, ..
                },
            ) => HiddenBlockConnection {
                kind: ConnectionMatrixKind::Hidden,
                connection: self
                    .hidden_matrix
                    .connection_from_nodes_indexes(in_node, out_node),
            },
            (
                Hidden { index: in_node, .. },
                Output { index: out_node } | Interposer { index: out_node },
            ) => HiddenBlockConnection {
                kind: ConnectionMatrixKind::Output,
                connection: self
                    .output_matrix
                    .connection_from_nodes_indexes(in_node, out_node),
            },
            _ => return Err("Pair of nodes cannot be connected"),
        };
        Ok(hbcon)
    }

    /// Get the pair of connected nodes by the given connection
    pub fn get_io_nodes(&self, con: &HiddenBlockConnection) -> (HiddenBlockNode, HiddenBlockNode) {
        // FIXME method too messy
        match con.kind {
            ConnectionMatrixKind::Hidden => (
                HiddenBlockNode::new_hidden(self, con.connection.input_node),
                HiddenBlockNode::new_hidden(self, con.connection.output_node),
            ),
            ConnectionMatrixKind::Input => {
                let input_node = match con.connection.input_node {
                    x if x < self.main_block.inputs as usize => HiddenBlockNode::Input {
                        index: con.connection.input_node,
                    },
                    _ => HiddenBlockNode::Interposer {
                        index: con.connection.input_node,
                    },
                };
                (
                    input_node,
                    HiddenBlockNode::new_hidden(self, con.connection.output_node),
                )
            }
            ConnectionMatrixKind::Output => {
                let conview = &con.connection;
                let output_node = match conview.input_node {
                    x if x < self.main_block.outputs as usize => HiddenBlockNode::Output {
                        index: conview.output_node,
                    },
                    _ => HiddenBlockNode::Interposer {
                        index: conview.output_node,
                    },
                };
                (
                    HiddenBlockNode::new_hidden(self, conview.input_node),
                    output_node,
                )
            }
        }
    }

    /// Renew a connection build it up again from the address in the old given.
    pub fn renew_connection(&self, hbcon: HiddenBlockConnection) -> HiddenBlockConnection {
        use ConnectionMatrixKind::*;
        let old_con = &hbcon.connection;
        let new_con = match hbcon.kind {
            Input => self.input_matrix.get_connection_index(old_con.index),
            Output => self.output_matrix.get_connection_index(old_con.index),
            Hidden => self.hidden_matrix.get_connection_index(old_con.index),
        };
        HiddenBlockConnection {
            kind: hbcon.kind,
            connection: new_con,
        }
    }

    /// Lists disconnected hidden nodes
    pub fn list_disconnected_hidden_nodes(&self) -> Vec<HiddenBlockNode> {
        let mut mask = vec![false; self.hidden_nodes_n];
        // checking connections between hidden nodes
        for i in 0..self.hidden_matrix.input_n {
            for j in 0..self.hidden_matrix.output_n {
                if self.hidden_matrix.get_weight(i, j).is_some() {
                    mask[i] = true;
                    mask[j] = true;
                }
            }
        }
        // checking connections coming from the input + interposer
        for i in 0..self.input_matrix.input_n {
            for j in 0..self.input_matrix.output_n {
                if self.input_matrix.get_weight(i, j).is_some() {
                    mask[j] = true;
                }
            }
        }
        // checking connections going to output + interposer
        for i in 0..self.output_matrix.input_n {
            for j in 0..self.output_matrix.output_n {
                if self.output_matrix.get_weight(i, j).is_some() {
                    mask[i] = true;
                }
            }
        }
        // creating node views list
        mask.iter()
            .enumerate()
            .filter_map(|x| match x {
                (i, false) => Some(HiddenBlockNode::new_hidden(self, i)),
                (_, true) => None,
            })
            .collect()
    }

    /// Checks if a given node is active (connected with some other)
    pub fn is_node_active(&self, index: usize) -> bool {
        self.hidden_matrix.is_input_node_active(index)
            //|| self.input_matrix.is_output_node_active(index)
            || self.output_matrix.is_input_node_active(index)
    }

    /// Removes connections that does not contribute to the output:
    /// connections to a node that has no output connection (inactive node)
    /// Caution: although this function does not change the output, it changes
    /// the future evolution of the hidden block.
    pub fn simplify(&mut self) {
        let connected_mask = self.list_nodes_with_path_to_output();
        for node_index in (0..self.hidden_nodes_n).rev() {
            if !connected_mask[node_index] {
                self.remove_node(node_index);
            }
        }
    }
    // pub fn simplify(&mut self) {
    //     for node_index in (0..self.hidden_nodes_n).rev() {
    //         if !self.is_node_active(node_index) {
    //             self.remove_node(node_index);
    //         }
    //     }
    // }

    // fn get_connection_matrix(&mut self, hbcon: &HiddenBlockConnection) -> &mut ConnectionMatrix {
    //     match hbcon {
    //         HiddenBlockConnection::Input(_) => &mut self.input_matrix,
    //         HiddenBlockConnection::Output => &mut self.output_matrix,
    //         HiddenBlockConnection::Hidden => &mut self.hidden_matrix,
    //     }
    // }

    /// Lists (in a bool mask type list) which nodes have some connection
    /// to the output.
    fn list_nodes_with_path_to_output(&self) -> Vec<bool> {
        //let mut mask_connected_to_output = vec![false; self.hidden_nodes_n];
        let mut list_connected_to_output = Vec::new();
        // Starting at the output matrix and marking all nodes directly connected
        // to the output
        // FIXME Write in a more idiomatic form or using recursion. Being lazy now...
        for i in 0..self.output_matrix.input_n {
            for j in 0..self.output_matrix.output_n {
                match self.output_matrix.get_weight(i, j) {
                    Some(_) => {
                        //mask_connected_to_output[i] = true;
                        list_connected_to_output.push(i);
                        break;
                    }
                    None => (),
                }
            }
        }

        self.hidden_matrix
            .mask_upstream_connected_nodes(&list_connected_to_output)
    }

    pub fn check_sanity(&self) {
        self.input_matrix.check_sanity();
        self.output_matrix.check_sanity();
        self.hidden_matrix.check_sanity();
        assert!(self.biases.len() == self.hidden_matrix.input_n);
        assert!(self.hidden_nodes_n == self.hidden_matrix.input_n);

    }
}

impl<'a> HiddenBlockConnection<'a> {
    pub fn get_weight(&self) -> Option<f32> {
        self.connection.get_weight()
    }
}

impl HiddenBlockNode {
    pub fn new_hidden(hb: &HiddenBlock, index: usize) -> Self {
        Self::Hidden {
            index,
            bias: hb.biases[index],
        }
    }

    pub fn get_bias(&self) -> Option<f32> {
        match self {
            Self::Hidden { bias, .. } => Some(*bias),
            _ => None,
        }
    }
}
