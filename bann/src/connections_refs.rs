/// This is a chunk of incomplete code that I don't have time to mess around using RefCell to
/// provide a connection "pointer" that allows a change in the connection values.

/// Connection interface to access and change its weight
/// Connections can be access in 3 levels: Connection matrix, hidden blocks and the network level.
/// To unify its weight access behavior the IConnection trait and the Connection structures are
/// defined here.
pub trait IConnection {
    /// Get the weight
    fn get_weight(&self) -> Option<f32>;
    /// Set the weight
    fn set_weight(&self, weight: Option<f32>);
}

/// Experimenting 
pub struct Connection<'a> {
    connection_matrix: &'a RefCell<ConnectionMatrix>,
    pub input_node: usize,
    pub output_node: usize,
    pub index: usize,
}

impl<'a> IConnection for Connection<'a> {
    fn get_weight(&self) -> Option<f32> {

        self.connection_matrix.borrow().values[self.index]
    }

    fn set_weight(&self, weight: Option<f32>) {
        self.connection_matrix.borrow_mut().values[self.index] = weight;
    }
}

impl<'a> Connection<'a> {
    fn new(connection_matrix: &'a RefCell<ConnectionMatrix>, index: usize) -> Self {
        let (input_n, output_n) = get_line_col(index, connection_matrix.output_n);
        Connection {
            input_node: input_n,
            output_node: output_n,
            connection_matrix: RefCell::new(connection_matrix),
            index,
        }
    }
}
