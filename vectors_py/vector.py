from math import sqrt, pi, atan2, cos, sin
from random import random


class Vector:
    """
    Simple 2D vectors.
    """

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __add__(self, vec2):
        return Vector(self.x + vec2.x, self.y + vec2.y)

    def __neg__(self):
        return Vector(-self.x, -self.y)

    def __iadd__(self, vector2):
        """
        Adição com a sintaxe +=
        """
        return self + vector2

    def __isub__(self, vector2):
        return self - vector2

    def __sub__(self, vec2):
        return Vector(self.x - vec2.x, self.y - vec2.y)

    def __mul__(self, value):
        return Vector(self.x * value, self.y * value)

    def __rmul__(self, value):
        return Vector(self.x * value, self.y * value)

    def __truediv__(self, value):
        return Vector(self.x / value, self.y / value)

    def __str__(self):
        return f"({self.x},{self.y})"

    def __eq__(self, other):
        return (self.x == other.x) and (self.y == other.y)

    def __getitem__(self, index):
        if index == 0:
            return self.x
        elif index == 1:
            return self.y
        else:
            raise IndexError
    
    def __repr__(self):
        return 'x={}, y={}'.format(self.x, self.y)

    @property
    def mod(self):
        """
        Módulo do vetor.
        """
        return sqrt(self.x**2 + self.y**2)

    @property
    def mod_squared(self):
        """
        Módulo quadrado do vetor.
        """
        return self.x**2 + self.y**2

    @property
    def angle(self):
        """
        Ângulo entre o vetor e a direção X.
        """
        return atan2(self.y, self.x)

    @property
    def versor(self):
        """
        Versor associado.
        """
        return (1 / self.mod) * self

    def dot(self, vec2):
        """
        Produto escalar entre dois vetores.
        """
        return self.x * vec2.x + self.y * vec2.y

    def rotate(self, theta):
        """
        Rotaciona o vetor.
        """
        cos_theta = cos(theta)
        sin_theta = sin(theta)
        x_trans = cos_theta * self.x - sin_theta * self.y
        y_trans = sin_theta * self.x + cos_theta * self.y
        return self.new(x_trans, y_trans)

    @classmethod
    def new(cls, x, y):
        """
        Cria uma nova instância de Vetor ou classe derivada.
        """
        return cls(x, y)

    @classmethod
    def random_versor(cls):
        """
        Cria uma nova instância de Vetor ou classe derivada.
        """
        theta = random() * 2 * pi
        return cls(cos(theta), sin(theta))
        
    @classmethod
    def zero(cls):
        """
        Cria um vetor nulo 
        """
        return cls(0,0)

if __name__ == "__main__":
    m = Vector(-12, 3)
    l = Vector(3, 5)
    print(m.mod)
    print(m.angle)
    print(m + l)
    print(10 * (m + l))
