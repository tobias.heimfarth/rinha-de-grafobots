use crate::Network;
use dot_writer::{Color, DotWriter, Attributes, Shape, Style, NodeId};
use std::fs;

pub fn aann_to_dot(aann_network: &Network, file_name: &str) {
    let mut output_bytes = Vec::new();
    let input_size = aann_network.in_nodes.len();
    let output_size = aann_network.out_nodes.len();
    let hidden_size = aann_network.hidden_nodes.len();
    {
        let mut writer = DotWriter::from(&mut output_bytes);
        writer.set_pretty_print(false);
        let mut digraph = writer.digraph();
        {
            let mut cluster = digraph.cluster();
            cluster.set_style(Style::Filled);
            cluster.set_color(Color::LightGrey);
            cluster.node_attributes()
                .set_style(Style::Filled)
                .set_color(Color::White);
            for i in aann_network.in_nodes.iter() {
                let id = NodeId::from(format!("{}",i));
                cluster.node_named(id);
            }
            cluster.set_label("Input layer");
        }
        {
            let mut cluster = digraph.cluster();
            cluster.set_style(Style::Unfilled);
            cluster.set_color(Color::Blue);
            cluster.node_attributes()
                .set_style(Style::Unfilled)
                .set_color(Color::Blue);
            for i in aann_network.out_nodes.iter() {
                let id = NodeId::from(format!("{}",i));
                cluster.node_named(id);
            }
            cluster.set_label("Output layer");
        }
        {
            let mut cluster = digraph.cluster();
            cluster.set_style(Style::Filled);
            cluster.set_color(Color::Red);
            cluster.set_rank_direction(dot_writer::RankDirection::TopBottom);
            cluster.node_attributes()
                .set_style(Style::Filled)
                .set_color(Color::White);
                for i in aann_network.hidden_nodes.iter() {
                let id = NodeId::from(format!("{}",i));
                cluster.node_named(id);
            }
            cluster.set_label("Hidden layer");
        }
        // Links
        {
            for link in aann_network.links.iter() {
                let id_in = NodeId::from(format!("{}",link.in_node_index));
                let id_out = NodeId::from(format!("{}",link.out_node_index));
                digraph.edge(id_in,id_out).attributes().set_label(&format!("{}",link.weight));
            };
        }

    }
    let data = String::from_utf8(output_bytes).unwrap();
    fs::write(file_name, data).expect("Unable to write file");
}

