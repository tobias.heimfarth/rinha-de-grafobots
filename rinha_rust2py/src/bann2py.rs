// Bann binding to python
use crate::aann2py;
use bann::breeding::{GeneticSimilarity, SimilarityPoints};
use bann::mutations::{
    MutationAddConnection, MutationAddMixNode, MutationAddNode, MutationPerturbBiases,
    MutationPerturbSingleBias, MutationPerturbSingleWeight, MutationPerturbWeights,
    MutationRemoveConnection, MutationChangeSingleBias
};
use pyo3::prelude::*;

#[pyclass]
#[derive(Clone)]
pub struct PyBannNetwork {
    pub network: bann::network::Network,
}

impl PyBannNetwork {
    fn mutate(&mut self, mutation: Box<dyn bann::mutations::IMutation>) {
        self.network.apply_mutation(&mutation);
    }
}

#[pymethods]
impl PyBannNetwork {
    pub fn to_pyaann(&self) -> aann2py::PyNetwork {
        aann2py::PyNetwork {
            network: self.network.to_aann(),
        }
    }

    // IO nodes managers pass through methods

    pub fn append_input_node(&mut self) {
        self.network.append_input_node()
    }

    pub fn insert_input_node(&mut self, index: usize) {
        self.network.insert_input_node(index)
    }

    pub fn remove_input_node(&mut self, index: usize) {
        self.network.remove_input_node(index)
    }

    pub fn append_output_node(&mut self) {
        self.network.append_output_node()
    }

    pub fn insert_output_node(&mut self, index: usize) {
        self.network.insert_output_node(index)
    }

    pub fn remove_output_node(&mut self, index: usize) {
        self.network.remove_output_node(index)
    }

    // Mutations methods

    pub fn perturbe_weights(&mut self, probability: f32, perturbation_factor: f32) {
        let mutation = MutationPerturbWeights::new(probability, perturbation_factor);
        self.mutate(Box::new(mutation));
    }

    pub fn add_connection(&mut self, probability: f32) {
        let mutation = MutationAddConnection::new(probability);
        self.mutate(Box::new(mutation));
    }

    pub fn remove_connection(&mut self, probability: f32) {
        let mutation = MutationRemoveConnection::new(probability);
        self.mutate(Box::new(mutation));
    }

    pub fn add_node(&mut self, probability: f32) {
        let mutation = MutationAddNode::new(probability);
        self.mutate(Box::new(mutation));
    }

    pub fn add_mix_node(&mut self, probability: f32) {
        let mutation = MutationAddMixNode::new(probability);
        self.mutate(Box::new(mutation));
    }

    pub fn perturbe_biases(&mut self, probability: f32, perturbation_factor: f32) {
        let mutation = MutationPerturbBiases::new(probability, perturbation_factor);
        self.mutate(Box::new(mutation));
    }

    pub fn change_single_bias(&mut self, probability: f32) {
        let mutation = MutationChangeSingleBias::new(probability);
        self.mutate(Box::new(mutation));
    }

    pub fn perturbe_single_weight(&mut self, probability: f32, perturbation_factor: f32) {
        let mutation = MutationPerturbSingleWeight::new(probability, perturbation_factor);
        self.mutate(Box::new(mutation));
    }

    pub fn perturbe_single_bias(&mut self, probability: f32, perturbation_factor: f32) {
        let mutation = MutationPerturbSingleBias::new(probability, perturbation_factor);
        self.mutate(Box::new(mutation));
    }

    pub fn save_file(&self, file_name: &str) {
        self.network.save_file(file_name);
    }

    pub fn to_dot(&self, file_name: &str) {
        bann::to_dot::bann_to_dot(&self.network, file_name);
    }

    pub fn clone(&self) -> Self {
        Self {
            network: self.network.clone(),
        }
    }

    pub fn simplify(mut slf: PyRefMut<Self>) -> PyRefMut<Self> {
        slf.network.simplify();
        slf
    }

    /// Reports the number of input, outputs, total number of nodes and links
    pub fn count_components(&self) -> (u32, u32, u32, u32) {
        (
            self.network.main_block.inputs,
            self.network.main_block.outputs,
            self.network.main_block.interposers
                + self.network.main_block.inputs
                + self.network.main_block.outputs
                + self.network.list_active_nodes().len() as u32,
            self.network.list_connections().len() as u32,
        )
    }

    /// Reports the innovs numbers from each hidden block
    pub fn list_innovs(&self) -> Vec<u32> {
        let innovs = self.network.hidden_blocks.iter().map(|x| x.innov).collect();
        return innovs;
    }

    /// Reports the main block shape
    pub fn get_main_block_shape(&self) -> (u32, u32, u32) {
        (
            self.network.main_block.inputs,
            self.network.main_block.outputs,
            self.network.main_block.interposers,
        )
    }

    #[staticmethod]
    pub fn mate(net1: &PyBannNetwork, net2: &PyBannNetwork) -> PyBannNetwork {
        Self {
            network: bann::breeding::mate_networks(&net1.network, &net2.network),
        }
    }

    #[staticmethod]
    pub fn get_similarity(net1: &PyBannNetwork, net2: &PyBannNetwork) -> (f32, i32) {
        use GeneticSimilarity::*;
        match bann::breeding::get_similarity(&net1.network, &net2.network) {
            Compatible(sim) => (sim.points, sim.total),
            Incompatible => (0.0, 0),
        }
    }
}

#[pyclass]
//#[derive(Clone)]
pub struct PyBannNetworkFactory {
    main_block_size: (usize, usize, usize),
    hidden_blocks_size: (usize, usize),
    innov: u32,
}

#[pymethods]
impl PyBannNetworkFactory {
    #[new]
    pub fn new(inputs: usize, outputs: usize, interposers: usize) -> Self {
        Self {
            main_block_size: (inputs, outputs, interposers),
            hidden_blocks_size: (0, 0),
            innov: 0,
        }
    }

    pub fn set_hidden_blocks(mut slf: PyRefMut<Self>, count: usize, size: usize) -> PyRefMut<Self> {
        slf.hidden_blocks_size = (count, size);
        slf
    }

    pub fn set_innov_start(mut slf: PyRefMut<Self>, innov: u32) -> PyRefMut<Self> {
        slf.innov = innov;
        slf
    }

    pub fn build(&mut self) -> PyBannNetwork {
        let net_fac = bann::network::NetworkFactory::new(
            self.main_block_size.0,
            self.main_block_size.1,
            self.main_block_size.2,
        );
        PyBannNetwork {
            network: net_fac
                .set_innov_start(self.innov)
                .set_hidden_blocks(self.hidden_blocks_size.0, self.hidden_blocks_size.1)
                .build(),
        }
    }

    #[staticmethod]
    pub fn load_file(file_name: &str) -> PyBannNetwork {
        PyBannNetwork {
            network: bann::network::NetworkFactory::load_file(file_name),
        }
    }
}
