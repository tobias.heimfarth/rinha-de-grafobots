use pyo3::prelude::*;
use rusty_rinha::bot_blueprints::{load_bot, BotBlueprint, LinkBlueprint, NodeBlueprint};
use rusty_rinha::constants::NODE_TYPES;

use vectors::Vector;

use crate::{BotDeliver, MutationPerturbLinksLenghts};

#[pyclass]
#[derive(Clone)]
/// Stores and manages the Bot blueprints
pub struct PyBotBlueprint {
    bot_blueprint: BotBlueprint,
}

impl PyBotBlueprint {
    fn mutate(&mut self, mutation: Box<dyn rusty_rinha::mutations::IMutation>) {
        self.bot_blueprint.apply_mutation(&mutation);
    }
}

#[pymethods]
impl PyBotBlueprint {
    #[new]
    pub fn new() -> Self {
        Self {
            bot_blueprint: BotBlueprint::new_empty(),
        }
    }

    pub fn add_node(
        &mut self,
        pos: (f32, f32),
        mass: f32,
        radius: f32,
        theta: f32,
        kind: String,
    ) -> usize {
        self.bot_blueprint.nodes.push(NodeBlueprint {
            pos: Vector::new(pos.0, pos.1),
            mass,
            radius,
            theta,
            kind: NODE_TYPES[&kind],
        });
        self.bot_blueprint.nodes.len()
    }

    pub fn add_link(
        &mut self,
        node_a_index: usize,
        node_a_fixation: bool,
        node_b_index: usize,
        node_b_fixation: bool,
    ) {
        self.bot_blueprint.links.push(LinkBlueprint {
            node_a_index,
            node_a_fixation,
            node_b_index,
            node_b_fixation,
        })
    }

    pub fn gen_bot(&self) -> BotDeliver {
        BotDeliver::new(self.bot_blueprint.gen_bot())
    }

    pub fn perturb_links_lenghts(&mut self, probability: f32, perturbation_factor: f32) {
        let mutation = MutationPerturbLinksLenghts::new(probability, perturbation_factor);
        self.mutate(Box::new(mutation));
    }

    pub fn copy(&self) -> Self {
        self.clone()
    }

    /// Saves the blueprint in json format
    pub fn save_file(&self, file_name: &str) {
        self.bot_blueprint.save_file(file_name);
    }

    /// Count bot blueprint components
    /// (input nodes, output nodes, total nodes, total links)
    pub fn count_components(&self) -> (u32, u32, u32, u32) {
        self.bot_blueprint.count_components()
    }

    #[staticmethod]
    pub fn load_file(file_name: &str) -> Self {
        Self {
            bot_blueprint: BotBlueprint::load_file(file_name),
        }
    }

    #[staticmethod]
    pub fn new_from_file(filename: &str) -> Self {
        Self {
            bot_blueprint: load_bot(filename),
        }
    }
}
