// Rinha de Grafobots python binding (not all functionalities are exposed).
use rayon::prelude::*;

use pyo3::prelude::*;
use pyo3::wrap_pyfunction;

use rusty_rinha::bot::{check_for_interference, total_links_length, Bot};
use rusty_rinha::constants::{GameParameters, GAME_PARAMETERS};
use rusty_rinha::mutations::MutationPerturbLinksLenghts;
use rusty_rinha::rinha::{GameState, MatchFingerprint};

mod aann2py;
use aann2py::PyNetwork;

mod bann2py;
use bann2py::{PyBannNetwork, PyBannNetworkFactory};

mod blueprints;
use blueprints::PyBotBlueprint;

#[pymodule]
fn librinha_rust2py(_py: Python, m: &PyModule) -> PyResult<()> {
    // PyO3 aware function. All of our Python interfaces could be declared in a separate module.
    // Note that the `#[pyfn()]` annotation automatically converts the arguments from
    // Python objects to Rust values, and the Rust return value back into a Python object.
    // The `_py` argument represents that we're holding the GIL.
    m.add_class::<RinhaDeGrafobots>()?;
    m.add_class::<BotDeliver>()?;
    m.add_class::<PyNetwork>()?;
    m.add_class::<PyBannNetwork>()?;
    m.add_class::<PyBannNetworkFactory>()?;
    m.add_class::<Parameters>()?;
    m.add_class::<RinhaAANNBundle>()?;
    m.add_class::<RinhaResults>()?;
    m.add_class::<PyBotBlueprint>()?;
    m.add_class::<PyMatchFingerprint>()?;
    m.add_function(wrap_pyfunction!(parallel_rinha, m)?)
        .unwrap();
    Ok(())
}

#[pyclass]
pub struct PyNode {
    #[pyo3(get)]
    pub pos: (f32, f32),
    #[pyo3(get)]
    pub radius: f32,
    #[pyo3(get)]
    pub theta: f32,
    #[pyo3(get)]
    pub sense_sight_endpoint: Option<(f32, f32)>,
    #[pyo3(get)]
    kind_id: char,
    #[pyo3(get)]
    energized: bool,
    #[pyo3(get)]
    index: usize,
    #[pyo3(get)]
    output_value: Option<f32>,
    #[pyo3(get)]
    input_value: Option<f32>,
}

fn bot_pynodes(bot: &Bot) -> Vec<PyNode> {
    let mut pynodes = Vec::new();
    for (i, node) in bot.nodes.iter().enumerate() {
        pynodes.push(PyNode {
            pos: (node.pos.x, node.pos.y),
            radius: node.radius,
            theta: node.theta,
            sense_sight_endpoint: {
                match node.sense_sight_endpoint() {
                    Some(vec) => Some((vec.x, vec.y)),
                    _ => None,
                }
            },
            kind_id: node.kind_id,
            energized: node.energized,
            index: i,
            output_value: None,
            input_value: None,
        })
    }
    for (i, j) in bot.output_nodes_i_list.iter().enumerate() {
        pynodes[*j].output_value = Some(bot.output_state[i]);
    }
    for (i, j) in bot.input_nodes_i_list.iter().enumerate() {
        pynodes[*j].input_value = Some(bot.input_state[i]);
    }
    pynodes
}

#[pyclass]
pub struct PyLink {
    #[pyo3(get)]
    pos_a: (f32, f32),
    #[pyo3(get)]
    pos_b: (f32, f32),
    #[pyo3(get)]
    broken: bool,
    #[pyo3(get)]
    health: f32,
    #[pyo3(get)]
    length: f32,
}

fn bot_pylinks(bot: &Bot) -> Vec<PyLink> {
    // List of links ends positions of a given bot.
    let mut bot_links = Vec::new();
    for link in bot.links.iter() {
        let pos_a = bot.nodes[link.node_a_index].pos;
        let pos_b = bot.nodes[link.node_b_index].pos;

        bot_links.push(PyLink {
            pos_a: (pos_a.x, pos_a.y),
            pos_b: (pos_b.x, pos_b.y),
            broken: link.broken,
            health: link.health / GAME_PARAMETERS.initial_link_health,
            length: link.length,
        })
    }
    bot_links
}

#[pyclass]
#[derive(Clone)]
pub struct BotDeliver {
    // Delivers a rusty bot to the rusty rinha by a python call
    bot: Bot,
}

impl BotDeliver {
    pub fn new(bot: Bot) -> Self {
        Self { bot }
    }
}

#[pymethods]
impl BotDeliver {
    pub fn total_links_length(&self) -> f32 {
        total_links_length(&self.bot)
    }

    pub fn total_nodes_number(&self) -> usize {
        self.bot.nodes.len()
    }

    pub fn check_for_interference(&self) -> bool {
        check_for_interference(&self.bot)
    }

    pub fn get_io_numbers(&self, nodes_only: Option<bool>) -> (usize, usize) {
        // Number of (input,output) nodes
        let inputs = self.bot.input_nodes_i_list.len();
        let mut outputs = self.bot.output_nodes_i_list.len();
        if nodes_only.unwrap_or(false) {
            outputs += self.bot.links.len();
        }
        (inputs, outputs)
    }
}

#[pyclass]
#[derive(Clone)]
pub struct RinhaDeGrafobots {
    rinha: GameState,
    _fingerprint: MatchFingerprint,
}

#[pymethods]
impl RinhaDeGrafobots {
    #[new]
    pub fn new(bot_a: BotDeliver, bot_b: BotDeliver) -> Self {
        Self {
            rinha: GameState::new(bot_a.bot, bot_b.bot),
            _fingerprint: MatchFingerprint::new(),
        }
    }

    pub fn setup(&mut self) {
        self.rinha.setup();
    }

    pub fn nodes(&self) -> (Vec<PyNode>, Vec<PyNode>) {
        (
            bot_pynodes(&self.rinha.bot_a),
            bot_pynodes(&self.rinha.bot_b),
        )
    }

    pub fn links(&self) -> (Vec<PyLink>, Vec<PyLink>) {
        (
            bot_pylinks(&self.rinha.bot_a),
            bot_pylinks(&self.rinha.bot_b),
        )
    }

    pub fn tick(&mut self) {
        self.rinha.tick();
        self.rinha.update_bots_output_nodes_only();
    }

    pub fn check_winner(&mut self) -> Option<u8> {
        self.rinha.check_winner()
    }

    pub fn compute_points(&self) -> (f32, f32) {
        self.rinha.bots.points
    }

    pub fn clear_input_state_a(&mut self) {
        self.rinha.bot_a.clear_input_state();
    }

    pub fn clear_input_state_b(&mut self) {
        self.rinha.bot_b.clear_input_state();
    }

    pub fn command_a(&mut self, node_index: usize, value: f32) {
        self.rinha.bot_a.single_input(node_index, value);
    }

    pub fn command_b(&mut self, node_index: usize, value: f32) {
        self.rinha.bot_b.single_input(node_index, value);
    }

    pub fn command_a_aann(&mut self, net: &mut PyNetwork) {
        let net_output = net
            .network
            .in_tick_out(self.rinha.bot_a.output_state.clone());
        self.rinha.bot_a.input_state = net_output;
    }

    pub fn command_b_aann(&mut self, net: &mut PyNetwork) {
        let net_output = net
            .network
            .in_tick_out(self.rinha.bot_b.output_state.clone());
        self.rinha.bot_b.input_state = net_output;
    }

    pub fn run_headless_match_aann(
        &mut self,
        max_ticks: u32,
        mut net_a: &mut PyNetwork,
        mut net_b: &mut PyNetwork,
    ) -> Option<u8> {
        while self.rinha.tick_counter < max_ticks
            && self.rinha.check_winner().is_none()
            && self.rinha.check_bots_contact()
        {
            self.command_a_aann(&mut net_a);
            self.command_b_aann(&mut net_b);
            self.tick();
        }
        self.check_winner()
    }

    pub fn run_headless_match_aann_fingerprint(
        &mut self,
        max_ticks: u32,
        mut net_a: &mut PyNetwork,
        mut net_b: &mut PyNetwork,
    ) -> Option<u8> {
        while self.rinha.tick_counter < max_ticks
            && self.rinha.check_winner().is_none()
            && self.rinha.check_bots_contact()
        {
            self.command_a_aann(&mut net_a);
            self.command_b_aann(&mut net_b);
            self.tick();
            self.update_fingerprint();
        }
        self.check_winner()
    }

    pub fn update_fingerprint(&mut self) {
        self._fingerprint.update(&self.rinha.collect_fingerprint());
    }

    pub fn run_headless_match_aann_no_win_checks(
        &mut self,
        max_ticks: u32,
        mut net_a: &mut PyNetwork,
        mut net_b: &mut PyNetwork,
    ) -> Option<u8> {
        while self.rinha.tick_counter < max_ticks {
            self.command_a_aann(&mut net_a);
            self.command_b_aann(&mut net_b);
            self.tick();
        }
        self.check_winner()
    }

    /// Gets the current input state from the bots
    pub fn get_bots_input_state(&self) -> (Vec<f32>, Vec<f32>) {
        (
            self.rinha.bot_a.input_state.clone(),
            self.rinha.bot_b.input_state.clone()
        )

    }

    /// Converts the fingerprint to a python friendly one
    #[getter]
    pub fn fingerprint(&self) -> PyMatchFingerprint {
        PyMatchFingerprint {match_fingerprint: self._fingerprint}
    }
    // pub fn parameters_dic(self) -> HashMap<str,f32> {
    //     let mut parameters = HashMap::new();
    //     parameters.insert("GAME_PARAMETERS.node_radar_max_range".to_string(), GAME_PARAMETERS.node_radar_max_range);
    //     parameters.insert("ARENA_RADAR_MAX_RANGE".to_string(), ARENA_RADAR_MAX_RANGE);
    //     parameters
    // }

    // pub fn run_headless_match(&mut self, max_ticks: u32) -> Option<u8> {
    //     self.rinha.run_headless_match(max_ticks)
    // }

    // pub fn run_headless_loop(&mut self) {
    //     self.rinha.run_headless_loop();
    // }
}

#[pyclass]
pub struct Parameters {}

#[pymethods]
impl Parameters {
    #[staticmethod]
    pub fn get_node_radar_max_range() -> f32 {
        GAME_PARAMETERS.node_radar_max_range.clone()
    }
    #[staticmethod]
    pub fn get_arena_radar_max_range() -> f32 {
        GAME_PARAMETERS.arena_radar_max_range.clone()
    }
}

/// Extra data on the result, mostly for the python side to know what
/// went about.
#[pyclass]
#[derive(Clone)]
pub struct PyMatchFingerprint {
    pub match_fingerprint: MatchFingerprint,
}

impl PyMatchFingerprint {
    fn from_option(fingerprint: Option<MatchFingerprint>) -> Option<Self> {
        match fingerprint {
            Some(x) => Some(Self {
                match_fingerprint: x,
            }),
            None => None,
        }
    }
}

#[pymethods]
impl PyMatchFingerprint {
    #[new]
    fn new() -> Self {
        Self {
            match_fingerprint: MatchFingerprint::new(),
        }
    }

    #[getter]
    fn moved(&self) -> (bool, bool) {
        self.match_fingerprint.moved
    }

    #[getter]
    fn acted(&self) -> (bool, bool) {
        self.match_fingerprint.acted
    }

    #[getter]
    fn sensed(&self) -> (bool, bool) {
        self.match_fingerprint.sensed
    }
}

#[pyclass]
pub struct RinhaResults {
    #[pyo3(get)]
    pub winner: Option<u8>,
    #[pyo3(get)]
    pub points: (f32, f32),
    #[pyo3(get)]
    pub healths: (f32, f32),
    #[pyo3(get)]
    pub fingerprint: Option<PyMatchFingerprint>,
}

impl RinhaResults {
    pub fn new(
        winner: Option<u8>,
        points: (f32, f32),
        healths: (f32, f32),
        fingerprint: MatchFingerprint,
    ) -> Self {
        Self {
            winner,
            points,
            healths,
            fingerprint: Option::None,
        }
    }
}

#[pyclass]
#[derive(Clone)]
pub struct RinhaAANNBundle {
    // Bundles the game with the networks to play in parallel
    rinha: RinhaDeGrafobots,
    nets: (PyNetwork, PyNetwork),
    max_ticks: u32,
}

#[pymethods]
impl RinhaAANNBundle {
    // FIXME avoid copying stuff so much: deal with lifetimes...
    #[new]
    pub fn new(rinha: RinhaDeGrafobots, max_ticks: u32, nets: (PyNetwork, PyNetwork)) -> Self {
        Self {
            rinha,
            max_ticks,
            nets,
        }
    }
}

impl RinhaAANNBundle {
    pub fn run(&mut self) {
        self.rinha.run_headless_match_aann_fingerprint(
            self.max_ticks,
            &mut self.nets.0,
            &mut self.nets.1,
        );
    }
}

#[pyfunction]
pub fn parallel_rinha(mut rinhas_bundle: Vec<RinhaAANNBundle>) -> Vec<RinhaResults> {
    rinhas_bundle.par_iter_mut().for_each(|bundle| {
        bundle.run();
    });
    let mut results = Vec::new();
    for bundle in rinhas_bundle.iter_mut() {
        results.push(RinhaResults {
            winner: bundle.rinha.check_winner(),
            points: bundle.rinha.compute_points(),
            healths: (
                bundle.rinha.rinha.bot_a.health,
                bundle.rinha.rinha.bot_b.health,
            ),
            fingerprint: Some(bundle.rinha.fingerprint()),
        });
    }
    results
}
