// aann binding to python
use pyo3::prelude::*;


#[pyclass]
#[derive(Clone)]
pub struct PyNetwork {
    pub network: aann::Network,
}

#[pymethods]
impl PyNetwork {
    #[new]
    pub fn new(nodes: Vec<(&str, f32)>, links: Vec<(usize, usize, f32)>) -> Self {
        let mut rust_nodes = Vec::new();
        let mut rust_links = Vec::new();
        for py_node in nodes.iter() {
            rust_nodes.push(aann::Node::new(py_node.0, py_node.1))
        }
        for py_link in links.iter() {
            rust_links.push(aann::Link::new(py_link.0, py_link.1, py_link.2))
        }
        Self {
            network: aann::Network::new(rust_nodes, rust_links),
        }
    }

    pub fn in_tick_out(&mut self, input: Vec<f32>) -> Vec<f32> {
        self.network.in_tick_out(input)
    }

    pub fn to_dot(&self, file_name: &str) {
        aann::to_dot::aann_to_dot(&self.network, file_name);
    }

    pub fn count_components(&self) -> (u32, u32, u32, u32) {
        (
            self.network.in_nodes.len() as u32,
            self.network.out_nodes.len() as u32,
            self.network.nodes.len() as u32,
            self.network.links.len() as u32,
        )
    }
}
