// Main game

use std::convert::TryInto;
use std::ops::{Deref, DerefMut};

use crate::bot::Bot;
use crate::constants::{ARENA_SIZE, GAME_PARAMETERS};
use crate::mass::Mass;
use crate::node::Node;
use std::f32::consts::PI;
use vectors::Vector;

pub type float = f32;

#[derive(Clone)]
pub struct GameState {
    // Main game type
    pub bots: Bots,
    pub tick_counter: u32,
    pub last_contact_timer: u32,
}

#[derive(Clone)]
pub struct Bots {
    // Holds both bots
    pub bot_a: Bot,
    pub bot_b: Bot,
    pub points: (f32, f32),
}

/// Stores some match fingerprint for various uses, debugging and
/// fight reviews.
#[derive(Clone, Copy)]
pub struct MatchFingerprint {
    pub moved: (bool, bool),
    pub acted: (bool, bool),
    pub sensed: (bool, bool),
}

impl Bots {
    pub fn new(bot_a: Bot, bot_b: Bot) -> Self {
        Self {
            bot_a,
            bot_b,
            points: (0., 0.),
        }
    }
    pub fn iter_all_nodes_mut(
        &mut self,
    ) -> std::iter::Chain<std::slice::IterMut<'_, Node>, std::slice::IterMut<'_, Node>> {
        self.bot_a
            .nodes
            .iter_mut()
            .chain(self.bot_b.nodes.iter_mut())
    }

    pub fn iter_all_nodes(
        &self,
    ) -> std::iter::Chain<std::slice::Iter<'_, Node>, std::slice::Iter<'_, Node>> {
        self.bot_a.nodes.iter().chain(self.bot_b.nodes.iter())
    }
}

impl Deref for GameState {
    // Allows access to bot_a and bot_b directly from GameState.
    type Target = Bots;
    fn deref(&self) -> &Self::Target {
        &self.bots
    }
}

impl DerefMut for GameState {
    // Allows access to mass fields directly.
    // Example: node.pos
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.bots
    }
}

impl GameState {
    pub fn new(bot_a: Bot, bot_b: Bot) -> GameState {
        let gs = GameState {
            bots: Bots::new(bot_a, bot_b),
            tick_counter: 0,
            last_contact_timer: 0,
        };
        gs
    }

    pub fn setup(&mut self) {
        // Setting up the game. Must be called before ticking.
        //
        // Resetting counters
        self.tick_counter = 0;
        self.last_contact_timer = 0;
        // Positioning the bots
        let mid_point = Vector::new(ARENA_SIZE.0 / 2., ARENA_SIZE.1 / 2.);
        self.bot_a
            .translate(Vector::new(mid_point.x - 448., mid_point.y));
        self.bot_b.rotate(PI);
        self.bot_b
            .translate(Vector::new(mid_point.x + 448., mid_point.y));
        // Setting up the bots.
        self.bot_a.setup();
        self.bot_b.setup();
    }

    pub fn tick(&mut self) {
        // Main update method.
        //
        // Applying internal forces
        self.bots.bot_a.act();
        self.bots.bot_b.act();
        self.bot_a.apply_internal_collision_forces();
        self.bot_b.apply_internal_collision_forces();
        // Applying external forces
        self.apply_external_forces_to_bots();
        // Updating bots internal state affairs
        self.bot_a.update();
        self.bot_b.update();
        //
        self.update_points();
        // More one tick completed.
        self.tick_counter += 1;
    }

    pub fn apply_external_forces_to_bots(&mut self) {
        // Apply external forces to the bots
        //
        // Applying collision forces between bot nodes
        let bots_made_contact =
            apply_collision_forces_between_bots(&mut self.bots.bot_a, &mut self.bots.bot_b);
        self.update_last_contact_timer(bots_made_contact);
        // Applying arena forces
        for node in self.bot_a.nodes.iter_mut() {
            apply_arena_force(&mut node.mass);
        }
        for node in self.bot_b.nodes.iter_mut() {
            apply_arena_force(&mut node.mass);
        }
    }

    pub fn update_bots_output(&mut self) {
        // Sensing and storing the data (must be done externally to the bot
        // to please the borrow checker).
        let bot_a_out = self.bots.bot_a.sense(&self);
        let bot_b_out = self.bots.bot_b.sense(&self);
        self.bot_a.output_state = bot_a_out;
        self.bot_b.output_state = bot_b_out;
    }

    pub fn update_bots_output_nodes_only(&mut self) {
        // Sensing and storing the data (must be done externally to the bot
        // to please the borrow checker).
        // FIXME once the sense method is made more flexible, than
        // this specialized update will be no more needed
        let bot_a_out = self.bots.bot_a.sense_nodes_only(&self);
        let bot_b_out = self.bots.bot_b.sense_nodes_only(&self);
        self.bot_a.output_state = bot_a_out;
        self.bot_b.output_state = bot_b_out;
    }

    pub fn check_bots_contact(&self) -> bool {
        // False if no contact was made for too long.
        self.last_contact_timer < GAME_PARAMETERS.max_time_without_contact
    }

    fn update_last_contact_timer(&mut self, made_contact: bool) {
        match made_contact {
            true => self.last_contact_timer = 0,
            false => self.last_contact_timer += 1,
        }
    }

    pub fn check_winner(&self) -> Option<u8> {
        match (self.bot_a.alive, self.bot_b.alive) {
            (true, true) => None,
            (true, false) => Some(1),
            (false, true) => Some(2),
            (false, false) => Some(0),
        }
    }

    pub fn update_points(&mut self) {
        self.bots.points.0 += Self::compute_points_for_this_tick(&self, &self.bot_b);
        self.bots.points.1 += Self::compute_points_for_this_tick(&self, &self.bot_a);
    }

    pub fn compute_points_for_this_tick(&self, enemy_bot: &Bot) -> f32 {
        // Computing the points gained in the current tick
        // Testing if some contact was made lately
        if self.last_contact_timer < 600 {
            let mut points = 0.;
            // Points from damage to links
            let health_lost = enemy_bot.health_lost_last_tick();
            if health_lost != 0. {
                points += health_lost * 100.;
            }
            // Points from breaking links
            let nodes_lost = enemy_bot.nodes_lost_energy_last_tick();
            if nodes_lost != 0 {
                points += nodes_lost as f32 / enemy_bot.nodes.len() as f32 * 100.;
            }
            points
        } else {
            0.
        }
    }

    pub fn compute_points(&self) -> (u32, u32) {
        // Use bots.points instead
        let points_a: u32 = (self.bot_b.nodes.len() - self.bot_b.energized_nodes_i_list.len())
            .try_into()
            .unwrap();
        let points_b: u32 = (self.bot_a.nodes.len() - self.bot_a.energized_nodes_i_list.len())
            .try_into()
            .unwrap();
        (points_a, points_b)
    }

    /// Collect some match fingerprint to be used in review what 
    /// happened.
    /// Collects:
    ///     bots moved?
    ///     some input had been given?
    ///     some output hab been given?
    pub fn collect_fingerprint(&mut self) -> MatchFingerprint {
        MatchFingerprint{
            moved: (self.bot_a.has_moved_last_tick(), self.bot_b.has_moved_last_tick()),
            acted: (
                !self.bot_a.input_state.iter().all(|&x| x == 0.0),
                !self.bot_b.input_state.iter().all(|&x| x == 0.0),
            ),
            sensed:(
                !self.bot_a.output_state.iter().all(|&x| x == 0.0),
                !self.bot_b.output_state.iter().all(|&x| x == 0.0),
            ),
        }
    }

    pub fn run_headless_loop(&mut self) {
        // Run without graphics (mainly for profiling)
        loop {
            self.tick();
            self.update_bots_output();
            if self.tick_counter % 10000 == 0 {
                println!("{}", self.tick_counter);
            }
        }
    }
}

impl MatchFingerprint {
    pub fn new() -> Self{
        Self {
            moved: (false, false),
            acted: (false, false),
            sensed: (false, false),
        }
    }
    pub fn update(&mut self, other: &MatchFingerprint) {
        self.moved = or_operator_for_size_2_tuple(&self.moved, &other.moved);
        self.acted = or_operator_for_size_2_tuple(&self.acted, &other.acted);
        self.sensed = or_operator_for_size_2_tuple(&self.acted, &other.acted);
    }
}

/// And operator for 2-sided tuple
fn or_operator_for_size_2_tuple(t1: &(bool,bool), t2: &(bool,bool)) -> (bool, bool) {
    (t1.0 | t2.0, t1.1 | t2.1)
}

pub fn apply_collision_forces_between_bots(bot1: &mut Bot, bot2: &mut Bot) -> bool {
    // Apply force between bots and returns true if contact was made.
    let mut contact = false;
    let n1 = bot1.nodes.len();
    let n2 = bot2.nodes.len();
    for i in 0..n1 {
        for j in 0..n2 {
            let m1 = &mut bot1.nodes[i].mass;
            let m2 = &mut bot2.nodes[j].mass;
            let separation = m2.pos - m1.pos;
            let separation_mod = separation.module();
            let impact_parameter = m1.radius + m2.radius;
            if separation_mod < impact_parameter {
                contact = true;
                let mut force = (1. - impact_parameter / separation_mod)
                    * separation
                    * GAME_PARAMETERS.mass_collision_elastic_constant;
                if m1.speed * m2.speed < 0. {
                    force = force * 0.8;
                }
                m1.apply_force(force);
                m2.apply_force(-force);
            };
        }
    }
    contact
}

pub fn apply_arena_force(mass: &mut Mass) {
    // Apply arena (boundary) forces.
    let mut force = Vector::new(0., 0.);
    let left_pos = mass.pos.x - mass.radius;
    let right_pos = mass.pos.x + mass.radius;
    let top_pos = mass.pos.y + mass.radius;
    let bottom_pos = mass.pos.y - mass.radius;
    if left_pos < 0. {
        force.x -= left_pos * GAME_PARAMETERS.arena_elastic_constant;
    } else if right_pos > ARENA_SIZE.0 {
        force.x += (ARENA_SIZE.0 - right_pos) * GAME_PARAMETERS.arena_elastic_constant;
    };
    if bottom_pos < 0. {
        force.y -= bottom_pos * GAME_PARAMETERS.arena_elastic_constant;
    } else if top_pos > ARENA_SIZE.1 {
        force.y += (ARENA_SIZE.1 - top_pos) * GAME_PARAMETERS.arena_elastic_constant;
    };
    if force.x != 0. || force.y != 0. {
        mass.apply_force(force)
    }
}
