// Miscellaneous bits and pieces.
use std::f32::consts::PI;

pub fn angle_between_pm_pi(theta: f32) -> f32 {
    // Ensures that the angle is between -Pi and +Pi
    if theta > PI {
        theta - ((theta + PI) / (2. * PI)).floor() * 2. * PI
    } else if theta <= -PI {
        theta + ((-theta + PI) / (2. * PI)).floor() * 2. * PI
    } else {
        theta
    }
}

// Testing..
#[cfg(test)]
mod tests {
    use super::*;
    use assert_approx_eq::assert_approx_eq;

    #[test]
    fn internal() {
        assert_approx_eq!(PI - 0.2, angle_between_pm_pi(PI - 0.2), 0.001);
        assert_approx_eq!(-PI + 0.2, angle_between_pm_pi(PI + 0.2), 0.001);
        assert_approx_eq!(PI - 0.2, angle_between_pm_pi(5. * PI - 0.2), 0.001);
    }
}
