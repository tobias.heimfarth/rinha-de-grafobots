// Game constants definition and import from file method.

use phf::phf_map;
use std::fs::File;
use std::io::{self, Read};

use lazy_static::lazy_static;

use crate::node::{ActBehavior, NodeKind, NodeRole, SenseBehavior};

pub struct GameParameters {
    pub mass_collision_elastic_constant: f32,
    pub mass_max_push_force: f32,
    pub mass_max_push_speed: f32,
    pub mass_max_push_torque: f32,
    pub mass_max_push_angular_speed: f32,
    pub arena_elastic_constant: f32,
    pub node_radar_max_range: f32,
    pub arena_radar_max_range: f32,
    pub max_inactivity_time: u16,
    pub max_time_without_contact: u32,
    pub radial_elastic_constant: f32,
    pub angular_elastic_constant: f32,
    pub breakage_radial_deformation: f32,
    pub breakage_angular_deformation: f32,
    pub dt: f32,
    pub static_friction_coefficient: f32,
    pub dynamic_friction_coefficient: f32,
    pub lateral_static_friction_coefficient: f32,
    pub lateral_dynamic_friction_coefficient: f32,
    pub radial_dampening_coefficient: f32,
    pub initial_link_health: f32,
    pub speed_dependent_friction_coefficient: f32,
}

impl GameParameters {
    pub fn new() -> Self {
        Self {
            dt: 0.05,
            mass_collision_elastic_constant: 300.,
            mass_max_push_force: 40.,
            mass_max_push_speed: 100.,
            mass_max_push_torque: 20.,
            mass_max_push_angular_speed: 5.,
            arena_elastic_constant: 50.,
            node_radar_max_range: 300.,
            arena_radar_max_range: 500.,
            max_inactivity_time: 2000,
            max_time_without_contact: 3000,
            radial_elastic_constant: 200.,
            angular_elastic_constant: 100.,
            breakage_radial_deformation: 0.2,
            breakage_angular_deformation: 0.6,
            static_friction_coefficient: 0.6, 
            dynamic_friction_coefficient: 0.8,
            lateral_static_friction_coefficient: 200.,
            lateral_dynamic_friction_coefficient: 5.,
            radial_dampening_coefficient: 1.,
            initial_link_health: 10.,
            speed_dependent_friction_coefficient: 0.05,
        }
    }
}

lazy_static! {
     pub static ref GAME_PARAMETERS: GameParameters = load_constants("./resources/constants.txt");
     //pub static ref GAME_PARAMETERS: GameParameters = GameParameters::new();
}

pub const ARENA_SIZE: (f32, f32) = (1120., 640.);

pub fn load_constants(filename: &str) -> GameParameters {
    // Loads a file with game parameters
    // FIXME bites if sees a blank line...
    let whole_file = file_to_string(filename).unwrap();
    let wbyl = words_by_line(&whole_file);
    let mut pars = GameParameters::new();
    for line in wbyl.iter() {
        match parse_line(line, &mut pars) {
            Ok(()) => (),
            Err(_) => (),
        }
    }
    pars
}

fn parse_line<'a>(line: &Vec<&str>, pars: &mut GameParameters) -> Result<(), &'a str> {
    if line.is_empty(){ return Err("Line empty, skipping.");
    };
    if line[0].chars().nth(0).unwrap() == '#' { return Err("Commented line, skipping");
    };
    let par = line[0];
    let value: f32 = line[1].parse().unwrap();
    match par {
        "mass_collision_elastic_constant" => pars.mass_collision_elastic_constant = value,
        "mass_max_push_force" => pars.mass_max_push_force = value,
        "mass_max_push_speed" => pars.mass_max_push_speed = value,
        "mass_max_push_torque" => pars.mass_max_push_torque = value,
        "mass_max_push_angular_speed" => pars.mass_max_push_angular_speed = value,
        "arena_elastic_constant" => pars.arena_elastic_constant = value,
        "node_radar_max_range" => pars.node_radar_max_range = value,
        "arena_radar_max_range" => pars.arena_radar_max_range = value,
        "max_inactivity_time" => pars.max_inactivity_time = value as u16,
        "max_time_without_contact" => pars.max_time_without_contact = value as u32,
        "radial_elastic_constant" => pars.radial_elastic_constant = value,
        "angular_elastic_constant" => pars.angular_elastic_constant = value,
        "breakage_radial_deformation" => pars.breakage_radial_deformation = value,
        "breakage_angular_deformation" => pars.breakage_angular_deformation = value,
        "dt" => pars.dt = value,
        "static_friction_coefficient" => pars.static_friction_coefficient = value,
        "dynamic_friction_coefficient" => pars.dynamic_friction_coefficient = value,
        "lateral_static_friction_coefficient" => pars.lateral_static_friction_coefficient = value,
        "lateral_dynamic_friction_coefficient" => pars.lateral_dynamic_friction_coefficient = value,
        "radial_dampening_coefficient" => pars.radial_dampening_coefficient = value,
        "initial_link_health" => pars.initial_link_health = value,
        "speed_dependent_friction_coefficient" => pars.speed_dependent_friction_coefficient = value,
        _ => panic!("Linha inválida no arquivo de parâmetros: {}", par),
    };
    Ok(())
}

fn file_to_string(s: &str) -> io::Result<String> {
    let mut file = File::open(s)?;
    let mut s = String::new();
    file.read_to_string(&mut s)?;
    Ok(s)
}

fn words_by_line<'a>(s: &'a str) -> Vec<Vec<&'a str>> {
    s.lines()
        .map(|line| line.split_whitespace().collect())
        .collect()
}

pub static NODE_TYPES: phf::Map<&str, NodeKind> = phf_map! {
    "traction" => NodeKind{
                    role: NodeRole::Input,
        act_behavior: ActBehavior::Traction,
        sense_behavior: SenseBehavior::None ,
        id: 't',
        },
    "noderadar" => NodeKind{
            role: NodeRole::Output,
            act_behavior: ActBehavior::None,
            sense_behavior: SenseBehavior::NodeRadar,
            id: 'n',
        },
    "arenaradar" => NodeKind{
            role: NodeRole::Output,
            act_behavior: ActBehavior::None,
            sense_behavior: SenseBehavior::ArenaRadar,
            id: 'a',
        },
    "anyradar" => NodeKind{
            role: NodeRole::Output,
            act_behavior: ActBehavior::None,
            sense_behavior: SenseBehavior::AnyRadar,
            id: 'y',
        },
    "battery" => NodeKind{
            role: NodeRole::Vital,
            act_behavior: ActBehavior::None,
            sense_behavior: SenseBehavior::None,
            id: 'b',
        },
    "simple" => NodeKind{
            role: NodeRole::DeadWeight,
            act_behavior: ActBehavior::None,
            sense_behavior: SenseBehavior::None,
            id: 's',
        },
    "torsion" => NodeKind{
            role: NodeRole::Input,
            act_behavior: ActBehavior::Torsion,
            sense_behavior: SenseBehavior::None,
            id: 'o',
        },
};
