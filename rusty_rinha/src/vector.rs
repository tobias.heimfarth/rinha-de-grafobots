use mint;
use std::fmt;
use std::ops;
use std::f32::consts::PI;
use rand::prelude::*;
use serde::{Deserialize, Serialize};

use fast_math;

#[derive(Serialize, Deserialize, Copy, Clone)]
pub struct Vector {
    pub x: f32,
    pub y: f32,
}

impl Vector {
    // Constructor for the zero vector
    pub fn zero() -> Vector {
        Vector { x: 0.0, y: 0.0 }
    }

    // Constructor for a new vector
    pub fn new(x: f32, y: f32) -> Vector {
        Vector { x: x, y: y }
    }

    /// Constructor of a versor in a random direction
    pub fn random_versor() -> Vector {
        let random_angle = rand::random::<f32>()*2.*PI;
        Self::new(1.,0.).rotate(random_angle)
    }

    pub fn theta(self) -> f32 {
        // Angle to the x axis (theta coordinate in polar coordinates system)
        fast_math::atan2(self.y, self.x)
    }

    pub fn module(self) -> f32 {
        (self.x.powi(2) + self.y.powi(2)).sqrt()
    }

    pub fn versor(self) -> Self {
        let module = self.module();
        if module != 0.0 {
            self / self.module()
        } else {
            Self::zero()
        }
    }

    pub fn perpendicular_versor(self) -> Self {
        Self::new(self.y, -self.x).versor()
    }

    pub fn rotate(self, theta: f32) -> Self {
        let cos = theta.cos();
        let sin = theta.sin();
        Self {
            x: cos * self.x - sin * self.y,
            y: sin * self.x + cos * self.y,
        }
    }

    pub fn to_mint(self) -> mint::Point2<f32> {
        // Converts a Vector to a "mint" vector.
        mint::Point2 {
            x: self.x,
            y: self.y,
        }
    }

    pub fn to_zero(&mut self) {
        // Fast way to zero the vector
        self.x = 0.;
        self.y = 0.;
    }


}

impl ops::Add for Vector {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl ops::Mul<f32> for Vector {
    type Output = Self;
    fn mul(self, factor: f32) -> Self {
        Self {
            x: self.x * factor,
            y: self.y * factor,
        }
    }
}

impl ops::Mul<Vector> for f32 {
    type Output = Vector;
    fn mul(self, vector: Vector) -> Vector {
        Vector {
            x: vector.x * self,
            y: vector.y * self,
        }
    }
}

impl ops::Mul<Vector> for Vector {
    // Dot product
    type Output = f32;
    fn mul(self, other: Vector) -> f32 {
        self.x * other.x + self.y * other.y
    }
}

impl ops::Div<f32> for Vector {
    type Output = Self;
    fn div(self, factor: f32) -> Self {
        Self {
            x: self.x / factor,
            y: self.y / factor,
        }
    }
}

impl ops::AddAssign for Vector {
    fn add_assign(&mut self, other: Self) {
        *self = Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl ops::Sub for Vector {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        Self {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl ops::SubAssign for Vector {
    fn sub_assign(&mut self, other: Self) {
        *self = Self {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl ops::Neg for Vector {
    type Output = Self;
    fn neg(self) -> Self {
        Self {
            x: -self.x,
            y: -self.y,
        }
    }
}

impl fmt::Display for Vector {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

impl PartialEq for Vector {
    fn eq(&self, other: &Self) -> bool {
        self.x == other.x && self.y == other.y
    }
}
