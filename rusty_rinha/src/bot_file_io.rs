use crate::constants::NODE_TYPES;
use std::fs::File;
use std::io::{self, Read};

use crate::bot::Bot;
use crate::mass::Mass;
use crate::node::Node;
use vectors::Vector;

pub fn load_bot(filename: &str) -> Bot {
    // Loads a file to construct a bot
    // FIXME bites if sees a blank line...
    let whole_file = file_to_string(filename).unwrap();
    let wbyl = words_by_line(&whole_file);
    let mut bot = Bot::new();
    for line in wbyl.iter() {
        let component = parse_line(&line);
        match component {
            Some(BotComponent::Link(
                node_a_index,
                node_a_fixation,
                node_b_index,
                node_b_fixation,
            )) => bot.add_link(node_a_index, node_b_index, node_a_fixation, node_b_fixation),
            Some(BotComponent::Node(node)) => bot.add_node(node),
            _ => {}
        }
    }
    bot
}

fn file_to_string(s: &str) -> io::Result<String> {
    let mut file = File::open(s)?;
    let mut s = String::new();
    file.read_to_string(&mut s)?;
    Ok(s)
}

fn words_by_line<'a>(s: &'a str) -> Vec<Vec<&'a str>> {
    s.lines()
        .map(|line| line.split_whitespace().collect())
        .collect()
}

enum BotComponent {
    Link(usize, bool, usize, bool),
    Node(Node),
}

fn parse_line(line: &Vec<&str>) -> Option<BotComponent> {
    // Skipping empty_lines
    if line.is_empty() {
        return None;
    };
    let kind = line[0];
    match kind {
        "#" => None,
        "link" => Some(parse_link(&line[..])),
        "node" => Some(parse_node(&line[..])),
        _ => None,
    }
}

fn parse_node(line: &[&str]) -> BotComponent {
    // kind
    //let kind = match line[1].as_ref()
    let kind = match NODE_TYPES.get(line[1]).cloned() {
        Some(kind) => kind,
        _ => panic!("Deu problema lendo as paradas por aqui ->: {}", line[1]),
    };
    let x = line[2].parse::<f32>().unwrap();
    let y = line[3].parse::<f32>().unwrap();
    let mass = line[4].parse::<f32>().unwrap();
    let radius = line[5].parse::<f32>().unwrap();
    let theta = line[6].parse::<f32>().unwrap();
    let mass = Mass::new(Vector::new(x, y), mass, radius, theta);
    BotComponent::Node(Node::new(mass, kind))
}

fn parse_link(line: &[&str]) -> BotComponent {
    // kind
    let node_a_index = line[1].parse::<usize>().unwrap();
    let node_a_fixation = line[2].parse::<bool>().unwrap();
    let node_b_index = line[3].parse::<usize>().unwrap();
    let node_b_fixation = line[4].parse::<bool>().unwrap();
    BotComponent::Link(node_a_index, node_a_fixation, node_b_index, node_b_fixation)
}
