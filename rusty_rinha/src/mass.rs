// Round masses (nodes physical part) main type and related stuff.

use crate::constants::GAME_PARAMETERS;
use crate::misc::angle_between_pm_pi;
use vectors::Vector;

#[derive(Clone)]
pub struct Mass {
    // Contains the physics related data of the nodes.
    pub pos: Vector,
    pub speed: Vector,
    pub mass: f32,
    pub radius: f32,
    pub theta: f32,
    pub angular_speed: f32,
    pub force: Vector,
    pub torque: f32,
    // pre computed constants
    moment_inertia: f32,
    linear_friction_force_module: f32,
    angular_friction_torque_module: f32,
}

impl Mass {
    pub fn new(pos: Vector, mass: f32, radius: f32, theta: f32) -> Self {
        let mut mass = Self {
            pos,
            speed: Vector { x: 0., y: 0. },
            mass,
            radius,
            moment_inertia: 1.,
            theta,
            angular_speed: 0.,
            force: Vector { x: 0., y: 0. },
            torque: 0.,
            linear_friction_force_module: 0.,
            angular_friction_torque_module: 0.,
        };
        // pre computing constants
        mass.set_disk_moment_inertia();
        mass.store_friction_forces_modules();
        mass
    }

    pub fn set_disk_moment_inertia(&mut self) {
        // updates the moment_inertia field based on the z axis moment of a disk
        self.moment_inertia = 0.01 * 0.5 * self.mass * self.radius.powi(2);
    }

    pub fn store_friction_forces_modules(&mut self) {
        // stores the delta velocities values to avoid having to compute them every tick
        self.linear_friction_force_module =
            GAME_PARAMETERS.dynamic_friction_coefficient * self.mass;
        self.angular_friction_torque_module =
            2. / 3. * GAME_PARAMETERS.dynamic_friction_coefficient * self.mass * 0.1 * self.radius;
    }

    pub fn update(&mut self) {
        // Update the dynamic variables from the external forces including
        // the friction (applied here).
        self.apply_friction_force();
        self.apply_friction_torque();
        // Updating linear variables
        self.speed += self.force * GAME_PARAMETERS.dt / self.mass;
        self.pos += self.speed * GAME_PARAMETERS.dt;
        self.force.to_zero();
        // Updating angular variables
        self.angular_speed += self.torque * GAME_PARAMETERS.dt / self.moment_inertia;
        self.theta += GAME_PARAMETERS.dt * self.angular_speed;
        self.theta = angle_between_pm_pi(self.theta);
        self.torque = 0.;
    }

    pub fn apply_force(&mut self, force: Vector) {
        self.force += force;
    }

    pub fn apply_torque(&mut self, torque: f32) {
        self.torque += torque;
    }

    fn apply_friction_force(&mut self) {
        // Apply linear friction force.
        // If the dynamic friction force is enough to stop the mass, than stop it.
        // Avoids that the friction reverses the movement.
        let speed_module = self.speed.module();
        if (self.speed / GAME_PARAMETERS.dt * self.mass + self.force).module()
            < self.linear_friction_force_module
        {
            self.speed.to_zero();
            self.force.to_zero();
        } else {
            // Applying the dynamic friction force.
            self.force -= (self.linear_friction_force_module 
                + GAME_PARAMETERS.speed_dependent_friction_coefficient * speed_module.powi(2))
                * self.speed.versor();
        }
    }

    fn apply_friction_torque(&mut self) {
        // Apply angular friction torque.
        // If the angular velocity is low enough that the friction will stop it, than
        // stop it. (for a disk T_f = 2/3 F_N * R * coef.)
        let resulting_torque_no_friction =
            self.angular_speed * self.moment_inertia / GAME_PARAMETERS.dt + self.torque;
        if resulting_torque_no_friction.abs() <= self.angular_friction_torque_module {
            self.angular_speed = 0.;
            self.torque = 0.;
        // Avoids signum +0 -> +1
        } else {
            self.torque -=
                self.angular_friction_torque_module * resulting_torque_no_friction.signum();
        }
    }

    pub fn apply_lateral_friction_force(&mut self) {
        // Apply an extra friction force in the orthogonal direction
        let heading_versor = self.heading_versor();
        let perp_versor = heading_versor.perpendicular_versor();
        let perp_speed_component = self.speed * perp_versor;
        let perp_force = self.force * perp_versor;
        let resulting_force_no_friction =
            perp_speed_component / GAME_PARAMETERS.dt * self.mass + perp_force;
        let friction_coefficient = match perp_speed_component.abs() {
            x if x == 0. => GAME_PARAMETERS.lateral_static_friction_coefficient,
            _ => GAME_PARAMETERS.lateral_dynamic_friction_coefficient,
        };
        if (resulting_force_no_friction).abs() 
            < friction_coefficient * self.mass {
                self.speed = (self.speed * heading_versor) * heading_versor;
                self.force = (self.force * heading_versor) * heading_versor;
        } else {
            // Applying the dynamic friction force.
            self.force -= GAME_PARAMETERS.lateral_dynamic_friction_coefficient
                * self.mass
                * perp_versor
                * resulting_force_no_friction.signum();
        }
    }

    pub fn heading_versor(&self) -> Vector {
        // Mass' heading versor (based on theta, not speed)
        Vector::new(1., 0.).rotate(self.theta)
    }

    pub fn push_forward(&mut self, force_module: f32) {
        // Push the mass in the heading direction
        self.apply_force(self.heading_versor() * force_module);
    }

    pub fn push_forward_limited(&mut self, factor: f32) {
        // Push the mass in the heading direction with a decreasing force
        // depending on the forward speed
        let heading = self.heading_versor() * factor;
        let force_limited = limited_output(
            GAME_PARAMETERS.mass_max_push_force,
            GAME_PARAMETERS.mass_max_push_speed,
            self.speed * heading,
        );
        self.apply_force(heading * force_limited);
    }

    pub fn apply_torque_limited(&mut self, factor: f32) {
        let torque_limited = limited_output(
            GAME_PARAMETERS.mass_max_push_torque,
            GAME_PARAMETERS.mass_max_push_angular_speed,
            self.angular_speed.abs(),
        );
        self.apply_torque(torque_limited * factor);
    }
}

fn limited_output(max_output: f32, max_speed: f32, speed: f32) -> f32 {
    // Limiting output function of speed
    if speed <= 0. {
        max_output
    } else if speed > max_speed {
        0.
    } else {
        max_output * (1. - speed / max_speed)
    }
}
