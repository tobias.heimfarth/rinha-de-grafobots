use crate::bot_blueprints::BotBlueprint;
use crate::constants::NODE_TYPES;
use rand::prelude::*;
use std::f32::consts::PI;
use std::fs::File;
use std::io::{self, Read};

use crate::bot::Bot;
use crate::mass::Mass;
use crate::node::{Node, NodeKind};
use crate::rinha::float;
use vectors::Vector;


/// Methods to mutate a bot
impl BotBlueprint {
    /// Apply a list of mutations with the given probabilities
    pub fn mutate(&mut self, mutations_list: &Vec<Box<dyn IMutation>>) {
        for mutation in mutations_list.iter() {
            self.apply_mutation(mutation);
        }
    }

    /// Apply a single mutation to the network
    pub fn apply_mutation(&mut self, mutation: &Box<dyn IMutation>) {
        if rand::random::<f32>() < mutation.get_probability() {
            match mutation.apply(self) {
                Ok(_) => (),
                Err(s) => (),
            };
        }
    }
}


/// Interface for mutations to include in the mutations list to be applied
/// in the network
pub trait IMutation {
    fn apply(&self, net: &mut BotBlueprint) -> Result<(), String>;
    fn get_probability(&self) -> f32;
}

/// Mutation: Perturb lenghts
#[derive(Clone, Copy)]
pub struct MutationPerturbLinksLenghts {
    probability: f32,
    perturbation_factor: f32,
}

impl MutationPerturbLinksLenghts {
    pub fn new(probability: f32, perturbation_factor: f32) -> Self {
        Self {
            probability,
            perturbation_factor,
        }
    }
}

impl IMutation for MutationPerturbLinksLenghts {
    fn apply(&self, botd: &mut BotBlueprint) -> Result<(), String> {
        for node in &mut botd.nodes {
            node.pos += Vector::random_versor() * self.perturbation_factor * rand::random::<f32>();
        }
        Ok(())
    }

    fn get_probability(&self) -> f32 {
        self.probability
    }
}
