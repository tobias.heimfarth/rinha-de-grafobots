// Link between nodes related code.

use std::f32::consts::PI;

use crate::constants::GAME_PARAMETERS;
use crate::misc::angle_between_pm_pi;
use crate::node::Node;
use vectors::Vector;


#[derive(Clone)]
pub struct Link {
    // Index of the nodes connected by the link.
    pub node_a_index: usize,
    pub node_b_index: usize,
    pub length: f32,
    // Angle between the link and the nodes headings
    pub phi_a: f32,
    pub phi_b: f32,
    // If true a spring like torque is applied if the node rotates
    // in relation to its rest angle.
    pub fixed_a: bool,
    pub fixed_b: bool,
    // Dynamic data to reuse in different calculations.
    pub separation: Vector,
    pub separation_mod: f32,
    pub delta_phi_a: f32,
    pub delta_phi_b: f32,
    pub health: f32,
    pub broken: bool,
    pub stretch_speed: f32,
    pub deformation: f32,
}

impl Link {
    pub fn new(
        node_a_index: usize,
        node_b_index: usize,
        length: f32,
        phi_a: f32,
        phi_b: f32,
        fixed_a: bool,
        fixed_b: bool,
    ) -> Link {
        Link {
            node_a_index,
            node_b_index,
            length,
            phi_a,
            phi_b,
            fixed_a,
            fixed_b,
            broken: false,
            separation: Vector::zero(),
            separation_mod: 0.,
            delta_phi_a: 0.,
            delta_phi_b: 0.,
            health: GAME_PARAMETERS.initial_link_health,
            stretch_speed: 0.,
            deformation: 0.,
        }
    }

    pub fn setup(&mut self, nodes: &Vec<Node>) {
        // Start the dynamic variables by updating the state twice
        // 
        self.update_state(nodes);
        self.update_state(nodes);
    }

    pub fn update(&mut self, nodes: &Vec<Node>) {
        self.update_state(nodes);
        // Compute damage
        self.take_damage();
        // broken?
        self.broken = self.test_breakage();
    }

    pub fn update_state(&mut self, nodes: &Vec<Node>) {
        // Update the dynamic variables.
        // updating separation
        let mass_a = &nodes[self.node_a_index].mass;
        let mass_b = &nodes[self.node_b_index].mass;
        let separation = mass_b.pos - mass_a.pos;
        let separation_mod = separation.module();
        self.stretch_speed = (separation_mod - self.separation_mod)/GAME_PARAMETERS.dt;
        self.separation = separation;
        self.separation_mod = separation_mod;
        self.deformation = (self.separation.module() - self.length).abs() / self.length;
        // updating deltas phi
        let separation_theta = self.separation.theta();
        let delta_a: f32 = separation_theta - (self.phi_a + mass_a.theta);
        let delta_b: f32 = PI + separation_theta - (self.phi_b + mass_b.theta);
        self.delta_phi_a = angle_between_pm_pi(delta_a);
        self.delta_phi_b = angle_between_pm_pi(delta_b);
    }

    pub fn get_force_and_torque(&self) -> (Vector, f32, Vector, f32) {
        // Computes forces and torques coming from the link.
        // Make sure the total force and torque are null (conservation of moment).
        let r_vec = self.separation;
        let r_mod = r_vec.module();
        let mut force_radial =
            GAME_PARAMETERS.radial_elastic_constant/self.length
            * (1. - self.length / r_mod) * r_vec;
        // Dampening
        force_radial += GAME_PARAMETERS.radial_dampening_coefficient 
            *self.stretch_speed * r_vec/r_mod ;
        // Computing the torque
        let mut torque_a: f32 = 0.;
        let mut torque_b: f32 = 0.;
        if self.fixed_a {
            torque_a = GAME_PARAMETERS.angular_elastic_constant * self.delta_phi_a;
        }
        if self.fixed_b {
            torque_b = GAME_PARAMETERS.angular_elastic_constant * self.delta_phi_b;
        }
        let perpendicular_to_r: Vector = -Vector::new(-r_vec.y, r_vec.x) / r_mod;
        // Computing the tangential force due to the link
        let force_tangential: Vector = (torque_a + torque_b) * perpendicular_to_r;
        (
            force_radial - force_tangential,
            torque_a,
            -force_radial + force_tangential,
            torque_b,
        )
    }

    pub fn take_damage(&mut self) {
        // Radial damage
        let delta_radial = self.deformation - GAME_PARAMETERS.breakage_radial_deformation;
        if delta_radial > 0. {
            self.health -= delta_radial/GAME_PARAMETERS.breakage_radial_deformation;
        }
        // Angular damage
        if self.fixed_a {
            let delta_theta_a = self.delta_phi_a.abs() - GAME_PARAMETERS.breakage_angular_deformation;
            if delta_theta_a > 0. {
                self.health -= delta_theta_a/GAME_PARAMETERS.breakage_radial_deformation;
            }
        }
        if self.fixed_b {
            let delta_theta_b = self.delta_phi_b.abs() - GAME_PARAMETERS.breakage_angular_deformation;
            if delta_theta_b > 0. {
                self.health -= delta_theta_b/GAME_PARAMETERS.breakage_radial_deformation;
            }
        }
        if self.health < 0. {self.health = 0.}
    }

    pub fn test_breakage(&mut self) -> bool {
        // Test if the link is broken
        self.health <= 0.
    }

    /// Returns the normalized current link health
    /// To be used, for example, as an input of the network
    pub fn get_normalized_health(&self) -> f32 {

        if self.broken {
            return -1.
        }
        self.deformation
        //self.health/GAME_PARAMETERS.initial_link_health
    }
}
