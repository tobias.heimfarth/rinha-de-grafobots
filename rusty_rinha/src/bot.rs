use crate::constants::GAME_PARAMETERS;
use crate::link::Link;
use crate::misc::angle_between_pm_pi;
use crate::node::{ActBehavior, Node, NodeRole, SenseBehavior};
use crate::rinha::GameState;
use std::f32::consts::PI;
use vectors::Vector;

#[derive(Clone)]
pub struct BotCache {
    // Stores needed data for a tick
    pub health: f32,
    pub energized_nodes_len: usize,
    pub battery_pos: Vector,
}

impl BotCache {
    pub fn new() -> Self {
        Self {
            health: 0.,
            energized_nodes_len: 0,
            battery_pos: Vector::zero(),
        }
    }
}

#[derive(Clone)]
pub struct Bot {
    pub nodes: Vec<Node>,
    pub links: Vec<Link>,
    pub input_nodes_i_list: Vec<usize>,
    pub output_nodes_i_list: Vec<usize>,
    pub energized_nodes_i_list: Vec<usize>,
    pub inactivity_timer: u16,
    pub alive: bool,
    pub output_state: Vec<f32>,
    pub input_state: Vec<f32>,
    pub health: f32,
    pub total_links_length: f32,
    pub cache: BotCache,
    pub initial_total_links_health: f32,
}

impl Bot {
    pub fn new() -> Bot {
        Bot {
            nodes: Vec::new(),
            links: Vec::new(),
            input_nodes_i_list: Vec::new(),
            output_nodes_i_list: Vec::new(),
            energized_nodes_i_list: Vec::new(),
            inactivity_timer: 0,
            alive: true,
            output_state: Vec::new(),
            input_state: Vec::new(),
            health: 1.,
            total_links_length: 0.,
            cache: BotCache::new(),
            initial_total_links_health: 0.,
        }
    }

    /// Updates the cached data, mostly to methods that need to compare
    /// current state to previous
    fn update_cache(&mut self) {
        self.cache.health = self.health;
        self.cache.energized_nodes_len = self.energized_nodes_i_list.len();
        self.cache.battery_pos = self.nodes[0].pos;
    }

    pub fn setup(&mut self) {
        // Cleaning input state
        self.input_state = vec![0.; self.input_nodes_i_list.len()];
        // Setting up links
        for link in self.links.iter_mut() {
            link.setup(&self.nodes);
        }
        self.energize();
        self.total_links_length = 0.;
        self.initial_total_links_health = 0.;
        // Summing the total length
        for link in self.links.iter() {
            self.total_links_length += link.length;
            self.initial_total_links_health += link.health;
        }
        // Filling the cache
        self.update_cache();
    }

    // FIXME ? maybe put all the update input data in the update
    // argument list
    /// Updates the bot given
    pub fn update(&mut self) {
        // Before updating some data from the current one will be needed.
        // Not every one will be needed outside this function, so
        // the cache is being keep to a minimum by not adding all
        // for example the old_links_len
        self.update_cache();
        let old_links_len = self.links.len();
        // Removing broken links
        for link in self.links.iter_mut() {
            link.update(&self.nodes);
        }
        self.links.retain(|link| !link.broken);
        // if something broke, reenergizes the bot
        if old_links_len != self.links.len() {
            self.energize();
        }
        // Updating the health
        self.update_health();
        // Applying links forces and torques
        for link in self.links.iter() {
            let (force_a, torque_a, force_b, torque_b) = link.get_force_and_torque();
            let mass_a = &mut self.nodes[link.node_a_index].mass;
            mass_a.apply_force(force_a);
            mass_a.apply_torque(torque_a);
            let mass_b = &mut self.nodes[link.node_b_index].mass;
            mass_b.apply_force(force_b);
            mass_b.apply_torque(torque_b);
        }
        // Updating nodes
        for node in self.nodes.iter_mut() {
            node.mass.update()
        }
        if self.alive {
            self.check_pulse();
            self.update_inactivity_timer();
        }
        // Done
    }

    pub fn health_lost_last_tick(&self) -> f32 {
        self.cache.health - self.health
    }

    pub fn nodes_lost_energy_last_tick(&self) -> usize {
        self.cache.energized_nodes_len - self.energized_nodes_i_list.len()
    }

    /// Reports if the bot has moved last tick
    /// If no ticks (updates) where done, than a
    pub fn has_moved_last_tick(&self) -> bool {
        self.nodes[0].pos != self.cache.battery_pos
    }

    fn update_health(&mut self) {
        let mut remaining_health: f32 = 0.;
        for link in self.links.iter_mut() {
            if self.nodes[link.node_a_index].energized {
                remaining_health += link.health;
            }
        }
        self.health = remaining_health / self.initial_total_links_health;
    }

    fn update_inactivity_timer(&mut self) {
        if self.has_moved_last_tick() {
            self.inactivity_timer = 0;
        } else {
            self.inactivity_timer += 1;
        }
    }

    pub fn check_pulse(&mut self) {
        // Checks if any input node is connected to the battery
        self.alive = self
            .input_nodes_i_list
            .iter()
            .any(|x| self.energized_nodes_i_list.contains(&x))
            && self.inactivity_timer < GAME_PARAMETERS.max_inactivity_time;
    }

    pub fn add_link(
        &mut self,
        node_a_index: usize,
        node_b_index: usize,
        node_a_fixation: bool,
        node_b_fixation: bool,
    ) {
        let mass_a = &self.nodes[node_a_index].mass;
        let mass_b = &self.nodes[node_b_index].mass;
        let separation = mass_b.pos - mass_a.pos;
        let length = separation.module();
        let phi_a = angle_between_pm_pi(separation.theta() - mass_a.theta);
        let phi_b = angle_between_pm_pi(separation.theta() + PI - mass_b.theta);
        self.links.push(Link::new(
            node_a_index,
            node_b_index,
            length,
            phi_a,
            phi_b,
            node_a_fixation,
            node_b_fixation,
        ));
    }

    pub fn add_node(&mut self, node: Node) {
        let index = self.nodes.len();
        match node.role {
            NodeRole::Input => {
                self.input_nodes_i_list.push(index);
                self.input_state.push(0.);
            }
            NodeRole::Output => {
                self.output_nodes_i_list.push(index);
                self.output_state.push(0.);
            }
            _ => (),
        }
        self.nodes.push(node);
    }

    pub fn act(&mut self) {
        for (i, d) in self.input_state.iter().enumerate() {
            self.nodes[self.input_nodes_i_list[i]].act(*d);
        }
    }

    pub fn sense(&self, gs: &GameState) -> Vec<f32> {
        let mut out = Vec::new();
        for i in self.output_nodes_i_list.iter() {
            out.push(self.nodes[*i].sense(&gs))
        }
        for i in self.links.iter() {
            out.push(i.get_normalized_health())
        }
        out
    }

    /// Returns the output of the sensing nodes only
    /// FIXME: make sense a more flexible method instead of creating
    /// specialized ones like this
    pub fn sense_nodes_only(&self, gs: &GameState) -> Vec<f32> {
        let mut out = Vec::new();
        for i in self.output_nodes_i_list.iter() {
            out.push(self.nodes[*i].sense(&gs))
        }
        out
    }

    pub fn translate(&mut self, distance: Vector) {
        // Translates (in the move sense) the hole bot.
        for node in self.nodes.iter_mut() {
            node.mass.pos += distance;
        }
    }

    pub fn rotate(&mut self, angle: f32) {
        // Rotates the hole bot around node0
        let pivot = self.nodes[0].mass.pos;
        for node in self.nodes[0..].iter_mut() {
            let new_pos = (node.mass.pos - pivot).rotate(angle) + pivot;
            let new_theta = angle_between_pm_pi(angle + node.mass.theta);
            node.mass.pos = new_pos;
            node.mass.theta = new_theta;
        }
    }

    pub fn apply_internal_collision_forces(&mut self) {
        let n = self.nodes.len();
        for i in 0..n {
            for j in i + 1..n {
                let separation = self.nodes[j].mass.pos - self.nodes[i].mass.pos;
                let separation_mod = separation.module();
                let impact_parameter = self.nodes[j].mass.radius + self.nodes[i].mass.radius;
                if separation_mod < impact_parameter {
                    let mut force = (1. - impact_parameter / separation_mod)
                        * separation
                        * GAME_PARAMETERS.mass_collision_elastic_constant;
                    // damping
                    // FIXME see if this condition is really necessary
                    if self.nodes[j].mass.speed * self.nodes[i].mass.speed < 0. {
                        force = force * 0.8;
                    }
                    self.nodes[i].mass.apply_force(force);
                    self.nodes[j].mass.apply_force(-force);
                } else {
                }
            }
        }
    }

    pub fn energize(&mut self) {
        // Energize only the nodes connected to the battery node (directly or not)
        self.energized_nodes_i_list.clear();
        let groups = self.separate_nodes_in_groups();
        for (i, id) in groups.iter().enumerate() {
            if *id == 0 {
                self.nodes[i].energized = true;
                self.energized_nodes_i_list.push(i);
            } else {
                self.nodes[i].energized = false;
            }
        }
    }

    pub fn single_input(&mut self, node_index: usize, value: f32) {
        // Changes the input state of a single element.
        // node_index is the bot global index of the input node.
        match self
            .input_nodes_i_list
            .iter()
            .position(|x| *x == node_index)
        {
            Some(input_index) => self.input_state[input_index] = value,
            None => (),
        };
    }

    pub fn clear_input_state(&mut self) {
        for v in &mut self.input_state {
            *v = 0.;
        }
    }

    // fn turn_nodes_energy_off(&mut self){
    //     for node in self.nodes.iter_mut(){
    //         node.energized = false;
    //     }
    // }

    // fn recursively_energize(& mut self, node_i: usize) {
    //     self.nodes[node_i].energized = true;
    //     for linked_node_i in self.linked_nodes_i_list[node_i].iter() {
    //         self.recursively_energize(*linked_node_i);
    //     }
    // }

    fn separate_nodes_in_groups(&self) -> Vec<usize> {
        let mut groups: Vec<usize> = (0..self.nodes.len()).collect();
        for link in self.links.iter() {
            let node_a_group = groups[link.node_a_index];
            let node_b_group = groups[link.node_b_index];
            if node_a_group < node_b_group {
                change_group_id(&mut groups, node_b_group, node_a_group);
            } else {
                change_group_id(&mut groups, node_a_group, node_b_group);
            }
        }
        groups
    }

    /// Count bot components
    /// (input nodes, output nodes, total nodes, total links)
    pub fn count_components(&self) -> (u32, u32, u32, u32) {
        let input_count = self
            .nodes
            .iter()
            .filter(|&node| node.act_behavior != ActBehavior::None)
            .count();
        let output_count = self
            .nodes
            .iter()
            .filter(|&node| node.sense_behavior != SenseBehavior::None)
            .count();
        (
            input_count as u32,
            output_count as u32,
            self.nodes.len() as u32,
            self.links.len() as u32,
        )
    }
}

fn change_group_id(groups: &mut Vec<usize>, group_id: usize, new_id: usize) {
    for id in groups.iter_mut() {
        if *id == group_id {
            *id = new_id;
        }
    }
}

pub fn total_links_length(bot: &Bot) -> f32 {
    let mut total_length: f32 = 0.;
    for link in bot.links.iter() {
        total_length += link.length;
    }
    total_length
}

/// Checks if some nods are overlapping for a given bot
pub fn check_for_interference(bot: &Bot) -> bool {
    let n = bot.nodes.len();
    for i in 0..n {
        for j in i + 1..n {
            if check_nodes_overlap(&bot.nodes[i], &bot.nodes[j]) {
                return true;
            }
        }
    }
    false
}

/// Checks if two nodes overlap
fn check_nodes_overlap(node1: &Node, node2: &Node) -> bool {
    let separation = (node1.mass.pos - node2.mass.pos).module();
    let impact_parameter = node1.mass.radius + node2.mass.radius;
    separation < impact_parameter
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_change_group_id() {
        let mut group: Vec<usize> = vec![1, 2, 3, 2];
        change_group_id(&mut group, 2, 1);
        let result: Vec<usize> = vec![1, 1, 3, 1];
        assert_eq!(group, result);
    }
}
