// Nodes related code

use serde::{Deserialize, Serialize};
use std::f32::consts::PI;
use std::ops::Deref;
use std::ptr;

use crate::constants::{ARENA_SIZE, GAME_PARAMETERS};
use crate::mass::Mass;
use crate::rinha::GameState;
use vectors::Vector;

#[derive(Serialize, Deserialize, Clone, Copy)]
pub enum NodeRole {
    // Defines the Node possible roles in the bot
    Input,
    Output,
    DeadWeight,
    Vital,
}

#[derive(Serialize, Deserialize, Clone, Copy, PartialEq)]
pub enum ActBehavior {
    // Possible behaviors when node is activated
    None,
    Traction,
    Torsion,
}

#[derive(Serialize, Deserialize, Clone, Copy, PartialEq)]
pub enum SenseBehavior {
    // Possible behaviors when node is asked to sense the enviroment
    None,
    NodeRadar,
    ArenaRadar,
    AnyRadar,
}

#[derive(Serialize, Deserialize, Clone, Copy)]
pub struct NodeKind {
    // Composition of proprieties that defines the node kind
    pub role: NodeRole,
    pub act_behavior: ActBehavior,
    pub sense_behavior: SenseBehavior,
    // The id defines the kind for external use (to draw in a unique color for example)
    // The possible ids are defined in the constants file
    pub id: char,
}

impl NodeKind {
    pub fn new_simple() -> Self {
        Self {
            role: NodeRole::DeadWeight,
            act_behavior: ActBehavior::None,
            sense_behavior: SenseBehavior::None,
            id: 's',
        }
    }
}

#[derive(Clone)]
pub struct Node {
    // Main node type
    pub mass: Mass,
    pub energized: bool,
    pub act_behavior: ActBehavior,
    pub sense_behavior: SenseBehavior,
    pub role: NodeRole,
    pub kind_id: char,
}

impl Node {
    pub fn new(mass: Mass, kind: NodeKind) -> Self {
        Self {
            mass,
            energized: false,
            act_behavior: kind.act_behavior,
            sense_behavior: kind.sense_behavior,
            role: kind.role,
            kind_id: kind.id,
        }
    }

    pub fn act(&mut self, value: f32) {
        // Makes the node do its thing (defined in the actbehavior)
        use ActBehavior::*;
        match self.act_behavior {
            Traction => traction_node_act(self, value),
            Torsion => torsion_node_act(self, value),
            None => println!("This node does not act!"),
        }
    }

    pub fn sense(&self, gs: &GameState) -> f32 {
        // Sense according to the its sense behavior
        if self.energized {
            self.sense_behavior_parser(gs)
        } else {
            0.
        }
    }

    fn sense_behavior_parser(&self, gs: &GameState) -> f32 {
        use SenseBehavior::*;
        match self.sense_behavior {
            NodeRadar => node_radar_sense(&self, &gs),
            ArenaRadar => node_arena_distance_sense(&self),
            AnyRadar => node_any_sense(&self, &gs),
            None => {
                println!(
                    "This node does not sense! (And should never be asked to in the first place.)"
                );
                0.
            }
        }
    }

    pub fn sense_sight_endpoint(&self) -> Option<Vector> {
        // Computes the end of sight if node have one
        // Useful to draw the "sensing stick"
        match self.sense_behavior {
            SenseBehavior::NodeRadar => {
                Some(self.pos + self.heading_versor() * GAME_PARAMETERS.node_radar_max_range)
            }
            SenseBehavior::ArenaRadar => {
                Some(self.pos + self.heading_versor() * GAME_PARAMETERS.arena_radar_max_range)
            }
            _ => None,
        }
    }
}

impl Deref for Node {
    // Allows access to mass fields directly.
    // Example: node.pos
    type Target = Mass;
    fn deref(&self) -> &Self::Target {
        &self.mass
    }
}

// Acting functions

fn traction_node_act(node: &mut Node, value: f32) {
    if node.energized && value.abs() > 0.5 {
        node.mass.push_forward_limited(value.signum())
    }
    node.mass.apply_lateral_friction_force()
}

fn torsion_node_act(node: &mut Node, value: f32) {
    if node.energized && value.abs() > 0.5 {
        node.mass.apply_torque_limited(value.signum())
    }
}

// Sensing functions

fn node_radar_sense(node: &Node, game_state: &GameState) -> f32 {
    // Detects nodes in a particular direction
    // Output value inversely proportional to the relative distance
    //(to the sense range). Ex.: close -> ~1., far -> 0.
    let node_hit = node_hit_distance(&node, &game_state);
    match node_hit {
        Some(x) => 1. - x / GAME_PARAMETERS.arena_radar_max_range,
        None => 0.,
    }
}

fn node_hit_distance(node_radar: &Node, gs: &GameState) -> Option<f32> {
    // Computes the minimal distance from the nodes in the sight line
    //
    // Pre computing the sin and cos to be used multiple times
    let cos_theta = node_radar.theta.cos();
    let sin_theta = node_radar.theta.sin();
    let mut hit_distances = Vec::new();
    for node in gs
        .bots
        // Does not distinguish nodes ownership
        .iter_all_nodes()
        .filter(|x| !ptr::eq(*x, node_radar))
    {
        let dx = node.pos.x - node_radar.pos.x;
        let dy = node.pos.y - node_radar.pos.y;
        let x_trans = cos_theta * dx + sin_theta * dy;
        if x_trans >= 0. {
            let y_trans = -sin_theta * dx + cos_theta * dy;
            if y_trans.abs() < node.radius && x_trans < GAME_PARAMETERS.arena_radar_max_range {
                let distance = x_trans; // - (node.radius.powi(2) - y_trans.powi(2)).sqrt();
                hit_distances.push(distance);
            };
        };
    }
    // returns the closest hit distance found
    hit_distances
        .into_iter()
        .min_by(|x, y| x.partial_cmp(y).unwrap())
}

fn node_arena_distance_sense(out_node: &Node) -> f32 {
    //Get the distance to the arena in the line straight ahead.
    //
    let cos_theta = out_node.theta.cos();
    let sin_theta = out_node.theta.sin();
    let dist_x = match out_node.theta {
        // Distance to the left wall
        t if t > -PI / 2. && t < PI / 2. => (ARENA_SIZE.0 - out_node.pos.x) / cos_theta,
        // Distance to the right wall
        t if t > PI / 2. || t < -PI / 2. => -out_node.pos.x / cos_theta,
        _ => 1.,
    };
    let dist_y = match out_node.theta {
        // Distance to the top wall
        t if t > 0. && t < PI => (ARENA_SIZE.1 - out_node.pos.y) / sin_theta,
        // Distance to the bottom wall
        t if t < 0. && t > -PI => -out_node.pos.y / sin_theta,
        _ => 1.,
    };
    // Horizontal or vertical wall is closer?
    if dist_x > dist_y {
        1. - clamp(dist_y / GAME_PARAMETERS.arena_radar_max_range)
    } else {
        1. - clamp(dist_x / GAME_PARAMETERS.arena_radar_max_range)
    }
}

/// Returns the sensing output of the nearest obstacle
fn node_any_sense(node_radar: &Node, gs: &GameState) -> f32 {
    let nearest_node = node_radar_sense(node_radar, gs);
    let nearest_arena = node_arena_distance_sense(node_radar);
    nearest_node.max(nearest_arena)
}

fn clamp(value: f32) -> f32 {
    // Clamps the value between 0. and 1.
    match value {
        x if x < 0. => 0.,
        x if x > 1. => 1.,
        x => x,
    }
}
