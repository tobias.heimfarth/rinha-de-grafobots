use ggez::{ContextBuilder, GameResult};
use ggez::event::{self};

mod drawing_ggez;
use drawing_ggez::GameStateGGEZ;

use rusty_rinha::bot_file_io;
use rusty_rinha::rinha::GameState;



fn main() -> GameResult {
    // Criando um contexto e um loop de eventos
    let (ctx, events_loop) = ContextBuilder::new("rinha_de_grafobots", "Tobias Heimfarth")
    .window_setup(ggez::conf::WindowSetup::default().title("Rinha de Grafobots"))
    .window_mode(ggez::conf::WindowMode::default().dimensions(drawing_ggez::SCREEN_SIZE.0, drawing_ggez::SCREEN_SIZE.1))
    // And finally we attempt to build the context and create the window. If it fails, we panic with the message
    // "Failed to build ggez context"
    .build()?;
    // Creating bots and starting the game state
    //let bot_a = Bot::create_simple_test_bot();
    let bot_a = bot_file_io::load_bot("bot_test.txt");
    let bot_b = bot_file_io::load_bot("bot_test.txt");
    //let bot_b = Bot::create_simple_test_bot();
    let mut rinha = GameStateGGEZ::new(bot_a, bot_b);
    //let mut rinha = GameState::new(bot_a, bot_b);
    rinha.setup();
    // Run!
    rinha.run_headless_loop();
    //event::run(ctx, events_loop, rinha)
    Ok(())
}
