use std::ops::{Deref, DerefMut};

use ggez::event::{self, EventHandler, KeyCode, KeyMods};
use ggez::graphics;
use ggez::{Context, GameResult, GameError};

use rusty_rinha::bot::Bot;
use vectors::Vector;
use rusty_rinha::node::{Node, SenseBehavior};
use rusty_rinha::rinha::GameState;
use rusty_rinha::link::Link;

use rusty_rinha::constants::{
    ARENA_SIZE,
    GAME_PARAMETERS,
};

pub const SCREEN_SIZE: (f32, f32) = ARENA_SIZE;

fn world_to_screen_coords(point: &Vector) -> Vector {
    let x = point.x;
    let y = SCREEN_SIZE.1 - point.y;
    Vector::new(x, y)
}

trait Actor {
    fn draw(&self, ctx: &mut Context) -> ggez::GameResult;
}

impl Actor for Node {
    fn draw(&self, ctx: &mut Context) -> ggez::GameResult<()> {
        let _pos = world_to_screen_coords(&self.pos);
        let circle = graphics::Mesh::new_circle(
            ctx,
            graphics::DrawMode::fill(),
            mint::Point2 {
                x: _pos.x,
                y: _pos.y,
            },
            self.radius,
            0.1,
            graphics::Color::new(1.0, 0.0, 0.0, 1.0),
        )?;
        graphics::draw(ctx, &circle, graphics::DrawParam::default())?;
        // Marcador de angulo
        draw_line_from_node(&self, self.radius, ctx)?;
        let a = match self.sense_behavior {
            SenseBehavior::ArenaRadar => draw_line_from_node(&self, GAME_PARAMETERS.arena_radar_max_range , ctx),
            SenseBehavior::NodeRadar => draw_line_from_node(&self, GAME_PARAMETERS.node_radar_max_range , ctx),
            _ =>  Ok(()),
        };
        a?;
        Ok(())
    }
}


fn draw_line_from_node(node: & Node, lenght: f32, ctx: &mut Context ) -> ggez::GameResult<()>  {
    let line_end = node.pos + Vector::new(lenght,0.).rotate(node.theta);
    let line_end_screen_coordinates = world_to_screen_coords(&line_end);
    let line_ini_world_pos = world_to_screen_coords(&node.pos);
    let line = graphics::Mesh::new_line(
        ctx,
        &[line_ini_world_pos.to_mint(), line_end_screen_coordinates.to_mint()],
        3.0,
        graphics::Color::new(0.0, 0.0, 1.0, 1.0),
    )?;
    graphics::draw(ctx, &line, graphics::DrawParam::default())?;
    Ok(())
}


impl Actor for Bot {
    fn draw(&self, ctx: &mut Context) -> ggez::GameResult<()> {
        for node in self.nodes.iter() {
            node.draw(ctx)?;
        }
        for link in self.links.iter() {
            link.draw(&self, ctx)?;
        }
        Ok(())
    }
}

trait ActorLink {
    fn draw(&self, bot: &Bot, ctx: &mut Context) -> ggez::GameResult;
}

impl ActorLink for Link {
    fn draw(&self, bot: &Bot, ctx: &mut Context) -> ggez::GameResult<()> {
        let pos_a = world_to_screen_coords(&bot.nodes[self.node_a_index].mass.pos);
        let pos_b = world_to_screen_coords(&bot.nodes[self.node_b_index].mass.pos);
        let line = graphics::Mesh::new_line(
            ctx,
            &[
                mint::Point2 {
                    x: pos_a.x,
                    y: pos_a.y,
                },
                mint::Point2 {
                    x: pos_b.x,
                    y: pos_b.y,
                },
            ],
            3.0,
            graphics::Color::new(1.0, 1.0, 1.0, 0.5),
        )?;
        graphics::draw(ctx, &line, graphics::DrawParam::default())?;
        Ok(())
    }
}

/// **********************************************************************
/// Now we implement the `EventHandler` trait from `ggez::event`, which provides
/// ggez with callbacks for updating and drawing our game, as well as
/// handling input events.
/// **********************************************************************


pub struct GameStateGGEZ{
    rinha : GameState
}

impl GameStateGGEZ{
    pub fn new(bot_a: Bot, bot_b: Bot) -> Self {
        Self {
            rinha: GameState::new(bot_a,bot_b),
        }
    }
}

impl Deref for GameStateGGEZ{
    // Allows access to mass fields directly.
    // Example: node.pos
    type Target = GameState;
    fn deref(&self) -> &Self::Target {
        &self.rinha
    }
}

impl DerefMut for GameStateGGEZ{
    // Allows access to mass fields directly.
    // Example: node.pos
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.rinha
    }
}


impl EventHandler<GameError>  for GameStateGGEZ {
    fn update(&mut self, _ctx: &mut Context) -> GameResult<()> {
        self.rinha.tick();
        if self.tick_counter%10000 == 0 {
            //println!("{:?}",self.rinha.bot_a.sense(&self.rinha));
            //println!("{:?}",self.rinha.bot_a.output_nodes_i_list);
            //println!("{}",self.tick_counter);
        }
        //println!("{:?}",self.rinha.bot_a.sense(&self.rinha));
        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        if true {
            graphics::clear(ctx, graphics::Color::new(0.0, 0.0, 0.0, 1.0));
            self.rinha.bot_a.draw(ctx)?;
            self.rinha.bot_b.draw(ctx)?;
            //println!("FPS: {}", ggez::timer::fps(ctx));
            graphics::present(ctx)
        } else {
            Ok(())
        }
    }

    fn key_down_event(
        &mut self,
        ctx: &mut Context,
        keycode: KeyCode,
        _keymod: KeyMods,
        _repeat: bool,
    ) {
        match keycode {
            KeyCode::Right => {
                self.rinha.bot_a.single_input(4,1.);
            }
            KeyCode::Left => {
                self.rinha.bot_a.single_input(5,1.)
            }
            KeyCode::Down => {
                self.rinha.bot_a.single_input(9,1.)
            }
            // KeyCode::Right => {
            //     self.bot_a.nodes[0].apply_force(Vector {x: 1., y: 0.});
            // }
            // KeyCode::Down => {
            //     self.bot_a.nodes[0].apply_force(Vector {x: 0., y: -1.});
            // }
            KeyCode::Escape => event::quit(ctx),
            _ => (), // Do nothing
        }
    }

    fn key_up_event(&mut self, _ctx: &mut Context, keycode: KeyCode, _keymod: KeyMods) {
        match keycode {
            KeyCode::Right => {
                self.rinha.bot_a.single_input(4,0.);
            }
            KeyCode::Left => {
                self.rinha.bot_a.single_input(5,0.)
            }
            KeyCode::Down => {
                self.rinha.bot_a.single_input(9,0.)
            }
            _ => (), // Do nothing
        }
    }
}
