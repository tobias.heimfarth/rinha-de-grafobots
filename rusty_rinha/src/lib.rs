// Imports everything to be used as a library
pub mod bot_file_io;
pub mod constants;
pub mod node;
//pub mod vector;
pub mod mass;
pub mod link;
pub mod rinha;
pub mod bot;
pub mod mutations;
pub mod bot_blueprints;

mod misc;


