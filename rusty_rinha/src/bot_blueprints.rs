use crate::constants::NODE_TYPES;
use rand::prelude::*;
use serde::{Deserialize, Serialize};
use std::f32::consts::PI;
use std::fs::File;
use std::io::{self, Read};

use crate::bot::Bot;
use crate::mass::Mass;
use crate::node::{ActBehavior, Node, NodeKind, SenseBehavior};
use crate::rinha::float;
use vectors::Vector;

#[derive(Serialize, Deserialize, Clone)]
/// Describes a node with only the essential information
/// To be used in the mutation of the bots
pub struct NodeBlueprint {
    pub pos: Vector,
    pub mass: float,
    pub radius: float,
    pub theta: float,
    pub kind: NodeKind,
}

#[derive(Serialize, Deserialize, Clone)]
/// Describes a link
/// To be used in the mutation of the bots
pub struct LinkBlueprint {
    pub node_a_index: usize,
    pub node_a_fixation: bool,
    pub node_b_index: usize,
    pub node_b_fixation: bool,
}

#[derive(Serialize, Deserialize, Clone)]
/// Describes a bot morphology
pub struct BotBlueprint {
    pub nodes: Vec<NodeBlueprint>,
    pub links: Vec<LinkBlueprint>,
}

impl BotBlueprint {
    pub fn new_empty() -> Self {
        Self {
            nodes: Vec::new(),
            links: Vec::new(),
        }
    }

    /// Generates a Bot from the blueprint
    pub fn gen_bot(&self) -> Bot {
        let mut bot = Bot::new();
        for node in self.nodes.iter() {
            let mass = Mass::new(node.pos, node.mass, node.radius, node.theta);
            bot.add_node(Node::new(mass, node.kind))
        }
        for link in self.links.iter() {
            bot.add_link(
                link.node_a_index,
                link.node_b_index,
                link.node_a_fixation,
                link.node_b_fixation,
            )
        }
        bot
    }

    /// Count bot blueprint components
    /// (input nodes, output nodes, total nodes, total links)
    pub fn count_components(&self) -> (u32, u32, u32, u32) {
        let input_count = self
            .nodes
            .iter()
            .filter(|&node| node.kind.act_behavior != ActBehavior::None)
            .count();
        let output_count = self
            .nodes
            .iter()
            .filter(|&node| node.kind.sense_behavior != SenseBehavior::None)
            .count();
        (
            input_count as u32,
            output_count as u32,
            self.nodes.len() as u32,
            self.links.len() as u32,
        )
    }

    /// Save to file json
    pub fn save_file(&self, file_name: &str) {
        let f = File::create(file_name).unwrap();
        serde_json::to_writer(&f, self).ok();
        //let serialized = serde_json::to_string(&self).unwrap();
        //println!("serialized = {}", serialized);
    }

    /// Loads a json file
    pub fn load_file(file_name: &str) -> Self {
        let f = File::open(file_name).unwrap();
        serde_json::from_reader(&f).unwrap()
        //let serialized = serde_json::to_string(&self).unwrap();
        //println!("serialized = {}", serialized);
    }
}

pub fn load_bot(filename: &str) -> BotBlueprint {
    // Loads a file to construct a bot
    // FIXME bites if sees a blank line...
    let whole_file = file_to_string(filename).unwrap();
    let wbyl = words_by_line(&whole_file);
    let mut links = Vec::new();
    let mut nodes = Vec::new();
    for line in wbyl.iter() {
        let component = parse_line(&line);
        match component {
            Some(BotComponent::Link(link_bp)) => links.push(link_bp),
            Some(BotComponent::Node(node)) => nodes.push(node),
            _ => {}
        }
    }
    BotBlueprint { nodes, links }
}

fn file_to_string(s: &str) -> io::Result<String> {
    let mut file = File::open(s)?;
    let mut s = String::new();
    file.read_to_string(&mut s)?;
    Ok(s)
}

fn words_by_line<'a>(s: &'a str) -> Vec<Vec<&'a str>> {
    s.lines()
        .map(|line| line.split_whitespace().collect())
        .collect()
}

enum BotComponent {
    Link(LinkBlueprint),
    Node(NodeBlueprint),
}

fn parse_line(line: &Vec<&str>) -> Option<BotComponent> {
    // Skipping empty_lines
    if line.is_empty() {
        return None;
    };
    let kind = line[0];
    match kind {
        "#" => None,
        "link" => Some(parse_link(&line[..])),
        "node" => Some(parse_node(&line[..])),
        _ => None,
    }
}

fn parse_node(line: &[&str]) -> BotComponent {
    // kind
    //let kind = match line[1].as_ref()
    let kind = match NODE_TYPES.get(line[1]).cloned() {
        Some(kind) => kind,
        _ => panic!("Deu problema lendo as paradas por aqui ->: {}", line[1]),
    };
    let x = line[2].parse::<f32>().unwrap();
    let y = line[3].parse::<f32>().unwrap();
    let mass = line[4].parse::<f32>().unwrap();
    let radius = line[5].parse::<f32>().unwrap();
    let theta = line[6].parse::<f32>().unwrap();
    //let mass = Mass::new(Vector::new(x, y), mass, radius, theta);
    BotComponent::Node(NodeBlueprint {
        pos: Vector::new(x, y),
        mass,
        radius,
        theta,
        kind,
    })
}

fn parse_link(line: &[&str]) -> BotComponent {
    // kind
    let node_a_index = line[1].parse::<usize>().unwrap();
    let node_a_fixation = line[2].parse::<bool>().unwrap();
    let node_b_index = line[3].parse::<usize>().unwrap();
    let node_b_fixation = line[4].parse::<bool>().unwrap();
    BotComponent::Link(LinkBlueprint {
        node_a_index,
        node_a_fixation,
        node_b_index,
        node_b_fixation,
    })
}
