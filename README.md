# Rinha de Grafobots
A game where graph structured bots evolve (behavior and morphology) to fight in an arena (like BattleBots). All the dynamics is based on a simulated simplified physics.

Most docs are in portuguese for now.

Jogo educativo onde autômatos constituídos de nodos e links (grafobots) duelam em uma arena com dinâmica baseado na física. Os grafobots podem ser evoluídos tanto na sua morfologia quanto no seu comportamento utilizando redes neurais.


![Current state](/docs/screen.png "Estado atual.")

[![Sample AI fight](/docs/video_yt.png)](https://youtu.be/zo0ADGqZkl0 "Exemplo de rinha entre IAs")

