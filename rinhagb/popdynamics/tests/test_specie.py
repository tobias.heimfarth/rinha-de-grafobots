from rinhagb.popdynamics.individuals import (
    MorphIndividual,
    BehavIndividual,
    GrafobotIndividual,
    GrafobotIndividualFactory,
)

from rinhagb.popdynamics.species import (
    SpecieFactory,
    Specie
)



# Testing SpecieFactory
# def test_default_factory() -> None:
#     specie = SpecieFactory().build()
#     assert isinstance(specie, Specie)
#     assert specie.individuals == []
#     assert specie.id == "SPECIE0"

