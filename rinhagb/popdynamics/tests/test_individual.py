from rinhagb.evomorph.tests.test_network_elements import fix_network
import pytest
import random

from rinhagb.popdynamics.individuals import (
    MorphIndividual,
    BehavIndividual,
    GrafobotIndividual,
    GrafobotIndividualFactory,
    GBIndividualIdManager
)
from rinhagb.popdynamics.mutators import basic_morph_mutator, basic_behav_mutator

from rinhagb.evomorph.network_elements import NetElementsIdManager
from rinhagb.id_manager import IdManager
import rinhagb.popdynamics.individuals as individuals
import rinhagb.resources.librinha_rust2py as rgb

random.seed(0)

# Testing GrafobotIndividualFactory


def test_grafobot_factory_default() -> None:
    id_man = GBIndividualIdManager()
    test_gb = (
        GrafobotIndividualFactory(id_man)
        .build()
    )
    assert (isinstance(test_gb.behav_ind, BehavIndividual))
    assert (isinstance(test_gb.morph_ind, MorphIndividual))
    assert (test_gb.bot_id == 0)


def test_grafobot_factory_morph_mutations() -> None:
    id_man = GBIndividualIdManager()
    default_gb = GrafobotIndividualFactory(id_man).build()
    mutated_gb = (
        GrafobotIndividualFactory(id_man)
        .build()
    )
    for i in range(100):
        mutated_gb.mutate(
            id_man,
            basic_morph_mutator,
            basic_behav_mutator
        )
    # The network starts with a base block and the battery node, no links.
    assert (default_gb.morph_ind.net.shape == (1, 1, 0))
    assert (default_gb.bot_id == 0)
    assert (mutated_gb.bot_id == 1)
    assert (default_gb.morph_ind.net.shape != mutated_gb.morph_ind.net.shape)


def test_grafobot_factory_behav_mutations() -> None:
    id_man = GBIndividualIdManager()
    fac = GrafobotIndividualFactory(id_man)
    interposer_size = fac.interposer_size
    default_gb = fac.build()
    mutated_gb = (
        GrafobotIndividualFactory(id_man)
        .fill_behav_network()
        .build()
    )
    assert (
        default_gb.behav_ind.pybann.count_components()
        == (1, 0, interposer_size+1, 0)
    )
    assert (
        default_gb.behav_ind.pybann.count_components()
        != mutated_gb.behav_ind.pybann.count_components()
    )


def test_grafobot_factory_set_behav_innov() -> None:
    id_man = GBIndividualIdManager()
    gb = (
        GrafobotIndividualFactory(id_man)
        .set_behav_innov(50)
        .build()
    )
    innovs = gb.behav_ind.pybann.list_innovs()
    assert (innovs[0] == 50)


def test_grafobot_factory_set_interposer_size() -> None:
    id_man = GBIndividualIdManager()
    fac = GrafobotIndividualFactory(id_man).set_interposer_size(13)
    assert (fac.interposer_size == 13)
    gb = fac.build()
    assert (gb.behav_ind.pybann.get_main_block_shape()[2] == 13)


def test_remap() -> None:
    old_morph_i = [2, 9, 3, 7]
    new_morph_i = [7, 9, 18, 20, 25]
    renew_data = individuals._remap(old_morph_i, new_morph_i)
    assert (renew_data.to_add == [18, 20, 25])
    assert (renew_data.to_remove == [2, 0])
    assert (renew_data.new_map == [9, 7, 18, 20, 25])


@pytest.fixture
def fix_behav_network() -> rgb.PyBannNetwork:
    io = (2, 3)
    interposer_size = 10
    hidden_blocks_size = 15
    hidden_blocks_count = 4
    pybann = (
        rgb
        .PyBannNetworkFactory(io[1], io[0], interposer_size)
        .set_hidden_blocks(hidden_blocks_count, hidden_blocks_size)
        .set_innov_start(0)
        .build()
    )
    return pybann


def test_match_behav_io_to_morph(fix_network, fix_behav_network) -> None:
    old_map = individuals.NetsIOMap(
        morph_inputs=[8, 3, 2],
        morph_outputs=[6, 1]
    )
    new_pybaan, new_map = individuals.match_behav_io_to_morph(
        morph_net=fix_network,
        behav_net=fix_behav_network,
        old_nets_io_map=old_map,
    )
    assert new_map.morph_inputs == [3, 2]
    assert new_map.morph_outputs == [1]
    # Reports the number of input, outputs, total number of nodes and links
    elements = new_pybaan.count_components()
    assert elements[0] == 2
    assert elements[1] == 1


# Testing BehavIndividual

@pytest.fixture
def behav_ind():
    # Creating the pybann
    io = (3, 5)
    interposer_size = 10
    hidden_blocks_size = 15
    hidden_blocks_count = 4
    pybann = (
        rgb
        .PyBannNetworkFactory(io[1], io[0], interposer_size)
        .set_hidden_blocks(hidden_blocks_count, hidden_blocks_size)
        .set_innov_start(0)
        .build()
    )
    return BehavIndividual(pybann)
