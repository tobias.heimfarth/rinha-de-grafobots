"""
Set of ready-to-use mutators
"""

import rinhagb.resources.librinha_rust2py as rgb
import rinhagb.evomorph.mutations as mutations
import rinhagb.evomorph.network_elements as ene
from .individuals import (
    BehavMutator,
    MorphMutator,
    GBIndividualIdManager,
    MorphIndividual,
    BehavIndividual,
)


def basic_morph_mutator(
        id_man: ene.NetElementsIdManager,
        ind: MorphIndividual,
) -> None:
    '''
    Basic round of mutations to the morphology to be applied every iteration
    '''
    muts = [
        mutations.AddBlock(id_man, times=1, probability=0.1),
        mutations.AddSlot(id_man, times=1, probability=0.2),
        mutations.AddNode(id_man, times=3, probability=0.2),
        mutations.PerturbRandomNodePosition(times=1, probability=0.2),
        mutations.ChangeNodeCharge(times=30, probability=0.1, change_rate= 0.1),
        mutations.RemoveSlot(times=1, probability=0.5),
        mutations.RemoveBlock(times=1, probability=0.5),
        mutations.DuplicateBlock(id_man, times=1, probability=0.01),
    ]
    mutations.apply_netmutations(ind.net, muts)


def basic_behav_mutator(
        ind: BehavIndividual
) -> None:
    '''
    Basic round of mutations to a Bann to be applied every iteration
    '''
    # pybann.add_node(0.01)
    ind.pybann.add_connection(0.5)
    ind.pybann.add_mix_node(0.1)
    ind.pybann.perturbe_weights(0.05, 0.1)
    ind.pybann.perturbe_biases(0.05, 0.1)
    ind.pybann.remove_connection(0.65)
    for i in range(10):
        ind.pybann.perturbe_single_weight(0.8, 0.5)
        ind.pybann.perturbe_single_bias(0.8, 0.5)
