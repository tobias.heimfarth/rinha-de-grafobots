from config_parser import RinhaConfigParser, print_bot_info
from arcade_rinha import ArcadeRinha
from graphics_arcade import GameWindow, RinhaView, MenuView
import arcade
from net_builder import construct_aann
import neural_network as nn
import librinha_rust2py as rgb
import sys
sys.path.append('../../')
sys.path.append('../../../')

#import libaann as an


# Definições da janela
SCREEN_TITLE = "Rinha de Grafobots"
SCREEN_WIDTH = 1120+64
SCREEN_HEIGHT = 640+64
#
UPDATE_RATE = 0.0166


#bot_a = rgb.BotDeliver("tupa.txt")
#bot_b = rgb.BotDeliver("givis.txt")
#bot_b = rgb.BotDeliver("tupa.txt")
bot_a = rgb.PyBotBlueprint.load_file("champ_2_exemplobot_blueprint.json").gen_bot()
#bot_b = rgb.PyBotBlueprint("tupa.txt").gen_bot()
bot_b = rgb.PyBotBlueprint.load_file("champ_9_givis_blueprint.json").gen_bot()
#bot_b = rgb.BotDeliver("./lazybot.txt")
rusty_rinha = rgb.RinhaDeGrafobots(bot_a, bot_b)
#print(rusty_rinha = rgb.PyBannNetwork.get_similarity(rgb.PyBannNetworkFactory.load_file('tupa_best_gen100.json'),rgb.PyBannNetworkFactory.load_file('tupa_best_gen100.json')))
# rgb.PyBannNetworkFactory.load_file('champ_9_exemplobot.json').simplify().to_dot('net1simp.dot')
net1 = rgb.PyBannNetworkFactory.load_file('champ_2_exemplobot.json').simplify().to_pyaann()
net2 = rgb.PyBannNetworkFactory.load_file('champ_9_givis.json').simplify().to_pyaann()
# rgb.PyBannNetworkFactory.load_file('champ_9_exemplobot.json').to_dot('net1.dot')
# rgb.PyBannNetworkFactory.load_file('botvinik_best_gen400.json').to_dot('net1.dot')
#net2 = rgb.PyBannNetworkFactory.load_file('champ_3_tupa.json').simplify().to_pyaann()
#net2 = rgb.PyBannNetworkFactory.load_file('champ_3_botvinik_anyradar.json').simplify().to_pyaann()
# rgb.PyBannNetworkFactory.load_file('givis_best_gen400.json').to_dot('net4.dot')
#net2 = rgb.PyBannNetworkFactory(0,1,1).build().to_pyaann()


print_bot_info(bot_a, 'a')
print_bot_info(bot_b, 'b')

# Criando a ligação entre o jogo em rust e os gráficos em arcade
arcade_rinha = ArcadeRinha(
    rusty_rinha, None, background='../../resources/fundo_sapo.png')
arcade_rinha.neural_network_a, arcade_rinha.neural_network_b = net1, net2
# Dançando a dança do Arcade...
window = GameWindow(SCREEN_WIDTH,
                    SCREEN_HEIGHT,
                    SCREEN_TITLE,
                    update_rate=UPDATE_RATE,
                    antialiasing=True,
                    fullscreen=False)
rinha_view = RinhaView(arcade_rinha)
menu = MenuView(rinha_view)
window.show_view(menu)

arcade.run()
