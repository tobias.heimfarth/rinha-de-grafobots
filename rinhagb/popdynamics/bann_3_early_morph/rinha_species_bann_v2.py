import librinha_rust2py as rgb
from neural_network import species
import matplotlib.pyplot as plt
import numpy as np
from copy import copy, deepcopy
from dataclasses import dataclass
import os
import random
from math import ceil, exp
import sys
sys.path.append('../../')
sys.path.append('../../../')


SPECIATING_DISTANCE = 0.05


def mutate(pybann):
    '''
    Round of mutations to a Bann to be applyed every iteration
    '''
    # pybann.add_node(0.01)
    pybann.add_connection(0.5)
    pybann.add_mix_node(0.1)
    pybann.perturbe_weights(0.05, 0.1)
    pybann.perturbe_biases(0.05, 0.1)
    pybann.remove_connection(0.65)
    for i in range(10):
        pybann.perturbe_single_weight(0.8, 0.5)
        pybann.perturbe_single_bias(0.8, 0.5)


def mutate_morphology(pybot_blueprint):
    '''
    Round of mutations to the PyBotBlueprint
    '''
    pybot_blueprint.perturb_links_lenghts(0.5, 0.2) #(chance, factor)


class NetIndividual:
    """
    Class with the behavior network (Bann)
    """

    def __init__(self, pybann):
        self.pybann = pybann
        self._pyaann = None

    @property
    def pyaann(self):
        if self._pyaann is None:
            self._pyaann = self.pybann.clone().simplify().to_pyaann()
            #self._pyaann = self.pybann.clone().to_pyaann()
            return self._pyaann
        else:
            return self._pyaann

    def save(self, filename):
        self.pybann.save_file(filename)

    def clear(self):
        self._pyaann = None

    def copy(self):
        new = copy(self)
        new.pybann = self.pybann.clone()
        return new

    def mutate(self):
        mutate(self.pybann)

    def report(self):
        count = self.pybann.count_components()
        r = (" {} inputs\n {} outputs\n {} total nodes\n {} total connections").format(*count)
        print(r)
        count = self.pyaann.count_components()
        r = (" {} inputs\n {} outputs\n {} total nodes\n {} total connections").format(*count)
        print(r)


class GrafobotIndividual:
    """
    Class with a individual grafobot data
    """

    def __init__(self,
                 net: NetIndividual,
                 bot_blueprint: rgb.PyBotBlueprint,
                 bot_id
                 ):
        self.net = net
        self.bot_blueprint = bot_blueprint
        self.bot = self.bot_blueprint.gen_bot()
        self.bot_id = bot_id
        self.points: float = 0
        self.wins: int = 0

    def compute_fitness(self):
        self.fitness = self.points

    def save_txt(self, prefix):
        self.net.save('./'+prefix+self.bot_id+'.json')
        self.bot_blueprint.save_file('./'+prefix+self.bot_id+'_blueprint.json')

    def clear(self):
        self.points = 0
        self.wins = 0
        self.net.clear()

    def copy(self):
        new = copy(self)
        new.net = self.net.copy()
        new.bot = new.bot_blueprint.gen_bot()
        return new

    def mutate(self):
        self.net.mutate()
        mutate_morphology(self.bot_blueprint)
        self.clear()
        self.bot = self.bot_blueprint.gen_bot()
    
    def __repr__(self):
        return f"{self.bot_id},{self.points} points, {self.wins} wins."
        


def mate_networks(net1, net2):
    """
    Mates two NetIndividuals
    """
    child = net1.copy()
    new_pybann = rgb.PyBannNetwork.mate(net1.pybann, net2.pybann)
    child.clear()
    child.pybann = new_pybann
    return child


def mate_grafobots(gb1, gb2):
    # Sanaty check
    assert gb1.bot_id == gb2.bot_id, 'Could not mate different id grafobots.'
    net = mate_networks(gb1.net, gb2.net)
    # FIXME for now just using the morphology of a random parent
    if random.random() > 0.5:
        bot_bp = gb1.bot_blueprint.copy()
    else:
        bot_bp = gb2.bot_blueprint.copy()
    return GrafobotIndividual(net, bot_bp, gb1.bot_id)


class SpecieIndividual(GrafobotIndividual):
    '''
    Extending the GrafobotIndividual to deal with the Specie
    '''

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.specie_distance = 0.


def belong_to_specie(ind: GrafobotIndividual, references: [GrafobotIndividual]):
    '''
    Given a list of reference networks, returns index for wich the
    also ginven individual is the nearest, and also the distance.
    '''
    #FIXME include a morphological contribution
    d = []
    for ref in references:
        d.append(network_distance(ind, ref))
    d_order = np.argsort(d)
    return d_order[0], d[d_order[0]]


def network_distance(ind1, ind2):
    '''
    The "distance" between 2 networks. Based on the similarity defined
    in the rust part of things
    '''
    similarity = rgb.PyBannNetwork.get_similarity(
        ind1.net.pybann, ind2.net.pybann)
    # Assuming the distance the inverse of the similarity (+1 to avoid
    # zero divisions, that should not matter as the typical values are big)
    if similarity[1] == 0:
        return 0
    return (similarity[1] - similarity[0])/similarity[1]


class SpecieLogs:
    '''
    Class to store the logs mainly for plotting
    '''

    def __init__(self):
        self.population: [int] = []
        self.max_points: [float] = []
        self.max_wins: [int] = []

    def remove_last(self):
        del self.population[-1]
        del self.max_points[-1]
        del self.max_wins[-1]

    def get_max_points(self):
        if self.max_points:
            return self.max_points[-1]
        else:
            return 0

    def get_max_wins(self):
        if self.max_points:
            return self.max_wins[-1]
        else:
            return 0


class Specie:
    def __init__(self, id):
        self.individuals: [SpecieIndividual] = []
        self.stagnated_clock = 0
        self.generations_alive_clock = 0
        self.reproduce = True
        self.representative = None
        self.logs = SpecieLogs()
        self.gen = 0
        self.max_points = 0
        # Specie unique id
        self.id = id
        self.mutation_probability = 0.5

    def _compute_specie_network_distance(self):
        '''
        Computes the genetic average distance for each individual
        compared to the specie
        '''
        # Reseting the individuals distance
        for ind in self.individuals:
            ind.specie_distance = 0.
        for i, ind_i in enumerate(self.individuals):
            for ind_j in self.individuals[i+1:]:
                d = network_distance(ind_i, ind_j)
                ind_i.specie_distance += d
                ind_j.specie_distance += d
        # Normalizing
        if len(self.individuals) > 1:
            for ind in self.individuals:
                ind.specie_distance = ind.specie_distance / \
                    (len(self.individuals)-1)

    def print_analitics(self):
        description = "Specie: {}, bot: {}".format(
            self.id, self.individuals[0].bot_id)
        data = "Population: {}, max. points {} max. wins {}".format(len(
            self.individuals), self.logs.get_max_points(), self.logs.get_max_wins())
        dists = [i.specie_distance for i in self.individuals]
        distance = "Genetic distance max/min: {}/{}".format(
            max(dists), min(dists))
        print(description + '\n' + data + '\n' + distance)

    def start_fresh(self, pop_size, bot_bp: rgb.PyBotBlueprint, bot_id: str):
        """
        Start fresh
        """
        bot = bot_bp.gen_bot()
        io = list(bot.get_io_numbers())
        # adds a dummy input if there is none in order to avoid
        # mutations failling
        if io[1] == 0:
            io[1] = 1
        # Creating first individual:
        net = NetIndividual(
            rgb
            .PyBannNetworkFactory(io[1], io[0], 5)
            .set_hidden_blocks(10, 15)
            .set_innov_start(0)
            .build()
        )
        for _ in range(1000):
            net.pybann.add_connection(1.)
            net.pybann.perturbe_weights(0.05, 0.5)
            net.pybann.perturbe_biases(0.05, 0.5)
        individual = SpecieIndividual(net, bot_bp, bot_id)
        self.individuals.append(individual)
        # Adding others
        for _ in range(pop_size-1):
            new_ind = individual.copy()
            # Initial mutations
            for _ in range(2):
                new_ind.mutate()
            self.individuals.append(new_ind)
        self._update_logs()

    def _find_representative(self):
        '''
        Looks for the individual with the lower distance with respect
        with all others 
        '''
        d_arg = np.argsort([ind.specie_distance for ind in self.individuals])
        #print([d[i] for i in d_arg])
        self.representative = self.individuals[d_arg[0]]

    def tick(self, N):
        """
        Main call for generation tick
        """
        self._sort_by_points()
        self._update_logs()
        # Saving the best network and logs each 50 generations
        # self._remove_unfit_individuals()
        self.next_generation(N, self.mutation_probability)
        self.gen += 1
        self._update_stagnated_clock()

    def _update_logs(self):
        self.logs.max_points.append(self.individuals[0].points)
        self.logs.max_wins.append(self.get_most_wins())
        self.logs.population.append(len(self.individuals))

    def get_most_wins(self):
        most_winds = 0
        for i in self.individuals:
            if i.wins > most_winds:
                most_winds = i.wins
        return most_winds

    def save_best_individual(self, gen):
        best = self.individuals[0]
        best.net.pybann.save_file(
            str(self.id)+'_'+str(best.bot_id)+'_best_gen'+str(gen)+'.json')
        best.bot_blueprint.save_file(
            str(self.id)+'_'+str(best.bot_id)+'_best_gen'+str(gen)+'_blueprint.json')

    def save_logs(self):
        np.savez(self.id+'_max_points', self.logs.max_points)
        np.savez(self.id+'_max_wins', self.logs.max_wins)
        np.savez(self.id+'_population', self.logs.population)

    def _remove_unfit_individuals(self, fraction=0.5):
        """
        Removing worst individuals
        """
        cut_point = int(len(self.individuals)*fraction)
        if cut_point == 0:
            cut_point = 1
        self.individuals = self.individuals[:cut_point]

    def speciate(self, threshold, id_generator=None):
        '''
        If there is enought network distance, split specie
        '''
        # Computing some "to be needed" stuff
        self._compute_specie_network_distance()
        self._find_representative()
        # Filtering outsiders (with more distance than the threshold)
        outsiders = []
        for i, ind in enumerate(self.individuals):
            # and network_distance(i.genome,self.representative.genome,normalize=False) > 3:
            if ind.specie_distance > threshold:
                outsiders.append(ind)
        individuals = self.individuals[:]
        self.individuals = []
        new_species = [self]
        for i in outsiders:
            id = None
            if id_generator is not None:
                id = next(id_generator)
            new_species.append(Specie(id))
           # new_species[-1].add_individual(i)
            new_species[-1].stagnated_clock = self.stagnated_clock
            new_species[-1].generations_alive_clock = self.generations_alive_clock
        representatives = [self.representative]+outsiders
        for ind in individuals:
            in_specie, distance = belong_to_specie(ind, representatives)
            new_species[in_specie].add_individual(ind)
        #print('Speciating',len(new_species),[(len(s.individuals),s.stagnated_clock) for s in new_species])
        # Cleaning possible empty species
        new_species = [s for s in new_species if len(s.individuals) > 0]
        return new_species

    def is_alive(self, max_clock):
        '''
        Check if the species is alive
        '''
        if max_clock is None:
            return self.individuals == []
        if self.individuals == [] or self.stagnated_clock > max_clock:
            return False
        return True

    def irradiate(self, times, preserve_first=False):
        '''
        As a last resort, go through the mutation procedure many times
        to try to unstuck the specie
        '''
        if preserve_first:
            for ind in self.individuals[1:]:
                for i in range(times):
                    ind.mutate()
        else:
            for ind in self.individuals:
                for i in range(times):
                    ind.mutate()

    def add_individual(self, ind):
        self.individuals.append(ind)

    def clear(self):
        for i in self.individuals:
            i.clear()

    def new_gen_from_best(self, N, mut):
        '''
        Creates the new generation from only the best individual
        '''
        self.generations_alive_clock += 1
        # Keeping the best unchanged
        self.individuals = self.individuals[:1]
        for i in range(N-1):
            ind = self.individuals[0].copy()
            for i in range(mut):
                ind.mutate()
            self.add_individual(ind)
        self.clear()

    def _sort_by_points(self):
        '''
        Sort the self.individuals list by points
        For the individuals set with the same points value, a shuffle
        is applied, that is especially important for the initial rounds
        where most individuals points the same (zero?).
        '''
        points_list = [ind.points for ind in self.individuals]
        index = np.flip(np.argsort(points_list), 0)
        # Shuffling the sections with same pontuation
        index_shuffled = []
        i_ini = 0
        i = 0
        while i < len(index):
            i_ini = i
            same_value = [index[i]]
            # Checking for a same pontuation section
            if i < (len(index)-1):
                while (points_list[index[i]] == points_list[index[i+1]]):
                    i += 1
                    same_value.append(index[i])
                    if i >= (len(index)-1):
                        break
            i += 1
            # Appending the same points value section in a random order
            while same_value != []:
                index_shuffled.append(same_value.pop(
                    random.randint(0, len(same_value)-1)))
        self.individuals = [self.individuals[i] for i in index_shuffled]
        # Updating max points
        try:
            if self.individuals[0].points > self.max_points:
                self.max_points = self.individuals[0].points
        except IndexError:
            print("Empty specie, can not sort.")

    def _update_stagnated_clock(self):
        if len(self.logs.max_points) > 1 and self.logs.max_points[-1] > self.logs.max_points[-2]:
            self.stagnated_clock = 0
        else:
            self.stagnated_clock += 1

    def random_individual(self):
        n = randint(0, len(self.individuals)-1)
        return self.individuals[n]

    def next_generation(self, N, mutation_probability, keep_ind=None, keep_best_threshold=5):
        """
        Create the next generation of individuals
        """
        self.generations_alive_clock += 1
        ng = []
        # Keeping given individual
        if keep_ind is not None and keep_ind in self.individuals:
            keep = keep_ind.copy()
            keep.clear()
            ng.append(keep)
            N -= 1
        # Keeping best individual if the species has a minimum pop size
        if N > keep_best_threshold:
            best = self.individuals[0].copy()
            best.clear()
            ng.append(best)
            N -= 1
        # Removing unfit
        self._remove_unfit_individuals()
        # Using the points as the probability for mating
        prob = np.array([i.points for i in self.individuals])
        prob_sum = np.sum(prob)
        if prob_sum == 0:
            prob = [1/len(self.individuals)]*len(self.individuals)
        else:
            prob = prob/prob_sum
        # Creating the next generation
        for i in range(N):
            i1, i2 = np.random.choice(
                self.individuals, 2, p=prob, replace=True)
            child = mate_grafobots(i1, i2)
            child.clear()
            while random.random() < mutation_probability:
                child.mutate()
            ng.append(child)
        self.individuals = ng

    def fitness(self):
        if len(self.individuals) == 0:
            return 0
        fit = 0
        for i in self.individuals:
            i.compute_fitness()
            fit += i.fitness
        return fit/len(self.individuals)


class GrafobotBannEcosystem:
    """
    A grafobot BANN population handler
    """

    def __init__(self,
                 botfiles,
                 pop_size,
                 initial_species_number,
                 ):
        self.botfiles = botfiles
        self.species: [Specie] = []
        self.pop_size = pop_size
        self.initial_species_number = initial_species_number
        self.max_clock = 100
        self.gen = 0
        self.logs: SpecieLogs = SpecieLogs()
        self.id_generator = self._get_next_specie_id()

    def start_fresh_population(self):
        """
        Start fresh
        """
        pop_size = int(self.pop_size/self.initial_species_number)
        if pop_size == 0:
            pop_size = 1
        for i in range(self.initial_species_number):
            new_specie = Specie(next(self.id_generator))
            # Extracting the bot data from the file
            botfile = self.botfiles[i]
            print("Loading file {}".format(botfile))
            bot_bp = rgb.PyBotBlueprint(botfile)
            bot_id = os.path.splitext(os.path.basename(botfile))[0]
            # Adding the a fresh set of individuals to the specie
            new_specie.start_fresh(pop_size, bot_bp, bot_id)
            self.species.append(new_specie)
        self._update_log()

    def _get_next_specie_id(self):
        id = 0
        while True:
            yield id
            id += 1

    def tick(self, min_pop_per_specie=20):
        """
        Main call for generation tickprint([i.specie_distance for i in s.individuals])
        """
        # Computing the population per specie
        N = self._compute_pop_per_specie(min_pop_per_specie)
        for i, s in enumerate(self.species):
            s.tick(N[i])
            s._compute_specie_network_distance()
            #print([i.specie_distance for i in s.individuals], len(s.individuals))
        #
        self._update_log()
        new_species = []
        for i, s in enumerate(self.species):
            if s.is_alive(self.max_clock):
                new_species += s.speciate(
                    SPECIATING_DISTANCE,
                    id_generator=self.id_generator
                )
        self.species = new_species
        self.gen += 1

    def _compute_pop_per_specie(self, min_pop_per_specie):
        """
        Distribute the total population into the existing species
        FIXME potential to overshoot the self.pop_size 
        by a min_pop_per_specie factor
        """
        prob = np.array([s.fitness() for s in self.species])
        prob_sum = np.sum(prob)
        assert prob_sum != 0, "All species are DEAD. RIP grafobots."
        # function to increase the population of not so good (yet) species
        def f(x): return 1-exp(-x*10)
        prob = [f(p/prob_sum) for p in prob]
        renorm = np.sum(prob)
        N = [int(p/renorm*self.pop_size) for p in prob]
        # Applying the minimal (can overshoot the pop_size)
        #N = [n if n > min_pop_per_specie else min_pop_per_specie for n in N]
        return N

    def _update_log(self):
        max_points = 0
        max_wins = 0
        pop = 0
        for s in self.species:
            if s.logs.max_points[-1] > max_points:
                max_points = s.logs.max_points[-1]
            if s.logs.max_wins[-1] > max_wins:
                max_wins = s.logs.max_wins[-1]
            pop += s.logs.population[-1]
        self.logs.max_points.append(max_points)
        self.logs.max_wins.append(max_wins)
        self.logs.population.append(pop)

    def print_analitics(self):
        for s in self.species:
            s.print_analitics()

    def speciate(self):
        # **Parei aqui. tem que ver como faz com os id das especies
        self.species = [s.speciate(
            id_generator=self.get_next_specie_id) for s in self.species]

    def list_individuals(self):
        '''
        Returns a list cotaining all the individuals from all species
        '''
        individuals = []
        for specie in self.species:
            individuals += specie.individuals
        return individuals


class GrafoEvoPlot:
    def __init__(self):
        plt.ion()
        self.fig, self.axs = plt.subplots(nrows=3,
                                          sharex=True,
                                          gridspec_kw={'height_ratios': [
                                              1, 1, 1], 'hspace': 0},
                                          figsize=(10, 10))
        # self.axs[2].set_ylabel('Espécies')
        self.axs[0].set_ylabel('Máximo de pontos')
        self.axs[1].set_ylabel('Vitórias')
        self.axs[2].set_ylabel('Spécies')
        #
        self.axs[0].plot([], [])
        self.axs[1].plot([], [])
        self.axs[2].plot([], [])
        #
        #self.axs[0].legend(bots_ids, loc='upper left')

    def update_points_data(self, gen, max_points):
        index = 0
        lines = self.axs[0].get_lines()
        lines[index].set_data(gen, max_points)

    def update_wins_data(self, gen, wins):
        index = 0
        lines = self.axs[1].get_lines()
        lines[index].set_data(gen, wins)

    def update_population_data(self, gen, species_n):
        index = 0
        lines = self.axs[2].get_lines()
        lines[index].set_data(gen, species_n)

    def redraw(self):
        self.axs[0].relim()
        self.axs[0].autoscale_view()
        self.axs[1].relim()
        self.axs[1].autoscale_view()
        self.axs[2].relim()
        self.axs[2].autoscale_view()
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()
