import sys
sys.path.append('../../')
sys.path.append('../../../')

import librinha_rust2py as rgb
#import libaann as an
import neural_network as nn
from net_builder import construct_aann


import arcade
from graphics_arcade import GameWindow, RinhaView, MenuView
from arcade_rinha import ArcadeRinha
from config_parser import RinhaConfigParser, print_bot_info

import librinha_rust2py as rgb


def mutate(pybann):
    '''
    Round of mutations to a Bann to be applyed every iteration
    '''
    #pybann.add_node(0.01)
    pybann.add_connection(0.4)
    pybann.perturbe_weights(0.05, 0.1)
    pybann.perturbe_biases(0.05, 0.1)
    pybann.remove_connection(0.1)
    for i in range(5):
        pybann.perturbe_single_weight(0.5, 0.5)
        pybann.perturbe_single_bias(0.5, 0.5)


net = rgb.PyBannNetworkFactory(3,4,5).set_hidden_blocks(3,5).build()


net.to_dot('fresh.dot')

for i in range(100):
    mutate(net)

net.to_dot('mutated.dot')
