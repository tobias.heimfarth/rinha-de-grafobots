from threading import Thread
import sys
from pop_dynamics import PopDynamics


keyboard_input = [None]

def start_keyboard_thread():
    input_thread = Thread(target=input_cacther,args=(keyboard_input,))
    input_thread.daemon = True
    input_thread.start()

def input_cacther(keyboard_input):
    while True:
        text = input("Command (save or quit):")
        keyboard_input[0] = text

def main():
    start_keyboard_thread()
    popdyn = PopDynamics()
    while True:
        popdyn.tick_generation()
        manage_keyboard_input(popdyn.eco,popdyn.gen)

def manage_keyboard_input(eco,gen):
    match keyboard_input[0]:
        case "save":
            print("Saving best in this generation.")
            for s in eco.species:
                s.save_best_individual(gen)
        case "quit":
            print("Exiting")
            sys.exit()
    keyboard_input[0] = None

if __name__ == '__main__':
    main()
