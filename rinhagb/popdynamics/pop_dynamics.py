import os
from os import listdir
from os.path import isfile, join

from math import pi
from tqdm import tqdm
import numpy as np
import matplotlib.pyplot as plt

from copy import copy, deepcopy
from random import random, randint, seed, choice
from multiprocess import Pool

from rinhagb.resources.librinha_rust2py as rgb
from .species import GrafobotBannEcosystem, BehavIndividual, GrafobotIndividual, GrafoEvoPlot



class PopDynamics:
    def __init__(self):
        self._start_populations()

    def _start_populations(self):
        POP = 500  # population number per generation per specie
        seed(152)
        # Lista de grafobots para incluir na rinha
        #grafobot_files_list = ['tupa.txt','botvinik.txt','driftero.txt','karin.txt','exemplobot.txt','zoiudo.txt','givis.txt','Alekhine.txt','Alfinha.txt','batedeira.txt']
        grafobot_files_list = ['tupa.txt', 'botvinik.txt',
                            'exemplobot.txt', 'givis.txt', 'botvinik_anyradar.txt']
        #grafobot_files_list = ['tupa.txt']

        # Criando as populações
        self.eco = GrafobotBannEcosystem(
            grafobot_files_list, pop_size=POP, initial_species_number=len(grafobot_files_list))
        self.eco.start_fresh_population()

        # Lazy bot - alvo fixo, primeiro adversário
        lazy_bp = rgb.PyBotBlueprint('./lazybot.txt')
        lazy_bot = lazy_bp.gen_bot()
        io = list(lazy_bot.get_io_numbers())
        lazy_ind = NetIndividual(rgb.PyBannNetworkFactory(io[1], io[0], 0).build())
        lazy_bot = GrafobotIndividual(lazy_ind, lazy_bp, 'lazybot')

        # Campeões
        self.champions = [lazy_bot]

        # Carregando campeons previos
        mypath = "./champs"
        file_names = [f for f in listdir(mypath) if isfile(join(mypath, f))]
        for file in file_names:
            net = NetIndividual(rgb.PyBannNetworkFactory.load_file(mypath+'/'+file))
            blueprint = os.path.splitext(file)[0].split('_')[-1] + '.txt'
            # print(blueprint)
            ind = GrafobotIndividual(net, rgb.BotBlueprint(blueprint), file)
            self.champions.append(ind)

        # Iniciando gráficos
        self.plot = GrafoEvoPlot()

        #Iniciando contador
        self.gen = 0


    def tick_generation(self):
        print('Geracao ', self.gen)
        print(self.champions)
        # Criando uma lista com todos indivíduos.
        all_individual_list = self.eco.list_individuals()
        new_champs = []
        bundles = []
        matches_number = []
        for ind in all_individual_list:
            ind_bundle = play_bundle(ind, self.champions)
            matches_number.append(len(ind_bundle))
            bundles += ind_bundle
        print("Jogando...")
        results = rgb.parallel_rinha(bundles)
        results.reverse()
        print("Analisando resultados...")
        for k, ind in enumerate(all_individual_list):
            wins = 0
            # Computando os pontos dos campeões exceto o último
            for l in range(matches_number[k]-2):
                r = results.pop()
                if r.winner == 1:
                    wins += 1
                    ind.points += 200 + r.points[0]
                else:
                    ind.points += r.points[0]
            # Computando os pontos para o último campeão que é jogado por
            # duas vezes (ambos lados)
            r1 = results.pop()
            r2 = results.pop()
            if r1.winner == 1 and r2.winner == 2:
                wins += 1
                ind.points += 200 + (r1.points[0] + r2.points[1])/2
            else:
                ind.points += (r1.points[0] + r2.points[1])/2
            #
            ind.wins = wins
            if wins == len(self.champions):
                new_champs.append(ind.copy())
        # Limitando o número de novos campeões para evitar um grande número inicial
        if len(new_champs) > 1:
            new_champs = new_champs[:1]
        self.champions += new_champs
        # Salvando os campeoes
        for i, champ in enumerate(new_champs):
            k = len(self.champions)-len(new_champs)  # + starting_champ_index
            champ.save_txt('champ_'+str(k+i)+'_')
            # plot.save('./'+prefix+'.npz')
            # first.plot('genomes/'+prefix+'_gen'+str(j)+'.png')
        # Saving every 50 gens
        if self.gen%50 == 0:
            for s in self.eco.species:
                s.save_best_individual(self.gen)
        #
        print(len(self.eco.species), [(s.max_points, len(s.individuals))
            for s in self.eco.species])
        gens = list(range(self.gen+2))
        self.eco.tick()
        # for k,gb in enumerate(grafobots):
        #    gb.tick()
        self.eco.print_analitics()
        self.plot.update_points_data(gens, self.eco.logs.max_points)
        self.plot.update_wins_data(gens, self.eco.logs.max_wins)
        self.plot.update_population_data(gens, self.eco.logs.population)
        # plot.update_wins_data(k,gens,gb.max_wins_log)
        # plot.update_species_data(k,gens,gb.species_number_log)
        self.plot.redraw()
        self.gen += 1


def play_vs(ind1, ind2):
    """
    Joga a rinha entre dois indivíduos
    """
    if ind1.net.pybann is ind2.net.pybann:
        print("1 contra ele mesmo !!!")
    # assert(genome1 is not genome2)
    # Criando o jogo
    rinha = rgb.RinhaDeGrafobots(ind1.bot, ind2.bot)
    rinha.setup()
    # Criando as redes controladoras
    net_a = ind1.net.pyaann
    net_b = ind2.net.pyaann
    winner = rinha.run_headless_match_aann(32400, net_a, net_b)
    return winner, rinha.compute_points()


def create_vs_bundle(ind1, ind2):
    """
    Cria um pacote com o jogo pronto para ser jogado pelo RustyRinha em paralelo.
    """
    if ind1.net.pybann is ind2.net.pybann:
        print("1 contra ele mesmo !!!")
    # assert(genome1 is not genome2)
    # Criando o jogo
    rinha = rgb.RinhaDeGrafobots(ind1.bot, ind2.bot)
    rinha.setup()
    # Criando as redes controladoras
    net_a = ind1.net.pyaann
    net_b = ind2.net.pyaann
    return rgb.RinhaAANNBundle(rinha, 36000, (net_a, net_b))


def random_matches(ind_list, N):
    '''
    Cria um lista de duelos com N duelos por membro da lista.
    Para evitar sobras, N deve ser par.
    '''
    matches = []
    rest = []
    for _ in range(N):
        to_play_ind_list = ind_list[:]
        for _ in range(int(len(to_play_ind_list)/2)):
            ind1 = choice(to_play_ind_list)
            to_play_ind_list.remove(ind1)
            ind2 = choice(to_play_ind_list)
            to_play_ind_list.remove(ind2)
            matches.append((ind1, ind2))
        rest += to_play_ind_list
    #
    for _ in range(int(len(rest)/2)):
        matches.append((rest.pop(0), rest.pop(0)))
    return matches


def all_pairs_matches(ind_list):
    matches = []
    for i, ind1 in enumerate(ind_list):
        for ind2 in ind_list[i+1:]:
            matches.append((ind1, ind2))
    return matches


def play_bundle(ind, champions):
    bundles = []
    for champ in champions:
        bundles.append(create_vs_bundle(ind, champ))
    # testar se ganha também invertendo os lados para evitar que
    # o campeão ganhe de si mesmo quando o lado esta trocado
    # gerando uma sucessão de campeões iguais
    bundles.append(create_vs_bundle(champions[-1], ind))
    return bundles



