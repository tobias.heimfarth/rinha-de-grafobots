import rinhagb.resources.librinha_rust2py as rgb
#from neural_network import species
import rinhagb.evomorph as em
from rinhagb.popdynamics.individuals import (
    MorphIndividual,
    BehavIndividual,
    GrafobotIndividual,
    GrafobotIndividualFactory,
    mate_grafobots_individuals
)

import matplotlib.pyplot as plt
import numpy as np
from copy import copy, deepcopy
from dataclasses import dataclass
import os
import random
from math import ceil, exp



class SpecieIndividual(GrafobotIndividual):
    '''
    Extending the GrafobotIndividual to be part of a Specie
    '''

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.specie_distance = 0.


def belong_to_specie(ind: GrafobotIndividual, references: [GrafobotIndividual]):
    '''
    Compares the given Grafobot to the list of reference individuals
    to find the nearest one based on the average of morphological and
    behavioral distances.
    Return the nearest reference index and distance
    '''
    d = []
    for ref in references:
        dist = (bann_distance(ind, ref) + morph_distance(ind, ref))/2
        d.append(dist)
    d_order = np.argsort(d)
    return d_order[0], d[d_order[0]]


def bann_distance(ind1, ind2):
    '''
    The "distance" between 2 networks. Based on the similarity defined
    in the rust part of things
    '''
    points, total = rgb.PyBannNetwork.get_similarity(
        ind1.behav_ind.pybann, ind2.net_bann.pybann)
    # Assuming the distance the inverse of the similarity (+1 to avoid
    # zero divisions, that should not matter as the typical values are big)
    if total == 0:
        return 0
    return (total - points)/total


def morph_distance(ind1, ind2):
    '''
    The "distance" between 2 morphological networks.
    Based on the similarity defined in evomorph module
    '''
    similarity = em.mating.networks_similarity(
        ind1.morph_net, ind2.morph_net)
    return 1-similarity

def grafobot_distance(ind1, ind2):
    '''
    Computes the network distance between two grafobot individuals.
    '''
    # Starting simple with the average value between morph and behav
    return 0.5*(bann_distance(ind1, ind2) + morph_distance(ind1, ind2))

class SpecieLogs:
    '''
    Class to store the logs mainly for plotting
    '''

    def __init__(self):
        self.population: [int] = []
        self.max_points: [float] = []
        self.max_wins: [int] = []

    def remove_last(self):
        del self.population[-1]
        del self.max_points[-1]
        del self.max_wins[-1]

    def get_max_points(self):
        if self.max_points:
            return self.max_points[-1]
        else:
            return 0

    def get_max_wins(self):
        if self.max_points:
            return self.max_wins[-1]
        else:
            return 0


def specie_id_generator(start=0):
    """
    Generates bot id in a sequency
    """
    specie_id_num = start
    while True:
        yield 'SPECIE'+str(specie_id_num)
        specie_id_num += 1


class SpecieFactory:

    id_gen = specie_id_generator()

    def __init__(self):
        self.specie = None
        self._id = None
    
    @property
    def id(self):
        if self._id is None:
            self._id = next(self.id_gen)
        return self._id

    def build(self):
        return Specie(self.id)
    
    
    def fresh_population(self):## ** PAREI AQUI< COLEI ABAIXO PRECISA REVER TUDO DESSA FUNCAO (pop_size, bot_id: str):
        """
        Start fresh a new specie.
        """
        # Creating the morphological individual
        morph_net = em.network_elements.Network()
        morph_ind = MorphIndividual(morph_net)
        # Giving it some mutations
        for i in range(10):
            morph_ind.mutate()
        # Getting the io numbers to create the behav net
        bp = morph_ind.export_blueprint()
        bot = bp.gen_bot()
        io = list(bot.get_io_numbers())
        # adds a dummy input if there is none in order to avoid
        # mutations failing
        if io[1] == 0:
            io[1] = 1
        # Creating first individual:
        behav_ind = BehavIndividual(
            rgb
            .PyBannNetworkFactory(io[1], io[0], 5)
            .set_hidden_blocks(10, 15)
            .set_innov_start(0)
            .build()
        )
        
        for _ in range(1000):
            behav_ind.pybann.add_connection(1.)
            behav_ind.pybann.perturbe_weights(0.05, 0.5)
            behav_ind.pybann.perturbe_biases(0.05, 0.5)
        individual = SpecieIndividual(behav_ind, morph_ind, bot_id)
        self.individuals.append(individual)
        # Adding others
        for _ in range(pop_size-1):
            new_ind = individual.copy()
            # Initial mutations
            for _ in range(2):
                new_ind.mutate()
            self.individuals.append(new_ind)
        self._update_logs()


class Specie:
    '''
    A specie is a collection of grafobots with similar networks that
    evolves competing internally
    '''

    def __init__(self, id):
        # individuals list
        self.individuals: [SpecieIndividual] = []
        # How many generations no improvement has occurred
        self.stagnated_clock = 0
        # How many generations the specie is alive
        self.generations_alive_clock = 0
        self.reproduce = True
        self.representative = None
        self.logs = SpecieLogs()
        self.gen = 0
        self.max_points = 0
        # Specie unique id
        self.id = id
        self.mutation_probability = 0.5

    def _reset_individuals_distances(self):
        for ind in self.individuals:
            ind.specie_distance = 0.
    
    def _normalize_distances(self):
        # Normalizing to get the average distance
        if len(self.individuals) > 1:
            for ind in self.individuals:
                ind.specie_distance = ind.specie_distance / \
                    (len(self.individuals)-1)

    def _compute_specie_network_distance(self):
        '''
        Computes the genetic average distance for each individual
        compared to the specie
        The results are stored in the individual
        #FIXE maybe store this in a dict to avoid the inheritance (SpecieIndividual)
        '''
        self._reset_individuals_distances()
        # Sums the distance to every other individual on the specie
        for i, ind_i in enumerate(self.individuals):
            for ind_j in self.individuals[i+1:]:
                d = grafobot_distance(ind_i, ind_j)
                ind_i.specie_distance += d
                ind_j.specie_distance += d
        self._normalize_distances()


    def print_analytics(self):
        '''
        Prints some of the specie info
        '''
        description = "Specie: {}, bot: {}".format(
            self.id, self.individuals[0].bot_id)
        data = "Population: {}, max. points {} max. wins {}".format(len(
            self.individuals), self.logs.get_max_points(), self.logs.get_max_wins())
        dists = [i.specie_distance for i in self.individuals]
        distance = "Genetic distance max/min: {}/{}".format(
            max(dists), min(dists))
        print(description + '\n' + data + '\n' + distance)

    def start_fresh(self, pop_size, bot_id: str):
        """
        Start fresh a new specie.
        """
        # Creating the morphological individual
        morph_net = em.network_elements.Network()
        morph_ind = MorphIndividual(morph_net)
        # Giving it some mutations
        for i in range(10):
            morph_ind.mutate()
        # Getting the io numbers to create the behav net
        bp = morph_ind.export_blueprint()
        bot = bp.gen_bot()
        io = list(bot.get_io_numbers())
        # adds a dummy input if there is none in order to avoid
        # mutations failing
        if io[1] == 0:
            io[1] = 1
        # Creating first individual:
        behav_ind = BehavIndividual(
            rgb
            .PyBannNetworkFactory(io[1], io[0], 5)
            .set_hidden_blocks(10, 15)
            .set_innov_start(0)
            .build()
        )
        
        for _ in range(1000):
            behav_ind.pybann.add_connection(1.)
            behav_ind.pybann.perturbe_weights(0.05, 0.5)
            behav_ind.pybann.perturbe_biases(0.05, 0.5)
        individual = SpecieIndividual(behav_ind, morph_ind, bot_id)
        self.individuals.append(individual)
        # Adding others
        for _ in range(pop_size-1):
            new_ind = individual.copy()
            # Initial mutations
            for _ in range(2):
                new_ind.mutate()
            self.individuals.append(new_ind)
        self._update_logs()

    def _find_representative(self):
        '''
        Looks for the individual with the lower distance with respect
        with all others 
        '''
        d_arg = np.argsort([ind.specie_distance for ind in self.individuals])
        #print([d[i] for i in d_arg])
        self.representative = self.individuals[d_arg[0]]

    def tick(self, N):
        """
        Main call for generation tick
        """
        self._sort_by_points()
        self._update_logs()
        # Saving the best network and logs each 50 generations
        # self._remove_unfit_individuals()
        self.next_generation(N, self.mutation_probability)
        self.gen += 1
        self._update_stagnated_clock()

    def _update_logs(self):
        self.logs.max_points.append(self.individuals[0].points)
        self.logs.max_wins.append(self.get_most_wins())
        self.logs.population.append(len(self.individuals))

    def get_most_wins(self):
        most_winds = 0
        for i in self.individuals:
            if i.wins > most_winds:
                most_winds = i.wins
        return most_winds

    def save_best_individual(self, gen):
        best = self.individuals[0]
        best.net.pybann.save_file(
            str(self.id)+'_'+str(best.bot_id)+'_best_gen'+str(gen)+'.json')
        best.bot_blueprint.save_file(
            str(self.id)+'_'+str(best.bot_id)+'_best_gen'+str(gen)+'_blueprint.json')

    def save_logs(self):
        np.savez(self.id+'_max_points', self.logs.max_points)
        np.savez(self.id+'_max_wins', self.logs.max_wins)
        np.savez(self.id+'_population', self.logs.population)

    def _remove_unfit_individuals(self, fraction=0.5):
        """
        Removing worst individuals
        """
        cut_point = int(len(self.individuals)*fraction)
        if cut_point == 0:
            cut_point = 1
        self.individuals = self.individuals[:cut_point]

    def speciate(self, threshold, id_generator=None):
        '''
        If there is enought network distance, split specie
        '''
        # Computing some "to be needed" stuff
        self._compute_specie_network_distance()
        self._find_representative()
        # Filtering outsiders (with more distance than the threshold)
        outsiders = []
        for i, ind in enumerate(self.individuals):
            # and network_distance(i.genome,self.representative.genome,normalize=False) > 3:
            if ind.specie_distance > threshold:
                outsiders.append(ind)
        individuals = self.individuals[:]
        self.individuals = []
        new_species = [self]
        for i in outsiders:
            id = None
            if id_generator is not None:
                id = next(id_generator)
            new_species.append(Specie(id))
           # new_species[-1].add_individual(i)
            new_species[-1].stagnated_clock = self.stagnated_clock
            new_species[-1].generations_alive_clock = self.generations_alive_clock
        representatives = [self.representative]+outsiders
        for ind in individuals:
            in_specie, distance = belong_to_specie(ind, representatives)
            new_species[in_specie].add_individual(ind)
        #print('Speciating',len(new_species),[(len(s.individuals),s.stagnated_clock) for s in new_species])
        # Cleaning possible empty species
        new_species = [s for s in new_species if len(s.individuals) > 0]
        return new_species

    def is_alive(self, max_clock):
        '''
        Check if the species is alive
        '''
        if max_clock is None:
            return self.individuals == []
        if self.individuals == [] or self.stagnated_clock > max_clock:
            return False
        return True

    def irradiate(self, times, preserve_first=False):
        '''
        As a last resort, go through the mutation procedure many times
        to try to unstuck the specie
        '''
        if preserve_first:
            for ind in self.individuals[1:]:
                for i in range(times):
                    ind.mutate()
        else:
            for ind in self.individuals:
                for i in range(times):
                    ind.mutate()

    def add_individual(self, ind):
        self.individuals.append(ind)

    def clear(self):
        for i in self.individuals:
            i.clear()

    def new_gen_from_best(self, N, mut):
        '''
        Creates the new generation from only the best individual
        '''
        self.generations_alive_clock += 1
        # Keeping the best unchanged
        self.individuals = self.individuals[:1]
        for i in range(N-1):
            ind = self.individuals[0].copy()
            for i in range(mut):
                ind.mutate()
            self.add_individual(ind)
        self.clear()

    def _sort_by_points(self):
        '''
        Sort the self.individuals list by points
        For the individuals set with the same points value, a shuffle
        is applied, that is especially important for the initial rounds
        where most individuals points the same (zero?).
        '''
        points_list = [ind.points for ind in self.individuals]
        index = np.flip(np.argsort(points_list), 0)
        # Shuffling the sections with same pontuation
        index_shuffled = []
        i_ini = 0
        i = 0
        while i < len(index):
            i_ini = i
            same_value = [index[i]]
            # Checking for a same pontuation section
            if i < (len(index)-1):
                while (points_list[index[i]] == points_list[index[i+1]]):
                    i += 1
                    same_value.append(index[i])
                    if i >= (len(index)-1):
                        break
            i += 1
            # Appending the same points value section in a random order
            while same_value != []:
                index_shuffled.append(same_value.pop(
                    random.randint(0, len(same_value)-1)))
        self.individuals = [self.individuals[i] for i in index_shuffled]
        # Updating max points
        try:
            if self.individuals[0].points > self.max_points:
                self.max_points = self.individuals[0].points
        except IndexError:
            print("Empty specie, can not sort.")

    def _update_stagnated_clock(self):
        if len(self.logs.max_points) > 1 and self.logs.max_points[-1] > self.logs.max_points[-2]:
            self.stagnated_clock = 0
        else:
            self.stagnated_clock += 1

    def random_individual(self):
        n = randint(0, len(self.individuals)-1)
        return self.individuals[n]

    def next_generation(self, N, mutation_probability, keep_ind=None, keep_best_threshold=5):
        """
        Create the next generation of individuals
        """
        self.generations_alive_clock += 1
        ng = []
        # Keeping given individual
        if keep_ind is not None and keep_ind in self.individuals:
            keep = keep_ind.copy()
            keep.clear()
            ng.append(keep)
            N -= 1
        # Keeping best individual if the species has a minimum pop size
        if N > keep_best_threshold:
            best = self.individuals[0].copy()
            best.clear()
            ng.append(best)
            N -= 1
        # Removing unfit
        self._remove_unfit_individuals()
        # Using the points as the probability for mating
        prob = np.array([i.points for i in self.individuals])
        prob_sum = np.sum(prob)
        if prob_sum == 0:
            prob = [1/len(self.individuals)]*len(self.individuals)
        else:
            prob = prob/prob_sum
        # Creating the next generation
        for i in range(N):
            i1, i2 = np.random.choice(
                self.individuals, 2, p=prob, replace=True)
            child = mate_grafobots(i1, i2)
            child.clear()
            while random.random() < mutation_probability:
                child.mutate()
            ng.append(child)
        self.individuals = ng

    def fitness(self):
        if len(self.individuals) == 0:
            return 0
        fit = 0
        for i in self.individuals:
            i.compute_fitness()
            fit += i.fitness
        return fit/len(self.individuals)


class GrafobotBannEcosystem:
    """
    A grafobot BANN population handler
    """

    def __init__(self,
                 botfiles,
                 pop_size,
                 initial_species_number,
                 ):
        self.botfiles = botfiles
        self.species: [Specie] = []
        self.pop_size = pop_size
        self.initial_species_number = initial_species_number
        self.max_clock = 100
        self.gen = 0
        self.logs: SpecieLogs = SpecieLogs()
        self.id_generator = self._get_next_specie_id()

    def start_fresh_population(self):
        """
        Start fresh
        """
        pop_size = int(self.pop_size/self.initial_species_number)
        if pop_size == 0:
            pop_size = 1
        for i in range(self.initial_species_number):
            new_specie = Specie(next(self.id_generator))
            # Extracting the bot data from the file
            botfile = self.botfiles[i]
            print("Loading file {}".format(botfile))
            bot_bp = rgb.PyBotBlueprint(botfile)
            bot_id = os.path.splitext(os.path.basename(botfile))[0]
            # Adding the a fresh set of individuals to the specie
            new_specie.start_fresh(pop_size, bot_bp, bot_id)
            self.species.append(new_specie)
        self._update_log()

    def _get_next_specie_id(self):
        id = 0
        while True:
            yield id
            id += 1

    def tick(self, min_pop_per_specie=20):
        """
        Main call for generation tick
        """
        # Computing the population per specie
        N = self._compute_pop_per_specie(min_pop_per_specie)
        for i, s in enumerate(self.species):
            s.tick(N[i])
            s._compute_specie_network_distance()
            #print([i.specie_distance for i in s.individuals], len(s.individuals))
        #
        self._update_log()
        new_species = []
        for i, s in enumerate(self.species):
            if s.is_alive(self.max_clock):
                new_species += s.speciate(
                    SPECIATING_DISTANCE,
                    id_generator=self.id_generator
                )
        self.species = new_species
        self.gen += 1

    def _compute_pop_per_specie(self, min_pop_per_specie):
        """
        Distribute the total population into the existing species
        FIXME potential to overshoot the self.pop_size 
        by a min_pop_per_specie factor
        """
        prob = np.array([s.fitness() for s in self.species])
        prob_sum = np.sum(prob)
        assert prob_sum != 0, "All species are DEAD. RIP grafobots."
        # function to increase the population of not so good (yet) species
        def f(x): return 1-exp(-x*10)
        prob = [f(p/prob_sum) for p in prob]
        renorm = np.sum(prob)
        N = [int(p/renorm*self.pop_size) for p in prob]
        # Applying the minimal (can overshoot the pop_size)
        #N = [n if n > min_pop_per_specie else min_pop_per_specie for n in N]
        return N

    def _update_log(self):
        max_points = 0
        max_wins = 0
        pop = 0
        for s in self.species:
            if s.logs.max_points[-1] > max_points:
                max_points = s.logs.max_points[-1]
            if s.logs.max_wins[-1] > max_wins:
                max_wins = s.logs.max_wins[-1]
            pop += s.logs.population[-1]
        self.logs.max_points.append(max_points)
        self.logs.max_wins.append(max_wins)
        self.logs.population.append(pop)

    def print_analitics(self):
        for s in self.species:
            s.print_analitics()

    def speciate(self):
        # **Parei aqui. tem que ver como faz com os id das especies
        self.species = [s.speciate(
            id_generator=self.get_next_specie_id) for s in self.species]

    def list_individuals(self):
        '''
        Returns a list cotaining all the individuals from all species
        '''
        individuals = []
        for specie in self.species:
            individuals += specie.individuals
        return individuals


class GrafoEvoPlot:
    def __init__(self):
        plt.ion()
        self.fig, self.axs = plt.subplots(nrows=3,
                                          sharex=True,
                                          gridspec_kw={'height_ratios': [
                                              1, 1, 1], 'hspace': 0},
                                          figsize=(10, 10))
        # self.axs[2].set_ylabel('Espécies')
        self.axs[0].set_ylabel('Máximo de pontos')
        self.axs[1].set_ylabel('Vitórias')
        self.axs[2].set_ylabel('Spécies')
        #
        self.axs[0].plot([], [])
        self.axs[1].plot([], [])
        self.axs[2].plot([], [])
        #
        #self.axs[0].legend(bots_ids, loc='upper left')

    def update_points_data(self, gen, max_points):
        index = 0
        lines = self.axs[0].get_lines()
        lines[index].set_data(gen, max_points)

    def update_wins_data(self, gen, wins):
        index = 0
        lines = self.axs[1].get_lines()
        lines[index].set_data(gen, wins)

    def update_population_data(self, gen, species_n):
        index = 0
        lines = self.axs[2].get_lines()
        lines[index].set_data(gen, species_n)

    def redraw(self):
        self.axs[0].relim()
        self.axs[0].autoscale_view()
        self.axs[1].relim()
        self.axs[1].autoscale_view()
        self.axs[2].relim()
        self.axs[2].autoscale_view()
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()
