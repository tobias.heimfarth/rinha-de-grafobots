from typing import Callable, NewType
import matplotlib.pyplot as plt
import numpy as np
from copy import copy, deepcopy
from dataclasses import dataclass, field
import os
import random
from math import ceil, exp

import rinhagb.resources.librinha_rust2py as rgb
from rinhagb import evomorph
import rinhagb.evomorph.network_elements as ene
from rinhagb.id_manager import IdManager

# FIXME instead of using directly the evomorph and rbg networks, some
# interface will be much better in the future, something like
# class MorphNetwork(Protocol)
# class BehavNetwork(Protocol)


class MorphIndividual:
    """
    Morphological network manager for the individual grafobot
    Just a basic layer to homogenize the architecture
    """

    def __init__(self, net: ene.Network):
        self.net = net

    def save(self, filename) -> None:
        self.net.save(filename)

    def copy(self):
        new = deepcopy(self)
        return new


def mate_morph_individuals(ind1: MorphIndividual, ind2: MorphIndividual):
    net = evomorph.mating.mate_networks(id1.morph_net, net2.morph_net)
    return MorphIndividual(net)


class BehavIndividual:
    """      
    Behavioral network manager for the individual grafobot
    """

    def __init__(self, pybann: rgb.PyBannNetwork):
        self._pybann = pybann
        self._pyaann = None

    @property
    def pyaann(self):
        if self._pyaann is None:
            self._gen_pyaan()
        return self._pyaann

    def _gen_pyaan(self):
        self._pyaann = self.pybann.clone().simplify().to_pyaann()

    @property
    def pybann(self):
        return self._pybann

    @pybann.setter
    def pybann(self, value):
        self._pybann = value
        self._gen_pyaan()

    def save(self, filename):
        self.pybann.save_file(filename)

    def clear(self):
        self._pyaann = None

    def copy(self):
        new = copy(self)
        new.pybann = self.pybann.clone()
        return new

    def report(self):
        count = self.pybann.count_components()
        r = (" {} inputs\n {} outputs\n {} total nodes\n {} total connections").format(
            *count)
        print(r)
        count = self.pyaann.count_components()
        r = (" {} inputs\n {} outputs\n {} total nodes\n {} total connections").format(
            *count)
        print(r)


def mate_behav_individuals(ind1: BehavIndividual, ind2: BehavIndividual):
    """
    Mates two BehavIndividual
    """
    child = ind1.copy()
    new_pybann = rgb.PyBannNetwork.mate(ind1.pybann, ind2.pybann)
    child.clear()
    child.pybann = new_pybann
    return child


@dataclass
class NetsIOMap:
    '''
    A map of the morphological network inputs and outputs to the
    behavioral network outputs and inputs
    '''
    morph_inputs: list[int]
    morph_outputs: list[int]

    @staticmethod
    def gen_map(net: evomorph.network_elements.Network):
        morph_inputs, morph_outputs = net.list_io()
        return NetsIOMap(morph_inputs, morph_outputs)


# Protocols for the mutators
# (just functions that transforms the networks inplace)
# FIXME To further decouple the popdynamics form specific networks, this
# protocols should eventually be generic
BehavMutator = NewType(
    'BehavMutator',
    Callable[MorphIndividual, None]
)
MorphMutator = NewType(
    'MorphMutator',
    Callable[[
        ene.NetElementsIdManager,
        MorphIndividual
    ], None]
)


class GBIndividualIdManager:
    def __init__(self):
        self.bot: IdManager = IdManager()
        self.behav_network: evomorph.network_elements.NetElementsIdManager = \
            evomorph.network_elements.NetElementsIdManager()


class GrafobotIndividual:
    """
    Handles a unique grafobot.
    """

    def __init__(self,
                 behav_ind: BehavIndividual,
                 morph_ind: MorphIndividual,
                 nets_io_map: NetsIOMap,
                 bot_id: str
                 ):
        self.behav_ind = behav_ind
        self.morph_ind = morph_ind
        self.nets_io_map = nets_io_map
        self._renew_bot_derivatives()
        self.bot_id = bot_id
        self.points: float = 0
        self.wins: int = 0

    def _renew_bot_derivatives(self):
        '''
        Renew the self.bot and self.bot_blueprint external accessible
        variable using the current morphological network
        '''
        self.bot_blueprint = self.morph_ind.net.export_blueprint()
        self.bot = self.bot_blueprint.gen_bot()

    @property
    def fitness(self):
        '''
        Individual fitness.
        For now is simply the points, but could be a more complicated
        function in the future.
        '''
        return self.points

    def save_txt(self, prefix):
        '''
        Saves the morphological and behavior individuals.
        '''
        self.behav_ind.save('./'+prefix+str(self.bot_id)+'_bann.json')
        self.morph_ind.save('./'+prefix+str(self.bot_id)+'_evomorph.json')

    def clear(self):
        '''
        Clear all the points and performance data.
        '''
        self.points = 0
        self.wins = 0
        self.behav_ind.clear()

    def copy(self):
        '''
        Copies the individual no leaving remaining shared states (hopefully).
        '''
        new = copy(self)
        new.behav_ind = self.behav_ind.copy()
        new.morph_ind = self.morph_ind.copy()
        new.bot = new.bot_blueprint.gen_bot()
        return new

    def mutate(self,
               id_man: GBIndividualIdManager,
               morph_mutator: MorphMutator,
               behav_mutator: BehavMutator,
               ):
        '''
        Mutation round, morphologic and behavioral
        '''
        old_map = deepcopy(self.nets_io_map)
        morph_mutator(id_man.behav_network, self.morph_ind)
        new_map = NetsIOMap.gen_map(self.morph_ind.net)
        self.behav_ind.pybann, self.nets_io_map = match_behav_io_to_morph(
            morph_net=self.morph_ind.net,
            behav_net=self.behav_ind.pybann,
            old_nets_io_map=old_map,
        )
        behav_mutator(self.behav_ind)
        self.clear()
        self._renew_bot_derivatives()

    def __repr__(self):
        return f"{self.bot_id},{self.points} points, {self.wins} wins."


@dataclass
class BehavRemapData:
    '''
    Bundles the operations to be done in a Behavior network to
    match a morphological one
    '''
    to_add: list[int]
    to_remove: list[int]
    new_map: list[int]


def match_behav_io_to_morph(
    morph_net: evomorph.network_elements.Network,
    behav_net: rgb.PyBannNetwork,
    old_nets_io_map: NetsIOMap,
) -> (rgb.PyBannNetwork, NetsIOMap):
    '''
    Matches the inputs and outputs of the behavior network to a mutated
    morph that io may have changed.
    #FIXME to enroscado em essa questão do mapeamento.
    O ponto é que esse new_nets_io_map na verdade teria que ser a saida
    desta funçao, e a entrada é a lista vinda da rede morphologica.
    Isto porque, assumindo que os nodos existentes podem mudar de ordem
    quando a lista de nodos for solicitada para a rede. Mas claro que os
    ids dos nodos vão ser mantidos. Então o ponto aqui seria gerar o
    novo mapa compatibilizando com a versão anterior.
    Esse mapa precisa ser passado também para a criação do blueprint na
    classe Network do evomorph, pois lá as IOs são mapeadas pela ordem
    que aparecem na lista.
    Então provavelmente várias coisas terão que mudar nessa fulia toda

    Atualização - jah fiz quase tudo, mas não verifiquei muito e tenho que sair.
    Boa sorte :) . Ass. Tobias do passado
    '''
    # Copying the original pybann (no in-place modification)
    new_pybann = behav_net.clone()
    new_morph_inputs, new_morph_outputs = morph_net.list_io()
    output_remap_data = _remap(
        old_nets_io_map.morph_outputs, new_morph_outputs)
    input_remap_data = _remap(old_nets_io_map.morph_inputs, new_morph_inputs)
    # Behav network modification
    # Note that the morph inputs are the behav outputs and vice-versa
    for i in output_remap_data.to_remove:
        new_pybann.remove_input_node(i)
    for i in input_remap_data.to_remove:
        new_pybann.remove_output_node(i)
    for nid in output_remap_data.to_add:
        new_pybann.append_input_node()
    for nid in input_remap_data.to_add:
        new_pybann.append_output_node()
    new_map = NetsIOMap(
        morph_inputs=input_remap_data.new_map,
        morph_outputs=output_remap_data.new_map
    )
    return new_pybann, new_map


def _remap(
    old_morph_io: list[int],
    new_morph_io: list[int],
) -> BehavRemapData:
    '''
    Given an input (or output) map list old and new, returns
    the nodes that are needed to be added, removed and the new map
    '''
    io_to_add = (
        [nid for nid in new_morph_io
         if nid not in old_morph_io]
    )
    io_to_remove = (
        [i for i, nid in enumerate(old_morph_io)
         if nid not in new_morph_io]
    )
    # The removal order must be reversed to not affect the previous indices
    io_to_remove.reverse()
    io_map = (
        [nid for nid in old_morph_io
         if nid in new_morph_io]
        + io_to_add
    )
    return BehavRemapData(
        to_add=io_to_add,
        to_remove=io_to_remove,
        new_map=io_map
    )


def bot_id_generator(start=0):
    """
    Generates bot id in a sequency
    """
    bot_id_num = start
    while True:
        yield 'GB'+str(bot_id_num)
        bot_id_num += 1


class GrafobotIndividualFactory:
    '''
    Builds Grafobots individuals
    '''

    def __init__(self, id_man: GBIndividualIdManager):
        # Creating the morphological individual
        self.id_man = id_man
        self.morph_ind = self._fresh_morph_ind()
        self._behav_ind = None
        self._bot_id = None
        self.hidden_blocks_count = 10
        self.hidden_blocks_size = 15
        self.interposer_size = 5
        self.behav_innov = 0

    @property
    def behav_ind(self):
        if self._behav_ind is None:
            self._behav_ind = self._fresh_behav_ind()
        return self._behav_ind

    @property
    def bot_id(self):
        if self._bot_id is None:
            self._bot_id = self.id_man.bot.next()
        return self._bot_id

    def build(self) -> GrafobotIndividual:
        '''
        Builds the GrafobotIndividual
        '''
        return GrafobotIndividual(
            behav_ind=self.behav_ind,
            morph_ind=self.morph_ind,
            nets_io_map=NetsIOMap.gen_map(self.morph_ind.net),
            bot_id=self.bot_id
        )

    def set_behav_innov(self, innov):
        self.behav_innov = innov
        return self

    def set_morph_individual(self, morph_individual: MorphIndividual):
        self.morph_ind = morph_individual
        return self

    def _fresh_morph_ind(self):
        '''
        Makes a fresh MorphIndividual
        '''
        morph_net = (
            evomorph.network_elements
            .NetworkFactory(self.id_man.behav_network)
            .build()
        )
        return MorphIndividual(morph_net)

    def _get_io(self) -> list[int]:
        '''
        Reports the morphological io node numbers.
        [in nodes, out nodes]
        '''
        # FIXME generating a blueprint just to get the io is kind of
        # ugly...
        bp = self.morph_ind.net.export_blueprint()
        bot = bp.gen_bot()
        io = list(bot.get_io_numbers())
        # adds a dummy input if there is none in order to avoid
        # mutations failing
        # if io[1] == 0:
        #    io[1] = 1
        return io

    def set_hidden_blocks(self, count: int, size: int):
        '''
        Sets the hidden blocks number (count) and size
        '''
        self.hidden_blocks_count = count
        self.hidden_blocks_size = size
        return self

    def set_interposer_size(self, size):
        self.interposer_size = size
        return self

    def _fresh_behav_ind(self):
        '''
        Makes a fresh BehavIndividual
        '''
        io = self._get_io()
        print(io)
        # Creating first individual:
        pybann = (
            rgb
            .PyBannNetworkFactory(io[1], io[0], self.interposer_size)
            .set_hidden_blocks(self.hidden_blocks_count, self.hidden_blocks_size)
            .set_innov_start(self.behav_innov)
            .build()
        )
        behav_ind = BehavIndividual(pybann)
        return behav_ind

    def fill_behav_network(self, fill: int = None):
        '''
        Adds "fill" connections to the behav network and perturbs the
        weights and biases.
        By default half the connections will be filled
        '''
        # FIXME a better idea is to use the mutation list query from the bann
        # If the number of times is not set, than activating half the
        # possible connections
        if fill is None:
            fill = int(self.hidden_blocks_count*self.hidden_blocks_size/2)
        for _ in range(fill):
            self.behav_ind.pybann.add_connection(1.)
            self.behav_ind.pybann.perturbe_weights(0.05, 0.5)
            self.behav_ind.pybann.perturbe_biases(0.05, 0.5)
        return self


def mate_grafobots_individuals(ind1: GrafobotIndividual, ind2: GrafobotIndividual):
    '''
    Mates two grafobots individuals.
    '''
    bot_id = None
    new_morph_individual = mate_morph_individuals(
        ind1.morph_ind, ind2.morph_ind)
    new_behav_individual = mate_behav_individuals(
        ind1.behav_ind, ind2.behav_ind)
    return GrafobotIndividual(new_behav_individual, new_morph_individual, bot_id)
