
# Constante elástica na colisão entre os nodos
mass_collision_elastic_constant 450

# Força máxima aplicada pelo nodo de tração
mass_max_push_force 60

# Velocidade de corte do nodo de tração.
# A força aplicada pelo nodo reduz linearmente com a velocidade na
# direção da força.
mass_max_push_speed 200

# Torque máxima aplicada pelo nodo de torção
mass_max_push_torque 100

# Velocidade angular de corte do nodo de torção
mass_max_push_angular_speed 10

# Constante elástica das paredes da arena
arena_elastic_constant 50

# Alcance máximo do radar de nodo
node_radar_max_range 500

# Alcance máximo do radar de paredes da arena
arena_radar_max_range 500

# Tempo máximo de inatividade (nodo da bateria parado) em tiques (atualizações)
# Se o bot ficar inativo por mais tempo, perde.
# Conversão para segundo: dividir por 480 (são feitas 480 atualizações por segundo)
max_inactivity_time 1000000

# Tempo máximo sem contato entre nodos rivais antes da rinha terminar
# empatada por falta de combate.
max_time_without_contact 5400

# Constante elástica radial dos links entre os nodos (por unidade de comprimento)
radial_elastic_constant 3000

# Constante elástica angular dos links
angular_elastic_constant 100

# Deformação radial máxima antes da quebra do link
breakage_radial_deformation 0.09

# Deformação angular máxima antes da quebra do link
breakage_angular_deformation 0.40

# Coeficiente de atrito estático (não utilizado no momento)
static_friction_coefficient 0.6

# Coeficiente de atrito dinâmico entre os nodos e a arena
# Incorporou-se a aceleração gravitacional, então esta constante é mu*g 
dynamic_friction_coefficient 1.0

# Coeficiente de atrito dependente da velocidade (arraste)
speed_dependent_friction_coefficient 0.0007

# Coeficiente de atrito estático entre os nodos de tração e a arena
# na direção ortogonal a sua orientação (faz com que os nodos de
# tração tendam a andar para frente ou para trás, sem derrapar para o lado)
lateral_static_friction_coefficient 180.

# Coeficiente de atrito dinâmico entre os nodos de tração e a arena
# na direção ortogonal a sua orientação.
lateral_dynamic_friction_coefficient 100.

# Coeficiente de amortecimento radial dos links
radial_dampening_coefficient 0.15

# Resistência inicial de cada link. Cada vez que o link passa do seu
# limite de carga perde resistência proporcional ao excesso.
initial_link_health 100

# Intervalo de integração
dt 0.025