## Planta para a construção de um Grafobot.
## Somente use espaços como separador.
#
## Nodos
#       Tipo        x       y    m   r   theta
node    battery     0       0    1   10  0
# arma frontal
node    torsion     60      0    1   10  0
node    simple      85      0    1   10  0
node    simple      47.5   21.65 1   10  0
node    simple      47.5  -21.65 1   10  0
# arma cima
node    torsion     20      60    1   10  0
node    simple     20      85    1   10  0
node    simple    41.65    47.5   1   10  0
node    simple      -1.65  47.5 1   10  0
# arma baixo
node    torsion     20      -60    1   10  0
node    simple     20      -85    1   10  0
node    simple    41.65    -47.5   1   10  0
node    simple      -1.65  -47.5 1   10  0

link 0 true  1 false
#arma frontal
link 1 true  2 true
link 1 true  3 true
link 1 true  4 true
link 1 true  2 true
link 1 true  3 true
link 1 true  4 true
link 2 true  3 true
link 3 true  4 true
link 2 true  4 true

arma cima
link 0 true 5 false
link 5 true  6 true
link 5 true  7 true
link 5 true  8 true
link 5 true  6 true
link 5 true  7 true
link 5 true  8 true
link 6 true  7 true
link 7 true  8 true
link 6 true  8 true
#link 1 true  2 true
arma baixo
link 0 true 9 false
link 9 true  10 true
link 9 true  11 true
link 9 true 12 true
link 9 true  10 true
link 9 true  11 true
link 9 true 12 true
link 10 true  11 true
link 11 true  12 true
link 10 true  12 true

#Motores
node traction -30 50 1 10 0
node traction -30 -50 1 10 0

link 0 true 13 true
link 0 true 14 true

#contorno
link 1 false 5 false
link 5 false 13 true
link 13 true 14 true
link 14 true 9 false
link 9 false 1 false
