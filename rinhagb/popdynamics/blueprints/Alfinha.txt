## Planta para a construção de um Grafobot.
## Somente use espaços como separador.
#
## Nodos
#       Tipo        x       y       m   r   theta
node    battery     0       0       1   10  0
node    traction  -30      30       1   10  0
node    traction  -30     -30       1   10  0
node    arenaradar  0      25       1   10  0
node    arenaradar  0     -25       1   10  0
node    noderadar  30      15       1   10  0
node    noderadar  30     -15       1   10  0
node    simple      0      60       1   10  0
node    simple      0     -60       1   10  0
node    simple     55      35       1   10  0
node    simple     55     -35       1   10  0
node    simple     90       0       1   10  0

## Links
link 0 true 1 true
link 0 true 2 true
link 0 true 3 true
link 0 true 4 true
link 0 true 5 true
link 3 true 5 true
link 4 true 6 true
link 0 true 6 true
link 1 true 2 true
link 9 true 5 true
link 9 true 5 true
link 11 true 5 true
link 11 true 5 true
link 9 true 7 true
link 3 true 7 true
link 3 true 7 true
link 1 true 7 true
link 4 true 8 true
link 4 true 8 true
link 2 true 8 true
link 9 true 11 true
link 9 true 11 true
link 9 true 10 true
link 9 true 10 true
link 6 true 10 true
link 6 true 10 true
link 10 true 8 true
link 10 true 11 true
link 10 true 11 true
link 11 true 6 true
link 11 true 6 true