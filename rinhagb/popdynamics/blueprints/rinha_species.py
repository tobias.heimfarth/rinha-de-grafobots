import sys
sys.path.append('../../')
sys.path.append('../../../')

import os
from dataclasses import dataclass

import numpy as np
import matplotlib.pyplot as plt

from neural_network import species

import librinha_rust2py as rgb


class GrafobotSpecies:
    """
    A grafobot ANN species handler
    """
    def __init__(self,
            botfile,
            pop,
            rates,
            stagnated_clock_max,
            mutation_probability,
            species_distance
        ):
        self.bot = rgb.BotDeliver(botfile)
        self.species = []
        self.pop = pop
        self.innov = 100
        self.rates = rates
        self.mutation_probability = mutation_probability
        self.stagnated_clock_max = stagnated_clock_max
        self.species_distance = species_distance
        self.id = os.path.splitext(os.path.basename(botfile))[0]
        self.gen = 0
        self.max_points_log = []
        self.species_number_log = []
        self.max_wins_log = []
        self.best_ind_points = None
        self.best_ind_wins = None
    
    def start_fresh_specie(self):
        """
        Start fresh
        """
        specie = species.Specie()
        g = []
        for _ in range(self.pop):
            g.append(species.Individual(species.Genome()))
            io = list(self.bot.get_io_numbers())
            # adds a dummy input if there is none in order to avoid
            # mutations failling
            if io[1] == 0: io[1] = 1
            g[-1].genome.generate_IO_innovations(io[1],io[0])
            g[-1].genome.fill()
            specie.add_individual(g[-1])
            for _ in range(10):
                self.innov = g[-1].genome.mutate(self.innov,self.rates)
        self.species = [specie]
    
    def append_fresh_specie(self,N_ind):
        """
        Adds a fresh specie to the existing species
        """
        specie = species.Specie()
        g = []
        for _ in range(N_ind):
            g.append(species.Individual(species.Genome()))
            io = list(self.bot.get_io_numbers())
            # adds a dummy input if there is none in order to avoid
            # mutations failling
            if io[1] == 0: io[1] = 1
            g[-1].genome.generate_IO_innovations(io[1],io[0])
            g[-1].genome.fill()
            specie.add_individual(g[-1])
            for _ in range(5):
                self.innov = g[-1].genome.mutate(self.innov,self.rates)
        self.species.append(specie)

    def tick(self):
        """
        Main call for generation tick
        """
        self.get_best_inds()
        self.max_points_log.append(self.best_ind_points.net.points)
        self.max_wins_log.append(self.best_ind_wins.net.wins)
        if self.gen%50 == 0:
            self.best_ind_wins.net.save(self.id+'_best_gen'+str(self.gen)+'.gen')
            self.save_logs()
        self.species_number_log.append(len(self.species))
        self.remove_unfit_individuals()
        self.create_next_generation_species()
        self.manage_stagnated_species()

    def remove_unfit_individuals(self):
        """
        Once all the individual has played, prepare for the next
        generation.
        """
        # Removing unfited
        for specie in self.species:
            # Computing the fitness of the individuals
            specie.fitness()
            # Sorting the individual by points
            max_points = specie.max_points
            specie.sort_by_points()
            specie.update_stagnated_clock(max_points)
            # Removint the unfit
            specie.remove_unfit(fraction=0.5)
        
    
    def create_next_generation_species(self):
        """
        If individuals inside a specie are too distant, fork the specie
        """
        # Creating new species
        
        self.gen += 1
        new_species = []
        for s in self.species:
            s.compute_specie_genetic_distance()
            s.find_representative()
            new_species += s.speciate(self.species_distance)
        self.species = new_species
        # Computing the total fitness for normalization
        total_fitness = 0
        for s in self.species:
                total_fitness += s.fitness()
        self._create_next_generation_of_individuals(total_fitness)

    
    def _create_next_generation_of_individuals(self,total_fitness):
        """
        Create the next generation of individuals in each species
        """
        # Next generation of individuals
        min_frac = 0.5
        N_min = int(round(self.pop*min_frac/ len(self.species)))
        if N_min == 0: N_min = 1
        pop_rest = self.pop - N_min * len(self.species)
        for s in self.species:
            # Number of individuals in the current specie
            if total_fitness == 0:
                N = int(round(self.pop/len(self.species)))
                if N == 0: N = N_min
            else:
                N = N_min + int(round(s.fitness()/total_fitness * pop_rest))
            # Populating specie
            keep = None
            if self.best_ind_points.net in s.individuals:
                keep = self.best_ind_points.net
                N += 1
            if self.best_ind_wins.net in s.individuals:
                keep = self.best_ind_wins.net
                N += 1
            self.innov =  s.next_generation(
                N,
                self.mutation_probability,
                self.rates,
                self.innov,
                keep_ind= keep
            )

    def manage_stagnated_species(self, irradiate_min_pop = 10):
        """
        Removing species that dont meet the minimum criteria
        """
        death_list = []
        for sn,s in enumerate(self.species):
            if not s.is_alive(self.stagnated_clock_max):
                # If the specie still have a minimum number of
                # individuals but is stagneted, try to irradiate it
                if len(s.individuals) >= irradiate_min_pop:
                    self.innov = s.irradiate(15,self.rates,self.innov)
                    s.stagnated_clock = 0
                else:
                    # Its time to go...
                    death_list.append(sn)
        # reversing the death list to avoid idex shift during poping
        death_list.reverse()
        for sn in death_list:
            self.species.pop(sn)
        if self.species == []: self.start_fresh_specie()
    
    def get_individuals_list(self):
        """
        List of individual genomes and bots
        """
        ind_list = []
        for s in self.species:
            for ind in s.individuals:
                ind_list.append(GrafobotIndividual(ind,self.bot,self.id))
        return ind_list

    def get_best_inds(self):
        """
        Returns the points of the best individual
        """
        ind_list = self.get_individuals_list()
        points = [ind.net.points for ind in ind_list]
        wins = [ind.net.wins for ind in ind_list]
        self.best_ind_points = ind_list[points.index(max(points))]
        self.best_ind_wins = ind_list[wins.index(max(wins))]
    
    def get_winner_ind(self):
        """
        Returns the individual with most wins (firs in list)
        """

    def save_logs(self):
        np.savez(self.id+'_max_points',self.max_points_log)
        np.savez(self.id+'_max_wins',self.max_wins_log)
        np.savez(self.id+'_species_number_log',self.species_number_log)



@dataclass
class GrafobotIndividual:
    net: species.Individual
    bot: rgb.BotDeliver
    bot_id: str

    def save_txt(self,prefix):
        self.net.save('./'+prefix+self.bot_id+'.gen')

    
class GrafoEvoPlot:
    def __init__(self,bots_ids):
        plt.ion()
        self.fig, self.axs = plt.subplots(nrows=3,
                                          sharex=True,
                                          gridspec_kw={'height_ratios': [1, 1,1],'hspace':0},
                                          figsize=(10,10))
        self.axs[2].set_ylabel('Espécies')
        self.axs[0].set_ylabel('Máximo de pontos')
        self.axs[1].set_ylabel('Vitórias')
        self.axs[2].set_xlabel('Geração')
        for i in bots_ids:
            self.axs[0].plot([],[])
            self.axs[1].plot([],[])
            self.axs[2].plot([],[])
        self.axs[0].legend(bots_ids, loc='upper left')

    def update_points_data(self,index,gen,max_points):
        lines = self.axs[0].get_lines()
        lines[index].set_data(gen,max_points)

    def update_wins_data(self,index,gen,wins):
        lines = self.axs[1].get_lines()
        lines[index].set_data(gen,wins)
    
    def update_species_data(self,index,gen,species_n):
        lines = self.axs[2].get_lines()
        lines[index].set_data(gen,species_n)
    
    def redraw(self):
        self.axs[0].relim()
        self.axs[0].autoscale_view()
        self.axs[1].relim()
        self.axs[1].autoscale_view()
        self.axs[2].relim()
        self.axs[2].autoscale_view()
        self.fig.canvas.draw()
        self.fig.canvas.flush_events()
    

