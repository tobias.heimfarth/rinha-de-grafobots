from random import seed
#from context import *
import rinhagb.neural_network as nn

dn = nn.genome.Genome()
dn.generate_deep_network([4,5,5,6,3])
dn.fill()
dn.plot('dn.png')

dn2 = nn.genome.Genome()
dn2.generate_deep_network([4,5,5,6,3])
dn2.fill()
dn2.plot('dn2.png')

dn_child = nn.genome.mate_genomes_just_weights(dn,dn2)
dn_child.fill()
dn_child.plot('dn_child.png')
