from random import seed

#from context import *
import rinhagb.neural_network as nn

seed(1)
g = nn.genome.Genome()
innov = g.generate_IO_innovations(2,2)

rates = nn.genome.MutationRates(
    connection          = 0.3,
    node                = 0.01,
    mix_node            = 0.1,
    disable_connection  = 0.05,
    disable_node        = 0.01,
    weight              = 0.5,
    all_weights         = 0.1,
    zero_weight         = 0.05,
    bias                = 0.1,
    int_time            = 0.2
)
                        
g.fill()
for i in range(10):
    innov = g.mutate(innov,rates)
    nodes_list = g.in_nodes + g.out_nodes + g.hidden_nodes
    for link in g.connections:
        nodes_list.index(link.in_node)
#g.plot('g.png')
#g.save('g.gen')



#h = nn.genome.load_genome('test_001_gen120.gen')
#h.fill()
#h.plot('h.png')
