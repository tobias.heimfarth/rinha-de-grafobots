from random import seed

from copy import copy,deepcopy

import rinhagb.neural_network as nn

seed(1)
g = nn.genome.Genome()
innov = g.generate_IO_innovations(1,1)

rates = nn.genome.MutationRates( 
        connection          = 0.3,
        node                = 0.01,
        mix_node            = 0.1,
        disable_connection  = 0.05,
        disable_node        = 0.01,
        weight              = 0.5,
        all_weights         = 0.1,
        zero_weight         = 0.05,
        bias                = 0.1,
        int_time            = 0.2
        )
        
g.fill()
for i in range(1):
    innov = g.mutate(innov,rates)

h = deepcopy(g)
for i in range(1):
    innov = h.mutate(innov,rates)


i = nn.genome.mate_genomes(g,h)
i.fill()
i.plot('i.png')

