from random import random, randint, seed
from copy import copy, deepcopy
from math import exp, atan

import numpy as np
#import pygraphviz as pgv

from .network import Node, Connection


class InNodeInnov:
    def __init__(self, *,
                 innov,
                 bias):
        self.innov = innov
        self.bias = bias
        self.dependencies = []

    def clean_fills(self):
        self.node = None

    def fill(self, innovations):
        self.node = Node(innov=self.innov,
                         bias=self.bias,
                         int_time=1)

    def update(self):
        self.bias = self.node.bias

    def save_line(self, separator):
        return ('InNodeInnov'+separator
                + str(self.innov)+separator
                + str(self.bias))


class OutNodeInnov:
    def __init__(self, *,
                 bias,
                 innov):
        self.innov = innov
        self.bias = bias
        self.dependencies = []

    def clean_fills(self):
        self.node = None

    def fill(self, innovations):
        self.node = Node(innov=self.innov,
                         bias=self.bias,
                         int_time=1)

    def update(self):
        self.bias = self.node.bias

    def save_line(self, separator):
        return 'OutNodeInnov'+separator+str(self.innov)+separator+str(self.bias)


class HiddenNodeInnov:
    def __init__(self, *,
                 innov,
                 bias,
                 int_time):

        self.innov = innov
        self.bias = bias
        self.int_time = int_time
        self.dependencies = []

    def clean_fills(self):
        self.node = None

    def fill(self, innovations):
        self.node = Node(innov=self.innov,
                         bias=self.bias,
                         int_time=self.int_time)

    def update(self):
        self.bias = self.node.bias
        self.int_time = self.node.int_time

    def save_line(self, separator):
        return ('HiddenNodeInnov'+separator
                + str(self.innov)+separator
                + str(self.bias) + separator
                + str(self.int_time))


class ConnectionInnov:
    def __init__(self, *,
                 innov,
                 in_node_i,
                 out_node_i,
                 weight):
        self.in_node_i = in_node_i
        self.out_node_i = out_node_i
        self.innov = innov
        self.weight = weight
        self.clean_fills()
        self.dependencies = [in_node_i, out_node_i]

    def clean_fills(self):
        self.in_node = None
        self.out_node = None
        self.node = None
        self.connection = None

    def fill(self, innovations):
        self.in_node = innovations[self.in_node_i].node
        self.in_node.connected = True
        self.out_node = innovations[self.out_node_i].node
        self.out_node.connected = True
        self.connection = Connection(innov=self.innov,
                                     in_node=self.in_node,
                                     out_node=self.out_node,
                                     weight=self.weight,
                                     enabled=True)

    def connection_by_nodes(self, in_node_i, out_node_i):
        '''
        Trivial function to math multiple connection innovations
        '''
        return self.connection

    def update(self):
        self.weight = self.connection.weight

    def save_line(self, separator):
        return ('ConnectionInnov'+separator
                + str(self.innov)+separator
                + str(self.in_node_i)+separator
                + str(self.out_node_i)+separator
                + str(self.weight))


class NodeInnov:
    def __init__(self, *,
                 innov,
                 split_connection_i,
                 in_node_i,
                 out_node_i,
                 in_connection_weight,
                 out_connection_weight,
                 node_bias,
                 node_int_time):
        self.innov = innov
        self.split_connection_i = split_connection_i
        # self.split_connection_in = split_connection_in #true if it is the in connection to be broken, false for the out one.
        self.in_node_i = in_node_i
        self.out_node_i = out_node_i
        self.in_connection_weight = in_connection_weight
        self.out_connection_weight = out_connection_weight
        self.node_bias = node_bias
        self.node_int_time = node_int_time
        self.clean_fills()
        self.dependencies = [split_connection_i]

    def clean_fills(self):
        self.in_node = None
        self.out_node = None
        self.node = None
        self.out_connection = None
        self.in_connection = None

    def fill(self, innovations):
        split_connection_innov = innovations[self.split_connection_i]
        self.split_connection = split_connection_innov.connection_by_nodes(
            self.in_node_i, self.out_node_i)
        self.split_connection.enabled = False
        self.in_node = self.split_connection.in_node
        self.out_node = self.split_connection.out_node
        self.node = Node(innov=self.innov,
                         bias=self.node_bias,
                         int_time=self.node_int_time)
        self.node.connected = True
        self.out_connection = Connection(innov=self.innov,
                                         in_node=self.node,
                                         out_node=self.out_node,
                                         weight=self.out_connection_weight,
                                         enabled=True)
        self.in_connection = Connection(innov=self.innov,
                                        in_node=self.in_node,
                                        out_node=self.node,
                                        weight=self.in_connection_weight,
                                        enabled=True)

    def update(self):
        # print(self.in_weight,self.out_weight)
        self.in_connection_weight = self.in_connection.weight
        self.out_connection_weight = self.out_connection.weight
        # print(self.in_weight,self.out_weight)
        self.node_bias = self.node.bias
        self.node_bias = self.node.int_time

    def connection_by_nodes(self, in_node_i, out_node_i):
        if self.out_connection.in_node.innov == in_node_i and self.out_connection.out_node.innov == out_node_i:
            return self.out_connection
        elif self.in_connection.in_node.innov == in_node_i and self.in_connection.out_node.innov == out_node_i:
            return self.in_connection
        else:
            print(
                'Deu problema, não bateu os in nodes e os out nodes de nenhuma conexao!!')

    def save_line(self, separator):
        return ('NodeInnov'+separator
                + str(self.innov)+separator
                + str(self.split_connection_i)+separator
                + str(self.in_node_i)+separator
                + str(self.out_node_i)+separator
                + str(self.in_connection_weight)+separator
                + str(self.out_connection_weight)+separator
                + str(self.node_bias)+separator
                + str(self.node_int_time)+separator)


# class FlipConnectionInnov:
    # def __init__(self, *,
        # innov,
        # flip_connection_i,
        # in_node_i,
        # out_node_i):
        #self.flip_connection_i = flip_connection_i
        #self.innov = innov
        #self.in_node_i = in_node_i
        #self.out_node_i = out_node_i
        #self.dependencies = [flip_connection_i]

    # def clean_fills(self):
        #self.flip_connection = None
        #self.in_node = None
        #self.out_node = None

    # def fill(self,innovations):
        #flip_connection_innov = innovations.get(self.flip_connection_i)
        # if flip_connection_innov is None:
        #self.flip_connection = None
        #self.in_node = None
        #self.out_node = None
        # else:
        #self.flip_connection = flip_connection_innov.connection_by_nodes(self.in_node_i,self.out_node_i)
        #self.flip_connection.enabled = not self.flip_connection.enabled
        #self.in_node = self.flip_connection.in_node
        #self.out_node = self.flip_connection.out_node

    # def save_line(self,separator):
        # return 'FlipConnectionInnov'+separator+str(self.innov)+separator+str(self.flip_connection_i)+separator+str(self.in_node_i)+separator+str(self.out_node_i)


class DisableConnectionInnov:
    def __init__(self, *,
                 innov,
                 disable_connection_i,
                 in_node_i,
                 out_node_i):
        self.disable_connection_i = disable_connection_i
        self.innov = innov
        self.in_node_i = in_node_i
        self.out_node_i = out_node_i
        self.dependencies = [disable_connection_i]

    def clean_fills(self):
        self.disable_connection = None
        #self.in_node = None
        #self.out_node = None

    def fill(self, innovations):
        disable_connection_innov = innovations.get(self.disable_connection_i)
        if disable_connection_innov is None:
            self.disable_connection = None
            #self.in_node = None
            #self.out_node = None
        else:
            self.disable_connection = disable_connection_innov.connection_by_nodes(
                self.in_node_i, self.out_node_i)
            self.disable_connection.enabled = False
            #self.in_node = self.disable_connection.in_node
            #self.out_node = self.disable_connection.out_node

    def save_line(self, separator):
        return ('DisableConnectionInnov'+separator
                + str(self.innov)+separator
                + str(self.disable_connection_i)+separator
                + str(self.in_node_i)+separator
                + str(self.out_node_i))


class DisableNodeInnov:
    def __init__(self, *,
                 innov,
                 disable_node_i):
        self.disable_node_i = disable_node_i
        self.innov = innov
        #self.in_node_i = in_node_i
        #self.out_node_i = out_node_i
        self.dependencies = [disable_node_i]

    def clean_fills(self):
        self.disable_node = None
        #self.in_node = None
        #self.out_node = None

    def fill(self, innovations):
        disable_node_innov = innovations.get(self.disable_node_i)
        if disable_node_innov is None:
            self.disable_node = None
            #self.in_node = None
            #self.out_node = None
        else:
            self.disable_node = disable_node_innov.node
            self.disable_node.enabled = False
            #self.in_node = self.disable_connection.in_node
            #self.out_node = self.disable_connection.out_node

    def save_line(self, separator):
        return ('DisableNodeInnov'+separator +
                str(self.innov)+separator +
                str(self.disable_node_i))


class MixNodeInnov:
    def __init__(self, *,
                 innov,
                 split_connection_i,
                 in_node_i,
                 out_node_i,
                 mix_node_i,
                 in_connection_weight,
                 out_connection_weight,
                 mix_connection_weight,
                 mix_connection_direction,
                 node_bias,
                 node_int_time):
        self.innov = innov
        self.split_connection_i = split_connection_i
        # self.split_connection_in = split_connection_in #true if it is the in connection to be broken, false for the out one.
        self.in_node_i = in_node_i
        self.out_node_i = out_node_i
        self.mix_node_i = mix_node_i
        self.in_connection_weight = in_connection_weight
        self.out_connection_weight = out_connection_weight
        self.mix_connection_weight = mix_connection_weight
        self.mix_connection_direction = mix_connection_direction
        self.node_bias = node_bias
        self.node_int_time = node_int_time
        self.clean_fills()
        self.dependencies = [split_connection_i, mix_node_i]

    def clean_fills(self):
        self.in_node = None
        self.out_node = None
        self.mix_node = None
        self.node = None
        self.out_connection = None
        self.in_connection = None
        self.mix_connection = None

    def fill(self, innovations):
        split_connection_innov = innovations[self.split_connection_i]
        self.split_connection = split_connection_innov.connection_by_nodes(
            self.in_node_i, self.out_node_i)
        self.split_connection.enabled = False
        self.in_node = self.split_connection.in_node
        self.out_node = self.split_connection.out_node
        self.node = Node(innov=self.innov,
                         bias=self.node_bias,
                         int_time=self.node_int_time)
        self.node.connected = True
        mix_node_innov = innovations[self.mix_node_i]
        self.mix_node = mix_node_innov.node
        self.mix_node.connected = True
        self.out_connection = Connection(innov=self.innov,
                                         in_node=self.node,
                                         out_node=self.out_node,
                                         weight=self.out_connection_weight,
                                         enabled=True)
        self.in_connection = Connection(innov=self.innov,
                                        in_node=self.in_node,
                                        out_node=self.node,
                                        weight=self.in_connection_weight,
                                        enabled=True)
        if self.mix_connection_direction == 0:
            self.mix_connection = Connection(innov=self.innov,
                                             in_node=self.node,
                                             out_node=self.mix_node,
                                             weight=self.mix_connection_weight,
                                             enabled=True)
        else:
            self.mix_connection = Connection(innov=self.innov,
                                             in_node=self.mix_node,
                                             out_node=self.node,
                                             weight=self.mix_connection_weight,
                                             enabled=True)

    def connection_by_nodes(self, in_node_i, out_node_i):
        if self.out_connection.in_node.innov == in_node_i and self.out_connection.out_node.innov == out_node_i:
            return self.out_connection
        elif self.in_connection.in_node.innov == in_node_i and self.in_connection.out_node.innov == out_node_i:
            return self.in_connection
        elif (self.mix_connection.in_node.innov == in_node_i and self.mix_connection.out_node.innov) == (out_node_i or self.mix_connection.in_node.innov == out_node_i and self.mix_connection.out_node.innov == in_node_i):
            return self.mix_connection
        else:
            print(
                'Deu problema, não bateu os in nodes e os out nodes de nenhuma conexao!!')

    def update(self):
        # print(self.in_weight,self.out_weight)
        self.in_connection_weight = self.in_connection.weight
        self.out_connection_weight = self.out_connection.weight
        self.mix_connection_weight = self.mix_connection.weight
        # print(self.in_weight,self.out_weight)
        self.node_bias = self.node.bias
        self.node_int_time = self.node.int_time

    def save_line(self, separator):
        return ('MixNodeInnov'+separator
                + str(self.innov)+separator
                + str(self.split_connection_i)+separator
                + str(self.in_node_i)+separator
                + str(self.out_node_i)+separator
                + str(self.mix_node_i)+separator
                + str(self.mix_connection_direction)+separator
                + str(self.in_connection_weight)+separator
                + str(self.out_connection_weight)+separator
                + str(self.mix_connection_weight)+separator
                + str(self.node_bias)+separator
                + str(self.node_int_time))

    # def load_line(self,line,separator=' '):
        #col = line.split(separator)
        # return MixNodeInnov(innov = int(col[1]),
        #split_connection = int(col[2]),
        #mix_node = int(col[3]),
        #in_connection_weight = float(col[4]),
        #out_connection_weight = float(col[5]),
        #mix_connection_weight = float(col[6]),
        #mix_connection_direction = int(col[7]),
        #node_bias = float(col[8]),
        # node_int_time = int(col[9])):


# def mate_genomes(g1,g2):
