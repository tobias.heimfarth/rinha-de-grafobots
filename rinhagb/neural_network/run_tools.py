from os import listdir
from os.path import isfile, join
from copy import copy,deepcopy
from random import random,randint

import numpy as np
import matplotlib.pyplot as plt

from math import pi,sin,cos
#from balancing_Trex import *
from .network import *


def reset_genomes():
    g = []
    for i in range(pop):
        g.append(Genome())
        g[-1].generate_deep_network_deep_memory([7,15,15,10,3],5)
        g[-1].fill()
    return g

def belong_to_specie(g,champions):
    d = []
    for champ in champions:
        d.append(genetic_distance(g,champ))
    d_order = np.argsort(d)
    return d_order[0],d[d_order[0]]


def run_play(g,theta,oticks):
    PB = MultiplePolesBalancing()
    PB.pole.theta = theta
    PB.obstacles_ticks = oticks
    #PB.initialize_graphics()
    pilot = NeuralNetwork(g)
    pilot.clear_all_stimuli()
    PB.initialize_neural_network(pilot)
    for i in range(10000):
        ## Get internal tick
        #input()
        #print(i)
        #PB.screen.fill((0,0,0))
        PB.tick = copy(i)
        #PB.move_car(PB.get_commands_keybord(pygame.key.get_pressed()))
        PB.move_car(PB.get_commands_AI())
        #PB.draw_frame()
        end = PB.update_game_positions()
        #pygame.time.Clock().tick(60)#50
        #pygame.display.update()
        if end: break
    return PB.points()



#### Plots

class EvolutionPlot:
    def __init__(self):
        plt.ion()
        self.fig = plt.figure()
        self.ax = []
        self.lines = []
        self.y = []
        self.x = []
    
    def add_new_subplot(self,pos):
        self.ax.append(self.fig.add_subplot(pos))
        self.lines.append((self.ax[-1].plot([],[]))[0])

    def update(self,x,y):
        self.x.append(x)
        for i,line in enumerate(self.lines):
            self.y[i].append(y[i])
            self.lines[i].set_data(self.x,self.y[i])

    
    def update_usual(self,gen,sorted_points):
        best_points = sorted_points[0]
        mean = np.mean(sorted_points,0)
        x = gen
        y = [best_points,mean]
        self.update(x,y)
        self.ax[0].relim()
        self.ax[0].autoscale_view()
        plt.pause(0.1)
    
    def start_usual(self):
        self.y = [[] for i in range(2)]
        self.add_new_subplot(111)
        self.add_new_subplot(111)

    def update_species(self,gen,sorted_points,species):
        best_points = sorted_points[0]
        mean = np.mean(sorted_points,0)
        x = gen
        y = [best_points,mean,species]
        self.update(x,y)
        self.ax[0].relim()
        self.ax[0].autoscale()
        self.ax[-1].relim()
        self.ax[-1].autoscale()
        plt.pause(0.2)
    
    def start_species(self):
        self.y = [[] for i in range(3)]
        self.add_new_subplot(211)
        self.lines.append((self.ax[-1].plot([],[]))[0])
        #self.add_new_subplot(211)
        self.add_new_subplot(212)
        
    
    def update_fix_species(self,gen,sorted_points,hits):
        best_points = []
        for i in range(self.speciesN):
            best_points.append(sorted_points[i][0])
        mean = np.mean(sorted_points)
        x = gen
        y = best_points + [mean,hits]
        print(y)
        self.update(x,y)
        self.ax[0].relim()
        self.ax[0].autoscale_view()
        self.ax[-1].relim()
        self.ax[-1].autoscale_view()
        plt.pause(0.05)
        
    
    def start_fix_species(self,speciesN):
        self.y = [[] for i in range(speciesN+2)]
        self.speciesN = speciesN
        for i in range(speciesN+1):
            self.add_new_subplot(211)
        self.add_new_subplot(212)
        plt.pause(0.05)

    def save(self,fname):
        np.savez(fname,self.x,self.y)
        


def argsort_random(values):
    index = np.flip(np.argsort(values),0)
    index_shuffled = []
    i_ini = 0
    i = 0
    while i < len(index):
        i_ini = i
        same_value = [index[i]]
        if i < (len(index)-1):
            while (values[index[i]] == values[index[i+1]]):
                i += 1
                same_value.append(index[i])
                if i >= (len(index)-1): break
        i += 1
        while same_value != []:
            index_shuffled.append(same_value.pop(randint(0,len(same_value)-1)))
    return index_shuffled


if __name__ == "__main__":
    g = load_genome('/home/tobiash/Dropbox/UFFS/Extra/Semana_academica_fisica_2017/python/pole_balancing/genomes/teste2_gen2.gen')
    g.fill()
    print(run_play(g,0.03))
        

