from random import random, randint, seed
import numpy as np
import pygraphviz as pgv
from copy import copy, deepcopy
from math import exp, atan
from .innovations import *


class Genome:
    def __init__(self, innovations={}):
        self.innovations = innovations
        self.in_nodes = []
        self.out_nodes = []
        self.hidden_nodes = []
        self.connections = []

    def generate_IO_innovations(self, in_N, out_N, innov=0):
        self.innovations = {}
        for i in range(in_N):
            self.innovations.update({innov: InNodeInnov(innov=innov, bias=0.)})
            innov += 1
        for i in range(out_N):
            self.innovations.update(
                {innov: OutNodeInnov(innov=innov, bias=0.)})
            innov += 1
        return innov

    def clear_all_stimuli(self):
        for node in self.hidden_nodes + self.out_nodes + self.in_nodes:
            node.clear_stimuli()
    
    def add_hidden_node(self,innov):
        inv_node = HiddenNodeInnov(innov=innov, bias=0., int_time=0)
        self.add_innovation(inv_node)
        return innov + 1

    def generate_deep_network(self,layers,innov = 0):
        print("Warning! innovations erased.")
        innov = self.generate_IO_innovations(layers[0],layers[-1])
        innov_list_layers = []
        for i,layer_n_nodes in enumerate(layers[1:-1]):
            layer_innov_num = []
            for node in range(layer_n_nodes):
                inv_node = HiddenNodeInnov(innov=innov, bias=0., int_time=0)
                self.add_innovation(inv_node)
                layer_innov_num.append(innov)
                innov +=1
            innov_list_layers.append(layer_innov_num)
        self.fill()
        # annotating out nodes
        innov_list_layers = [[node.innov for node in self.in_nodes]] + innov_list_layers
        innov_list_layers.append([node.innov for node in self.out_nodes])
        # connections
        print(innov_list_layers)
        for i,i_layer in enumerate(layers[:-1]):
            for j in range(i_layer):
                for k in range(layers[i+1]):
                    weight = (random()-0.5)*2
                    inv = ConnectionInnov(
                            innov=innov,
                            in_node_i= innov_list_layers[i][j],
                            out_node_i= innov_list_layers[i+1][k],
                            weight=weight
                            )
                    self.add_innovation(inv)
                    innov +=1
        return innov

    def clean_fills(self):
        for i in self.innovations.values():
            i.clean_fills()
        self.in_nodes = []
        self.out_nodes = []
        self.hidden_nodes = []
        self.connections = []

    def fill(self, cut=None):
        self.clean_fills()
        innovs = sorted(list(self.innovations.keys()))
        # option leave the last innovations out
        if cut is not None:
            innovs = innovs[:cut]
        for innov in innovs:
            i = self.innovations[innov]
            i.fill(self.innovations)
            self.append_element(i)

    def append_element(self, i):
        if type(i) == InNodeInnov:
            self.in_nodes.append(i.node)
        elif type(i) == HiddenNodeInnov:
            self.hidden_nodes.append(i.node)
        elif type(i) == OutNodeInnov:
            self.out_nodes.append(i.node)
        elif type(i) == ConnectionInnov:
            self.connections.append(i.connection)
        elif type(i) == NodeInnov:
            self.hidden_nodes.append(i.node)
            self.connections.append(i.in_connection)
            self.connections.append(i.out_connection)
            if i.split_connection in self.connections:
                self.connections.remove(i.split_connection)
        elif type(i) == MixNodeInnov:
            self.hidden_nodes.append(i.node)
            self.connections.append(i.in_connection)
            self.connections.append(i.out_connection)
            self.connections.append(i.mix_connection)
            if i.split_connection in self.connections:
                self.connections.remove(i.split_connection)              

        elif type(i) == DisableNodeInnov:
            if i.disable_node in self.hidden_nodes:
                self.disable_element(i.disable_node)
        elif type(i) == DisableConnectionInnov:
            if i.disable_connection in self.connections:
                self.disable_element(i.disable_connection)
        elif type(i) == FlipConnectionInnov:
            pass
        else:
            raise ValueError('Not known type: ', type(i))


    def add_innovation(self, inv):
        '''
        Adds an innovation (all the hard work is inside the innovations classes)
        '''
        inv.fill(self.innovations)
        self.innovations.update({inv.innov: inv})
        self.append_element(inv)

    def mutate_add_connection(self, innov):
        # get random nodes indices
        n_in_node = randint(0, len(self.in_nodes)+len(self.hidden_nodes)-1)
        n_out_node = randint(0, len(self.hidden_nodes)+len(self.out_nodes)-1)
        # label the chosen ones
        in_node = (self.in_nodes+self.hidden_nodes)[n_in_node]
        out_node = (self.hidden_nodes+self.out_nodes)[n_out_node]
        # checks for reconnection
        for con in self.connections:
            if con.in_node.innov == in_node.innov and con.out_node.innov == out_node.innov:
                #print('Repeted connection, skipping mutation.')
                return innov
        # Creates the innovation
        inv = ConnectionInnov(innov=innov,
                              in_node_i=in_node.innov,
                              out_node_i=out_node.innov,
                              weight=(random()-0.5)*2)  # ** ver esse -0.5
        self.add_innovation(inv)
        #print('connection added from node ',in_node.innov,' to ',out_node.innov, 'innov: ', innov)
        return innov+1

    def mutate_add_node(self, innov):
        n_connect = randint(0, len(self.connections)-1)
        split_connection = self.connections[n_connect]
        inv = NodeInnov(innov=innov,
                        split_connection_i=split_connection.innov,
                        in_node_i=split_connection.in_node.innov,
                        out_node_i=split_connection.out_node.innov,
                        in_connection_weight=1,
                        out_connection_weight=split_connection.weight,
                        node_bias=0.,
                        node_int_time=1)
        self.add_innovation(inv)
        #print('node added to connection ',n_connect)
        return innov + 1

    def mutate_add_mix_node(self, innov):
        n_connect = randint(0, len(self.connections)-1)
        split_connection = self.connections[n_connect]
        if random() > 0.5:  # **
            n_mix_node = randint(0, len(self.in_nodes+self.hidden_nodes)-1)
            mix_node = (self.in_nodes+self.hidden_nodes)[n_mix_node]
            mix_connection_direction = 1
            if mix_node == split_connection.in_node:
                #print('Split connection in_node = mix_node, skipping mutation.')
                return innov
        else:
            n_mix_node = randint(0, len(self.out_nodes+self.hidden_nodes)-1)
            mix_node = (self.out_nodes+self.hidden_nodes)[n_mix_node]
            mix_connection_direction = 0
            if mix_node == split_connection.out_node:
                #print('Split connection out_node = mix_node, skipping mutation.')
                return innov

        mix_connection_weight = (random()-0.5)*2
        inv = MixNodeInnov(innov=innov,
                           split_connection_i=split_connection.innov,
                           in_node_i=split_connection.in_node.innov,
                           out_node_i=split_connection.out_node.innov,
                           mix_node_i=mix_node.innov,
                           in_connection_weight=1.,
                           out_connection_weight=split_connection.weight,
                           mix_connection_weight=mix_connection_weight,
                           mix_connection_direction=mix_connection_direction,
                           node_bias=0.,
                           node_int_time=1)
        self.add_innovation(inv)
        #print('node added to connection ',n_connect)
        return innov + 1

    # def mutate_flip_connection(self,innov):
        #n_connect = randint(0,len(self.connections)-1)
        #flip_connection = self.connections[n_connect]
        #inv = FlipConnectionInnov(flip_connection.innov,flip_connection.in_node.innov,flip_connection.out_node.innov,innov)
        # self.add_innovation(inv)
        ##print('Flipped connection ',n_connect)
        # return innov + 1

    def mutate_disable_connection(self, innov):
        n_connect = randint(0, len(self.connections)-1)
        disable_connection = self.connections[n_connect]
        inv = DisableConnectionInnov(innov=innov,
                                     disable_connection_i=disable_connection.innov,
                                     in_node_i=disable_connection.in_node.innov,
                                     out_node_i=disable_connection.out_node.innov)
        self.add_innovation(inv)
        #print('Flipped connection ',n_connect)
        return innov + 1

    def mutate_disable_node(self, innov):
        if self.hidden_nodes == []:
            #print('No node to disable, skipping..')
            return innov
        n_connect = randint(0, len(self.hidden_nodes)-1)
        disable_node = self.hidden_nodes[n_connect]
        inv = DisableNodeInnov(innov=innov,
                               disable_node_i=disable_node.innov)
        self.add_innovation(inv)
        #print('Flipped connection ',n_connect)
        return innov + 1

    def mutate_change_weight(self, innov):
        # change weight
        n_connect = randint(0, len(self.connections)-1)
        rand_weight_change = random()
        if rand_weight_change < 0.9:
            old_weight = self.connections[n_connect].weight
            new_weight = old_weight + 0.1*(random()-0.5)
        else:
            old_weight = self.connections[n_connect].weight
            new_weight = (random()-0.5) * 2
        self.connections[n_connect].weight = new_weight
        self.innovations[self.connections[n_connect].innov].update()
        #print('Weight of connection ',n_connect,' changed from ', old_weight, 'to ',new_weight)
        return innov

    def mutate_change_all_weights(self, innov):
        # change weight
        for con in self.connections:
            old_weight = con.weight
            if random() < 0.90:
                new_weight = old_weight + 0.05*(random()-0.5)
            else:
                new_weight = (random()-0.5) * 2
            con.weight = new_weight
            self.innovations[con.innov].update()
        #print('Weight of connection ',n_connect,' changed from ', old_weight, 'to ',new_weight)
        return innov

    def mutate_zero_weight(self, innov):
        # change weight
        n_connect = randint(0, len(self.connections)-1)
        self.connections[n_connect].weight = 0.
        self.innovations[self.connections[n_connect].innov].update()
        #print('Weight of connection ',n_connect,' changed from ', old_weight, 'to ',new_weight)
        return innov

    def mutate_change_bias(self, innov):
        # change weight
        nodes = self.hidden_nodes+self.out_nodes
        n_node = randint(0, len(nodes)-1)
        nodes[n_node].bias += 0.1*(random()-0.5)
        self.innovations[nodes[n_node].innov].update()
        #print('Weight of connection ',n_connect,' changed from ', old_weight, 'to ',new_weight)
        return innov

    def mutate_change_int_time(self, innov):
        # change weight
        if self.hidden_nodes == []:
            print('No int time to change, skipping..')
            return innov
        nodes = self.hidden_nodes
        n_node = randint(0, len(nodes)-1)
        if random() > .5 and nodes[n_node].int_time > 1:
            nodes[n_node].int_time -= 1
        else:
            nodes[n_node].int_time += 1
        self.innovations[nodes[n_node].innov].update()
        return innov

    def mutate(self, innov, rates):
        # at least one connection must exist
        if len(self.connections) == 0:
            innov = self.mutate_add_connection(innov)
            return innov
        # If one connection exist, then we can proceed
        # add connection
        rand = random()
        mut = {
            self.mutate_add_connection: rates.connection,
            self.mutate_add_node: rates.node,
            self.mutate_add_mix_node: rates.mix_node,
            self.mutate_disable_connection: rates.disable_connection,
            self.mutate_disable_node: rates.disable_node,
            self.mutate_change_bias: rates.bias,
            self.mutate_change_all_weights: rates.all_weights,
            self.mutate_zero_weight: rates.zero_weight,
            self.mutate_change_weight: rates.weight,
            self.mutate_change_int_time: rates.int_time
        }
        mut_f = (np.random.choice(list(mut.keys()), 1,
                 p=list(mut.values()), replace=True))[0]
        innov = mut_f(innov)
        return innov

    def disable_element(self, disable_element):
        #print('Disabling element: ', disable_element.innov)
        affected_nodes = []
        affected_connections = []
        if type(disable_element) == Connection:
            affected_connections.append(disable_element)
        elif type(disable_element) == Node:
            affected_nodes.append(disable_element)
            in_con, out_con = self.node_connections(disable_element)
            # removing duplicates (lose order)
            affected_connections += list(set(in_con+out_con))
            #print([i.innov for i in affected_connections])  # **

        while affected_connections != [] or affected_nodes != []:
            for con in affected_connections:
                affected_nodes.append(con.in_node)
                affected_nodes.append(con.out_node)
                con.disable = True
                # print(con.innov,'asfd')
                self.connections.remove(con)
                #print(con.innov, 'connection removed')
            affected_connections = []
            for node in affected_nodes:
                #print(node.innov,'node innov')
                in_con, out_con = self.node_connections(node)
                if in_con == [] and out_con == []:
                    node.enabled = False
                    if node in self.hidden_nodes:
                        self.hidden_nodes.remove(node)
                        #print(node.innov, 'node removed')
                    else:
                        pass
                        #print("not a hidden node, not removing")
                elif (in_con == [] and node not in self.in_nodes) or (out_con == [] and node not in self.out_nodes):
                    affected_connections = affected_connections + in_con + out_con
                    # print(in_con,out_con)
            affected_connections = list(set(affected_connections))
            affected_nodes = []

    def node_connections(self, node):
        node_in_connections = []
        node_out_connections = []
        for con in self.connections:
            if con.in_node == node:
                node_out_connections.append(con)
            if con.out_node == node:
                node_in_connections.append(con)
        # print(node_in_connections,node_out_connections)
        return node_in_connections, node_out_connections

    def generate_genotype(self):
        '''
        Generate a lighter version of the genome to be used in the neural network, removing
        unused nodes and other unnecessary stuff
        '''
        in_nodes = []
        connected = False
        for in_node in self.in_nodes:
            for con in self.connections:
                if in_node.innov == con.in_node.innov:
                    connected = True
                break
            if connected:
                in_nodes.append(in_node)
        return Genotype(in_nodes, self.hidden_nodes[:], self.out_nodes[:], self.connections[:])

    def plot(self, fileName):
        G = pgv.AGraph(directed=True, strict=True)
        in_subgraph = []
        for node in self.in_nodes:
            #connected = False
            # for c in self.connections:
            #    if c.in_node == node or c.out_node == node: connected = True
            #if node.connected:
            label = str(node.innov)  # +' '+str(node.bias)
            G.add_node(label, shape='square')
            in_subgraph.append(label)
        G.add_subgraph(in_subgraph, rank='same')
        for node in self.hidden_nodes:
            # +' '+str(node.bias)+' '+str(node.int_time)
            label = str(node.innov)
            G.add_node(label)
        out_subgraph = []
        for node in self.out_nodes:
            label = str(node.innov)  # +' '+str(node.bias)
            G.add_node(label, shape='circle')
            out_subgraph.append(label)
        G.add_subgraph(out_subgraph, rank='same')
        for con in self.connections:
            if con.enabled:
                inNode = str(con.in_node.innov)
                outNode = str(con.out_node.innov)
                G.add_edge(inNode, outNode, weith=con.weight,
                           label='{0:.2f}'.format(con.weight))
        # G.layout(prog="circo")
        G.layout(prog="dot")
        G.draw(fileName)

    def save_dot(self, fileName):
        G = pgv.AGraph(directed=True, strict=True)
        in_subgraph = []
        for node in self.in_nodes:
            #connected = False
            # for c in self.connections:
            #    if c.in_node == node or c.out_node == node: connected = True
            #if node.connected:
            label = str(node.innov)  # +' '+str(node.bias)
            G.add_node(label, shape='square')
            in_subgraph.append(label)
        G.add_subgraph(in_subgraph, rank='same')
        for node in self.hidden_nodes:
            # +' '+str(node.bias)+' '+str(node.int_time)
            label = str(node.innov)
            G.add_node(label)
        out_subgraph = []
        for node in self.out_nodes:
            label = str(node.innov)  # +' '+str(node.bias)
            G.add_node(label, shape='circle')
            out_subgraph.append(label)
        G.add_subgraph(out_subgraph, rank='same')
        for con in self.connections:
            if con.enabled:
                inNode = str(con.in_node.innov)
                outNode = str(con.out_node.innov)
                G.add_edge(inNode, outNode, weith=con.weight,
                           label='{0:.2f}'.format(con.weight))
        # G.layout(prog="circo")
        G.write(fileName)

    def save(self, fileName, separator=' '):
        savefile = open(fileName, 'w')
        for i in self.innovations.values():
            line = i.save_line(separator)
            savefile.write(line+'\n')
        savefile.close()


class MutationRates:
    def __init__(self, *,
                 connection,
                 node,
                 mix_node,
                 disable_connection,
                 disable_node,
                 weight,
                 all_weights,
                 zero_weight,
                 bias,
                 int_time):
        norm = [connection,
                node,
                mix_node,
                disable_connection,
                disable_node,
                weight,
                all_weights,
                zero_weight,
                bias,
                int_time]
        norm = 1/sum(norm)
        self.connection = connection * norm
        self.node = node * norm
        self.mix_node = mix_node * norm
        self.disable_connection = disable_connection * norm
        self.disable_node = disable_node * norm
        self.weight = weight * norm
        self.all_weights = all_weights * norm
        self.zero_weight = zero_weight * norm
        self.bias = bias * norm
        self.int_time = int_time * norm


def mate_genomes(g1, g2):
    # create list of keys
    g1 = deepcopy(g1)
    g2 = deepcopy(g2)
    innovs = list(g1.innovations.keys())
    for k in g2.innovations.keys():
        if k not in g1.innovations:
            innovs.append(k)
    sorted(innovs, reverse=True)
    # iterate throught the innovations
    child_innovations = {}
    g1_dependencies = []
    g2_dependencies = []
    for innov in innovs:
        g1_i = g1.innovations.get(innov)
        g2_i = g2.innovations.get(innov)
        if random() > 0.5:
            if g1_i is not None:
                child_innovations.update({innov: copy(g1_i)})
                g1_dependencies += g1_i.dependencies
        else:
            if g2_i is not None:
                child_innovations.update({innov: copy(g2_i)})
                g2_dependencies += g2_i.dependencies

    ##
    def satisfy_dendency(dependencies, g):
        new_deps = []
        for dep in dependencies:
            if dep not in child_innovations:
                i = copy(g.innovations[dep])
                child_innovations.update({dep: i})
                new_deps += i.dependencies
        return new_deps

    while g1_dependencies != []:
        #print(g1_dependencies,'g1 antes')
        g1_dependencies = satisfy_dendency(g1_dependencies, g1)
        #print(g1_dependencies,'g1 depois')
    while g2_dependencies != []:
        #print(g2_dependencies,'g2 antes')
        g2_dependencies = satisfy_dendency(g2_dependencies, g2)
        #print(g2_dependencies,'g2 depois')

    return Genome(innovations=child_innovations)

    #inovations = [[None,None]]*inov
    # for i in g1.in_nodes + g1.connections:
    #inovations[i.inov][0] = copy.copy(i)
    # for in_node in g2.in_nodes:
    #inov_hidden_nodes[in_node.inov][0] = copy.copy(in_node)
    # combining genomes
    # for i in inovations:
    #if i[0] is not None and i[1] is not None: pass
    # ** list = sorted(dict)


def genetic_distance(g1, g2, normalize=True):
    '''
    compares the genomes relateness
    '''
    #common = 0
    # for i in g1.innovations.keys():
    #if i in g2.innovations: common += 1
    #total = len(g1.innovations)+len(g2.innovations) - common

    dist = 0
    for i in g1.hidden_nodes:  # ** ineficiente
        d = None
        for j in g2.hidden_nodes:
            if i.innov == j.innov:
                d = abs(i.bias-j.bias)/2
                if d > 1:
                    d = 1
                dist += d
                break
        if d is None:
            dist += 1

    for i in g1.connections:  # ** ineficiente
        d = None
        for j in g2.connections:
            if i.in_node.innov == j.in_node.innov and i.out_node.innov == j.out_node.innov:
                d = abs(i.weight-j.weight)/2
                if d > 1:
                    d = 1
                dist += d
                break
        if d is None:
            dist += 1

    den = len(g1.connections)+len(g2.connections) + \
        len(g1.hidden_nodes)+len(g2.hidden_nodes)
    if den == 0:
        norm_dist = 0
    else:
        norm_dist = dist/den
    if not normalize:
        return dist

    # print(total,common)
    # for i in g2.innovations.keys():
       #if i not in g1: d += 1
    # return (total-common)/total +
    return norm_dist


def genetic_deep_distance(g1, g2):
    '''
    compares the genomes relateness
    '''
    d = 0
    for i, g1c in enumerate(g1.connections):
        d += abs(g1.connections[i].weight - g2.connections[i].weight)
    # pseudo normalizing assuming the maximum range is from -1 to 1
    d = d/(len(g1.connections)*2)
    # print(total,common)
    # for i in g2.innovations.keys():
    #if i not in g1: d += 1
    return d


def load_genome(fileName, separator=' '):
    innovations = {}
    loadfile = open(fileName, 'r')
    for line in loadfile:
        col = line[:-1].split(' ')
        innov = int(col[1])
        if col[0] == 'InNodeInnov':
            i = InNodeInnov(innov=int(col[1]),
                            bias=float(col[2]))
            innovations.update({innov: i})
        elif col[0] == 'HiddenNodeInnov':
            i = HiddenNodeInnov(innov=int(col[1]),
                                bias=float(col[2]),
                                int_time=int(col[3]))
            innovations.update({innov: i})
        elif col[0] == 'OutNodeInnov':
            i = OutNodeInnov(innov=int(col[1]),
                             bias=float(col[2]))
            innovations.update({innov: i})
        elif col[0] == 'ConnectionInnov':
            i = ConnectionInnov(innov=int(col[1]),
                                in_node_i=int(col[2]),
                                out_node_i=int(col[3]),
                                weight=float(col[4]))
            innovations.update({innov: i})
        elif col[0] == 'NodeInnov':
            i = NodeInnov(innov=int(col[1]),
                          split_connection_i=int(col[2]),
                          in_node_i=int(col[3]),
                          out_node_i=int(col[4]),
                          in_connection_weight=float(col[5]),
                          out_connection_weight=float(col[6]),
                          node_bias=float(col[7]),
                          node_int_time=int(col[8]))
            innovations.update({innov: i})
        elif col[0] == 'MixNodeInnov':
            i = MixNodeInnov(innov=int(col[1]),
                             split_connection_i=int(col[2]),
                             in_node_i=int(col[3]),
                             out_node_i=int(col[4]),
                             mix_node_i=int(col[5]),
                             mix_connection_direction=int(col[6]),
                             in_connection_weight=float(col[7]),
                             out_connection_weight=float(col[8]),
                             mix_connection_weight=float(col[9]),
                             node_bias=float(col[10]),
                             node_int_time=int(col[11]))

            innovations.update({innov: i})
        # elif col[0] == 'FlipConnectionInnov':
            #i = FlipConnectionInnov(int(col[2]), int(col[3]), int(col[4]),innov)
            # innovations.update({innov:i})
        elif col[0] == 'DisableConnectionInnov':
            i = DisableConnectionInnov(innov=int(col[1]),
                                       disable_connection_i=int(col[2]),
                                       in_node_i=int(col[3]),
                                       out_node_i=int(col[4]))
            innovations.update({innov: i})
        elif col[0] == 'DisableNodeInnov':
            i = DisableNodeInnov(innov=int(col[1]),
                                 disable_node_i=int(col[2]))
            innovations.update({innov: i})
        else:
            print('Unidentified line, skiping: ', line)
    return Genome(innovations=innovations)


def mate_genomes_just_weights(g1 ,g2):
    """
    Mates two identical networks by mixing its connections weights
    """
    g1 = deepcopy(g1)
    g2 = deepcopy(g2)
    for i in range(len(g1.connections)):
        if random() > 0.5:
            g1.connections[i].weight = g2.connections[i].weight
    for i in range(len(g1.hidden_nodes)):
            if random() > 0.5:
                g1.hidden_nodes[i].bias = g2.hidden_nodes[i].bias
    for i in range(len(g1.out_nodes)):
            if random() > 0.5:
                g1.out_nodes[i].bias = g2.out_nodes[i].bias
    g1.clean_fills()
    g1.clear_all_stimuli()
    return g1
