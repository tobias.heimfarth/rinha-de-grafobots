# from random import random,randint,seed
# import numpy as np
# import pygraphviz as pgv
# from copy import copy,deepcopy
from math import exp, atan


class NeuralNetwork:
    def __init__(self, genome):
        self.genome = genome
        self.in_nodes = genome.in_nodes
        self.hidden_nodes = genome.hidden_nodes
        self.out_nodes = genome.out_nodes
        self.connections = genome.connections
        #self.network = self.create_network(network_shape)
        self.clear_all_stimuli()
        self.transfer_function = self.retif

    def sigmoidal(self, x):
        # sigmoidal
        return 1/(1+exp(-4.9*x))

    def heavyside(self, x):
        # heavyside
        if x > 0.5:
            return 1
        else:
            return 0

    def retif(self, x):
        # ramp
        if x < 0:
            return 0
        else:
            if x < 1:
                return x
            else:
                return 1

    def atan_retif(self, x):
        # ramp
        if x <= 0:
            return 0
        else:
            return atan(x)

    def act(self, inputs, raw_output=False):
        # updating the input
        for i, node in enumerate(self.in_nodes):
            if node.connected:
                node.output = inputs[i]
        # updating the hidden nodes output
        for node in self.hidden_nodes:
            node.output = self.transfer_function(sum(node.input_)+node.bias)
        # generating boolean output from output values
        action = []
        if raw_output:
            for node in self.out_nodes:
                action.append(sum(node.input_)+node.bias)
        else:
            for node in self.out_nodes:
                node.output = self.transfer_function(
                    sum(node.input_)+node.bias)
                #action.append(node.output > 0.5)
                action.append(node.output)

        # Stimuli all network for the next round
        self.update_all_inputs()
        for con in self.connections:
            if con.enabled:
                con.out_node.stimuli(con.weight*con.in_node.output)
        return action

    def update_all_inputs(self):
        for node in self.hidden_nodes + self.out_nodes + self.in_nodes:
            node.update_input()

    def clear_all_stimuli(self):
        for node in self.hidden_nodes + self.out_nodes + self.in_nodes:
            node.clear_stimuli()


class Connection:
    def __init__(self, in_node, out_node, weight, enabled, innov):
        self.in_node = in_node
        self.out_node = out_node
        self.weight = weight
        self.enabled = enabled
        self.innov = innov


class Node:
    def __init__(self, bias, int_time, innov):
        self.innov = innov
        self.input_ = [0]*int_time
        self.output = 0
        self.int_time = int_time
        self.connected = False
        self.bias = bias

    def stimuli(self, stimuli_value):
        #self.stimulus = np.append(self.stimulus,stimuli_value)
        self.input_[-1] += stimuli_value

    def clear_stimuli(self):
        #self.stimulus = np.array([])
        self.input_ = [0]*self.int_time

    def update_input(self):
        self.input_.pop(0)
        self.input_.append(0)


class Genotype:
    def __init__(self, in_nodes, hidden_nodes, out_nodes, connections):
        self.in_nodes = in_nodes
        self.out_nodes = out_nodes
        self.hidden_nodes = hidden_nodes
        self.connections = connections
