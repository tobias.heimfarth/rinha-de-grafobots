from copy import copy,deepcopy
from random import random,randint
import numpy as np
from math import pi
from .network import *
from .genome import *
############### Start fresh

def belong_to_specie(g,champions):
    d = []
    for champ in champions:
        d.append(genetic_distance(g,champ))
    d_order = np.argsort(d)
    return d_order[0],d[d_order[0]]


class Individual:
    def __init__(self,genome):
        self.genome = genome
        self.points = 0
        self.wins = 0
        self.hits = 0
        self.fitness = 0
        self.sh = 0
        self.sdist = 0

    
    def compute_fitness(self):
        self.fitness = self.points
    
    def save(self,filename):
        self.genome.save(filename)
    
    def clear(self):
        self.points = 0
        self.hits = 0
        self.fitness = 0
        self.sh = 0
        self.sdist = 0
    
class Specie:
    def __init__(self):
        self.individuals = []
        self.max_points = 0
        self.stagnated_clock = 0
        self.generations_alive_clock = 0
        self.reproduce = True
        self.representative = None
        #self.stagnated_clock_max = 50
        
    
    def compute_specie_genetic_distance(self):
        for i in self.individuals:
            i.sdist = 0
        for i_n,i in enumerate(self.individuals):
            for j in self.individuals[:i_n]:
                d = genetic_distance(i.genome,j.genome)
                i.sdist += d
                j.sdist += d
        for i_n,i in enumerate(self.individuals):
            i.sdist = i.sdist/(len(self.individuals))
    
    def find_representative(self):
        d = [i.sdist for i in self.individuals]
        d_arg = np.argsort(d)
        #print([d[i] for i in d_arg])
        self.representative = self.individuals[d_arg[0]] 
    
    def speciate(self,threshold):
        '''
        If there is enougth genetic distance, split specie
        '''
        outsiders = []
        for i in self.individuals:
            if i.sdist > threshold and genetic_distance(i.genome,self.representative.genome,normalize=False) >3:
                outsiders.append(i)
        individuals = self.individuals[:]
        self.individuals = []
        new_species = [self]
        for i in outsiders:
           new_species.append(Specie())
          #new_species[-1].add_individual(i)
           new_species[-1].stagnated_clock = self.stagnated_clock
           new_species[-1].generations_alive_clock = self.generations_alive_clock
        representatives_genomes = [self.representative.genome]+[ind.genome for ind in outsiders] 
        for ind in individuals:
            in_specie,distance = belong_to_specie(ind.genome,representatives_genomes)
            new_species[in_specie].add_individual(ind)
        #print('Speciating',len(new_species),[(len(s.individuals),s.stagnated_clock) for s in new_species])
        ## Cleaning possible empty species
        new_species = [s for s in new_species if len(s.individuals) > 0]
        return new_species
    
    def is_alive(self,max_clock):
        if max_clock is Node:
            return self.individuals == []
        if self.individuals == [] or self.stagnated_clock > max_clock:
            return False
        else:
            return True
    
    def irradiate(self,times,rates,innov,preserve_first=False):
        if preserve_first:
            for ind in self.individuals[1:]:
                for i in range(times):
                    innov = ind.genome.mutate(innov,rates=rates)
        else:
            for ind in self.individuals:
                for i in range(times):
                    innov = ind.genome.mutate(innov,rates=rates)
        return innov
        
    def add_individual(self,ind):
        self.individuals.append(ind)
    
    def clear(self):
        for i in self.individuals:
            i.clear()

    def new_gen_from_best(self,N,mut,rates,innov):
        self.generations_alive_clock += 1
        self.individuals = self.individuals[:1]
        for i in range(N-1):
            ind = deepcopy(self.individuals[0])
            ind.genome.fill()
            for i in range(mut):
                innov = ind.genome.mutate(innov,rates=rates)
            self.add_individual(ind)
        self.clear()
        return innov
                
            
        
    def sort_by_points(self):
        values = []
        for ind in self.individuals:
            values.append(ind.points)
        index = np.flip(np.argsort(values),0)
        index_shuffled = []
        i_ini = 0
        i = 0
        while i < len(index):
            i_ini = i
            same_value = [index[i]]
            if i < (len(index)-1):
                while (values[index[i]] == values[index[i+1]]):
                    i += 1
                    same_value.append(index[i])
                    if i >= (len(index)-1): break
            i += 1
            while same_value != []:
                index_shuffled.append(same_value.pop(randint(0,len(same_value)-1)))
        self.individuals = [self.individuals[i] for i in index_shuffled]
        # Updating max points
        if self.individuals[0].points > self.max_points:
            self.max_points = self.individuals[0].points
        

    def update_stagnated_clock(self,threshold):
        if self.individuals[0].points > threshold:
            self.stagnated_clock = 0
        else:
            self.stagnated_clock += 1
        
    def remove_unfit(self,fraction=0.5):
        n = int(fraction*len(self.individuals))
        if n == 0: n=1
        self.individuals = self.individuals[:n]

    def random_individual(self):
        n = randint(0,len(self.individuals)-1)
        return self.individuals[n]

        
    def next_generation(self,N,mutation_probability,rates,innov,keep_ind=None):
        ng = []
        self.generations_alive_clock += 1
        if keep_ind is not None and keep_ind in self.individuals:
            child = deepcopy(keep_ind.genome)
            child.fill()
            ng.append(Individual(child))
            N -= 1
        if N > 5:
            child = deepcopy(self.individuals[0].genome)
            child.fill()
            ng.append(Individual(child))
            N -= 1
        #for i in range(int(0.25*N)):
            #i1 = randint(0,len(self.individuals)-1)
            #child = deepcopy(self.individuals[i1].genome)
            #child.fill()
            #while random() < mutation_probability:
                #innov = child.mutate(innov,rates)
            #ng.append(Individual(child))
            #N = N-int(0.25*N)
        prob = np.array([i.points for i in self.individuals])
        prob_sum = np.sum(prob)
        if prob_sum == 0:
            prob = [1/len(self.individuals)]*len(self.individuals)
        else:
            prob = prob/prob_sum
        for i in range(N):
            i1,i2 = np.random.choice(self.individuals,2,p=prob,replace=True)
            #i2 = randint(0,len(self.individuals)-1)
            child = mate_genomes(i1.genome,i2.genome)
            child.fill()
            while random() < mutation_probability:
                innov = child.mutate(innov,rates)
            ng.append(Individual(child))
        self.individuals = ng
        return innov
        
    def fitness(self):
        if len(self.individuals) == 0: return 0
        fit = 0
        for i in self.individuals:
            i.compute_fitness()
            fit += i.fitness
        return fit/len(self.individuals)
    



