## Planta para a construção de um Grafobot.
## Somente use espaços como separador.
#
## Nodos
#       Tipo        x       y   m   r   theta
node    battery     0       0   1   10   0
node    traction    0      40   1   10   0.
node    traction    0     -40   1   10  -0.
node    arenaradar  30     40   1   10   0.8
node    arenaradar  30    -40   1   10  -0.8
node    noderadar   24     17   1   10   0.8
node    noderadar   24    -17   1   10  -0.8
# Arma  
node	torsion     98      0     1   10  0
node	simple      150     0     1   10  3.1415
node	simple      72     45.03      1   10  0
node	simple      72    -45.03     1   10  0
#
##Links
# NodeA NodeB
link 0 true 1 true
link 0 true 2 true
link 0 true 5 true
link 0 true 6 true
link 0 true 1 true
link 1 true 3 true
link 2 true 4 true
link 5 true 3 true
link 6 true 4 true
link 1 true 5 true
link 2 true 6 true
link 5 true 6 true
#Arma
link 5 true 7 false
link 6 true 7 false
link 0 true 7 false
link 7 true 8 false
link 7 true 9 false
link 7 true 10 false
#
link 7 true 8 false
link 7 true 9 false
link 7 true 10 false
#
link 8 true 9 true
link 9 true 10 true
link 8 true 10 true
link 3 true 7 false
link 4 true 7 false