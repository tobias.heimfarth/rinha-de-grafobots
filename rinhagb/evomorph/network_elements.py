"""
First try: Build a tree like structure and make a second
round of connections by givin nodes a set of charges that
can connect to nearby nodes with the same charge.
For now the main structure is constructed by a set of blocks that can contain
slots and nodes. The slots are placeholder for down the tree new blocks.
After the node are positioned, the links are added using a charge scheme.
"""

"""
Trying to use a data oriented kind of architecture, where the
data is mostly separated from the methods. Especially in the
mutations section (moved to another file).
"""




import rinhagb.resources.librinha_rust2py as rgb
from rinhagb.id_manager import IdManager
from vectors_py import Vector
import pickle
from copy import copy, deepcopy
from math import pi
import random
from enum import Enum, auto
from dataclasses import dataclass, field

@dataclass
class NetElementsIdManager:
    node: IdManager = field(default_factory=IdManager)
    block: IdManager = field(default_factory=IdManager)
    slot: IdManager = field(default_factory=IdManager)
    network: IdManager = field(default_factory=IdManager)

# class IdManager:
#     '''
#     Id generators
#     '''
#     def __init__(self):
#         self.current_nid = 0
#         self.current_bid = 0
#         self.current_sid = 0
#         self.nid_gen: Generator[int, None, None] = id_generator()
#         self.bid_gen: Generator[int, None, None] = id_generator()
#         self.sid_gen: Generator[int, None, None] = id_generator()
#         self.net_id_gen: Generator[int, None, None] = id_generator()

#     def next_nid(self):
#         new_nid = next(self.nid_gen)
#         self.current_nid = new_nid
#         return self.current_nid

#     def next_bid(self):
#         return next(self.bid_gen)

#     def next_sid(self):
#         return next(self.sid_gen)

#     def next_net_id(self):
#         return next(self.net_id_gen)


class Charge:
    """Charge for the charge style logic to that links nodes."""
    # How many different types of charges there are
    charge_number = 5

    def __init__(self, value: list[int] = None):
        self.value = self._set_value(value)

    def _set_value(self, value):
        if value is None:
            return [0] * self.charge_number
        return value

    def interact(self, other):
        """Determine if the two charges interact"""
        for i in range(self.charge_number):
            if self.value[i] > 0 and other.value[i] > 0:
                self.consume_charge(i)
                other.consume_charge(i)
                return True
        return False

    def consume_charge(self, i):
        """ Consumes a charge from the i kind """
        self.value[i] -= 1

    def copy(self):
        return deepcopy(self)

    def __repr__(self):
        return str(self.value)


class NodeIOKind(Enum):
    '''
    Enum listing the possible node roles
    '''
    input = auto()
    output = auto()
    hidden = auto()

# @dataclass
# class NodeKind:
#     '''
#     Node kinds intrinsic proprieties
#     '''
#     id: str
#     io: NodeIOKind


NODE_KIND = {
    'simple': NodeIOKind.hidden,
    'battery': NodeIOKind.hidden,
    'noderadar': NodeIOKind.output,
    'anyradar': NodeIOKind.output,
    'arenaradar': NodeIOKind.output,
    'torsion': NodeIOKind.input,
    'traction': NodeIOKind.input,
}

class NodeKind(Enum):
    '''
    List of node kinds
    '''
    battery = auto()
    simple = auto()
    noderadar = auto()
    anyradar = auto()
    arenaradar = auto()
    torsion = auto()
    traction = auto()


@dataclass
class GBNode:
    '''
    Grafobot node (without evomorph specific data)
    '''
    kind: NodeKind
    mass: float
    radius: float
    # pos and yaw here are the absolute values, not the relative to
    # the block as in the Node class. This must be filled in context
    # so starting blank.
    pos: Vector = None
    yaw: float = None
    # io: NodeIOKind

    @property
    def io_kind(self):
        return NODE_KIND[self.kind.name]


def _id_generator(start=0):
    """
    Generator for innovations unique ids
    """
    id = start
    while True:
        yield id
        id += 1


class Node:
    """
    Nodes that eventually will turn into the grafobot nodes
    """

    # id_gen = _id_generator()

    def __init__(
            self,
            pos: Vector,
            yaw: float,
            charge: Charge,
            gbnode: GBNode,
            nid: int,
    ):
        self.pos = pos
        self.charge = charge
        self.yaw = yaw
        self.nid = nid
        self.gbnode = gbnode

    def interact_charge(self, other):
        """ Interact the owned charge with the other node charge,
            returns if an interaction has occurred.
         """
        return self.charge.interact(other.charge)

    def copy(self):
        return deepcopy(self)

    @property
    def io(self) -> NodeIOKind:
        return self.gbnode.io_kind


class NodeFactory:
    '''
    Build fresh nodes.
    '''

    def __init__(self, id_gen: IdManager):
        pos = Vector.zero()
        kind: NodeKind = NodeKind.simple
        yaw = 0
        mass = 1
        radius = 10
        charge = Charge()
        gbnode = GBNode(kind, mass, radius)
        self.id_gen = id_gen
        self.nid = None
        self.node = Node(pos, yaw, charge, gbnode, nid=None)

    def build(self):
        # returning the copy so the factory can be used to build another node
        new_node = self.node.copy()
        new_node.nid = self._nid()
        return new_node

    def _nid(self):
        if self.nid is None:
            return self.id_gen.next()
        return self.nid

    def set_pos(self, pos: Vector):
        self.node.pos = pos
        return self

    def set_id(self, value):
        '''
        Manually sets the node id
        '''
        self.nid = value
        return self

    def set_kind(self, kind: NodeKind):
        self.node.gbnode.kind = kind
        return self

    def set_yaw(self, yaw: float):
        self.node.yaw = yaw
        return self

    def set_mass(self, mass: float):
        self.node.gbnode.mass = mass
        return self

    def set_charge(self, charge: Charge):
        self.node.charge = charge
        return self

    def set_radius(self, radius: float):
        self.node.gbnode.radius = radius
        return self

    def random_pos(self):
        self.node.pos = Vector.random_versor() * random.normalvariate(70, 20)
        return self

    def random_kind(self, weights=None):
        self.node.gbnode.kind = random.choices(
            list(NodeKind)[1:], weights=weights)[0]
        return self

    def random_yaw(self):
        self.node.yaw = random.random() * 2 * pi
        return self

    def random_mass(self):
        self.node.gbnode.mass = random.normalvariate(1, 0.5)
        if self.node.gbnode.mass <= 0:
            self.node.gbnode.mass = -self.node.gbnode.mass
        return self

    def random_radius(self):
        self.node.gbnode.radius = random.normalvariate(10, 1)
        if self.node.gbnode.radius <= 0:
            self.node.gbnode.radius = -self.node.radius
        return self

    def random_charge(self):
        """Random charge for the given probabilities."""
        charge = Charge()
        for i in range(charge.charge_number):
            rand_value = random.normalvariate(-1, 2)
            charge_value = 0
            if rand_value > 0:
                charge_value = round(rand_value)
            charge.value[i] = charge_value
        self.node.charge = charge
        return self

    def set_battery(self):
        charge = Charge()
        charge.value[0] = 4
        self.node.charge = charge
        self.node.gbnode.kind = NodeKind.battery
        return self


class Slot:
    """
    Slots are placeholder for down the tree blocks.
    A Slot can contain one or none blocks.
    A Block can contain any number of slots.
    That way, the slot is designated block position to attach
    another block and continue the tree. That way one mutation
    can change the branch from a block without changing the
    root position and orientation.
    """

    def __init__(self, pos: Vector, yaw: float, sid: int):
        self.pos = pos
        self.yaw = yaw
        self.block: Block = None
        self.sid = sid

    def attach_block(self, block):
        self.block = block

    def is_available(self):
        return self.block is None

    def remove_block(self):
        self.block = None

    def copy(self, recursive=True):
        if recursive:
            return deepcopy(self)
        else:
            return self.__class__(self.pos, self.yaw, id=self.sid)

    def copy_block(self, **kwargs):
        if self.block is not None:
            return self.block.copy(**kwargs)
        return None


class Link:
    """Describes the links"""

    def __init__(self, node_a: Node, node_b: Node):
        self.node_a = node_a.nid
        self.node_b = node_b.nid
        self.node_a_fixation = False
        self.node_b_fixation = False


class Block:
    """
    The block is the main element of the tree like 
    "wanna be a grafobot" structure.
    A block contains nodes and links between this owned links.
    Also contain slots, placeholder for down the tree blocks.
    Different block nodes will be linked by the charge
    mechanism.
    """

    def __init__(self, bid: int):
        self.nodes = []
        self.links = []
        self.slots: [Slot] = []
        self.bid = bid

    def get_node(self, nid):
        for node in self.nodes:
            if node.nid == nid:
                return node
        return None

    def get_slot(self, sid):
        for slot in self.slots:
            if slot.sid == sid:
                return slot
        return None

    def add_slot(self, slot):
        self.slots.append(slot)

    def add_node(self, node):
        self.nodes.append(node)

    def remove_slot(self, slot):
        self.slots.remove(slot)

    def remove_node(self, node):
        self.nodes.remove(node)

    def list_nodes_recursively(self, offset=None, yaw_offset=None):
        """
        Returns a list of the nodes from the current
        block and from all the blocks down the tree.
        Also returns a position list relative to this
        block origin.
        """
        # adding this block nodes
        if offset is None:
            offset = Vector.zero()
        if yaw_offset is None:
            yaw_offset = 0
        nodes = self.nodes[:]
        pos = [node.pos + offset for node in self.nodes]
        yaw = [node.yaw + yaw_offset for node in self.nodes]
        # passing the hat down the tree
        for slot in self.slots:
            if not slot.is_available():
                n, p, y = slot.block.list_nodes_recursively(
                    offset=offset + slot.pos, yaw_offset=yaw_offset + slot.yaw)
                nodes.extend(n)
                pos.extend(p)
                yaw.extend(y)
        return nodes, pos, yaw

    def list_blocks_recursively(self):
        """
        Returns a list of the blocks containing the current
        block and all other from down the tree.
        """
        blocks = [self]
        for slot in self.slots:
            if not slot.is_available():
                blocks.extend(slot.block.list_blocks_recursively())
                blocks.extend([slot.block])
        return blocks

    def list_slots_recursively(self):
        """
        Returns a list of the slots (occupied or not)
        from the current block and from all the blocks
        down the tree.
        """
        slots = []
        for block in self.list_blocks_recursively():
            slots.extend(block.slots)
        return slots

    def renew_ids(self, id_man: NetElementsIdManager):
        """
        Generate new ids for all elements down the tree
        # TODO: change the block links nodes ids
        """
        for block in self.list_blocks_recursively():
            block.bid = id_man.block.next()
        for slot in self.list_slots_recursively():
            slot.sid = id_man.slot.next()
        for node in self.list_nodes_recursively()[0]:
            node.nid = id_man.node.next()

    def copy(self, recursive=True, id_man=NetElementsIdManager | None):
        if recursive:
            new = deepcopy(self)
        else:
            new = self.__class__(bid=self.bid)
        if id_man is not None:
            new.renew_ids(id_man)
        return new


class Network:
    """
    Base structure
    """

    def __init__(self, net_id: int, base_block: Block):
        self.base_block = base_block
        # self.base_block.nodes.append(NodeFactory().set_id(battery_nid).set_battery().build())
        self.links = []
        self.net_id = net_id

    @property
    def shape(self):
        '''
        Returns the network total number of (blocks, nodes, links).
        '''
        block_len = len(self.blocks)
        nodes_len = len(self.nodes)
        self.add_links_by_charges()
        links_len = len(self.links)
        return (block_len, nodes_len, links_len)

    @property
    def blocks(self):
        return self.base_block.list_blocks_recursively()

    @property
    def nodes(self):
        nodes, pos, yaw = self.base_block.list_nodes_recursively(
            offset=Vector.zero(), yaw_offset=0)
        for i, node in enumerate(nodes):
            node.gbnode.pos = pos[i]
            node.gbnode.yaw = yaw[i]
        return nodes

    @property
    def slots(self):
        """
        Lists all network slots, available or not
        """
        return [slot for slot in self.base_block.list_blocks_recursively()]

    def get_node(self, nid: int):
        for block in self.blocks:
            node = block.get_node(nid)
            if node is not None:
                return node

    def export_blueprint(
        self,
        input_remap: list[int] = None,
        output_remap: list[int] = None
    ) -> rgb.PyBotBlueprint:
        """
        Export in a grafobot blueprint format.
        The ordered list of input and/or outputs can be passed.
        """
        bp = rgb.PyBotBlueprint()
        nodes = self.nodes
        # Separating nodes
        if input_remap is not None:
            input_nodes = [self.get_node(nid) for nid in input_remap]
            assert all([node.gbnode.kind.value.name == 'input' for node in input_nodes]
                       ), "Not all input nodes passed in the remap are inputs"
        else:
            input_nodes = [
                node for node in nodes if node.gbnode.io_kind == NodeIOKind.input]
        if output_remap is not None:
            output_nodes = [self.get_node(nid) for nid in output_remap]
            assert all([node.gbnode.kind.value.name == 'output' for node in output_nodes]), \
                "Not all output nodes passed in the remap are outputs"
        else:
            output_nodes = [
                node for node in nodes if node.gbnode.io_kind == NodeIOKind.output]
        other_nodes = (
            [node for node in self.nodes
             if node not in (input_nodes + output_nodes)]
        )
        # Adding to the blueprint
        for node in other_nodes + input_nodes + output_nodes:
            # print(node, pos)
            gbnode = node.gbnode
            bp.add_node(
                (gbnode.pos.x, gbnode.pos.y),
                gbnode.mass, gbnode.radius, gbnode.yaw, gbnode.kind.name
            )
        # Adding links
        self.add_links_by_charges()
        # Creating a dictionary for reverse search
        nodes_dic = {node.nid: i for i, node in enumerate(nodes)}
        for link in self.links:
            # print('asdf',nodes_dic[link.node_a],nodes_dic[link.node_b])
            bp.add_link(nodes_dic[link.node_a], False,
                        nodes_dic[link.node_b], False)
        return bp

    def list_available_slots(self):
        return [
            slot
            for slot in self.base_block.list_slots_recursively()
            if slot.is_available()
        ]

    def list_occupied_slots(self):
        return [
            slot
            for slot in self.base_block.list_slots_recursively()
            if not slot.is_available()
        ]

    def pick_random_available_slot(self):
        slots = self.list_available_slots()
        if slots:
            return random.choice(slots)
        else:
            return None

    def add_links_by_charges(self):
        # creating a list of charges for consumption
        nodes = self.nodes
        charges = [node.charge.copy() for node in nodes]
        # cleaning the links
        self.links = []
        for i, charge_a in enumerate(charges):
            for j, charge_b in enumerate(charges[i + 1:]):
                if charge_a.interact(charge_b):
                    link = Link(nodes[i], nodes[i+j+1])
                    self.links.append(link)

    def remove_block(self, block: Block):
        """Removes a given block"""
        self.blocks.remove(block)

    def list_io(self) -> tuple[list[int], list[int]]:
        '''
        Returns the current io nodes id list 
        (ins, outs)
        '''
        self.add_links_by_charges()
        nodes = self.nodes
        inputs = []
        outputs = []
        for node in self.nodes:
            if node.io == NodeIOKind.input:
                inputs.append(node.nid)
            if node.io == NodeIOKind.output:
                outputs.append(node.nid)
        return (inputs, outputs)

    def copy(self):
        return deepcopy(self)

    def save(self, file_name):
        """save class """
        with open(file_name, 'wb') as outfile:
            pickle.dump(self, outfile)

    @staticmethod
    def load(file_name):
        with open(file_name, 'rb') as outfile:
            net = pickle.load(outfile)
        return net


class NetworkFactory:
    '''
    Build Networks.
    '''

    def __init__(self, id_gen: NetElementsIdManager):
        self.id_gen = id_gen
        self.network = self._gen_base_network()

    def _gen_base_block(self):
        battery = NodeFactory(self.id_gen.node).set_battery().build()
        base_block = Block(bid=self.id_gen.block.next())
        base_block.add_node(battery)
        return base_block

    def _gen_base_network(self):
        return Network(
            net_id=self.id_gen.network.next(),
            base_block=self._gen_base_block()
        )

    def build(self):
        return self.network.copy()
