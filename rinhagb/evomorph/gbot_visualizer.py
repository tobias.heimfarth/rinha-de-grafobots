"""
Gráficos utilizando a biblioteca "arcade" e a librinha_rust2py.
"""

from math import sin, cos
from dataclasses import dataclass
import time
import tkinter.filedialog as filedialog

import arcade
import pyglet.gl as gl
from vectors_py import Vector

from rinhagb.evomorph.network_elements import Network

# Definições da janela
SCREEN_TITLE = "Evomorph visualizer"
SCREEN_WIDTH = 1120+64
SCREEN_HEIGHT = 640+64
#
UPDATE_RATE = 0.0166


class GameWindow(arcade.Window):
    def __init__(self, **kwargs):
        """
        Iniciando a janela.
        """
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE, **kwargs)


class RinhaView(arcade.View):
    """
    Janela principal do "arcade" 
    """

    def __init__(self,
                 evomorphview,
                 ):
        """
        Iniciando a janela.
        """
        super().__init__()
        self.evomorphview = evomorphview

    def on_key_press(self, key, modifiers):
        """
        Chamado quando uma tecla é liberada.
        """
        if key == arcade.key.F:
            filename = filedialog.askopenfile(mode='r')
            net = Network.load(filename.name)
            self.evomorphview.net = net

    def on_key_release(self, key, modifiers):
        pass

    def on_update(self, delta_time):
        """
        Atualização do jogo.
        """
        # self.evomorphview.update(self._pressed_keys)
        pass

    def on_draw(self):
        # Comunica o arcade que a renderização vai começar.
        arcade.start_render()
        self.evomorphview.draw()


colors = [
    (255, 255, 255),
    (0, 0, 255),
    (0, 255, 255),
    (255, 0, 255),
    (255, 0, 0),
    (245, 245, 220),
    (255, 228, 196),
    (0, 0, 0),
    (255, 235, 205),
    (0, 0, 255),
    (138, 43, 226),
    (165, 42, 42),
    (222, 184, 135),
    (95, 158, 160),
    (127, 255, 0),
    (210, 105, 30),
    (255, 127, 80),
    (100, 149, 237),
    (255, 248, 220),
    (220, 20, 60),
    (0, 255, 255),
    (0, 0, 139),
    (0, 139, 139),
    (184, 134, 11),
    (169, 169, 169),
    (0, 100, 0),
    (169, 169, 169),
    (189, 183, 107),
    (139, 0, 139),
    (85, 107, 47)
]


def map_pos_to_screen(pos):
    """
    Converte as coordenadas do jogo para coordenadas da janela do jogo.
    """
    return pos + Vector(300, 300)


class EvoMorphView:
    def __init__(self, net: Network):
        self.net = net

    def draw_node(self, node, color):
        """
        Desenha um nodo
        """
        gbnode = node.gbnode
        pos = map_pos_to_screen(gbnode.pos)
        # Desenhando o círculo principal do nodo
        arcade.draw_circle_filled(*pos, gbnode.radius, color, num_segments=25)
        # Desenhando o marcador de proa (frente do nodo)
        marker_endpoint = (gbnode.radius*cos(gbnode.yaw) +
                           pos[0], gbnode.radius*sin(gbnode.yaw) + pos[1])
        arcade.draw_line(*pos, *marker_endpoint,
                         arcade.color.BLACK, line_width=3)
        arcade.draw_text(f"{node.nid} {node.charge.value} {node.gbnode.kind.name}",
                         pos[0]+10,
                         pos[1],
                         arcade.color.WHITE,
                         10, bold=False)

    def draw_block(self, block, color):
        """
        Desenha um nodo
        """
        for node in block.nodes:
            self.draw_node(node, color)
            self._draw_block_node_connection(block, node, color)
        for slot in block.slots:
            pos = map_pos_to_screen(slot.pos)
            arcade.draw_point(*pos, color, 5)

    def _draw_block_node_connection(self, block, node, color):
        arcade.draw_line(
            *map_pos_to_screen(node.gbnode.pos),
            *map_pos_to_screen(node.gbnode.pos-node.pos),
            color, line_width=1
        )

    def draw_links(self, links):
        """
        Desenha os links.
        """
        color = (255, 255, 255)
        for link in links:
            node_a = self.net.get_node(link.node_a)
            node_b = self.net.get_node(link.node_b)
            arcade.draw_line(*map_pos_to_screen(node_a.gbnode.pos), *
                             map_pos_to_screen(node_b.gbnode.pos), color,
                             line_width=2.5)

    def draw(self):
        for i, block in enumerate(self.net.base_block.list_blocks_recursively()):
            self.draw_block(block, colors[i])
        self.draw_links(self.net.links)
