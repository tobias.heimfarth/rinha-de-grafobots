
from dataclasses import dataclass, field
from typing import Protocol
import random
from math import pi

from vectors_py import Vector

from .network_elements import (
    Charge,
    Link,
    GBNode,
    Node,
    Slot,
    Block,
    Network,
    NodeFactory,
    NetElementsIdManager,
    NodeKind,
)

from rinhagb.id_manager import IdManager


# Intra Block basic mutations (functions to be used in the mutations operators)
def add_slot(sid: int, block: Block):
    """
    Adds a slot in a random position close to a node
    """
    pos = Vector.random_versor() * random.random() * 100
    yaw = random.random()*2*pi
    if block.nodes:
        random_node = random.choice(block.nodes)
        pos += random_node.pos
    new_slot = Slot(pos, yaw, sid=sid)
    block.add_slot(new_slot)


def add_node(nid: int, block: Block):
    """Adds a random node to a block"""
    new_node = (
        NodeFactory(id_gen=None)
        .set_id(nid)
        .random_kind()
        .random_charge()
        .random_pos()
        .random_yaw()
        .random_mass()
        .random_radius()
        .build()
    )
    # Makes the distance relative to a random node to promote structures and avoid clumping
    if block.nodes:
        new_node.pos += random.choice(block.nodes).pos
    block.nodes.append(new_node)


# Node basic mutations
def perturb_node_pos(node: Node, mu=0, var=5):
    """Makes a small change to a node position"""
    offset = Vector.random_versor() * random.normalvariate(mu, var)
    node.pos += offset


def perturb_node_yaw(node: Node, mu=0, var=5):
    """Makes a small change to a node yaw"""
    offset = random.normalvariate(mu, var)
    node.yaw += offset


def perturb_node_mass(node: Node, mu=0, var=5):
    """Makes a small change to a node mass"""
    offset = random.normalvariate(mu, var)
    node.gbnode.mass += offset


def change_node_kind(node: Node, weights: list[float] = None):
    # FIXME One must check if the kind changes from an hidden to an input,
    # for example, if this will not break the behavior net matching
    #
    # Only one battery node for bot, so leaving it out (index 0)
    available_node_kinds_list = list(NodeKind)[1:]
    if weights is None:
        weights = [1]*len(available_node_kinds_list)
    node.gbnode.kind = random.choices(
        available_node_kinds_list,
        weights=weights
    )[0]


def change_node_charge(node: Node, change_rate: float):
    for i, value in enumerate(node.charge.value):
        if random.random() < change_rate:
            node.charge.value[i] += random.choice([+1, -1])
            if node.charge.value[i] < 0:
                node.charge.value[i] = 0


class NetMutation(Protocol):
    '''
    A mutation operation to be applied in a network
    '''
    probability: float = 1
    times: int = 1

    def apply(self, net, **kwargs) -> None:
        '''
        Applying the mutation to the network (in place)
        '''


def apply_netmutations(net: Network, mutations: list[NetMutation]):
    """
    Applies a list of NetMutations in a network (inplace modifying it)
    taking into account the probabilities and number of times it must
    be applied.
    """
    for i, mut in enumerate(mutations):
        for j in range(mut.times):
            if random.random() < mut.probability:
                try:
                    mut.apply(net)
                except IndexError:
                    pass

# Mutations


@dataclass
class AddBlock:
    """
    Adds a new empty block to a random slot in the network
    """
    id_gen: NetElementsIdManager
    probability: float = 1.
    times: int = 1

    def apply(self, net):
        block = Block(bid=self.id_gen.block.next())
        slot = net.pick_random_available_slot()
        if slot:
            slot.attach_block(block)
            return block
        else:
            return None


@dataclass
class AddSlot:
    """
    Adds a new slot to a random block in the network
    """
    id_gen: NetElementsIdManager
    probability: float = 1.
    times: int = 1

    def apply(self, net):
        random_block = random.choice(net.blocks)
        add_slot(sid=self.id_gen.slot.next(), block=random_block)


@dataclass
class AddNode:
    """
    Adds a new empty node to a random block in the network
    """
    id_gen: NetElementsIdManager
    probability: float = 1.
    times: int = 1

    def apply(self, net):
        random_block = random.choice(net.blocks)
        add_node(nid=self.id_gen.node.next(), block=random_block)


@dataclass
class PerturbRandomNodePosition:
    """
    Perturbs a random node position
    """
    probability: float = 1.
    times: int = 1

    def apply(self, net, **kwargs):
        # Only choose nodes that are not the battery to avoid
        # bot roaming
        random_node = random.choice(net.nodes[1:])
        perturb_node_pos(random_node, **kwargs)


@dataclass
class ChangeNodeCharge:
    """
    Changes a random node charge to a random charge
    """
    probability: float = 1.
    times: int = 1
    change_rate: float = 0.1

    def apply(self, net):
        random_node = random.choice(net.nodes)
        change_node_charge(random_node, self.change_rate)


@dataclass
class ChangeNodeKind:
    """
    Changes a random node kind to a random kind
    """
    probability: float = 1.
    times: int = 1
    weights: list[float] = field(
        default_factory=lambda: [1] * (len(NodeKind)-1)
    )

    def apply(self, net):
        random_node = random.choice(net.nodes[1:])
        change_node_kind(random_node, weights=self.weights)


@dataclass
class RemoveNode:
    """
    Removes a random node
    """
    probability: float = 1.
    times: int = 1

    def apply(self, net):
        random_block = random.choice(net.blocks)
        random_node = random.choice(random_block.nodes)
        random_block.remove_node(random_node)


@dataclass
class RemoveBlock:
    """
    Removes a random block
    """
    probability: float = 1.
    times: int = 1

    def apply(self, net):
        random_slot = random.choice(net.list_occupied_slots())
        random_slot.remove_block()


@dataclass
class RemoveSlot:
    """
    Removes a random slot
    """
    probability: float = 1.
    times: int = 1

    def apply(self, net):
        random_block = random.choice(net.blocks)
        unlucky_slot = random.choice(
            [slot for slot in random_block.slots if slot.is_available()]
        )
        random_block.remove_slot(unlucky_slot)


@dataclass
class DuplicateBlock:
    """
    Duplicates a random block
    """
    id_man: NetElementsIdManager
    probability: float = 1.
    times: int = 1

    def apply(self, net):
        # Slot to put the duplicated block
        random_empty_slot = random.choice(net.list_available_slots())
        if random_empty_slot is not None:
            random_block = random.choice(net.blocks)
            random_block = random_block.copy(
                recursive=True, id_man=self.id_man)
            random_empty_slot.attach_block(random_block)
