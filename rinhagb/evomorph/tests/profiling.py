import cProfile
import pstats
import io

import context

import arcade
from graphics_arcade import GameWindow
from rinha_de_grafobots import RinhaDeGrafobots
from bot import load_bot
from controllers import BotControllerKeyboard,BotControllerANN, BotControllerDummy
from neural_network import genome,network


def profile(fnc):
    """
    A decorator that uses cProfile to profile a function.
    Used to measure the time a patch of code takes to run.
    """
    def inner(*args, **kwargs):
        pr = cProfile.Profile()
        pr.enable()
        retval = fnc(*args, **kwargs)
        pr.disable()
        s = io.StringIO()
        sortby = 'cumulative'
        ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
        ps.print_stats()
        print(s.getvalue())
        return retval
    return inner




# Definicoes da janela
SCREEN_TITLE = "Rinha de Grafobots"
SCREEN_WIDTH = 1280
SCREEN_HEIGHT = 768
#
GRAPHIC = True
UPDATE_RATE = 0.016

rinha = RinhaDeGrafobots(SCREEN_WIDTH, SCREEN_HEIGHT)
rinha.bot1 = load_bot('../rinha_de_grafobots/grafobots_blueprints/bot_003.txt')
rinha.bot2 = load_bot('../rinha_de_grafobots/grafobots_blueprints/bot_target.txt')
g = genome.load_genome('../rinha_de_grafobots/grafobots_blueprints/test_001_gen0.gen')
g.fill()
ann = network.NeuralNetwork(g)
ann.clear_all_stimuli()
rinha.controller1 = BotControllerANN(rinha.bot1,ann,rinha)
rinha.controller2 = BotControllerDummy(rinha.bot2)

# window = GameWindow(SCREEN_WIDTH,
#                         SCREEN_HEIGHT,
#                         SCREEN_TITLE,
#                         rinha,
#                         update_rate=UPDATE_RATE)
# window.map_file = '../rinha_de_grafobots/map.tmx'
# window.setup()

@profile
def run():
    rinha.setup()
    for i in range (1000):
        rinha.update()
        # cumtime 0.584

if __name__ == "__main__":
    #map_azimuth_energy_prof()
    run()