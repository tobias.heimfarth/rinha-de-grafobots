import pytest

from vectors_py import Vector

from rinhagb.evomorph import network_elements as ne
from rinhagb.evomorph.network_elements import (
    Network,
    Block,
    Node,
    Slot,
    NodeFactory,
    NodeKind,
    Charge,
    IdManager,
    NetworkFactory,
    NetElementsIdManager,
    NodeIOKind
)


@pytest.fixture
def fix_network():
    #Building a simple network for testing
    id_man = NetElementsIdManager()
    net = NetworkFactory(id_man).build()
    node_fac = NodeFactory(id_man.node)
    #Branch 1
    node1 = (
        node_fac
        .set_pos(Vector(-50,50))
        .set_yaw(1.)
        .set_kind(NodeKind.arenaradar)
        .set_charge(Charge(value=[3,4,0,1,0]))
        .build()
    )
    block1 = Block(id_man.block.next())
    block1.add_node(node1)
    slot1 = Slot(pos =Vector(100,100), yaw=0.5, sid=id_man.slot.next())
    slot1.attach_block(block1)
    net.base_block.add_slot(slot1)
    #Branch 2
    node2 = (
        node_fac
        .set_pos(Vector(50,50))
        .set_yaw(0.1)
        .set_kind(NodeKind.traction)
        .set_charge(Charge(value=[3,4,0,1,0]))
        .build()
    )
    node3 = (
        node_fac
        .set_pos(Vector(50,-50))
        .set_yaw(0.1)
        .set_kind(NodeKind.traction)
        .set_charge(Charge(value=[0,4,0,1,0]))
        .build()
    )
    block2 = Block(id_man.block.next())
    block2.add_node(node2)
    block2.add_node(node3)
    slot2 = Slot(pos =Vector(100,-100), yaw=-0.5, sid=id_man.slot.next())
    slot2.attach_block(block2)
    net.base_block.add_slot(slot2)
    # Node in base block
    node3 = (
        node_fac
        .set_pos(Vector(50,0))
        .set_yaw(0.)
        .set_kind(NodeKind.simple)
        .set_charge(Charge(value=[0,4,0,1,0]))
        .build()
    )
    net.base_block.add_node(node3)
    net.add_links_by_charges()
    return net

def test_node_factory():
    id_man = IdManager()
    node = (
        ne.NodeFactory(id_man)
        .build()
        )

def test_network_io_list(fix_network):
    assert fix_network.base_block.nodes[0].nid == 0
    io = fix_network.list_io()
    # Two input nodes
    assert len(io[0]) == 2
    # One output node
    assert len(io[1]) == 1
    assert io == ([2,3],[1])


def test_export_blueprint(fix_network):
    fix_network.export_blueprint()

def test_nodes_list_battery_first(fix_network):
    '''
    Checks if the battery is the first node listed, which is assumed
    in the mutations
    '''
    assert fix_network.nodes[0].gbnode.kind == NodeKind.battery
