import pytest
from copy import copy, deepcopy

from rinhagb.evomorph.network_elements import *
from rinhagb.evomorph.mating import *
from rinhagb.evomorph.mutations import *
from rinhagb.evomorph.gbot_visualizer import *
import rinhagb.evomorph.mutations as mutations
from rinhagb.evomorph.network_elements import NetElementsIdManager


# creating the test networks
def create_test_net():
    id_man = NetElementsIdManager()
    net = NetworkFactory(id_man).build()
    muts = [
        mutations.AddBlock(id_man, times=1, probability=0.1),
        mutations.AddSlot(id_man, times=1, probability=0.2),
        mutations.AddNode(id_man, times=3, probability=0.2),
        mutations.PerturbRandomNodePosition(times=1, probability=0.2),
        mutations.ChangeNodeCharge(times=30, probability=0.1, change_rate= 0.1),
        mutations.RemoveSlot(times=1, probability=0.5),
        mutations.RemoveBlock(times=1, probability=0.5),
        mutations.DuplicateBlock(id_man,times=1, probability=0.01),
    ]
    for i in range(10):
        apply_netmutations(net, muts)
    net.add_links_by_charges()
    return net


def fix_network():
    # Building a simple network for testing
    id_man = NetElementsIdManager()
    net = NetworkFactory(id_man).build()
    node_fac = NodeFactory(id_man.node)
    # Branch 1
    node1 = (
        node_fac
        .set_pos(Vector(-50, 50))
        .set_yaw(1.)
        .set_kind(NodeKind.arenaradar)
        .set_charge(Charge(value=[3, 4, 0, 1, 0]))
        .build()
    )
    block1 = Block(id_man.block.next())
    block1.add_node(node1)
    slot1 = Slot(pos=Vector(100, 100), yaw=0.5, sid=id_man.slot.next())
    slot1.attach_block(block1)
    net.base_block.add_slot(slot1)
    # Branch 2
    node2 = (
        node_fac
        .set_pos(Vector(50, 50))
        .set_yaw(0.1)
        .set_kind(NodeKind.traction)
        .set_charge(Charge(value=[3, 4, 0, 1, 0]))
        .build()
    )
    node3 = (
        node_fac
        .set_pos(Vector(50, -50))
        .set_yaw(0.1)
        .set_kind(NodeKind.traction)
        .set_charge(Charge(value=[0, 4, 0, 1, 0]))
        .build()
    )
    block2 = Block(id_man.block.next())
    block2.add_node(node2)
    block2.add_node(node3)
    slot2 = Slot(pos=Vector(100, -100), yaw=-0.5, sid=id_man.slot.next())
    slot2.attach_block(block2)
    net.base_block.add_slot(slot2)
    # Node in base block
    node3 = (
        node_fac
        .set_pos(Vector(50, 0))
        .set_yaw(0.)
        .set_kind(NodeKind.simple)
        .set_charge(Charge(value=[0, 4, 0, 1, 0]))
        .build()
    )
    net.base_block.add_node(node3)
    net.add_links_by_charges()
    return net


def test_mate_nets():
    net1 = create_test_net()
    net2 = mutate_net(net1)
    net3 = mate_networks(net1, net2)


if __name__ == "__main__":
    net4 = create_test_net()
    # net2 = net.copy()
    # for i in range(10): net_mutator_sample.apply(net2)
    # net2.add_links_by_charges()
    # net3 = mate_networks(net, net2)
    # net3.add_links_by_charges()
    # net.save('net.mnet')
    # net2.save('net2.mnet')
    # net3.save('net3.mnet')
    # bp = net.export_blueprint()
    # bp.save_file("net.json")
    # bp = net2.export_blueprint()
    # bp.save_file("net2.json")
    # bp = net3.export_blueprint()
    # bp.save_file("net3.json")

    #net4 = fix_network()
    print(net4.list_io())
    # net4.export_blueprint(input_remap = [3,2], output_remap=[1])
    emv = EvoMorphView(net4)
    window = GameWindow()
    view = RinhaView(emv)
    window.show_view(view)
    arcade.run()
