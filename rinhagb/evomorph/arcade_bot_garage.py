"""
Show the bot in game arena
"""

import sys
sys.path.append('../rinha_de_grafobots/resources')
sys.path.append('../rinha_de_grafobots')
sys.path.append('../')
sys.path.append('./')

from config_parser import RinhaConfigParser, print_bot_info
from graphics_arcade import GameWindow, RinhaView, MenuView
from arcade_rinha import ArcadeRinha
from net_builder import construct_aann
import rinha_gb.resources.librinha_rust2py as rgb
import arcade

# Definições da janela
SCREEN_TITLE = "Rinha de Grafobots"
SCREEN_WIDTH = 1120+64
SCREEN_HEIGHT = 640+64
#
UPDATE_RATE = 0.0166

print(sys.path)

bot_a = rgb.PyBotBlueprint.load_file("net.json").gen_bot()
io = list(bot_a.get_io_numbers())
net_a = rgb.PyBannNetworkFactory(io[1], io[0], 0).build().to_pyaann()

bot_b = rgb.PyBotBlueprint.load_file("net3.json").gen_bot()
io = list(bot_b.get_io_numbers())
net_b = rgb.PyBannNetworkFactory(io[1], io[0], 0).build().to_pyaann()

print_bot_info(bot_a, 'a')
print_bot_info(bot_b, 'b')

rusty_rinha = rgb.RinhaDeGrafobots(bot_a, bot_b)






# Criando a ligação entre o jogo em rust e os gráficos em arcade
arcade_rinha = ArcadeRinha(
    rusty_rinha, None, background='../rinha_de_grafobots/resources/fundo_sapo.png')
arcade_rinha.neural_network_a, arcade_rinha.neural_network_b = net_a, net_b
# Dançando a dança do Arcade...
window = GameWindow(SCREEN_WIDTH,
                    SCREEN_HEIGHT,
                    SCREEN_TITLE,
                    update_rate=UPDATE_RATE,
                    antialiasing=True,
                    fullscreen=False)
rinha_view = RinhaView(arcade_rinha)
menu = MenuView(rinha_view)
window.show_view(menu)

arcade.run()
