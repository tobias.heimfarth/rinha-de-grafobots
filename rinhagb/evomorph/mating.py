import random
from math import pi

from vectors_py import Vector
from .network_elements import (
    Charge,
    Link,
    GBNode,
    Node,
    Slot,
    Block,
    Network
)


def pair_elements_by_id(list1, list2):
    """
    Given a list of same kind elements, match the pairs and
    provides a list with the unmatched ones
    #TODO kind of lazy. Do better.
    """
    ids1 = [x.id for x in list1]
    ids2 = [x.id for x in list2]
    common = []
    unique_from1 = []
    unique_from2 = []
    for id1 in ids1:
        if id1 in ids2:
            ids2.remove(id1)
            common.append(id1)
        else:
            unique_from1.append(id1)
    unique_from2 = ids2[:]
    return common, unique_from1, unique_from2


def get_element_from_id(elements, id):
    """
    Given a list of elements with id, returns the first element
    in the list with that id
    """
    for e in elements:
        if e.id == id:
            return e
    return None


def mate_elements(list1, list2):
    """
    Mates "standard" elements
    """
    common_elements_ids, unique_elements_ids_from_1, unique_elements_ids_from_2 \
        = pair_elements_by_id(list1, list2)
    child_elements = []
    # mating elements common in both list
    for id in common_elements_ids:
        if random.random() > 0.5:
            child_elements.append(get_element_from_id(list1, id).copy())
        else:
            child_elements.append(get_element_from_id(list2, id).copy())
    # mating isolated elements in list1
    for id in unique_elements_ids_from_1:
        if random.random() > 0.5:
            child_elements.append(get_element_from_id(list1, id).copy())
    # mating isolated elements in list2
    for id in unique_elements_ids_from_2:
        if random.random() > 0.5:
            child_elements.append(get_element_from_id(list2, id).copy())
    return child_elements


def mate_slots(list1, list2):
    """
    Mates "standard" elements
    ** verify is the recursion is copying right
    """
    # list1, list2 = blk1.slots, blk2.slots
    common_elements_ids, unique_elements_ids_from_1, unique_elements_ids_from_2 \
        = pair_elements_by_id(list1, list2)
    child_slots = []
    # mating elements common in both list
    for id in common_elements_ids:
        if random.random() > 0.5:
            slotA = get_element_from_id(list1, id)
            slotB = get_element_from_id(list2, id)
        else:
            slotB = get_element_from_id(list1, id)
            slotA = get_element_from_id(list2, id)
        # slot parameters from A
        new_slot = slotA.copy(recursive=False)
        # Adding a block
        # If both slots have blocks the new block depends on several cases
        if not slotA.is_available() and not slotB.is_available():
            # Checking if both slots have the same id block to match
            if slotA.block.id == slotB.block.id:
                new_block = mate_blocks(slotA.block, slotB.block)
            # If the blocks are different, choosing one randomly
            else:
                new_block = random.choice(
                    [slotA.block.copy(recursive=True),
                     slotB.block.copy(recursive=True)]
                )
        # In case ony one block exists, than is simpler
        else:
            new_block = random.choice(
                (slotA.copy_block(recursive=True), slotB.copy_block(recursive=True)))
        new_slot.attach_block(new_block)
        child_slots.append(new_slot)
    # mating isolated elements in list1
    for id in unique_elements_ids_from_1:
        if random.random() > 0.5:
            child_slots.append(get_element_from_id(list1, id).copy())
    # mating isolated elements in list2
    for id in unique_elements_ids_from_2:
        if random.random() > 0.5:
            child_slots.append(get_element_from_id(list2, id).copy())
    return child_slots


def mate_blocks(blk1: Block, blk2: Block):
    """
    Mates two blocks
    """
    assert blk1.id == blk2.id, "Block's ids does not match, {blk1.id} != {blk2.id}."
    new_block = blk1.copy(recursive=False)
    new_block.nodes = mate_elements(blk1.nodes, blk2.nodes)
    new_block.links = mate_elements(blk1.links, blk2.links)
    new_block.slots = mate_slots(blk1.slots, blk2.slots)
    return new_block


def mate_networks(net1: Network, net2: Network):
    new_net = Network()
    new_net.base_block = mate_blocks(
        net1.base_block,
        net2.base_block
    )
    return new_net

def networks_similarity(net1: Network, net2: Network):
    """
    Computes a distance between two networks based on the ratio of
    common ids.
    0 -> no matching ids
    1 -> all elements are common
    # FIXME: no chargeless links considerations
    """
    nodes_common, nodes_total = _count_commons_and_total(net1.nodes, net2.nodes)
    slots_common, slots_total = _count_commons_and_total(net1.slots, net2.slots)
    blocks_common, blocks_total = _count_commons_and_total(net1.blocks, net2.blocks)
    total = nodes_total + slots_total + blocks_total
    if total == 0:
        # avoiding zero division
        return 0
    return (nodes_common + slots_common + blocks_common)/total


def _count_commons_and_total(list1,list2):
    '''
    Computes the number of common elements in a list as well as
    the total number of elements
    '''
    elements = pair_elements_by_id(list1, list2)
    commons = len(elements[0])
    total = commons + len(elements[1]) + len(elements[2])
    return commons, total

