"""
Simple Id manager to be used across the bots and network elements.
"""


def id_generator(start=0):
    """
    Generator for sequential integer ids
    """
    id_ = start
    while True:
        yield id_
        id_ += 1


class IdManager:
    """
    Manages Ids
    """

    def __init__(self, start=0):
        self._current = None
        self.gen: Generator[int, None, None] = id_generator()

    def next(self):
        new = next(self.gen)
        self._current = new
        return new

    def is_valid(self, value) -> bool:
        assert type(value) == int, "Id type must be an integer."
        if value <= self.current:
            return False
        True

    @property
    def current(self):
        return self._current
