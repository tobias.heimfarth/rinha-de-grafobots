"""
Cria uma rede na biblioteca aann (rust) a partir de um genome (python)
"""

import rinhagb.resources.librinha_rust2py as rgb
import rinhagb.neural_network as nn
import sys


def construct_aann(genome):
    """
    Dado um genoma, constrói uma rede neural com a biblioteca aann.
    """
    # Os genomas contém inicialmente somente as instruções para a construção
    # da rede, que deve ser concretizada preenchendo as litas de nodos e
    # conexões.
    genome.fill()
    # Ordenando os nodos com sua função na rede conforme requerido pela
    # biblioteca.
    nodes = []
    for node in genome.in_nodes:
        nodes.append(('i', node.bias))
    for node in genome.out_nodes:
        nodes.append(('o', node.bias))
    for node in genome.hidden_nodes:
        nodes.append(('h', node.bias))
    # Os links (conexões) são um pouco mais complicados pois precisa-se
    # dos índices dos nodos que estes conectam.
    links = []
    # Criando uma lista com os nodos na ordem que corresponda aos seus
    # índices.
    nodes_list = genome.in_nodes + genome.out_nodes + genome.hidden_nodes
    # Adicionando os links à lista.
    for link in genome.connections:
        # Links desabilitados são ignorados
        # (não deveriam passar pelo fill, mas...)
        if link.enabled:
            # Procurando o nodo de entrada do link
            try:
                in_index = nodes_list.index(link.in_node)
            except ValueError:
                # Nodos desabilitados mas reabilitados por cruzamentos não
                # aparecem na lista de nodos escondidos.. colocando na marra
                nodes.append(('h', link.in_node.bias))
                in_index = len(nodes)-1
            # Procurando o nodo de saida do link
            try:
                out_index = nodes_list.index(link.out_node)
            except ValueError:
                # Nodos desabilitados mas reabilitados por cruzamentos não
                # aparecem na lista de nodos escondidos.. colocando na marra
                nodes.append(('h', link.out_node.bias))
                out_index = len(nodes)-1
            links.append((in_index, out_index, link.weight))
        else:
            assert False, "Link desabilitado dentro do fill"
    return rgb.PyNetwork(nodes, links)


if __name__ == "__main__":
    # Testando...
    genome = nn.genome.load_genome('./neural_network/g.gen')
    net = construct_aann(genome)
    for i in range(400):
        o = net.in_tick_out([1])
        print(o)
