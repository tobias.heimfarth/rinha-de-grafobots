"""
Gráficos utilizando a biblioteca "arcade" e a librinha_rust2py.
"""

from math import sin, cos
from dataclasses import dataclass
import time

import arcade
import pyglet.gl as gl

from .arcade_rinha import ArcadeRinha

# Definições da janela
SCREEN_TITLE = "Rinha de Grafobots"
SCREEN_WIDTH = 1120+64
SCREEN_HEIGHT = 640+64
#
UPDATE_RATE = 0.0166




class GameWindow(arcade.Window):
    def __init__(self, width, height, title, **kwargs):
        """
        Iniciando a janela.
        """
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE, **kwargs)

class RinhaView(arcade.View):
    """
    Janela principal do "arcade" 
    """

    def __init__(self,
                rinha,
                ):
        """
        Iniciando a janela.
        """
        super().__init__()
        self.rinha = rinha
        self._pressed_keys = set()
     
    def on_key_press(self, key, modifiers):
        """
        Chamado quando uma tecla é liberada.
        """
        self._pressed_keys.add(key)

    def on_key_release(self, key, modifiers):
        try:
            self._pressed_keys.remove(key)
        except KeyError:
            pass

    def on_update(self, delta_time):
        """
        Atualização do jogo.
        """
        self.rinha.update(self._pressed_keys)

    def on_draw(self):
        # Comunica o arcade que a renderização vai começar.
        arcade.start_render()
        self.rinha.draw_game()

class MenuView(arcade.View):
    def __init__(self,rinha_view):
        super().__init__()
        self.rinha_view = rinha_view
        self.rinha_view.rinha.setup()
        self.timer = 3
        self.tick_number = 0

    def on_show(self):
        arcade.set_background_color(arcade.color.WHITE)

    def on_update(self, delta_time):
        self.timer -= delta_time
        if self.timer < 0:
            self.window.show_view(self.rinha_view)
        
    def on_draw(self):
        arcade.start_render()
        self.rinha_view.rinha.draw_game()
        arcade.draw_text("Rinha de Grafobots", int(SCREEN_WIDTH/2), int(SCREEN_HEIGHT/2+25),
                         arcade.color.WHITE_SMOKE, font_size=50, anchor_x="center")
        arcade.draw_text("Start in {}".format(int(self.timer)), SCREEN_WIDTH/2, SCREEN_HEIGHT/2-75,
                         arcade.color.RED, font_size=50, anchor_x="center")

    def on_mouse_press(self, _x, _y, _button, _modifiers):
        pass
        #self.window.show_view(self.rinha_view)