"""
Lê e analiza o arquivo de configurações
"""
from math import copysign
import configparser

import arcade

import sys
sys.path.append('../resources')
sys.path.append('../')
import rinhagb.resources.librinha_rust2py as rgb
import rinhagb.neural_network as nn
from .net_builder import construct_aann
from .arcade_rinha import GraphicsSettings

class RinhaConfigParser:
    """ Classe que lê os dados de configuração"""

    def __init__(self, conf_file):
        self.config = configparser.ConfigParser()
        self.config.read(conf_file)

    def get_blueprints(self):
        print(self.config['bot_a']['arquivo_modelo'])
        return (
            rgb.PyBotBlueprint.new_from_file(self.config['bot_a']['arquivo_modelo']).gen_bot(),
            rgb.PyBotBlueprint.new_from_file(self.config['bot_b']['arquivo_modelo']).gen_bot()
        )

    def get_keymaps(self):
        return (
            self._human_controller('bot_a'),
            self._human_controller('bot_b')
        )
    
    def _human_controller(self,bot_name):
        controller = None
        if self.config[bot_name]['controlador'] == "humano":
            controller = self._parse_controller(bot_name)
        return controller

    def get_aann(self):
        return (
            self._aann('bot_a'),
            self._aann('bot_b')
        )

    def _aann(self,bot_name):
        aann_net = None
        if self.config[bot_name]['controlador'] == "ann":
            controller_file = self.config[bot_name]['arquivo_genoma']
            genome = nn.genome.load_genome(controller_file)
            aann_net = construct_aann(genome)
        return aann_net

    def _parse_controller(self,bot_name):
        keys_dic = {}
        for key,commands in self.config[bot_name+'_keymap'].items():
            raw_command_list = commands.split(',')
            command_list = []
            for command in raw_command_list:
                command = int(command)
                node = abs(command)
                value = copysign(1,command)
                command_list.append((node,value))
            key = getattr(arcade.key, key.upper())
            keys_dic.update({key: command_list})
        return keys_dic
    
    def get_graphics_settings(self):
        go = GraphicsSettings()
        if self.config['graphics']['nice_links'].lower() in ['nao', 'não']:
            go.nice_links = False
        return go


def print_bot_info(bot,id):
    print('Grafobot '+id+':\n' 
        + '  Nodos: '+str(bot.total_nodes_number())+'\n'
        + '  Comprimento total dos links: {:.2f}'.format(bot.total_links_length()))

if __name__ == "__main__":
    # Testando...
    config = RinhaConfigParser('config.txt')
    print(config.get_blueprints())
    print(config.get_keymaps())
