"""
Performance profiling stuff
"""
import cProfile
import pstats
import io
import sys
sys.path.append('../target/release/')

import arcade

from graphics_arcade import GameWindow
from net_builder import construct_aann
import neural_network as nn
import libaann as an
import librinha_rust2py as rgb


def profile(fnc):
    """
    A decorator that uses cProfile to profile a function.
    Used to measure the time a patch of code takes to run.
    """
    def inner(*args, **kwargs):

        pr = cProfile.Profile()
        pr.enable()
        retval = fnc(*args, **kwargs)
        pr.disable()
        s = io.StringIO()
        sortby = 'cumulative'
        ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
        ps.print_stats()
        print(s.getvalue())
        return retval
    return inner


# Definicoes da janela
SCREEN_TITLE = "Rinha de Grafobots"
SCREEN_WIDTH = 1280
SCREEN_HEIGHT = 768
#
GRAPHIC = True
UPDATE_RATE = 0.016

bot_a = rgb.BotDeliver("./grafobots_blueprints/bot_test_rotating.txt")
bot_b = rgb.BotDeliver("./grafobots_blueprints/bot_test_2_mini.txt")
rinha = rgb.RinhaDeGrafobots(bot_a, bot_b)

# Lendo rede neural
genome1 = nn.genome.load_genome('./runs/22/_champ_12.gen')
genome2 = nn.genome.load_genome('./runs/22/_champ_8.gen')
net1 = construct_aann(genome1)
net2 = construct_aann(genome2)

window = GameWindow(SCREEN_WIDTH,
                    SCREEN_HEIGHT,
                    SCREEN_TITLE,
                    rinha,
                    update_rate=UPDATE_RATE,
                    fullscreen=False)
window.setup()


@profile
def run():
    arcade.run()


if GRAPHIC:
    window = GameWindow(SCREEN_WIDTH,
                        SCREEN_HEIGHT,
                        SCREEN_TITLE,
                        rinha,
                        update_rate=UPDATE_RATE,
                        fullscreen=False)
    #window.neural_network_a = net1
    #window.neural_network_b = net2
    window.setup()
    arcade.run()


if __name__ == "__main__":
    pass
