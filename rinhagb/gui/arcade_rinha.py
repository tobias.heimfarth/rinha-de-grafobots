
from math import sin, cos
from dataclasses import dataclass
import time

import arcade
import pyglet.gl as gl


@dataclass
class GraphicsSettings:
    nice_links: bool = True


class ArcadeRinha:
    """
    Desenha a arena principal
    """

    def __init__(self,
                 rusty_rinha,
                 graphics_settings=None,
                 background="resources/fundo_sapo.png",
                 ):
        """
        Iniciando a janela.
        """
        self.background = background
        self.rusty_rinha = rusty_rinha
        self.neural_network_a = None
        self.neural_network_b = None
        self.key_mapping_bot_a = None
        self.key_mapping_bot_b = None
        self.nodes_sprites_list = []
        self.tick_number = 0
        self.points = [0, 0]
        self.time = 0
        self.update_display_info()
        self.winner = None
        self.width = 1120 + 64
        self.height = 640 + 64

        if graphics_settings is None:
            self.graphics_settings = GraphicsSettings()
        else:
            self.graphics_settings = graphics_settings

    def setup(self):
        """
        O arcade tem esta função de setup para facilitar um restart.
        Usando para carregar o cenário.
        """
        self.rusty_rinha.setup()
        if self.background:
            self.background = arcade.load_texture(self.background)
        # lendo os nodos do rust
        bot_a_nodes, bot_b_nodes = self.rusty_rinha.nodes()
        self.nodes_sprite_list = arcade.SpriteList()
        for node in bot_a_nodes + bot_b_nodes:
            node_sprite = NodeSprite(node.radius, (255, 255, 255))
            self.nodes_sprite_list.append(node_sprite)
        # self.draw_countdown()
        self.time = 0

    def keyboard_act(self, pressed_keys):
        self.rusty_rinha.clear_input_state_a()
        self.rusty_rinha.clear_input_state_b()
        # Verificando se a tecla comando o bot a
        if self.key_mapping_bot_a is not None:
            for key in pressed_keys:
                commands = self.key_mapping_bot_a.get(key)
                if commands is not None:
                    for command in commands:
                        self.rusty_rinha.command_a(*command)

        # Verificando se a tecla comando o bot b
        if self.key_mapping_bot_b is not None:
            for key in pressed_keys:
                commands = self.key_mapping_bot_b.get(key)
                if commands is not None:
                    for command in commands:
                        self.rusty_rinha.command_b(*command)

    def command_aann_a(self):
        # Atualiza a entrada do bot_a com o comando da rede neural responsável.
        # FIXME acho que esta limpeza do estado de entrada não é necessária. Checar.
        self.rusty_rinha.clear_input_state_a()
        self.rusty_rinha.command_a_aann(self.neural_network_a)

    def command_aann_b(self):
        # Atualiza a entrada do bot_b com o comando da rede neural responsável
        self.rusty_rinha.clear_input_state_b()
        self.rusty_rinha.command_b_aann(self.neural_network_b)

    def update(self, pressed_keys):
        """
        Atualização do jogo.
        """
        # Para melhorar a física (evitar tunelamentos, etc.) realiza-se
        # 5 atualizações do estado do jogo para cada quadro desenhado na tela.
        self.keyboard_act(pressed_keys)
        for _ in range(5):
            if self.neural_network_a is not None:
                self.command_aann_a()
            if self.neural_network_b is not None:
                self.command_aann_b()
            self.rusty_rinha.tick()
            if self.winner is None:
                self.winner = self.rusty_rinha.check_winner()
        print(self.rusty_rinha.get_bots_input_state())
        self.tick_number += 1

    def draw_game(self):
        """ 
        Desenha o jogo.
        """
        # Colocando o fundo.
        if self.background:
            arcade.draw_lrwh_rectangle_textured(0, 0,
                                                self.width, self.height,
                                                self.background)
        # Acessando o estado dos bots para renderizá-los.
        bot_a_nodes, bot_b_nodes = self.rusty_rinha.nodes()
        bot_a_links, bot_b_links = self.rusty_rinha.links()
        # for i, node in enumerate(bot_a_nodes+bot_b_nodes):
        #    self.nodes_sprite_list[i].update_pos(node)
        # Desenhando os links
        if self.graphics_settings.nice_links:
            self.draw_links(bot_a_links, bot_a_nodes)
            self.draw_links(bot_b_links, bot_b_nodes)
        else:
            self.draw_links_less_slow(bot_a_links, bot_a_nodes)
            self.draw_links_less_slow(bot_b_links, bot_b_nodes)
        # Desenhando os nodos
        self.draw_nodes(bot_a_nodes[1:], arcade.color.PALE_BLUE)
        self.draw_nodes(bot_a_nodes[0:1], arcade.color.GREEN_YELLOW)
        self.draw_nodes(bot_b_nodes[1:], arcade.color.PASTEL_ORANGE)
        self.draw_nodes(bot_b_nodes[0:1], arcade.color.RUSTY_RED)
        # Desenhando o retângulo do ringue.
        arcade.draw_rectangle_outline(
            self.width/2, self.height/2, self.width-64, self.height-64,
            arcade.color.GRAY, border_width=4)
        #
        # self.nodes_sprite_list.draw()
        self.draw_info()

    def draw_nodes(self, nodes, color):
        """
        Desenha os nodos
        FIXME Altamente não otimizada.
        """
        # Diminuindo o valor da cor dos nodos não energizados para melhor
        # visualização da parte funcional do bot.
        not_energized_color = tuple(int(i/2) for i in color)
        # Iterando sobre os nodos da lista.
        for node in nodes:
            pos = map_pos_to_screen(node.pos)
            # Desenhando o círculo principal do nodo
            if node.energized:
                arcade.draw_circle_filled(
                    *pos, node.radius, color, num_segments=25)
            else:
                arcade.draw_circle_filled(
                    *pos, node.radius, not_energized_color, num_segments=25)
            # Desenhando o marcador de proa (frente do nodo)
            marker_endpoing = (node.radius*cos(node.theta) +
                               pos[0], node.radius*sin(node.theta) + pos[1])
            arcade.draw_line(*pos, *marker_endpoing,
                             arcade.color.BLACK, line_width=3)
            # Caso o nodo seja do tipo que tem "visão", desenhando a sua
            # linha de visada.
            sense_sight_endpoint = node.sense_sight_endpoint
            if sense_sight_endpoint is not None and node.energized:
                pos_ep = map_pos_to_screen(sense_sight_endpoint)
                arcade.draw_line(
                    *pos, *pos_ep, (255, 255, 255, 20), line_width=3)
            # Desenhando o círculo secundário que indica atividade.
            # Atividade de saída.
            if node.output_value is not None:
                if node.energized:
                    alpha = int(node.output_value * 255)
                else:
                    alpha = 0
                arcade.draw_circle_filled(
                    *pos, 7, (alpha, 0, 0), num_segments=25)
            # Atividade de entrada.
            if node.input_value is not None:
                if node.energized:
                    alpha = int(input_threshold(node.input_value) * 255)
                else:
                    alpha = 0
                arcade.draw_circle_filled(
                    *pos, 7, (alpha/2, alpha/2, alpha), num_segments=20)

    def draw_links(self, links, nodes):
        """
        Desenha os links.
        """
        for link in links:
            color = (180, 180*link.health, 180*link.health)
            arcade.draw_line(*map_pos_to_screen(link.pos_a), *
                             map_pos_to_screen(link.pos_b), color,
                             line_width=2.5)

    def draw_links_less_slow(self, links, nodes):
        """
        Desenha os links de forma menos lenta, mas a largura da linha
        é limitada a 1 px.
        """
        pos = []
        colors = []
        for link in links:
            pos.append(map_pos_to_screen(link.pos_a))
            pos.append(map_pos_to_screen(link.pos_b))
            color = (180, 180*link.health, 180*link.health)
            colors += [color, color]
        # avoids crash if pos is empty
        if pos:
            lines = arcade.create_line_generic_with_colors(
                pos, colors,  gl.GL_LINES)
            lines.draw()

    def draw_info(self):
        if self.tick_number % 60 == 0 and self.tick_number != 0:
            self.update_display_info()
        # font_name = ('calibri','calibri')
        font_name = './resources/LiberationMono-Regular.ttf',
        arcade.draw_text(self.points[0], 50, self.height-25, arcade.color.WHITE,
                         anchor_x="left", font_size=15)  # ,font_name=font_name)
        arcade.draw_text(self.points[1], self.width-50, self.height-25,
                         arcade.color.WHITE, anchor_x="right", font_size=15)  # ,font_name=font_name)
        arcade.draw_text('Time: {}'.format(self.time), self.width/2, self.height-25,
                         arcade.color.YELLOW, anchor_x="center", font_size=15)  # , font_name=font_name)
        if self.winner is not None:
            arcade.draw_text('Winner: '+str(self.winner), self.width/2, self.height/2-75,
                             arcade.color.RED, font_size=80, anchor_x="center")
        # print('Pontos bot A: {} \n Pontos bot B {}'.format(self.points[0],self.points[1]))

    def update_display_info(self):
        points = self.rusty_rinha.compute_points()
        self.points[0] = 'Grafobot A  {:.2f}'.format(points[0])
        self.points[1] = 'Grafobot B  {:.2f}'.format(points[1])
        self.time += 1


def map_pos_to_screen(pos):
    """
    Converte as coordenadas do jogo para coordenadas da janela do jogo.
    """
    return (pos[0] + 32, pos[1] + 32)


def _map_pos_to_screen(pos):
    """
    Converte as coordenadas do jogo para coordenadas da janela do jogo.
    """
    return (pos[0] + 33, pos[1] + 33)


def input_threshold(x):
    if abs(x) > 0.5:
        return 1
    return 0


class NodeSprite(arcade.SpriteCircle):
    def __init__(self, radius, color):
        super().__init__(int(radius), color)

    def update_pos(self, node):
        self.center_x, self.center_y = map_pos_to_screen(node.pos)
