import sys
sys.path.append('./resources')
sys.path.append('../resources')
sys.path.append('../')

import arcade
from graphics_arcade import GameWindow, RinhaView, MenuView
from arcade_rinha import ArcadeRinha
from config_parser import RinhaConfigParser, print_bot_info

import rinhagb.resources.librinha_rust2py as rgb

# Definições da janela
SCREEN_TITLE = "Rinha de Grafobots"
SCREEN_WIDTH = 1120+64
SCREEN_HEIGHT = 640+64
#
UPDATE_RATE = 0.0166


# Lendo o arquivo de configurações
config = RinhaConfigParser('config.txt')
# Carregando os bots
bot_a, bot_b = config.get_blueprints()
# Imprimindo a informação dos bots
print_bot_info(bot_a,'a')
print_bot_info(bot_b,'b')
# Criando o jogo em rust
rusty_rinha = rgb.RinhaDeGrafobots(bot_a, bot_b,)
# Criando a ligação entre o jogo em rust e os gráficos em arcade
arcade_rinha = ArcadeRinha(rusty_rinha,config.get_graphics_settings())
# Configurando as configurações de rede e teclado
arcade_rinha.neural_network_a , arcade_rinha.neural_network_b = config.get_aann()
arcade_rinha.key_mapping_bot_a , arcade_rinha.key_mapping_bot_b = config.get_keymaps()

# Dançando a dança do Arcade...
window = GameWindow(SCREEN_WIDTH,
                    SCREEN_HEIGHT,
                    SCREEN_TITLE,
                    update_rate=UPDATE_RATE,
                    antialiasing = True,
                    fullscreen=False)
rinha_view = RinhaView(arcade_rinha)
menu = MenuView(rinha_view)
window.show_view(menu)

arcade.run()
