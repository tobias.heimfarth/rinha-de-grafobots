from os import listdir
from os.path import isfile, join
from copy import copy,deepcopy
from random import random,randint,seed,choice
from math import pi
from tqdm import tqdm

import numpy as np
import matplotlib.pyplot as plt

from neural_network.run_tools import *
from neural_network.species import *
from neural_network.genome import *
from neural_network import network

import sys
sys.path.append('../target/release/')
import librinha_rust2py as rgb
from net_builder import construct_aann

############### Start fresh
prefix = 'test_011_vs'

## Iniciando a população
innov = 10
pop = 200 #population number per generation per specie
species_distance = 0.25
mutation_probability = 0.75
pop_max = 500
rates = MutationRates(  connection          = 0.3,
                        node                = 0.01,
                        mix_node            = 0.1,
                        disable_connection  = 0.05,
                        disable_node        = 0.01,
                        weight              = 0.5,
                        all_weights         = 0.1,
                        zero_weight         = 0.05,
                        bias                = 0.1,
                        int_time            = 0.0)
stagnated_clock_max = 50
seed(152)

g = []
species = [Specie()]
for i in range(pop):
    #g.append(Individual(deepcopy(g1)))
    g.append(Individual(Genome()))
    g[-1].genome.generate_IO_innovations(2,2)
    g[-1].genome.fill()
    species[0].add_individual(g[-1])
    for j in range(10):
        innov = g[-1].genome.mutate(innov,rates)
   
# Gráficos
plot = EvolutionPlot()
plot.start_species()


# Rodar o jogo
SCREEN_WIDTH = 1280
SCREEN_HEIGHT = 768

bot_a = rgb.PyBot("./grafobots_blueprints/bot_test.txt")
bot_b = rgb.PyBot("./grafobots_blueprints/bot_test.txt")

def play_vs(genome1,genome2):
    # Criando o jogo
    rinha = rgb.RinhaDeGrafobots(bot_a, bot_b)
    rinha.setup()
    # Criando os controladores
    net_a = construct_aann(genome1)
    net_b = construct_aann(genome2)
    winner = rinha.run_headless_match_aann(10000,net_a,net_b)
    #points_a, points_b = rinha.compute_points()
    points_a, points_b = 1,1
    # corrigindo os pontos
    #points_a += 10 - points_b
    #points_b += 10 - points_a
    # 50 pontos extras para vitoria
    if winner == 1:
        points_a += 10
    elif winner == 2:
        points_b += 10
    return points_a,points_b


def random_matches(ind_list,N):
    '''
    Cria um lista de duelos com N duelos por membro da lista.
    Para evitar sobras, N deve ser par.
    '''
    matches = []
    rest = []
    for _ in range(N):
        to_play_ind_list = ind_list[:]
        for _ in range(int(len(to_play_ind_list)/2)):
            ind1 = choice(to_play_ind_list)
            to_play_ind_list.remove(ind1)
            ind2 = choice(to_play_ind_list)
            to_play_ind_list.remove(ind2)
            matches.append((ind1,ind2))
        rest += to_play_ind_list
    # 
    for _ in range(int(len(rest)/2)):
        matches.append((rest.pop(0),rest.pop(0)))
    return matches



for j in range(100000):
    print('Geracao ',j)
    #Criando uma lista com todos indivíduos.
    all_individual_list = []
    for s in species:
        for ind in s.individuals:
            all_individual_list.append(ind)
    #
    matches = random_matches(all_individual_list,20)
    points = []
    for match in tqdm(matches):
        ind1 = match[0]
        ind2 = match[1]
        ps = play_vs(ind1.genome,ind2.genome)
        ind1.points += ps[0]
        ind2.points += ps[1]
    points = [ind.points for ind in all_individual_list]
    print('Points: ', points)
    plot.update_species(j,np.flip(np.sort(points),0),len(species))
 
    f= np.flip(np.sort(points),0)[0]
 
    for s in species:
        s.fitness()
        s.sort_by_points()
        s.remove_unfit(fraction=0.5) 

    #saving file
    sbest = [s.individuals[0].points for s in species]
    classification = argsort_random(sbest)
    print(species[classification[0]].individuals[0].points,f)
    #if f != species[classification[0]].individuals[0].points: input()
    first = deepcopy(species[classification[0]].individuals[0].genome)
    first.fill()
    if j%50 == 0:
        first.save('grafobots_blueprints/'+prefix+'_gen'+str(j)+'.gen')
        plot.save('grafobots_blueprints/'+prefix+'.npz')
            #first.plot('genomes/'+prefix+'_gen'+str(j)+'.png')
       

        
 
    #Interspecies wild cards
    #while random() < 0.05:
        #s1 = randint(0,len(species)-1)
        #s2 = randint(0,len(species)-1)
        #ind1 = species[s1].random_individual()
        #ind2 = species[s2].random_individual()
        #child = mate_genomes(ind1.genome,ind2.genome)
        #child.fill()
        ##s = Specie()
        #species[s1].add_individual(Individual(child))
        ##species.append(s)
        
     ## species total fitness computations
    new_species = []
    for s in species:
        s.compute_specie_genetic_distance()
        s.find_representative()
        new_species += s.speciate(species_distance)
    species = new_species 
    total_fitness = 0
    for s in species:
            total_fitness += s.fitness()
    if total_fitness == 0: input()
    ## Next generation
    
    for s in species:
        N = int(round(s.fitness()/total_fitness * pop))
        #print(s.fitness(),'fitness',N,s.individuals)
        innov =  s.next_generation(N,mutation_probability,rates,innov)

    #Removing dead species
    death_list = []
    for sn,s in enumerate(species):
        #s.clear()
        if not s.is_alive(stagnated_clock_max): 
            if len(s.individuals) > 10:
                innov = s.irradiate(15,rates,innov)
                s.stagnated_clock = 0
            else:
                death_list.append(sn)
    death_list.reverse()
    for sn in death_list:
        species.pop(sn)
    if species == []: input()

    #Interspecies wild cards
    #while random() < 0.5:
        #s1 = randint(0,len(species)-1)
        #s2 = randint(0,len(species)-1)
        #ind1 = species[s1].random_individual()
        #ind2 = species[s2].random_individual()
        #child = mate_genomes(ind1.genome,ind2.genome)
        #child.fill()
        ##s = Specie()
        #species[s1].add_individual(Individual(child))
        ##species.append(s)
        

    print('innov:',innov)




